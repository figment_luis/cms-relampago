<?php namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

class Auth implements FilterInterface {

    // Antes de atender la peticiópn, verificar si el usuario esta logeado
    public function before(RequestInterface $request, $arguments = null)
    {
        $encrypter = \Config\Services::encrypter();
        $userModel = model('UserModel');
        $roleModel = model('RoleModel');
        helper('cookie');

        if (!session()->get('logged_in')) {
            if (get_cookie('email')) {
                $email = $encrypter->decrypt(get_cookie('email'));
                $user  = $userModel->where('email', $email)->first();
                if ($user) {
                    $role = $roleModel->find($user['role_id']);
                    $sessionData = [
                        'user_id'       => $user['id'],
                        'user_fullname' => $user['first_name'] . ' ' . $user['last_name'],
                        'user_email'    => $user['email'],
                        'role'          => $role->name,
                        'logged_in'     => TRUE
                    ];
                    session()->set($sessionData);
                } else {
                    return redirect()->to('/');
                }
            } else {
                return redirect()->to('/');
            }
        }
    }
 
    //--------------------------------------------------------------------
    
    // Acciones a realizar después de la petición
    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        
    }

}