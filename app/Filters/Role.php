<?php namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

class Role implements FilterInterface {

    // Antes de atender la peticiópn, verificar si el usuario esta logeado
    public function before(RequestInterface $request, $arguments = null)
    {
        $roleName = session()->get('role');
        if (!in_array($roleName, $arguments))
            return redirect()->to('/');
    }
 
    //--------------------------------------------------------------------
    
    // Acciones a realizar después de la petición
    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        
    }

}