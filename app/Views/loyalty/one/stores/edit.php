<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Tiendas Luxury ONE | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Plan de Lealtad LuxuryONE<br><small style="padding-left: 0;">Esta sección le permite editar una tienda sobre el sitio web de LuxuryONE</small></h1>
<ol class="breadcrumb" >
    <li><a href="<?= base_url('/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?= base_url('/one/dashboard') ?>">Luxury ONE</a></li>
    <li><a href="<?= base_url('/one/stores') ?>">Tiendas</a></li>
    <li class="active">Editar Tienda</li>
</ol>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="box">
    <form action="<?= route_to('one.stores.update', $store->id) ?>" method="post" novalidate id="formStoresEdit" enctype="multipart/form-data">
        <div class="box-header">
            <h3 class="box-title">Editar Tienda</h3>
        </div>
        <div class="box-body">
            <div class="row">
            <div class="col-md-6">
                    <div class="form-group">
                        <label for="form-title">Titulo <span class="text-red">*</span>
                        <span class="help">Tip: Zara Home</span></label>
                        <input type="text" name="title" id="form-title" class="form-control" value="<?= esc($store->name) ?>" placeholder="Nombre de tienda" autocomplete="off" autofocus required>
                    </div>
                    <div class="form-group">
                        <label for="form-multiple">Multiplicador <span class="text-red">*</span>
                        <span class="help">Tip: Seleccione 2 si la tienda ofrece puntos al doble</span></label>
                        <select name="multiple" id="form-multiple" class="form-control" required>
                            <option value="" disabled>Seleccione una opción</option>
                            <?php foreach([1,2] as $index => $value): ?>
                                <option value="<?= $value ?>" <?= $value == $store->multipler ? 'selected' : '' ?> ><?= $value ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="form-group">Grupo Comercial <span class="text-red">*</span>
                        <span class="help">Tip: El grupo comercial asociado a esta tienda</span></label>
                        <select name="group" id="form-group" class="form-control" required>
                            <option value="" disabled>Seleccione un grupo comercial</option>
                            <?php foreach($groups as $index => $group): ?>
                                <option value="<?= $group->id ?>" <?= $group->id == $store->group_id ? 'selected' : '' ?> ><?= esc($group->name) ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-default pull-right"><span class="fa fa-database icon" aria-hidden="true"></span> Actualizar</button>
        </div>
    </form>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/css/fileinput.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/libs/fileinput/js/fileinput.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/js/locales/es.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

<script>
    let baseUrl = '<?= base_url() ?>'

    $(document).ready(function() {
        $("#formStoresEdit").submit(function(e) {
            e.preventDefault();
            e.stopPropagation();
            let formData = new FormData($(this)[0]);
            Swal.fire({
                title: '¿Desea actualizar esta tienda?',
                text: "Usted esta a un paso de realizar una actualización en el sistema",
                icon: 'warning',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showCancelButton: true,
                confirmButtonText: 'Si, deseo continuar',
                cancelButtonText: 'No, cancelar',
            }).then((result) => {
                if (result.isConfirmed) {
                    $(".message-error").hide();
                    $(".input-error").removeClass('input-error')

                    Swal.fire({
                        title: "Procesando solicitud",
                        text: "Espere un momento por favor.",
                        imageUrl: baseUrl + "/assets/images/loader.gif",
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                    });
                    setTimeout(function() {
                        $.ajax({
                            url: baseUrl + '/one/stores/update/' + '<?= $store->id ?>',
                            type: 'POST',
                            data: formData,
                            contentType: false,
                            processData: false,
                            headers: {'X-Requested-With': 'XMLHttpRequest'},
                            dataType: 'json',
                        }).done(function(data, textStatus, jqXHR) {
                            if (data.code == 200) {
                                // Proceso exitoso
                                Swal.fire("Proceso terminado", data.message, "success");
                            } else if (data.code == 400) {
                                // Error
                                Swal.fire("Lo sentimos", data.message, "error");
                            } else {
                                // Problemas de Validación
                                Swal.fire("Error de validación", data.message, "error");
                                $.each(data.validation, function(key, value) {
                                    $(`*[name='${key}']`).addClass('input-error').parent().append(`<span class="message-error"><i class="fa fa-exclamation-circle"></i> ${value}</span>`)
                                })
                            }
                        }).fail(function(jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR.responseText)
                            Swal.fire("Error", "Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.", "error");
                        });
                    }, 1000);
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire('Proceso cancelado', 'No se realizaron modificaciones en el sistema', 'info');
                }
            })
        })
    })
</script>
<?= $this->endSection() ?>
