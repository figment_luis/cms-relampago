<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
PearlAndamar | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Plan de Lealtad PearlAndamar<br><small style="padding-left: 0;">Panel de Control Administrativo</small></h1>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <div class="dashboard dashboard--pearlandamar">
        <h3 class="dashboard__title">PearlAndamar<span class="dashboard__slogan">El plan de lealtad de Andamar</span></h3>
        <div class="dashboard__body">
            <ul class="dashboard__menu">
                <li class="dashboard__item">
                    <a href="<?= route_to('pearlandamar.stores.index') ?>" class="dashboard__link">
                        <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                        Tiendas
                    </a>
                    <p class="dashboard__description">Gestione las tiendas participantes en el plan de lealtad.</p>
                </li>
                <li class="dashboard__item">
                    <a href="<?= route_to('pearlandamar.awards.index') ?>" class="dashboard__link">
                        <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                        Certificados
                    </a>
                    <p class="dashboard__description">Gestione los certificados participantes en el plan de lealtad.</p>
                </li>
                <li class="dashboard__item">
                    <a href="<?= route_to('pearlandamar.benefits.index') ?>" class="dashboard__link">
                        <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                        Beneficios
                    </a>
                    <p class="dashboard__description">Gestione los beneficios participantes en el plan de lealtad.</p>
                </li>
                <li class="dashboard__item dashboard__item--square">
                    <a href="<?= route_to('pearlandamar.popups.index') ?>" class="dashboard__link dashboard__link--square">
                        <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                        Popups
                    </a>
                    <p class="dashboard__description">Registre y gestione los popups publicados en sitio Web.</p>
                </li>
            </ul>
        </div>
    </div>
<?= $this->endSection() ?>
