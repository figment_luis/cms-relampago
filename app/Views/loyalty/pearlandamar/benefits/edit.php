<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Beneficios PearlAndamar | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Plan de Lealtad PearlAndamar<br><small style="padding-left: 0;">Esta sección le permite editar un beneficio sobre el sitio web de PearlAndamar</small></h1>
<ol class="breadcrumb" >
    <li><a href="<?= base_url('/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?= base_url('/pearlandamar/dashboard') ?>">Pearl Andamar</a></li>
    <li><a href="<?= base_url('/pearlandamar/benefits') ?>">Beneficios</a></li>
    <li class="active">Editar Beneficio</li>
</ol>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="box">
    <form action="<?= route_to('pearlandamar.benefits.update', $benefit->id) ?>" method="post" novalidate id="formBenefitsEdit" enctype="multipart/form-data">
        <div class="box-header">
            <h3 class="box-title">Editar Beneficio</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="form-title">Titulo <span class="text-red">*</span>
                        <span class="help">Tip: Cortesía de estacionamiento para socios</span></label>
                        <input type="text" name="title" id="form-title" class="form-control" value="<?= esc($benefit->name) ?>" placeholder="Titulo principal del beneficio" autocomplete="off" autofocus required>
                    </div>
                    <div class="form-group">
                        <label for="form-stock">Stock <span class="text-red">*</span>
                        <span class="help">Tip: El stock real en Concierge que tiene este beneficio</span></label>
                        <input type="number" name="stock" id="form-stock" min="0" class="form-control" value="<?= $benefit->stock ?>" placeholder="Unidades disponibles para obsequiar" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label>Tarjetas beneficiadas <span class="text-red">*</span>
                        <span class="help">Tip: Los usuarios que pueden ser acreedores de este beneficio</span></label>
                        <br>
                        <div class="checkbox-group required">
                            <?php foreach(['Silver','Gold','Black'] as $index => $value): ?>
                                <label style="margin-right: 1.5rem;">
                                    <input type="checkbox" name="cards[]" value="<?= $index + 1 ?>" <?= array_search($index + 1, array_column($currentCards, 'card_type_id')) !== false ? 'checked' : '' ?> class="flat-red">&nbsp; <?= esc($value) ?>
                                </label>  
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="form-description">Descripción <span class="text-red">*</span>
                        <span class="help">Tip: Contenido detallado acerca de este beneficio</span></label>
                        <textarea name="description" id="form-description" class="form-control" rows="6" placeholder="Descripción detallada del beneficio" autocomplete="off" required><?= $benefit->description ?></textarea>
                    </div>
                </div>
                <!-- <div class="col-md-6">
                    <div class="form-group">
                        <label for="form-image">Portada <span class="text-red">*</span>
                        <span class="help">Tip: La imagen que representa este beneficio <strong>Formato JPG de 680x340</strong></span></label>
                        <input id="form-image" name="image" type="file" class="file" data-preview-file-type="text" >
                    </div>
                </div> -->
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-default pull-right"><span class="fa fa-database icon" aria-hidden="true"></span> Actualizar</button>
        </div>
    </form>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/css/fileinput.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/libs/adminLTE/plugins/iCheck/all.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/libs/adminLTE/plugins/iCheck/icheck.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/js/fileinput.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/js/locales/es.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

<script>
    let baseUrl = '<?= base_url() ?>'

    /*$("#form-image").fileinput({
        'showUpload': false,
        'showRemove': false,
        'initialPreviewShowDelete': false,
        maxImageWidth: 680,
        maxImageHeight: 340,
        minImageWidth: 680,
        minImageHeight: 340,
        maxFileSize: 500,
        'maxFileCount': 1,
        'language': 'es',
        'previewFileType': "image",
        'theme': "explorer",
        'allowedFileExtensions': ["jpg"],
        msgFileRequired: 'El archivo de imagen es requerido',
        msgSizeTooLarge: 'La imagen supera el tamaño permitido de <b>{maxSize} kilobytes.</b><br>Recomendamos optimizarla para Web',
        msgInvalidFileExtension: 'Extensión de archivo no válida, solo se admiten <b>{extensions}</b>',
        msgImageWidthSmall: 'El ancho de imagen debe ser de al menos <b>{size} px.</b>',
        msgImageHeightSmall: 'La altura de imagen debe ser de al menos <b>{size} px.</b>',
        msgImageWidthLarge: 'El ancho de imagen no debe exceder los <b>{size} px.</b>',
        msgImageHeightLarge: 'El alto de imagen no debe exceder los <b>{size} px.</b>',
        <?php if ($benefit->img): ?>
            'initialPreview': [
                '<img src="<?= esc($benefit->img) ?>" class="file-preview-image" width="70">'
            ],
            'initialPreviewConfig': [
                {
                    'caption': "<?= esc($benefit->name) ?>"
                }
            ],
        <?php endif; ?>
    });*/

    $(document).ready(function() {
        $('input[type="checkbox"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass   : 'iradio_flat-green'
        });

        $("#formBenefitsEdit").submit(function(e) {
            e.preventDefault();
            e.stopPropagation();

            // Detener el proceso si existe uno o más errores con los archivos de imagen
            // if ($(".file-input.theme-explorer").hasClass("has-error")) return false;
            // Detener el proceso si no se selecciona al menos una tarjeta beneficiada
            if($('div.checkbox-group.required :checkbox:checked').length == 0) {
                $(".checkbox-group.required .message-error").hide();
                $(".checkbox-group.required").append(`<span class="message-error"><i class="fa fa-exclamation-circle"></i>Seleccione al menos una Tarjeta Beneficiada</span>`)
                return false;
            }

            let formData = new FormData($(this)[0]);
            Swal.fire({
                title: '¿Desea actualizar este beneficio?',
                text: "Usted esta a un paso de realizar una actualización en el sistema",
                icon: 'warning',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showCancelButton: true,
                confirmButtonText: 'Si, deseo continuar',
                cancelButtonText: 'No, cancelar',
            }).then((result) => {
                if (result.isConfirmed) {
                    $(".message-error").hide();
                    $(".input-error").removeClass('input-error')

                    Swal.fire({
                        title: "Procesando solicitud",
                        text: "Espere un momento por favor.",
                        imageUrl: baseUrl + "/assets/images/loader.gif",
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                    });
                    setTimeout(function() {
                        $.ajax({
                            url: baseUrl + '/pearlandamar/benefits/update/' + '<?= $benefit->id ?>',
                            type: 'POST',
                            data: formData,
                            contentType: false,
                            processData: false,
                            headers: {'X-Requested-With': 'XMLHttpRequest'},
                            dataType: 'json',
                        }).done(function(data, textStatus, jqXHR) {
                            if (data.code == 200) {
                                // Proceso exitoso
                                Swal.fire("Proceso terminado", data.message, "success");
                            } else if (data.code == 400) {
                                // Error
                                Swal.fire("Lo sentimos", data.message, "error");
                            } else {
                                // Problemas de Validación
                                Swal.fire("Error de validación", data.message, "error");
                                $.each(data.validation, function(key, value) {
                                    $(`*[name='${key}']`).addClass('input-error').parent().append(`<span class="message-error"><i class="fa fa-exclamation-circle"></i> ${value}</span>`)
                                })
                            }
                        }).fail(function(jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR.responseText)
                            Swal.fire("Error", "Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.", "error");
                        });
                    }, 1000);
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire('Proceso cancelado', 'No se realizaron modificaciones en el sistema', 'info');
                }
            })
        })
    })
</script>
<?= $this->endSection() ?>
