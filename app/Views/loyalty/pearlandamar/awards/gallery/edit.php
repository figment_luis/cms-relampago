<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Certificados PearlAndamar | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Plan de Lealtad PearlAndamar<br><small style="padding-left: 0;">Esta sección le permite editar la galería de un certificado sobre el sitio web de PearlAndamar</small></h1>
<ol class="breadcrumb" >
    <li><a href="<?= base_url('/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?= base_url('/pearlandamar/dashboard') ?>">Pearl Andamar</a></li>
    <li><a href="<?= base_url('/pearlandamar/awards') ?>">Certificados</a></li>
    <li><a href="<?= base_url('/pearlandamar/awards/edit/'. $award->id) ?>"><?= $award->name ?></a></li>
    <li class="active">Editar Galería</li>
</ol>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="box">
    <div class="box-container">
        <div class="box-header">
            <h3 class="box-title">Editar Galería</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="form-title">Titulo <span class="text-red">*</span>
                        <span class="help">Tip: Certificado Kipling de $500 pesos</span></label>
                        <input type="text" name="title" id="form-title" class="form-control" value="<?= esc($award->name) ?>" placeholder="Titulo principal del certificado" autocomplete="off" autofocus required readonly disabled>
                    </div>
                    <div class="form-group">
                        <label for="form-gallery">Galería <span class="text-red">*</span>
                        <span class="help">Tip: Conjunto de imágenes alusivas a este certificado <strong>Formato JPG de 1140x390</strong></span></label>
                        <input id="form-gallery" name="images[]" type="file" class="file" multiple data-preview-file-type="text" >
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/css/fileinput.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/libs/fileinput/js/fileinput.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/js/locales/es.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

<script>
    let baseUrl = '<?= base_url() ?>'
    let storeId = '<?= $award->id ?>'

    let imagesPHP = '<?= esc($award->img) ?>'
    imagesPHP = imagesPHP == "" ? [] : imagesPHP.split('|')

    let imagesPreview = imagesPHP;
    let imagesPreviewConfig = []

    for (let image of imagesPHP) {
        let fileName = image.split('/').reverse();
        imagesPreviewConfig.push({
            "caption": "Imagen publicada",
            "width": "70px",
            "key" : image,
            "url": baseUrl + "/pearlandamar/awards/gallery/destroy/" + storeId + '/' + fileName[0],
        });
    }

    let $elFileInput = $("#form-gallery");

    $elFileInput.fileinput({
        'showUpload': false,
        'showRemove': false,
        'required': true,
        'uploadAsync': false,
        'uploadUrl': baseUrl + "/pearlandamar/awards/gallery/update/" + storeId,
        //'maxImageWidth': 200,
        //'maxImageHeight': 150,
        'maxFileCount': 3,
        'language': 'es',
        'previewFileType': "image",
        'theme': "explorer",
        'allowedFileExtensions': ["jpg"],
        'msgFileRequired': 'Seleccione uno o más archivos de tipo imagen para subir',
        'validateInitialCount': true,
        'overwriteInitial': false,
        'initialPreviewAsData': true, // identify if you are sending preview data only and not the raw markup
        'initialPreviewFileType': 'image',
        'initialPreview': imagesPreview,
        'initialPreviewConfig': imagesPreviewConfig,
    }).on("filebatchselected", function(event, files) {
        $elFileInput.fileinput("upload");
    }).on('fileuploaded', function(event, previewId, index, fileId) {
        //console.log('File uploaded', previewId, index, fileId);
        //$elFileInput.fileinput('refresh');
    });

    $(document).ready(function() {
        $("#formGalleryEdit").submit(function(e) {
            e.preventDefault();
            e.stopPropagation();
            let formData = new FormData($(this)[0]);
            Swal.fire({
                title: '¿Desea actualizar este certificado?',
                text: "Usted esta a un paso de realizar una actualización en el sistema",
                icon: 'warning',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showCancelButton: true,
                confirmButtonText: 'Si, deseo continuar',
                cancelButtonText: 'No, cancelar',
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire({
                        title: "Procesando solicitud",
                        text: "Espere un momento por favor.",
                        imageUrl: baseUrl + "/assets/images/loader.gif",
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                    });
                    setTimeout(function() {
                        $.ajax({
                            url: baseUrl + '/pearlandamar/awards/update/' + '<?= $award->id ?>',
                            type: 'POST',
                            data: formData,
                            contentType: false,
                            processData: false,
                            headers: {'X-Requested-With': 'XMLHttpRequest'},
                            dataType: 'json',
                        }).done(function(data, textStatus, jqXHR) {
                            if (data.code == 200) {
                                Swal.fire("Proceso terminado", data.message, "success");
                            } else {
                                Swal.fire("Lo sentimos", data.message, "error");
                            }
                        }).fail(function(jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR.responseText)
                            Swal.fire("Error", "Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.", "error");
                        });
                    }, 1000);
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire('Proceso cancelado', 'No se realizaron modificaciones en el sistema', 'info');
                }
            })
        })
    })
</script>
<?= $this->endSection() ?>
