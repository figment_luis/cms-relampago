<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Certificados Rewards | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Plan de Lealtad Rewards<br><small style="padding-left: 0;">Esta sección le permite editar la galería de un certificado sobre el sitio web de Rewards</small></h1>
<ol class="breadcrumb" >
    <li><a href="<?= base_url('/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?= base_url('/rewards/dashboard') ?>">Angelópolis Rewards</a></li>
    <li><a href="<?= base_url('/rewards/awards') ?>">Certificados</a></li>
    <li><a href="<?= base_url('/rewards/awards/edit/'. $award->id) ?>"><?= $award->name ?></a></li>
    <li class="active">Editar Galería</li>
</ol>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="box">
    <div class="box-container">
        <div class="box-header">
            <h3 class="box-title">Editar Galería</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="form-title">Titulo <span class="text-red">*</span>
                        <span class="help">Tip: Gift Card por $500 pesos para consumo</span></label>
                        <input type="text" name="title" id="form-title" class="form-control" value="<?= esc($award->name) ?>" placeholder="Titulo principal del certificado" autocomplete="off" autofocus required readonly disabled>
                    </div>
                    <div class="form-group">
                        <label for="form-gallery">Galería <span class="text-red">*</span>
                        <span class="help">Tip: Conjunto de imágenes alusivas a este certificado <strong>Formato JPG de 1140x390</strong></span></label>
                        <input id="form-gallery" name="images[]" type="file" class="file" multiple data-preview-file-type="text" >
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/css/fileinput.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/libs/fileinput/js/fileinput.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/js/locales/es.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

<script>
    let baseUrl = '<?= base_url() ?>'
    let storeId = '<?= $award->id ?>'

    let imagesPHP = '<?= esc($award->img) ?>'
    imagesPHP = imagesPHP == "" ? [] : imagesPHP.split('|')

    let imagesPreview = imagesPHP;
    let imagesPreviewConfig = []

    for (let image of imagesPHP) {
        // 1. image es una url que apunta a un recurso de imagen, 
        // 2. generaramos un array tomando como base el caracter / que aparece en dicha url
        // 3. invertimos el array, por tanto el primer elemento es el nombre del recurso de imagen
        let fileName = image.split('/').reverse();
        imagesPreviewConfig.push({
            "caption": "Imagen publicada",
            "width": "70px",
            "key" : image,
            "url": baseUrl + "/rewards/awards/gallery/destroy/" + storeId + '/' + fileName[0],
        });
    }

    let $elFileInput = $("#form-gallery");

    $elFileInput.fileinput({
        'showUpload': false,
        'showRemove': false,
        'required': true,
        'uploadAsync': false,
        'uploadUrl': baseUrl + "/rewards/awards/gallery/update/" + storeId,
        //'maxImageWidth': 200,
        //'maxImageHeight': 150,
        'maxFileCount': 3,
        'language': 'es',
        'previewFileType': "image",
        'theme': "explorer",
        'allowedFileExtensions': ["jpg"],
        'msgFileRequired': 'Seleccione uno o más archivos de tipo imagen para subir',
        'validateInitialCount': true,
        'overwriteInitial': false,
        'initialPreviewAsData': true, // identify if you are sending preview data only and not the raw markup
        'initialPreviewFileType': 'image',
        'initialPreview': imagesPreview,
        'initialPreviewConfig': imagesPreviewConfig,
    }).on("filebatchselected", function(event, files) {
        $elFileInput.fileinput("upload");
    }).on('fileuploaded', function(event, previewId, index, fileId) {
        //console.log('File uploaded', previewId, index, fileId);
        //$elFileInput.fileinput('refresh');
    });
</script>
<?= $this->endSection() ?>
