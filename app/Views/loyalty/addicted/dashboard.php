<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Addicted | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Plan de Lealtad Addicted<br><small style="padding-left: 0;">Panel de Control Administrativo</small></h1>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <div class="dashboard">
        <h3 class="dashboard__title">Addicted<span class="dashboard__slogan">El plan de lealtad de Antara</span></h3>
        <div class="dashboard__body">
            <ul class="dashboard__menu">
                <li class="dashboard__item">
                    <a href="<?= route_to('addicted.stores.index') ?>" class="dashboard__link">
                        <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                        Tiendas
                    </a>
                    <p class="dashboard__description">Gestione las tiendas participantes en el plan de lealtad.</p>
                </li>
                <li class="dashboard__item">
                    <a href="<?= route_to('addicted.awards.index') ?>" class="dashboard__link">
                        <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                        Certificados
                    </a>
                    <p class="dashboard__description">Gestione los certificados participantes en el plan de lealtad.</p>
                </li>
                <li class="dashboard__item">
                    <a href="<?= route_to('addicted.benefits.index') ?>" class="dashboard__link">
                        <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                        Beneficios
                    </a>
                    <p class="dashboard__description">Gestione los beneficios participantes en el plan de lealtad.</p>
                </li>
                <li class="dashboard__item">
                    <a href="<?= route_to('addicted.discounts.index') ?>" class="dashboard__link">
                        <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                        Descuentos
                    </a>
                    <p class="dashboard__description">Gestione los descuentos participantes en el plan de lealtad.</p>
                </li>
                <li class="dashboard__item">
                    <a href="<?= route_to('addicted.experiences.index') ?>" class="dashboard__link">
                        <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                        Experiencias
                    </a>
                    <p class="dashboard__description">Gestione las experiencias participantes en el plan de lealtad.</p>
                </li>
                <li class="dashboard__item dashboard__item--square">
                    <a href="<?= route_to('addicted.popups.index') ?>" class="dashboard__link dashboard__link--square">
                        <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                        Popups
                    </a>
                    <p class="dashboard__description">Registre y gestione los popups publicados en sitio Web.</p>
                </li>
            </ul>
        </div>
    </div>
<?= $this->endSection() ?>
