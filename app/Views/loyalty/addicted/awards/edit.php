<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Certificados Addicted | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Plan de Lealtad Addicted<br><small style="padding-left: 0;">Esta sección le permite editar un certificado sobre el sitio web de Addicted</small></h1>
<ol class="breadcrumb" >
    <li><a href="<?= base_url('/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?= base_url('/addicted/dashboard') ?>">Addicted</a></li>
    <li><a href="<?= base_url('/addicted/awards') ?>">Certificados</a></li>
    <li class="active">Editar Certificado</li>
</ol>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="box">
    <form action="<?= route_to('addicted.awards.update', $award->id) ?>" method="post" novalidate id="formAwardsEdit" enctype="multipart/form-data">
        <div class="box-header">
            <h3 class="box-title">Editar Certificado</h3>
        </div>
        <div class="box-body">
            <div class="row">
            <div class="col-md-6">
                    <div class="form-group">
                        <label for="form-title">Titulo
                        <span class="help">Tip: Certificado Kipling de $500 pesos</span></label>
                        <input type="text" name="title" id="form-title" class="form-control" value="<?= esc($award->title) ?>" placeholder="Titulo principal del certificado" autocomplete="off" required readonly>
                    </div>
                    <div class="form-group">
                        <label for="form-points">Puntos
                        <span class="help">Tip: Los puntos necesarios para canjear este certificado</span></label>
                        <input type="number" name="points" id="form-points" min="0" class="form-control" value="<?= $award->puntos ?>" placeholder="Valor en puntos Addicted" autocomplete="off" required readonly>
                    </div>
                    <div class="form-group">
                        <label for="form-price">Precio <span class="text-red">*</span>
                        <span class="help">Tip: El precio real en tienda que tiene este artículo o servicio</span></label>
                        <input type="number" name="price" id="form-price" min="0" class="form-control" value="<?= $award->precio ?>" placeholder="Precio real en tienda" autocomplete="off" autofocus required>
                    </div>
                    <div class="form-group">
                        <label for="form-link">Link
                            <span class="help">Tip: URL que apunta a más información sobre este certificado</span>
                        </label>
                        <input type="url" id="form-link" name="link" class="form-control" value="<?= esc($award->link_url) === 'no-link' ? '' : esc($award->link_url) ?>" placeholder="URL asociada al certificado" autocomplete="off">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="form-image">Portada <span class="text-red">*</span>
                        <span class="help">Tip: La imagen que representa este certificado <strong>Formato JPG de 300x300</strong></span></label>
                        <input id="form-image" name="image" type="file" class="file" data-preview-file-type="text" >
                    </div>
                    <div class="form-group">
                        <label for="form-description">Descripción <span class="text-red">*</span>
                        <span class="help">Tip: Contenido detallado acerca de este certificado</span></label>
                        <textarea name="description" id="form-description" class="form-control" rows="6" placeholder="Descripción detallada del certificado" autocomplete="off" required><?= $award->description ?></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-default pull-right"><span class="fa fa-database icon" aria-hidden="true"></span> Actualizar</button>
        </div>
    </form>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/css/fileinput.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/libs/fileinput/js/fileinput.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/js/locales/es.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

<script>
    let baseUrl = '<?= base_url() ?>'

    $("#form-image").fileinput({
        'showUpload': false,
        'showRemove': false,
        'initialPreviewShowDelete': false,
        maxImageWidth: 300,
        maxImageHeight: 300,
        minImageWidth: 300,
        minImageHeight: 300,
        maxFileSize: 500,
        'maxFileCount': 1,
        'language': 'es',
        'previewFileType': "image",
        'theme': "explorer",
        'allowedFileExtensions': ["jpg"],
        msgFileRequired: 'El archivo de imagen es requerido',
        msgSizeTooLarge: 'La imagen supera el tamaño permitido de <b>{maxSize} kilobytes.</b><br>Recomendamos optimizarla para Web',
        msgInvalidFileExtension: 'Extensión de archivo no válida, solo se admiten <b>{extensions}</b>',
        msgImageWidthSmall: 'El ancho de imagen debe ser de al menos <b>{size} px.</b>',
        msgImageHeightSmall: 'La altura de imagen debe ser de al menos <b>{size} px.</b>',
        msgImageWidthLarge: 'El ancho de imagen no debe exceder los <b>{size} px.</b>',
        msgImageHeightLarge: 'El alto de imagen no debe exceder los <b>{size} px.</b>',
        'initialPreview': [
            '<img src="http://www.addicted.com.mx/assets/img/<?= esc($award->image_url) ?>" class="file-preview-image" width="70">'
        ],
        'initialPreviewConfig': [
            {
                'caption': '<?= esc($award->title) ?>'
            }
        ],
    });

    $(document).ready(function() {
        $("#formAwardsEdit").submit(function(e) {
            e.preventDefault();
            e.stopPropagation();
            
            // Detener el proceso si existe uno o más errores con los archivos de imagen
            if ($(".file-input.theme-explorer").hasClass("has-error")) return false;

            let formData = new FormData($(this)[0]);
            Swal.fire({
                title: '¿Desea actualizar este certificado?',
                text: "Usted esta a un paso de realizar una actualización en el sistema",
                icon: 'warning',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showCancelButton: true,
                confirmButtonText: 'Si, deseo continuar',
                cancelButtonText: 'No, cancelar',
            }).then((result) => {
                if (result.isConfirmed) {
                    $(".message-error").hide();
                    $(".input-error").removeClass('input-error')

                    Swal.fire({
                        title: "Procesando solicitud",
                        text: "Espere un momento por favor.",
                        imageUrl: baseUrl + "/assets/images/loader.gif",
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                    });
                    setTimeout(function() {
                        $.ajax({
                            url: baseUrl + '/addicted/awards/update/' + '<?= $award->id ?>',
                            type: 'POST',
                            data: formData,
                            contentType: false,
                            processData: false,
                            headers: {'X-Requested-With': 'XMLHttpRequest'},
                            dataType: 'json',
                        }).done(function(data, textStatus, jqXHR) {
                            if (data.code == 200) {
                                // Proceso exitoso
                                Swal.fire("Proceso terminado", data.message, "success");
                            } else if (data.code == 400) {
                                // Error
                                Swal.fire("Lo sentimos", data.message, "error");
                            } else {
                                // Problemas de Validación
                                Swal.fire("Error de validación", data.message, "error");
                                $.each(data.validation, function(key, value) {
                                    $(`*[name='${key}']`).addClass('input-error').parent().append(`<span class="message-error"><i class="fa fa-exclamation-circle"></i> ${value}</span>`)
                                })
                            }
                        }).fail(function(jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR.responseText)
                            Swal.fire("Error", "Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.", "error");
                        });
                    }, 1000);
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire('Proceso cancelado', 'No se realizaron modificaciones en el sistema', 'info');
                }
            })
        })
    })
</script>
<?= $this->endSection() ?>
