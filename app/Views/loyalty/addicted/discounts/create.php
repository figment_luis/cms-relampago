<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Descuentos Addicted | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Plan de Lealtad Addicted<br><small style="padding-left: 0;">Esta sección le permite registrar un descuento sobre el sitio web de Addicted</small></h1>
<ol class="breadcrumb" >
    <li><a href="<?= base_url('/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?= base_url('/addicted/dashboard') ?>">Addicted</a></li>
    <li><a href="<?= base_url('/addicted/discounts') ?>">Descuentos</a></li>
    <li class="active">Registrar Descuento</li>
</ol>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="box">
    <form action="<?= route_to('addicted.discounts.store') ?>" method="post" novalidate id="formDiscountsCreate" enctype="multipart/form-data">
        <div class="box-header">
            <h3 class="box-title">Registrar Descuento</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="form-title">Titulo <span class="text-red">*</span>
                        <span class="help">Tip: La tienda que ofrece este descuento</span></label>
                        <input type="text" name="title" id="form-title" class="form-control" placeholder="Titulo principal del descuento" autocomplete="off" autofocus required>
                    </div>
                    <div class="form-group">
                        <label for="form-link">Link
                            <span class="help">Tip: URL que apunta a más información sobre este descuento</span>
                        </label>
                        <input type="url" id="form-link" name="link" class="form-control" placeholder="URL asociada al descuento" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label for="form-description">Descripción <span class="text-red">*</span>
                        <span class="help">Tip: Contenido detallado acerca de este descuento</span></label>
                        <textarea name="description" id="form-description" class="form-control" rows="6" placeholder="Descripción detallada del descuento" autocomplete="off" required></textarea>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="form-image">Portada <span class="text-red">*</span>
                        <span class="help">Tip: La imagen que representa este descuento <strong>Formato JPG de 300x300</strong></span></label>
                        <input id="form-image" name="image" type="file" class="file" data-preview-file-type="text" >
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-default pull-right"><span class="fa fa-database icon" aria-hidden="true"></span> Registrar</button>
        </div>
    </form>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/css/fileinput.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/libs/fileinput/js/fileinput.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/js/locales/es.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<!-- Optional: include a polyfill for ES6 Promises for IE11 -->

<script>
    let baseUrl = '<?= base_url() ?>'

    $("#form-image").fileinput({
        'showUpload': false,
        'showRemove': false,
        'required': true,
        maxImageWidth: 300,
        maxImageHeight: 300,
        minImageWidth: 300,
        minImageHeight: 300,
        maxFileSize: 500,
        'maxFileCount': 1,
        'language': 'es',
        'previewFileType': "image",
        'theme': "explorer",
        'allowedFileExtensions': ["jpg"],
        'validateInitialCount': true,
        msgFileRequired: 'El archivo de imagen es requerido',
        msgSizeTooLarge: 'La imagen supera el tamaño permitido de <b>{maxSize} kilobytes.</b><br>Recomendamos optimizarla para Web',
        msgInvalidFileExtension: 'Extensión de archivo no válida, solo se admiten <b>{extensions}</b>',
        msgImageWidthSmall: 'El ancho de imagen debe ser de al menos <b>{size} px.</b>',
        msgImageHeightSmall: 'La altura de imagen debe ser de al menos <b>{size} px.</b>',
        msgImageWidthLarge: 'El ancho de imagen no debe exceder los <b>{size} px.</b>',
        msgImageHeightLarge: 'El alto de imagen no debe exceder los <b>{size} px.</b>'
    })
    
    $(document).ready(function() {
        $("#formDiscountsCreate").submit(function(e) {
            e.preventDefault();
            e.stopPropagation();

            // Detener el proceso si existe uno o más errores con los archivos de imagen
            if ($(".file-input.theme-explorer").hasClass("has-error")) return false;

            let formData = new FormData($(this)[0]);
            Swal.fire({
                title: '¿Desea registrar este descuento?',
                text: "Usted esta a un paso de realizar un registro en el sistema",
                icon: 'warning',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showCancelButton: true,
                confirmButtonText: 'Si, deseo continuar',
                cancelButtonText: 'No, cancelar',
                //reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $(".message-error").hide();
                    $(".input-error").removeClass('input-error')

                    Swal.fire({
                        title: "Procesando solicitud",
                        text: "Espere un momento por favor.",
                        imageUrl: baseUrl + "/assets/images/loader.gif",
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                    });
                    setTimeout(function() {
                        $.ajax({
                            url: baseUrl + '/addicted/discounts/store',
                            type: 'POST',
                            data: formData,
                            contentType: false,
                            processData: false,
                            headers: {'X-Requested-With': 'XMLHttpRequest'},
                            dataType: 'json',
                        }).done(function(data, textStatus, jqXHR) {
                            if (data.code == 200) {
                                // Proceso exitoso
                                Swal.fire("Proceso terminado", data.message, "success");
                                $("#formDiscountsCreate")[0].reset();
                            } else if (data.code == 400) {
                                // Error
                                Swal.fire("Lo sentimos", data.message, "error");
                            } else {
                                // Problemas de Validación
                                Swal.fire("Error de validación", data.message, "error");
                                $.each(data.validation, function(key, value) {
                                    $(`*[name='${key}']`).addClass('input-error').parent().append(`<span class="message-error"><i class="fa fa-exclamation-circle"></i> ${value}</span>`)
                                })
                            }
                        }).fail(function(jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR.responseText)
                            Swal.fire("Error", "Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.", "error");
                        });
                    }, 1000);
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire('Proceso cancelado', 'No se realizaron modificaciones en el sistema', 'info');
                }
            })
        })
    })
</script>
<?= $this->endSection() ?>
