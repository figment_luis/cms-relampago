<!DOCTYPE html>
<html lang="es-MX">
    <head>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-KCJDZLN');</script>
        <!-- End Google Tag Manager -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>CMS Relámpago | Log in</title>
        <meta name="description" content="CMS Relámpago es el gestor de contenidos número uno para clientes Kinetiq">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- FAVICONS -->
        <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url('assets/favicon/apple-icon-57x57.png') ?>">
        <link rel="apple-touch-icon" sizes="60x60" href="<?= base_url('assets/favicon/apple-icon-60x60.png') ?>">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url('assets/favicon/apple-icon-72x72.png') ?>">
        <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url('assets/favicon/apple-icon-76x76.png') ?>">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url('assets/favicon/apple-icon-114x114.png') ?>">
        <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url('assets/favicon/apple-icon-120x120.png') ?>">
        <link rel="apple-touch-icon" sizes="144x144" href="<?= base_url('assets/favicon/apple-icon-144x144.png') ?>">
        <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url('assets/favicon/apple-icon-152x152.png') ?>">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url('assets/favicon/apple-icon-180x180.png') ?>">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?= base_url('assets/favicon/android-icon-192x192.png') ?>">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url('assets/favicon/favicon-32x32.png') ?>">
        <link rel="icon" type="image/png" sizes="96x96" href="<?= base_url('assets/favicon/favicon-96x96.png') ?>">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('assets/favicon/favicon-16x16.png') ?>">
        <link rel="manifest" href="<?= base_url('assets/favicon/manifest.json') ?>">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?= base_url('assets/favicon/ms-icon-144x144.png') ?>">
        <meta name="theme-color" content="#ffffff">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<?= base_url('assets/libs/adminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css') ?>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?= base_url('assets/libs/adminLTE/bower_components/font-awesome/css/font-awesome.min.css') ?>">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?= base_url('assets/libs/adminLTE/bower_components/Ionicons/css/ionicons.min.css') ?>">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?= base_url('assets/libs/adminLTE/dist/css/AdminLTE.min.css') ?>">
        <!-- iCheck -->
        <link rel="stylesheet" href="<?= base_url('assets/libs/adminLTE/plugins/iCheck/square/blue.css') ?>">
        <link rel="stylesheet" href="<?= base_url('css/auth.css') ?>">


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition login-page">
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KCJDZLN"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <div class="login-box">
            <div class="login-logo">
                <!--a href="<?= base_url('/') ?>"><b>CMS</b> <span>KINETIQ</span></a-->
                <!--a href="<?= base_url('/') ?>">
                    <img src="<?= base_url('/assets/images/kinetiq_min.png') ?>" class="home__image" alt="Logotipo Kinetiq">
                </a-->
                
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">
                <a href="<?= base_url('/') ?>" style="margin-bottom: .5rem; display: block; text-align: center;">
                    <img src="<?= base_url('/assets/images/kinetiq_min.png') ?>" class="home__image" alt="Logotipo Kinetiq">
                </a>
                <p class="login-box-msg">Favor de ingresas sus credenciales de acceso</p>
                
                <form action="<?= route_to('login.auth') ?>" method="post" novalidate id="formUsersAuth">
                    <div class="form-group has-feedback">
                        <input type="email" class="form-control" name="email" placeholder="Email">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" name="password" placeholder="Password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                            <div class="checkbox icheck">
                                <label>
                                    <input type="checkbox" name="rememberme" value="1"> Mantenerme conectado
                                </label>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>
                        </div>
                        <!-- /.col -->
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <p id="login-error-message"></p>
                        </div>
                    </div>
                </form>

            </div>
            <!-- /.login-box-body -->
        </div>
        <!-- /.login-box -->

        <!-- jQuery 3 -->
        <script src="<?= base_url('assets/libs/adminLTE/bower_components/jquery/dist/jquery.min.js') ?>"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="<?= base_url('assets/libs/adminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
        <!-- iCheck -->
        <script src="<?= base_url('assets/libs/adminLTE/plugins/iCheck/icheck.min.js') ?>"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
        <script>
            let baseUrl = '<?= base_url() ?>'

$(document).ready(function() {


        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' /* optional */
        });



    $("#formUsersAuth").submit(function(e) {
        e.preventDefault();
        e.stopPropagation();

        let formData = new FormData($(this)[0]);
        
        $(".message-error").hide();
        $(".input-error").removeClass('input-error')
        $("#login-error-message").text('');
                
        $.ajax({
            url: baseUrl + '/login/auth',
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            dataType: 'json',
        }).done(function(data, textStatus, jqXHR) {
            if (data.code == 200) {
                // Proceso exitoso - Redireccionar al Dashboard
                window.location = baseUrl + '/dashboard',
                //Swal.fire("Proceso terminado", data.message, "success");
                $("#formUsersAuth")[0].reset();
            } else if (data.code == 400) {
                // Error
                $("#login-error-message").text(data.message);
            } else {
                // Problemas de Validación
                $.each(data.validation, function(key, value) {
                    $(`*[name='${key}']`).addClass('input-error').parent().append(`<span class="message-error"><i class="fa fa-exclamation-circle"></i> ${value}</span>`)
                })
            }
        }).fail(function(jqXHR, textStatus, errorThrown) {
            console.log(jqXHR.responseText)
            Swal.fire("Error", "Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.", "error");
        });
                
    })
})
        </script>
    </body>
</html>
