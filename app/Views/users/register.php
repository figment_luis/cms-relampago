<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Registro de Usuarios | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>CMS KINETIQ<br><small style="padding-left: 0;">Esta sección le permite registrar un usuario en el sistema.</small></h1>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="box">
    <form action="<?= route_to('users.store') ?>" method="post" novalidate id="formUsersCreate">
        <div class="box-header">
            <h3 class="box-title">Registrar Usuario</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="form-firstname">Nombre <span class="text-red">*</span>
                        <span class="help">Tip: Alejandro</span></label>
                        <input type="text" name="firstname" id="form-firstname" class="form-control" placeholder="Nombre del usuario" autocomplete="off" autofocus required>
                    </div>
                    <div class="form-group">
                        <label for="form-lastname">Apellidos <span class="text-red">*</span>
                        <span class="help">Tip: González Reyes</span></label>
                        <input type="text" name="lastname" id="form-lastname" class="form-control" placeholder="Apellidos del usuario" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label for="form-email">Email <span class="text-red">*</span>
                        <span class="help">Tip: alejandro.gonzalez@figment.com.mx</span></label>
                        <input type="email" id="form-email" name="email" class="form-control" placeholder="Email corporativo del usuario" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label for="form-area">Área <span class="text-red">*</span>
                        <span class="help">Tip: El área a la cuál pertenece este usuario dentro de la empresa</span></label>
                        <select name="area" class="form-control" id="form-area">
                            <option value="" selected disabled>Seleccione una opción</option>
                            <?php foreach($areas as $area): ?>
                                <option value="<?= $area->id ?>"><?= $area->name ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="form-role">Role <span class="text-red">*</span>
                        <span class="help">Tip: El nivel de privilegios que tendrá este usuario en el sistema</span></label>
                        <select name="role" class="form-control" id="form-role">
                            <option value="" selected disabled>Seleccione una opción</option>
                            <?php foreach($roles as $role): ?>
                                <option value="<?= $role->id ?>"><?= $role->display_name ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="form-password">Contraseña <span class="text-red">*</span>
                        <span class="help">Tip: La contraseña de acceso para este usuario en el sistema</span></label>
                        <input type="password" name="password" id="form-password" class="form-control" placeholder="Contraseña de acceso - CMS KINETIQ" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label for="form-confirmpassword">Confirmar Contraseña <span class="text-red">*</span>
                        <span class="help">Tip: Ingrese nuevamente la contraseña de acceso para este usuario en el sistema</span></label>
                        <input type="password" name="confirmpassword" id="form-confirmpassword" class="form-control" placeholder="Contraseña de acceso - CMS KINETIQ" autocomplete="off" required>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-default pull-right"><span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> Registrar Usuario</button>
        </div>
    </form>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/css/fileinput.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/libs/fileinput/js/fileinput.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/js/locales/es.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    let baseUrl = '<?= base_url() ?>'

    $(document).ready(function() {
        $("#formUsersCreate").submit(function(e) {
            e.preventDefault();
            e.stopPropagation();

            let formData = new FormData($(this)[0]);
            Swal.fire({
                title: '¿Desea registrar este usuario?',
                text: "Usted esta a un paso de realizar un registro en el sistema",
                icon: 'warning',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showCancelButton: true,
                confirmButtonText: 'Si, deseo continuar',
                cancelButtonText: 'No, cancelar',
                //reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $(".message-error").hide();
                    $(".input-error").removeClass('input-error')

                    Swal.fire({
                        title: "Procesando solicitud",
                        text: "Espere un momento por favor.",
                        imageUrl: baseUrl + "/assets/images/loader.gif",
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                    });
                    setTimeout(function() {
                        $.ajax({
                            url: baseUrl + '/users/store',
                            type: 'POST',
                            data: formData,
                            contentType: false,
                            processData: false,
                            headers: {'X-Requested-With': 'XMLHttpRequest'},
                            dataType: 'json',
                        }).done(function(data, textStatus, jqXHR) {
                            if (data.code == 200) {
                                // Proceso exitoso
                                Swal.fire("Proceso terminado", data.message, "success");
                                $("#formUsersCreate")[0].reset();
                            } else if (data.code == 400) {
                                // Error
                                Swal.fire("Lo sentimos", data.message, "error");
                            } else {
                                // Problemas de Validación
                                Swal.fire("Error de validación", data.message, "error");
                                $.each(data.validation, function(key, value) {
                                    $(`*[name='${key}']`).addClass('input-error').parent().append(`<span class="message-error"><i class="fa fa-exclamation-circle"></i> ${value}</span>`)
                                })
                            }
                        }).fail(function(jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR.responseText)
                            Swal.fire("Error", "Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.", "error");
                        });
                    }, 1000);
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire('Proceso cancelado', 'No se realizaron modificaciones en el sistema', 'info');
                }
            })
        })
    })
</script>
<?= $this->endSection() ?>
