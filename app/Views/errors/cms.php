<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Error | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Información del sistema<br><small style="padding-left: 0;">Estimado usuario, hemos experimentado algunos problemas
        con su solicitud.</small></h1>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="messages">
    <div>
        <img src="<?= base_url('assets/images/error.gif') ?>" alt="">
    </div>
    <div>
        <h4>Descripción detallada del error</h4>
        <ul>
            <li><b>Sitio Web: </b><?= $message['site'] ?></li>
            <li><b>Sección: </b><?= $message['section'] ?></li>
            <li><b>Descripción: </b><?= $message['description'] ?></li>
        </ul>
    </div>


</div>
<?= $this->endSection() ?>