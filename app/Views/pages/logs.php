<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Logs del Sistema | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Logs del Sistema<br><small style="padding-left: 0;">Esta sección le permite visualizar los logs registradas sobre las diferentes cuentas gestionadas por el sistema</small></h1>
<ol class="breadcrumb" >
    <li><a href="<?= base_url('/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active">Logs Publicados</li>
</ol>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="box box-danger">
    <div class="box-container">
        <div class="box-header">
            <h3 class="box-title">Logs Publicados</h3>
        </div>
        <div class="box-body">
            <table id="tableMicrosites" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th width="120">Módulo</th>
                    <th width="150">Acción</th>
                    <th>Descripción</th>
                    <th width="130">Fecha de operación</th>
                    <th width="140">Usuario</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($logs as $index => $log): ?>
                    <tr id="log-<?= $log->id ?>">
                        <td style="vertical-align: middle;"><?= esc($log->module) ?></td>
                        <td style="vertical-align: middle;"><?= esc($log->action) ?></td>
                        <td style="vertical-align: middle;"><?= esc($log->description) ?></td>
                        <td style="vertical-align: middle;"><?= esc($log->created_at) ?></td>
                        <td style="vertical-align: middle;"><?= esc($log->user) ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
                <tfoot>

                </tfoot>
            </table>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('assets/libs/adminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<!-- DataTables -->
<script src="<?= base_url('assets/libs/adminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/adminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    $(document).ready(function() {
        let baseUrl = '<?= base_url() ?>'

        $('#tableMicrosites').DataTable({
            'pageLength': 10,
            "order": [],
            "lengthMenu": [[10, 15, 25, 50, -1], [10, 15, 25, 50, "Todos"]],
            language: {
                url: '<?= base_url('assets/libs/spanish-datatables.json') ?>'
            }
        });
    })
</script>
<?= $this->endSection() ?>
