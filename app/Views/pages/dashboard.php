<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Dashboard | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<!--h1>Page Header<small>Optional description</small></h1>
<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
</ol-->
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="home">
    <!--img src="<?= base_url('/assets/images/navidad.png') ?>" style="position: relative; top: 85px; right: 250px" alt="Navidad CMS Relámpago"-->
    <img src="<?= base_url('/assets/images/cms-relampago.png') ?>" class="home__image" alt="Logotipo CMS Relámpago">
    <p class="home__message text-center">
    <?php
        $role = session()->get('role');
        switch ($role) {
            case 'admin':
                echo 'Has iniciado sesión como:<span>Administrador del sistema (root)</span>';
                break;
            case 'thor':
                echo 'Has iniciado sesión como:<span>Administrador del Grupo Thor Urbana</span>';
                break;
            case 'gsm':
                echo 'Has iniciado sesión como:<span>Administrador del Grupo Sordo Madaleno</span>';
                break;
        }
    ?>
    </p>
    <div style="display: flex; justify-content: space-between; width: 100%; max-width: 800px;">
        <a href="https://docs.google.com/document/d/14Y5xc9U7nsYXF64t-OKbroC2l2HK6g8sbQPcGwO8jIc/edit?usp=sharing" class="home__button" target="_blank"><i class="fa fa-cloud-download home__icon"></i> Descargar manual de usuario</a>
        <a href="https://docs.google.com/document/d/1hXtRfljxs8Q22gmcVKfCImKSRxf8QS-Ayqb5i2yjHXM/edit?usp=sharing" class="home__button" target="_blank"><i class="fa fa-cloud-download home__icon"></i> Documento Centros Comerciales</a>
        <a href="https://docs.google.com/document/d/1cpXoJh2RjDwtQpmnYVhzuDJSmM4wNCzvCvzNCXxDi6Q/edit?usp=sharing" class="home__button" target="_blank"><i class="fa fa-cloud-download home__icon"></i> Documento Planes de Lealtad</a>
    </div>

</div>
<?= $this->endSection() ?>
