<?php $request = \Config\Services::request();?>
<ul class="sidebar-menu" data-widget="tree">
    <li class="header">MENÚ PRINCIPAL</li>
    <?php if(in_array(session()->get('role'), ['admin', 'gsm'])): ?>
        <li class="treeview <?= in_array($request->uri->getSegment('1'), ['antea', 'andamar', 'antara', 'angelopolis', 'luxury']) ? 'active' : '' ?>">
        <a href="#">
            <i class="fa fa-bank"></i> <span>Grupo GSM</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
            <li class="<?= $request->uri->getSegment('1') == 'antara'? 'active' : '' ?>">
                <a href="<?= route_to('antara.dashboard') ?>">
                    <i class="fa <?= $request->uri->getSegment('1') == 'antara' ? 'fa-circle text-orange' : 'fa-circle-o' ?>"></i> Antara
                </a>
            </li>
            <li class="<?= $request->uri->getSegment('1') == 'andamar'? 'active' : '' ?>">
                <a href="<?= route_to('andamar.dashboard') ?>">
                    <i class="fa <?= $request->uri->getSegment('1') == 'andamar' ? 'fa-circle text-orange' : 'fa-circle-o' ?>"></i> Andamar
                </a>
            </li>
            <li class="<?= $request->uri->getSegment('1') == 'antea'? 'active' : '' ?>">
                <a href="<?= route_to('antea.dashboard') ?>">
                    <i class="fa <?= $request->uri->getSegment('1') == 'antea' ? 'fa-circle text-orange' : 'fa-circle-o' ?>"></i> Antea
                </a>
            </li>
            <li class="<?= $request->uri->getSegment('1') == 'angelopolis'? 'active' : '' ?>">
                <a href="<?= route_to('angelopolis.dashboard') ?>">
                    <i class="fa <?= $request->uri->getSegment('1') == 'angelopolis' ? 'fa-circle text-orange' : 'fa-circle-o' ?>"></i> Angelópolis
                </a>
            </li>
            <li class="<?= $request->uri->getSegment('1') == 'luxury'? 'active' : '' ?>">
                <a href="<?= route_to('luxury.dashboard') ?>">
                    <i class="fa <?= $request->uri->getSegment('1') == 'luxury' ? 'fa-circle text-orange' : 'fa-circle-o' ?>"></i> Luxury Hall
                </a>
            </li>
        </ul>
    </li>
    <?php endif; ?>

    <?php if(in_array(session()->get('role'), ['admin', 'thor'])): ?>
        <li class="treeview <?= in_array($request->uri->getSegment('1'), ['townsquare', 'harbor', 'marina', 'corazon', 'altavista', 'landmark']) ? 'active' : '' ?>">
        <a href="#">
            <i class="fa fa-bank"></i> <span>Grupo THOR</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
            <li class="<?= $request->uri->getSegment('1') == 'townsquare'? 'active' : '' ?>">
                <a href="<?= route_to('townsquare.dashboard') ?>">
                    <i class="fa <?= $request->uri->getSegment('1') == 'townsquare' ? 'fa-circle text-orange' : 'fa-circle-o' ?>"></i> Townsquare Metepec
                </a>
            </li>
            <li class="<?= $request->uri->getSegment('1') == 'harbor'? 'active' : '' ?>">
                <a href="<?= route_to('harbor.dashboard') ?>">
                    <i class="fa <?= $request->uri->getSegment('1') == 'harbor' ? 'fa-circle text-orange' : 'fa-circle-o' ?>"></i> The Harbor Mérida
                </a>
            </li>
            <li class="<?= $request->uri->getSegment('1') == 'marina'? 'active' : '' ?>">
                <a href="<?= route_to('marina.dashboard') ?>">
                    <i class="fa <?= $request->uri->getSegment('1') == 'marina' ? 'fa-circle text-orange' : 'fa-circle-o' ?>"></i> Marina Puerto Cancún
                </a>
            </li>
            <li class="<?= $request->uri->getSegment('1') == 'corazon'? 'active' : '' ?>">
                <a href="<?= route_to('corazon.dashboard') ?>">
                    <i class="fa <?= $request->uri->getSegment('1') == 'corazon' ? 'fa-circle text-orange' : 'fa-circle-o' ?>"></i> Calle Corazón
                </a>
            </li>
            <li class="<?= $request->uri->getSegment('1') == 'landmark'? 'active' : '' ?>">
                <a href="<?= route_to('landmark.dashboard') ?>">
                    <i class="fa <?= $request->uri->getSegment('1') == 'landmark' ? 'fa-circle text-orange' : 'fa-circle-o' ?>"></i> Landmark Guadalajara
                </a>
            </li>
            <!--li class="<?= $request->uri->getSegment('1') == 'altavista'? 'active' : '' ?>">
                <a href="<?= route_to('altavista.dashboard') ?>">
                    <i class="fa <?= $request->uri->getSegment('1') == 'altavista' ? 'fa-circle text-orange' : 'fa-circle-o' ?>"></i> Altavista 147
                </a>
            </li-->
        </ul>
    </li>
    <?php endif; ?>

    <?php if(in_array(session()->get('role'), ['admin', 'gsm'])): ?>
        <li class="treeview <?= in_array($request->uri->getSegment('1'), ['addicted', 'pearlandamar', 'limitless', 'rewards', 'one']) ? 'active' : '' ?>">
        <a href="#">
            <i class="fa fa-diamond"></i> <span>Planes de Lealtad</span>
            <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
        </a>
        <ul class="treeview-menu">
            <li class="<?= $request->uri->getSegment('1') == 'addicted' ? 'active' : '' ?>">
                <a href="<?= route_to('addicted.dashboard') ?>">
                    <i class="fa <?= $request->uri->getSegment('1') == 'addicted' ? 'fa-circle text-orange' : 'fa-circle-o' ?>"></i> Addicted
                </a>
            </li>
            <li class="<?= $request->uri->getSegment('1') == 'pearlandamar' ? 'active' : '' ?>">
                <a href="<?= route_to('pearlandamar.dashboard') ?>">
                    <i class="fa <?= $request->uri->getSegment('1') == 'pearlandamar' ? 'fa-circle text-orange' : 'fa-circle-o' ?>"></i> Pearlandamar
                </a>
            </li>
            <li class="<?= $request->uri->getSegment('1') == 'limitless' ? 'active' : '' ?>">
                <a href="<?= route_to('limitless.dashboard') ?>">
                    <i class="fa <?= $request->uri->getSegment('1') == 'limitless' ? 'fa-circle text-orange' : 'fa-circle-o' ?>"></i> Limitless
                </a>
            </li>
            <li class="<?= $request->uri->getSegment('1') == 'rewards' ? 'active' : '' ?>">
                <a href="<?= route_to('rewards.dashboard') ?>" >
                    <i class="fa <?= $request->uri->getSegment('1') == 'rewards' ? 'fa-circle text-orange' : 'fa-circle-o' ?>"></i> Rewards
                </a>
            </li>
            <li class="<?= $request->uri->getSegment('1') == 'one' ? 'active' : '' ?>">
                <a href="<?= route_to('one.dashboard') ?>">
                    <i class="fa <?= $request->uri->getSegment('1') == 'one' ? 'fa-circle text-orange' : 'fa-circle-o' ?>"></i> Luxuryone
                </a>
            </li>
        </ul>
    </li>
    <?php endif; ?>

    <?php if(session()->get('role') === 'admin'): ?>
        <li class="treeview <?= in_array($request->uri->getSegment('1'), ['users', 'logs']) ? 'active' : '' ?>">
            <a href="#">
                <i class="fa fa-cog"></i> <span>Gestiones Administrativas</span>
                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>
            <ul class="treeview-menu">
                <li class="<?= $request->uri->getSegment('1') == 'users'? 'active' : '' ?>">
                    <a href="<?= route_to('users.create') ?>">
                        <i class="fa <?= $request->uri->getSegment('1') == 'users' ? 'fa-circle text-orange' : 'fa-circle-o' ?>"></i> Registrar Usuario
                    </a>
                </li>
                <li class="<?= $request->uri->getSegment('1') == 'logs' ? 'active' : '' ?>">
                    <a href="<?= route_to('logs.index') ?>">
                        <i class="fa <?= $request->uri->getSegment('1') == 'logs' ? 'fa-circle text-orange' : 'fa-circle-o' ?>"></i> Logs
                    </a>
                </li>
            </ul>
        </li>
    <?php endif; ?>
</ul>
