<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">

    <!-- Sidebar user panel (optional) -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?= cms_getAvatar(session()->get('user_email'), 45) ?>" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?= session()->get('user_fullname'); ?></p>
        <!-- Status -->
        <a href="#"><i class="fa fa-circle text-success"></i> Activo</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <?= $this->include('partials/menu') ?>
    <!-- /.sidebar-menu -->
  </section>
  <!-- /.sidebar -->
</aside>
