<header class="main-header">
  <!-- Logo -->
  <a href="<?= base_url('/dashboard') ?>" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>CMS</b></span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>CMS </b>Relámpago 3.6.1</span>
  </a>

  <!-- Header Navbar -->
  <nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- User Account Menu -->
        <li class="dropdown user user-menu">
          <!-- Menu Toggle Button -->
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <!-- The user image in the navbar-->
            <img src="<?= cms_getAvatar(session()->get('user_email'), 25) ?>" class="user-image" alt="User Image">
            <!-- hidden-xs hides the username on small devices so only the image appears. -->
            <span class="hidden-xs"><?= session()->get('user_fullname'); ?></span>
          </a>
          <ul class="dropdown-menu">
            <!-- The user image in the menu -->
            <li class="user-header">
              <!--img src="<?= base_url('assets/libs/adminLTE/dist/img/user2-160x160.jpg') ?>" class="img-circle" alt="User Image"-->
              <img src="<?= cms_getAvatar(session()->get('user_email'), 160) ?>" class="img-circle" alt="User Image">
              <p>
                <?= session()->get('user_fullname'); ?>
                <small>Miembro del equipo Figment</small>
              </p>
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-right">
                <form action="<?= route_to('login.logout') ?>" method="post">
                  <button class="btn btn-default btn-flat">Cerrar Sesión</button>
                </form>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
</header>
