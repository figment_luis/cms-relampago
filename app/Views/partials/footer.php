<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Desarrollado con &#x2764;&#xFE0F; para el equipo Figment
    </div>
    <!-- Default to the left -->
    Versión <strong style="color: #1998f6;">3.6.1 Santa</strong> - Todos los derechos reservados.
</footer>
