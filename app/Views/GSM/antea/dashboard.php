<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Antea | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Antea Lifestyle Center<br><small style="padding-left: 0;">Panel de Control Administrativo</small></h1>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="dashboard dashboard--antea">
  <h3 class="dashboard__title">Antea<span class="dashboard__slogan">Grupo Sordo Madaleno</span></h3>
  <div class="dashboard__body">
      <ul class="dashboard__menu">
          <li class="dashboard__item dashboard__item--square">
              <a href="<?= route_to('antea.news.index') ?>" class="dashboard__link dashboard__link--square">
                  <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                  Novedades
              </a>
              <p class="dashboard__description">Registre y gestione las novedades publicadas en sitio Web.</p>
          </li>
          <li class="dashboard__item dashboard__item--square">
              <a href="<?= route_to('antea.promotions.index') ?>" class="dashboard__link dashboard__link--square">
                  <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                  Promociones
              </a>
              <p class="dashboard__description">Registre y gestione las promociones publicadas en sitio Web.</p>
          </li>
          <li class="dashboard__item dashboard__item--square">
              <a href="<?= route_to('antea.slides.index') ?>" class="dashboard__link dashboard__link--square">
                  <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                  Slides
              </a>
              <p class="dashboard__description">Registre y gestione los slides publicados en sitio Web.</p>
          </li>
          <li class="dashboard__item dashboard__item--square">
              <a href="<?= route_to('antea.popups.index') ?>" class="dashboard__link dashboard__link--square">
                  <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                  Popups
              </a>
              <p class="dashboard__description">Registre y gestione los popups publicados en sitio Web.</p>
          </li>
          <li class="dashboard__item dashboard__item--square">
              <a href="<?= route_to('antea.stores.index') ?>" class="dashboard__link dashboard__link--square">
                  <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                  Tiendas
              </a>
              <p class="dashboard__description">Registre y gestione las tiendas publicadas en sitio Web.</p>
          </li>
          <li class="dashboard__item dashboard__item--square">
              <a href="<?= route_to('antea.microsites.index') ?>" class="dashboard__link dashboard__link--square">
                  <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                  Micrositios
              </a>
              <p class="dashboard__description">Registre y gestione los micrositios publicados en sitio Web.</p>
          </li>
      </ul>
  </div>
</div>
<?= $this->endSection() ?>
