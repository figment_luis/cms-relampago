<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Slides Angelópolis | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Angelópolis Lifestyle Center<br><small style="padding-left: 0;">Esta sección le permite ordenar los slides registrados sobre el sitio web de Angelópolis</small></h1>
<ol class="breadcrumb" >
    <li><a href="<?= base_url('/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?= base_url('/angelopolis/dashboard') ?>">Angelópolis</a></li>
    <li><a href="<?= base_url('/angelopolis/slides') ?>">Slides</a></li>
    <li class="active">Ordenar Slide</li>
</ol>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="box">
    <div class="box-container">
        <div class="box-header">
            <h3 class="box-title">Orden Slides</h3>
        </div>
        <div class="box-body">
            <div class="form-group">
                <label>Cambiar el orden de los slides <span class="text-red">*</span>
                    <span class="help">Tip: Arrastre y suelte el slide en su nueva posición dentro del carrusel principal</span></label>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="slides" class="slides row">
                        <?php foreach($slides as $index => $slide): ?>
                            <div data-id="<?= $slide->id?>" class="col-md-4">
                                <div class="slides__item">
                                    <div class="slides__number">
                                        <?= $slide->position ?>
                                    </div>
                                    <div>
                                        <img src="https://www.angelopolispuebla.com.mx/<?= esc($slide->image) ?>" alt="Slide" class="img-responsive slides__image">
                                        <p class="slides__title"><?= $slide->title ?></p>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <a href="#" id="btnGuardarOrden" class="btn btn-default pull-right"><span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> Guardar</a>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>

<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<!-- SortableJS -->
<script src="<?= base_url('assets/libs/sortableJS/Sortable.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    $(document).ready(function() {
        let baseUrl = '<?= base_url() ?>'
        let $btnGuardarOrden = $("#btnGuardarOrden")

        let slides = document.getElementById('slides');
        let sortable = Sortable.create(slides, {
            animation: 150,
            ghostClass: 'blue-background-class',
            onUpdate: function(evt) {
                $btnGuardarOrden.attr('disabled', false).css('pointer-events', 'auto');
            }
        });

        // Solo se permite guardar si el orden de los slides ha cambiado
        $btnGuardarOrden.attr('disabled', true).css('pointer-events', 'none');

        $btnGuardarOrden.click(function(e) {
            e.preventDefault();
            e.stopPropagation();
            console.log(sortable.toArray());
            Swal.fire({
                title: '¿Desea guardar la nueva posición de los slides?',
                text: "Usted esta a un paso de realizar un registro en el sistema",
                icon: 'warning',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showCancelButton: true,
                confirmButtonText: 'Si, deseo continuar',
                cancelButtonText: 'No, cancelar',
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire({
                        title: "Procesando solicitud",
                        text: "Espere un momento por favor.",
                        imageUrl: baseUrl + "/assets/images/loader.gif",
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                    });
                    setTimeout(function() {
                        $.ajax({
                            url: baseUrl + '/angelopolis/slides/updateposition/' + sortable.toArray().join('|'),
                            type: 'POST',
                            headers: {'X-Requested-With': 'XMLHttpRequest'},
                            dataType: 'json',
                        }).done(function(data, textStatus, jqXHR) {
                            if (data.code == 200){
                                Swal.fire("Proceso terminado!", data.message, "success");
                                // Mostrar la nueva numeración de posicionamiento de los slides
                                $(".slides__number").each(function(index) {
                                    $(this).text(index + 1)
                                })
                                // Volver a desactivar el botón de guardar
                                $btnGuardarOrden.attr('disabled', true).css('pointer-events', 'none');
                            } else {
                                Swal.fire("Lo sentimos", data.message, "error");
                            }
                        }).fail(function(jqXHR, textStatus, errorThrown) {
                            Swal.fire("Error", "Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.", "error");
                        });
                    }, 1000)
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire('Proceso cancelado', 'No se realizaron modificaciones en el sistema', 'info')
                }
            })
        })
    })
</script>
<?= $this->endSection() ?>
