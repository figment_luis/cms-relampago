<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Novedades Antara | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Antara Fashion Hall<br><small style="padding-left: 0;">Esta sección le permite registrar una novedad sobre el sitio web de Antara</small></h1>
<ol class="breadcrumb" >
    <li><a href="<?= base_url('/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?= base_url('/antara/dashboard') ?>">Antara</a></li>
    <li><a href="<?= base_url('/antara/news') ?>">Novedades</a></li>
    <li class="active">Registrar novedad</li>
</ol>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="box">
    <form action="<?= route_to('antara.news.store') ?>" method="post" novalidate id="formNewsCreate" enctype="multipart/form-data">
        <div class="box-header">
            <h3 class="box-title">Registrar Novedad</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="form-title">Titulo <span class="text-red">*</span>
                            <span class="help">Tip: Llegaron las ofertas a este centro comercial</span></label>
                        <input type="text" name="title" id="form-title" class="form-control" placeholder="Titulo principal de la novedad" autocomplete="off" autofocus required>
                    </div>
                    <div class="form-group">
                        <label for="form-subtitle">Subtitulo <span class="text-red">*</span>
                            <span class="help">Tip: Del 15 al 21 de Noviembre de <?= date('Y') ?></span></label>
                        <input type="text" name="subtitle" id="form-subtitle" class="form-control" placeholder="Subtitulo o slogan de la novedad" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label for="form-image">Portada <span class="text-red">*</span>
                            <span class="help">Tip: La imagen que representa esta novedad <strong>Formato JPG de 750x370</strong></span></label>
                        <input id="form-image" name="image" type="file" class="file" data-preview-file-type="text" >
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="form-link">Link
                            <span class="help">Tip: URL que apunta a más información sobre esta novedad</span></label>
                        <input type="url" id="form-link" name="link" class="form-control" placeholder="URL asociada a la novedad" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label for="form-description">Descripción <span class="text-red">*</span>
                            <span class="help">Tip: Contenido detallado acerca de esta novedad</span></label>
                        <textarea name="description" id="form-description" class="form-control" rows="6" placeholder="Descripción detallada de la novedad" autocomplete="off" required></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-default pull-right"><span class="fa fa-database icon" aria-hidden="true"></span> Registrar</button>
        </div>
    </form>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/css/fileinput.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/libs/fileinput/js/fileinput.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/js/locales/es.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<!-- Optional: include a polyfill for ES6 Promises for IE11 -->

<script>
    let baseUrl = '<?= base_url() ?>'

    $("#form-image").fileinput({
        'showUpload': false,
        'showRemove': false,
        'required': true,
        maxImageWidth: 750,
        maxImageHeight: 370,
        minImageWidth: 750,
        minImageHeight: 370,
        maxFileSize: 500,
        'maxFileCount': 1,
        'language': 'es',
        'previewFileType': "image",
        'theme': "explorer",
        'allowedFileExtensions': ["jpg"],
        'validateInitialCount': true,
        msgFileRequired: 'El archivo de imagen es requerido',
        msgSizeTooLarge: 'La imagen supera el tamaño permitido de <b>{maxSize} kilobytes.</b><br>Recomendamos optimizarla para Web',
        msgInvalidFileExtension: 'Extensión de archivo no válida, solo se admiten <b>{extensions}</b>',
        msgImageWidthSmall: 'El ancho de imagen debe ser de al menos <b>{size} px.</b>',
        msgImageHeightSmall: 'La altura de imagen debe ser de al menos <b>{size} px.</b>',
        msgImageWidthLarge: 'El ancho de imagen no debe exceder los <b>{size} px.</b>',
        msgImageHeightLarge: 'El alto de imagen no debe exceder los <b>{size} px.</b>'
    })
    
    $(document).ready(function() {
        $("#formNewsCreate").submit(function(e) {
            e.preventDefault();
            e.stopPropagation();

            // Detener el proceso si existe uno o más errores con los archivos de imagen
            if ($(".file-input.theme-explorer").hasClass("has-error")) return false;

            let formData = new FormData($(this)[0]);
            Swal.fire({
                title: '¿Desea registrar esta novedad?',
                text: "Usted esta a un paso de realizar un registro en el sistema",
                icon: 'warning',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showCancelButton: true,
                confirmButtonText: 'Si, deseo continuar',
                cancelButtonText: 'No, cancelar',
                //reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $(".message-error").hide();
                    $(".input-error").removeClass('input-error')

                    Swal.fire({
                        title: "Procesando solicitud",
                        text: "Espere un momento por favor.",
                        imageUrl: baseUrl + "/assets/images/loader.gif",
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                    });
                    setTimeout(function() {
                        $.ajax({
                            url: baseUrl + '/antara/news/store',
                            type: 'POST',
                            data: formData,
                            contentType: false,
                            processData: false,
                            headers: {'X-Requested-With': 'XMLHttpRequest'},
                            dataType: 'json',
                        }).done(function(data, textStatus, jqXHR) {
                            if (data.code == 200) {
                                // Proceso exitoso
                                Swal.fire("Proceso terminado", data.message, "success");
                                $("#formNewsCreate")[0].reset();
                            } else if (data.code == 400 || data.code == 500) {
                                // Error
                                Swal.fire("Lo sentimos", data.message, "error");
                            } else {
                                // Problemas de Validación
                                Swal.fire("Error de validación", data.message, "error");
                                $.each(data.validation, function(key, value) {
                                    $(`*[name='${key}']`).addClass('input-error').parent().append(`<span class="message-error"><i class="fa fa-exclamation-circle"></i> ${value}</span>`)
                                })
                            }
                        }).fail(function(jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR.responseText)
                            Swal.fire("Error", "Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.", "error");
                        });
                    }, 1000);
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire('Proceso cancelado', 'No se realizaron modificaciones en el sistema', 'info');
                }
            })
        })
    })
</script>
<?= $this->endSection() ?>
