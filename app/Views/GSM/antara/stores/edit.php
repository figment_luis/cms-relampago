<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Tiendas Antara | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Antara LifeStyle Center<br><small style="padding-left: 0;">Esta sección le permite editar una tienda sobre el sitio web de Antara</small></h1>
<ol class="breadcrumb" >
    <li><a href="<?= base_url('/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?= base_url('/antara/dashboard') ?>">Antara</a></li>
    <li><a href="<?= base_url('/antara/stores') ?>">Tiendas</a></li>
    <li class="active">Editar Tienda</li>
</ol>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="box">
    <form action="<?= route_to('antara.stores.update', $store->id) ?>" method="post" novalidate id="formStoresEdit" enctype="multipart/form-data">
        <div class="box-header">
            <h3 class="box-title">Editar Tienda</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="form-name">Nombre <span class="text-red">*</span>
                            <span class="help">Tip: Zara Home</span></label>
                        <input type="text" name="name" id="form-name" class="form-control" value="<?= esc($store->name) ?>" placeholder="Nombre de tienda" autocomplete="off" autofocus required>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="form-phone">Teléfono
                            <span class="help">Tip: Teléfono de 10 dígitos (552 1020300)</span></label>
                        <input type="text" name="phone" id="form-phone" class="form-control" value="<?= esc($store->telephone) ?>" placeholder="Teléfono de atención a clientes" autocomplete="off">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="form-hourhand">Horarios <span class="text-red">*</span>
                            <span class="help">Tip: Lunes a Domingo de 10:00am. a 20:00hrs.</span></label>
                        <input type="text" name="hourhand" id="form-hourhand" class="form-control" value="<?= esc($store->horario) ?>" placeholder="Horarios de atención a clientes" autocomplete="off" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="form-url">Sitio Web
                            <span class="help">Tip: Link que apunta al sitio Web de esta tienda</span></label>
                        <input type="url" name="url" id="form-url" class="form-control" value="<?= esc($store->url) ?>" placeholder="https://www.adidas.com/pages/productos" autocomplete="off">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="form-shorturl">Texto del enlace (Sitio Web)
                            <span class="help">Tip: Texto a mostrar en el Link del sitio Web de esta tienda</span></label>
                        <input type="url" name="shorturl" id="form-shorturl" class="form-control" value="<?= esc($store->short_url) ?>" placeholder="www.adidas.com" autocomplete="off">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="form-email">Email
                            <span class="help">Tip: Correo electrónico de contacto</span></label>
                        <input type="email" name="correo" id="form-email" class="form-control" value="<?= esc($store->email) ?>" placeholder="Correo electrónico de atención a clientes" autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="form-location">Ubicación en Plaza Comercial <span class="text-red">*</span>
                            <span class="help">Tip: Piso o nivel dónde se localiza esta tienda</span></label>
                        <select name="location" id="form-location" class="form-control" required>
                            <option value="" disabled>Seleccione una ubicación</option>
                            <?php foreach($levels as $level): ?>
                                <option value="<?= $level->id ?>" <?= $level->id == $store->location ? 'selected' : '' ?> ><?= esc($level->name) ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="form-category">Categoría <span class="text-red">*</span>
                            <span class="help">Tip: Categoría asociada a esta tienda</span></label>
                        <select name="category" id="form-category" class="form-control" required>
                            <?php if ($currentCategory): ?>
                                <option value="" disabled>Seleccione una categoría</option>
                                <?php foreach($categories as $index => $category): ?>
                                    <option value="<?= $category->id ?>" <?= $currentCategory->category_id == $category->id ? 'selected' : '' ?> ><?= esc($category->name) ?></option>
                                <?php endforeach; ?>
                            <?php else:?>
                                <option value="" selected disabled>Seleccione una categoría</option>
                                <?php foreach($categories as $index => $category): ?>
                                    <option value="<?= $category->id ?>"><?= esc($category->name) ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="form-keywords">Etiquetas <span class="text-red">*</span>
                            <span class="help">Tip: ¿Qué comercializa esta tienda?</span></label>
                        <input type="text" name="keywords" id="form-keywords" class="form-control" value="<?= esc($store->keywords) ?>" placeholder="ropa, moda, tecnología, fragancias, comida" autocomplete="off" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="form-description">Descripción <span class="text-red">*</span>
                            <span class="help">Tip: Contenido detallado acerca de esta tienda</span></label>
                        <textarea name="description" id="form-description" class="form-control" rows="16" placeholder="Descripción detallada de la tienda" autocomplete="off" required><?= esc($store->description) ?></textarea>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="form-logotipo">Logotipo <span class="text-red">*</span>
                            <span class="help">Tip: Logotipo asociado a esta tienda <strong>Formato JPG de 170x170</strong></span></label>
                        <input id="form-logotipo" name="logotipo" type="file" class="file" data-preview-file-type="text" >
                    </div>
                    <div class="form-group">
                        <label for="form-cover">Portada <span class="text-red">*</span>
                            <span class="help">Tip: Imagen de portada asociada a esta tienda <strong>Formato JPG de 800x400</strong></span></label>
                        <input id="form-cover" name="cover" type="file" class="file" data-preview-file-type="text" >
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <p style="float: left; margin:8px 0 0 0;"><span class="text-red" style="font-weight: bold;">*</span> Campos con información obligatoria</p>
            <button type="submit" class="btn btn-default pull-right"><span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> Actualizar Tienda</button>
        </div>
    </form>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<link rel="stylesheet" href="<?= base_url('assets/libs/adminLTE//bower_components/select2/dist/css/select2.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/css/fileinput.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/libs/adminLTE/bower_components/select2/dist/js/select2.full.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/js/fileinput.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/js/locales/es.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    let baseUrl = '<?= base_url() ?>'
    let storeId = '<?= $store->id ?>'

    $("#form-logotipo").fileinput({
        'showUpload': false,
        'showRemove': false,
        'initialPreviewShowDelete': false,
        maxImageWidth: 170,
        maxImageHeight: 170,
        minImageWidth: 170,
        minImageHeight: 170,
        maxFileSize: 500,
        'maxFileCount': 1,
        'language': 'es',
        'previewFileType': "image",
        'theme': "explorer",
        'allowedFileExtensions': ["jpg"],
        msgFileRequired: 'El archivo de imagen es requerido',
        msgSizeTooLarge: 'La archivo supera el tamaño permitido de <b>{maxSize} kilobytes.</b>',
        msgInvalidFileExtension: 'Extensión de archivo no válida, solo se admiten <b>{extensions}</b>',
        msgImageWidthSmall: 'El ancho de imagen debe ser de al menos <b>{size} px.</b>',
        msgImageHeightSmall: 'La altura de imagen debe ser de al menos <b>{size} px.</b>',
        msgImageWidthLarge: 'El ancho de imagen no debe exceder los <b>{size} px.</b>',
        msgImageHeightLarge: 'El alto de imagen no debe exceder los <b>{size} px.</b>',
        <?php if ($store->image): ?>
        'initialPreview': [
            '<img src="https://www.antara.com.mx/<?= esc($store->image) ?>" class="file-preview-image" width="70">'
        ],
        'initialPreviewConfig': [
            {
                'caption': '<?= esc($store->name) ?>'
            }
        ],
        <?php endif; ?>
    });

    $("#form-cover").fileinput({
        'showUpload': false,
        'showRemove': false,
        'initialPreviewShowDelete': false,
        maxImageWidth: 800,
        maxImageHeight: 400,
        minImageWidth: 800,
        minImageHeight: 400,
        maxFileSize: 500,
        'maxFileCount': 1,
        'language': 'es',
        'previewFileType': "image",
        'theme': "explorer",
        'allowedFileExtensions': ["jpg"],
        msgFileRequired: 'El archivo de imagen es requerido',
        msgSizeTooLarge: 'La archivo supera el tamaño permitido de <b>{maxSize} kilobytes.</b>',
        msgInvalidFileExtension: 'Extensión de archivo no válida, solo se admiten <b>{extensions}</b>',
        msgImageWidthSmall: 'El ancho de imagen debe ser de al menos <b>{size} px.</b>',
        msgImageHeightSmall: 'La altura de imagen debe ser de al menos <b>{size} px.</b>',
        msgImageWidthLarge: 'El ancho de imagen no debe exceder los <b>{size} px.</b>',
        msgImageHeightLarge: 'El alto de imagen no debe exceder los <b>{size} px.</b>',
        <?php if ($imageStore): ?>
        'initialPreview': [
            '<img src="https://www.antara.com.mx/assets/img/microsites/<?= $store->id ?>/<?= esc($imageStore) ?>" class="file-preview-image" width="70">'
        ],
        'initialPreviewConfig': [
            {
                'caption': '<?= esc($store->name) ?>'
            }
        ],
        <?php endif; ?>
    });

    $(document).ready(function() {
        $(".select2").select2();

        $("#formStoresEdit").submit(function(e) {
            e.preventDefault();
            e.stopPropagation();

            // Detener el proceso si existe uno o más errores con los archivos de imagen
            if ($(".file-input.theme-explorer").hasClass("has-error")) return false;

            let formData = new FormData($(this)[0]);
            Swal.fire({
                title: '¿Desea actualizar esta tienda?',
                text: "Usted esta a un paso de realizar una actualización en el sistema",
                icon: 'warning',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showCancelButton: true,
                confirmButtonText: 'Si, deseo continuar',
                cancelButtonText: 'No, cancelar',
            }).then((result) => {
                if (result.isConfirmed) {
                    $(".message-error").hide();
                    $(".input-error").removeClass('input-error')

                    Swal.fire({
                        title: "Procesando solicitud",
                        text: "Espere un momento por favor.",
                        imageUrl: baseUrl + "/assets/images/loader.gif",
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                    });
                    setTimeout(function(){
                        $.ajax({
                            url: baseUrl + '/antara/stores/update/' + storeId,
                            type: 'POST',
                            data: formData,
                            contentType: false,
                            processData: false,
                            headers: {'X-Requested-With': 'XMLHttpRequest'},
                            dataType: 'json',
                        }).done(function(data, textStatus, jqXHR) {
                            if (data.code == 200) {
                                // Proceso exitoso
                                Swal.fire("Proceso terminado", data.message, "success");
                            } else if (data.code == 400) {
                                // Error
                                Swal.fire("Lo sentimos", data.message, "error");
                            } else {
                                // Problemas de Validación
                                Swal.fire("Error de validación", data.message, "error");
                                $.each(data.validation, function(key, value) {
                                    $(`*[name='${key}']`).addClass('input-error').parent().append(`<span class="message-error"><i class="fa fa-exclamation-circle"></i> ${value}</span>`)
                                })
                            }
                        }).fail(function(jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR.responseText)
                            Swal.fire("Error", "Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.", "error");
                        });
                    }, 1000);
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire('Proceso cancelado', 'No se realizaron modificaciones en el sistema', 'info');
                }
            })
        })
    })
</script>
<?= $this->endSection() ?>
