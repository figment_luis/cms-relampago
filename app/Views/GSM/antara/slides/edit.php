<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Slides Antara | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Antara Fashion Hall<br><small style="padding-left: 0;">Esta sección le permite editar un slide sobre el sitio web de Antara</small></h1>
<ol class="breadcrumb" >
    <li><a href="<?= base_url('/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?= base_url('/antara/dashboard') ?>">Antara</a></li>
    <li><a href="<?= base_url('/antara/slides') ?>">Slides</a></li>
    <li class="active">Editar Slide</li>
</ol>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="box">
    <form action="<?= route_to('antara.slides.update', $slide->id) ?>" method="post" novalidate id="formSlidesEdit" enctype="multipart/form-data">
        <div class="box-header">
            <h3 class="box-title">Editar Slide</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="form-title">TituTítulo descriptivo para SEO <span class="text-red">*</span>
                            <span class="help">Tip: Felíz día de las madres <?= date('Y') ?> en Andamar</span></label>
                        <input type="text" name="title" id="form-title" class="form-control" value="<?= esc($slide->title) ?>" placeholder="Titulo descriptivo de la imagen" autocomplete="off" autofocus required>
                    </div>
                    <div class="form-group">
                        <label for="form-enddate">Fecha de cierre
                            <span class="help">Tip: Momento a partir del cuál el slide dejará de ser visible en el sitio</span></label>
                        <input type="datetime-local" name="end_date" id="form-enddate" class="form-control" value="<?= $slide->end_date ? date('Y-m-d\TH:i:s', strtotime(esc($slide->end_date))) : '' ?>" step="1" autocomplete="off" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="form-image">Imagen <span class="text-red">*</span>
                            <span class="help">Tip: La imagen que representa este slide <strong>Formato JPG de 2560x900</strong></span></label>
                        <input id="form-image" name="image" type="file" class="file">
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <p style="float: left; margin:8px 0 0 0;"><span class="text-red" style="font-weight: bold;">*</span> Aviso importante:
                <br>La fecha de cierre puede tener un margen de diferencia de 5 minutos (posteriores)
                <br>con respecto a su zona horaria local.</p>
            <button type="submit" class="btn btn-default pull-right"><span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> Actualizar Slide</button>
        </div>
    </form>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/css/fileinput.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/libs/fileinput/js/fileinput.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/js/locales/es.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    let baseUrl = '<?= base_url() ?>'

    $("#form-image").fileinput({
        'showUpload': false,
        'showRemove': false,
        'initialPreviewShowDelete': false,
        maxImageWidth: 2560,
        maxImageHeight: 900,
        minImageWidth: 2560,
        minImageHeight: 900,
        maxFileSize: 1024,
        'maxFileCount': 1,
        'language': 'es',
        'previewFileType': "image",
        'theme': "explorer",
        'allowedFileExtensions': ["jpg"],
        msgFileRequired: 'El archivo de imagen es requerido',
        msgSizeTooLarge: 'La imagen supera el tamaño permitido de <b>{maxSize} kilobytes.</b><br>Recomendamos optimizarla para Web',
        msgInvalidFileExtension: 'Extensión de archivo no válida, solo se admiten <b>{extensions}</b>',
        msgImageWidthSmall: 'El ancho de imagen debe ser de al menos <b>{size} px.</b>',
        msgImageHeightSmall: 'La altura de imagen debe ser de al menos <b>{size} px.</b>',
        msgImageWidthLarge: 'El ancho de imagen no debe exceder los <b>{size} px.</b>',
        msgImageHeightLarge: 'El alto de imagen no debe exceder los <b>{size} px.</b>',
        'initialPreview': [
            '<img src="https://www.antara.com.mx/<?= esc($slide->image) ?>" class="file-preview-image" width="60">'
        ],
        'initialPreviewConfig': [
            {
                'caption': '<?= esc($slide->title) ?>'
            }
        ],
    });

    $(document).ready(function() {
        $("#formSlidesEdit").submit(function(e) {
            e.preventDefault();
            e.stopPropagation();

            let formData = new FormData($(this)[0]);
            Swal.fire({
                title: '¿Desea actualizar este slide?',
                text: "Usted esta a un paso de realizar una actualización en el sistema",
                icon: 'warning',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showCancelButton: true,
                confirmButtonText: 'Si, deseo continuar',
                cancelButtonText: 'No, cancelar',
            }).then((result) => {
                if (result.isConfirmed) {
                    $(".message-error").hide();
                    $(".input-error").removeClass('input-error')

                    Swal.fire({
                        title: "Procesando solicitud",
                        text: "Espere un momento por favor.",
                        imageUrl: baseUrl + "/assets/images/loader.gif",
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                    });
                    setTimeout(function(){
                        $.ajax({
                            url: baseUrl + '/antara/slides/update/' + '<?= $slide->id ?>',
                            type: 'POST',
                            data: formData,
                            contentType: false,
                            processData: false,
                            headers: {'X-Requested-With': 'XMLHttpRequest'},
                            dataType: 'json',
                        }).done(function(data, textStatus, jqXHR) {
                            if (data.code == 200){
                                // Proceso exitoso
                                Swal.fire("Proceso terminado", data.message, "success");
                            } else if (data.code == 400) {
                                // Error
                                Swal.fire("Lo sentimos", data.message, "error");
                            } else {
                                // Problemas de Validación
                                Swal.fire("Error de validación", data.message, "error");
                                $.each(data.validation, function(key, value) {
                                    $(`*[name='${key}']`).addClass('input-error').parent().append(`<span class="message-error"><i class="fa fa-exclamation-circle"></i> ${value}</span>`)
                                })
                            }
                        }).fail(function(jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR.responseText)
                            Swal.fire("Error", "Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.", "error");
                        });
                    }, 1000);
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire('Proceso cancelado', 'No se realizaron modificaciones en el sistema', 'info');
                }
            })
        })
    })
</script>
<?= $this->endSection() ?>
