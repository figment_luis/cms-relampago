<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Slides Antara | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Antara Fashion Hall<br><small style="padding-left: 0;">Esta sección le permite gestionar los slides registrados sobre el sitio web de Antara</small></h1>
<ol class="breadcrumb" >
    <li><a href="<?= base_url('/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?= base_url('/antara/dashboard') ?>">Antara</a></li>
    <li class="active">Slides Publicados</li>
</ol>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="box">
    <div class="box-container">
        <div class="box-header">
            <h3 class="box-title">Slides Publicados</h3>
            <a href="<?= route_to('antara.slides.create') ?>" class="btn btn-secondary"><i class="fa fa-plus-circle icon"></i> Registrar slide</a>
            <a href="<?= route_to('antara.slides.orderbyposition') ?>" class="btn btn-secondary btn-secondary--gallery" style="margin-left: 8px;"><i class="fa fa-reorder icon"></i> Ordenar slides</a>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <table id="tableSlides" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="200">Imagen</th>
                                <th>Titulo</th>
                                <th width="110" class="text-center">Fecha de cierre</th>
                                <th width="60" class="text-center">Posición</th>
                                <th width="110">Fecha de registro</th>
                                <th width="130" class="text-center">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($slides as $index => $slide): ?>
                                <tr id="slide-<?= $slide->id ?>">
                                    <td>
                                        <img src="https://www.antara.com.mx/<?= esc($slide->image) ?>" alt="Slide" title="Slide Antara <?= $index + 1 ?>" class="img-responsive" width="250">
                                    </td>
                                    <td style="vertical-align: middle;"><?= esc($slide->title) ?></td>
                                    <td style="vertical-align: middle;" class="text-center"><?= $slide->end_date ? '<small class="label bg-green">Programada</span>' : '' ?></td>
                                    <td style="vertical-align: middle;" class="text-center"><?= $slide->position ?></td>
                                    <td style="vertical-align: middle;"><?= esc($slide->created_at) ?></td>
                                    <td style="vertical-align: middle;" class="text-center">
                                        <a href="<?= route_to('antara.slides.edit', $slide->id) ?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Editar</a>
                                        <button class="btn btn-sm btn-danger btnRemoveSlide" data-id="<?= $slide->id ?>">
                                            <i class="fa fa-trash"></i> Eliminar
                                        </button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>
                </div>
                
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('assets/libs/adminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<!-- DataTables -->
<script src="<?= base_url('assets/libs/adminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/adminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    $(document).ready(function() {
        let baseUrl = '<?= base_url() ?>'

        $('#tableSlides').DataTable({
            'pageLength': 3,
            "lengthMenu": [[3, 8, 25, 50, -1], [3, 8, 25, 50, "Todos"]],
            language: {
                url: '<?= base_url('assets/libs/spanish-datatables.json') ?>'
            }
        });

        $("#tableSlides").on("click", ".btnRemoveSlide", function(e) {
            e.preventDefault();
            e.stopPropagation();
            let id = $(this).data('id');
            Swal.fire({
                title: '¿Desea eliminar este slide?',
                text: "Usted esta a un paso de eliminar un registro en el sistema",
                icon: 'warning',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showCancelButton: true,
                confirmButtonText: 'Si, deseo continuar',
                cancelButtonText: 'No, cancelar',
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire({
                        title: "Procesando solicitud",
                        text: "Espere un momento por favor.",
                        imageUrl: baseUrl + "/assets/images/loader.gif",
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                    });
                    setTimeout(function() {
                        $.ajax({
                            url: baseUrl + '/antara/slides/destroy/' + id,
                            type: 'POST',
                            headers: {'X-Requested-With': 'XMLHttpRequest'},
                            dataType: 'json',
                        }).done(function(data, textStatus, jqXHR) {
                            if (data.code == 200){
                                Swal.fire("Proceso terminado!", data.message, "success");
                                $("#slide-" + id).fadeOut(200);
                            } else {
                                Swal.fire("Lo sentimos", data.message, "error");
                            }
                        }).fail(function(jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR.responseText)
                            Swal.fire("Error", "Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.", "error");
                        });
                    }, 1000)
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire('Proceso cancelado', 'No se realizaron modificaciones en el sistema', 'info')
                }
            })
        })
    })
</script>
<?= $this->endSection() ?>
