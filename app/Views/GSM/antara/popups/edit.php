<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Popups Antara | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Antara Fashion Hall<br><small style="padding-left: 0;">Esta sección le permite editar un popup sobre el sitio web de Antara</small></h1>
<ol class="breadcrumb" >
    <li><a href="<?= base_url('/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?= base_url('/antara/dashboard') ?>">Antara</a></li>
    <li><a href="<?= base_url('/antara/popups') ?>">Popups</a></li>
    <li class="active">Editar Popup</li>
</ol>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="box">
    <form action="<?= route_to('antara.popups.update', $popup->id) ?>" method="post" novalidate id="formPopupsEdit" enctype="multipart/form-data">
        <div class="box-header">
            <h3 class="box-title">Editar Popup</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="form-title">Titulo <span class="text-red">*</span>
                        <span class="help">Tip: Promociones del BuenFin <?= date('Y') ?></span></label>
                        <input type="text" name="title" class="form-control" id="form-title" value="<?= esc($popup->title) ?>" placeholder="Ingrese un titulo descriptivo asociado a este popup" autocomplete="off" autofocus required>
                    </div>
                    <div class="form-group">
                        <label for="form-startdate">Fecha de inicio <span class="text-red">*</span>
                        <span class="help">Tip: Momento a partir del cuál el popup será visible en el sitio</span></label>
                        <input type="datetime-local" name="start_date" id="form-startdate" class="form-control" value="<?= date('Y-m-d\TH:i:s', strtotime(esc($popup->start_date))) ?>" step="1" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label for="form-enddate">Fecha de cierre <span class="text-red">*</span>
                        <span class="help">Tip: Momento a partir del cuál el popup dejará de ser visible en el sitio</span></label>
                        <input type="datetime-local" name="end_date" id="form-enddate" class="form-control" value="<?= date('Y-m-d\TH:i:s', strtotime(esc($popup->end_date))) ?>" step="1" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label for="form-link">Link
                            <span class="help">Tip: URL que apunta a más información sobre este popup (micrositio)</span>
                        </label>
                        <input type="url" name="link" id="form-link" class="form-control" value="<?= esc($popup->link) ?>" placeholder="Ingrese la URL asociada al Popup" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label>
                          <input type="checkbox" name="enabled" <?= $popup->enabled ? 'checked' : '' ?> class="flat-red"> ¿El popup se encuentra habilitado?
                          <span class="help" style="margin-left: 2.2rem;">Tip 1: Deshabilite este popup sin importar su fecha de cierre</span>
                          <span class="help" style="margin-left: 2.2rem;">Tip 2: Antes de habilitar nuevamente este popup, verifique su fecha de cierre</span>
                        </label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="form-imagedesktop">Imagen Desktop <span class="text-red">*</span>
                        <span class="help">Tip: La imagen que muestra este popup <strong>Formato JPG de 500x500</strong></span></label>
                        <input id="form-imagedesktop" name="image_desktop" type="file" class="file">
                    </div>
                    <div class="form-group">
                        <label for="form-imagemobile">Imagen Mobile <span class="text-red">*</span>
                        <span class="help">Tip: La imagen que muestra este popup <strong>Formato JPG de 300x500</strong></span></label>
                        <input id="form-imagemobile" name="image_mobile" type="file" class="file">
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <p style="float: left; margin:8px 0 0 0;"><span class="text-red" style="font-weight: bold;">*</span> Campos con información obligatoria</p>
            <button type="submit" class="btn btn-default pull-right"><span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> Editar</button>
        </div>
    </form>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/css/fileinput.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/libs/adminLTE/plugins/iCheck/all.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/libs/adminLTE/plugins/iCheck/icheck.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/js/fileinput.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/js/locales/es.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    let baseUrl = '<?= base_url() ?>'

    $("#form-imagedesktop").fileinput({
        'showUpload': false,
        'showRemove': false,
        'initialPreviewShowDelete': false,
        maxImageWidth: 500,
        maxImageHeight: 500,
        minImageWidth: 500,
        minImageHeight: 500,
        maxFileSize: 500,
        'maxFileCount': 1,
        'language': 'es',
        'previewFileType': "image",
        'theme': "explorer",
        'allowedFileExtensions': ["jpg"],
        msgFileRequired: 'El archivo de imagen es requerido',
        msgSizeTooLarge: 'La imagen supera el tamaño permitido de <b>{maxSize} kilobytes.</b><br>Recomendamos optimizarla para Web',
        msgInvalidFileExtension: 'Extensión de archivo no válida, solo se admiten <b>{extensions}</b>',
        msgImageWidthSmall: 'El ancho de imagen debe ser de al menos <b>{size} px.</b>',
        msgImageHeightSmall: 'La altura de imagen debe ser de al menos <b>{size} px.</b>',
        msgImageWidthLarge: 'El ancho de imagen no debe exceder los <b>{size} px.</b>',
        msgImageHeightLarge: 'El alto de imagen no debe exceder los <b>{size} px.</b>',
        'initialPreview': [
            '<img src="http://antara.com.mx/<?= esc($popup->image_desktop) ?>" class="file-preview-image" width="60">'
        ],
        'initialPreviewConfig': [
            {
                'caption': 'Imagen Popup (tamaño escritorio)'
            }
        ],
    });

    $("#form-imagemobile").fileinput({
        'showUpload': false,
        'showRemove': false,
        'initialPreviewShowDelete': false,
        maxImageWidth: 300,
        maxImageHeight: 500,
        minImageWidth: 300,
        minImageHeight: 500,
        maxFileSize: 500,
        'maxFileCount': 1,
        'language': 'es',
        'previewFileType': "image",
        'theme': "explorer",
        'allowedFileExtensions': ["jpg"],
        msgFileRequired: 'El archivo de imagen es requerido',
        msgSizeTooLarge: 'La imagen supera el tamaño permitido de <b>{maxSize} kilobytes.</b><br>Recomendamos optimizarla para Web',
        msgInvalidFileExtension: 'Extensión de archivo no válida, solo se admiten <b>{extensions}</b>',
        msgImageWidthSmall: 'El ancho de imagen debe ser de al menos <b>{size} px.</b>',
        msgImageHeightSmall: 'La altura de imagen debe ser de al menos <b>{size} px.</b>',
        msgImageWidthLarge: 'El ancho de imagen no debe exceder los <b>{size} px.</b>',
        msgImageHeightLarge: 'El alto de imagen no debe exceder los <b>{size} px.</b>',
        'initialPreview': [
            '<img src="http://antara.com.mx/<?= esc($popup->image_mobile) ?>" class="file-preview-image" width="60">'
        ],
        'initialPreviewConfig': [
            {
                'caption': 'Imagen Popup (tamaño mobile)'
            }
        ],
    });

    $(document).ready(function() {

        $('input[type="checkbox"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass   : 'iradio_flat-green'
        });

        $("#formPopupsEdit").submit(function(e) {
            e.preventDefault();
            e.stopPropagation();

            // Detener el proceso si existe uno o más errores con los archivos de imagen
            if ($(".file-input.theme-explorer").hasClass("has-error")) return false;

            let formData = new FormData($(this)[0]);
            Swal.fire({
                title: '¿Desea actualizar este popup?',
                text: "Usted esta a un paso de realizar una actualización en el sistema",
                icon: 'warning',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showCancelButton: true,
                confirmButtonText: 'Si, deseo continuar',
                cancelButtonText: 'No, cancelar',
            }).then((result) => {
                if (result.isConfirmed) {
                    $(".message-error").hide();
                    $(".input-error").removeClass('input-error')

                    Swal.fire({
                        title: "Procesando solicitud",
                        text: "Espere un momento por favor.",
                        imageUrl: baseUrl + "/assets/images/loader.gif",
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                    });
                    setTimeout(function(){
                        $.ajax({
                            url: baseUrl + '/antara/popups/update/' + '<?= $popup->id ?>',
                            type: 'POST',
                            data: formData,
                            contentType: false,
                            processData: false,
                            headers: {'X-Requested-With': 'XMLHttpRequest'},
                            dataType: 'json',
                        }).done(function(data, textStatus, jqXHR) {
                            if (data.code == 200) {
                                // Proceso exitoso
                                Swal.fire("Proceso terminado", data.message, "success");
                            } else if (data.code == 400) {
                                // Error
                                Swal.fire("Lo sentimos", data.message, "error");
                            } else if (data.code == 500) {
                                // Error
                                Swal.fire("Lo sentimos", data.message, "error");
                            } else {
                                // Problemas de Validación
                                Swal.fire("Error de validación", data.message, "error");
                                $.each(data.validation, function(key, value) {
                                    $(`*[name='${key}']`).addClass('input-error').parent().append(`<span class="message-error"><i class="fa fa-exclamation-circle"></i> ${value}</span>`)
                                })
                            }
                        }).fail(function(jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR.responseText)
                            Swal.fire("Error", "Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.", "error");
                        });
                    }, 1000);
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire('Proceso cancelado', 'No se realizaron modificaciones en el sistema', 'info');
                }
            })
        })
    })
</script>
<?= $this->endSection() ?>
