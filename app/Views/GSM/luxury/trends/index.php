<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Temas del Mes Luxury Hall | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Luxury Hall Lifestyle Center<br><small style="padding-left: 0;">Esta sección le permite gestionar los temas del mes registrados sobre la App de Luxury Hall</small></h1>
<ol class="breadcrumb" >
    <li><a href="<?= base_url('/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?= base_url('/luxury/dashboard') ?>">Luxury Hall</a></li>
    <li class="active">Temas del Mes Publicados</li>
</ol>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="box">
    <div class="box-container">
        <div class="box-header">
            <h3 class="box-title">Temas del Mes Publicados</h3>
            <a href="<?= route_to('luxury.trends.create') ?>" class="btn btn-secondary"><i class="fa fa-plus-circle icon"></i> Registrar tema del mes</a>
        </div>
        <div class="box-body">
            <table id="tableTrends" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th width="60">Portada</th>
                        <th width="200">Titulo</th>
                        <th>Resumen</th>
                        <th width="130" class="text-center">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($trends as $index => $trend): ?>
                        <tr id="trend-<?= $trend->id ?>">
                            <td><img src="<?= esc($trend->img) ?>" alt="Portada" title="Portada <?= esc($trend->title) ?>" class="img-responsive" width="80"></td>
                            <td style="vertical-align: middle;"><?= esc($trend->title) ?></td>
                            <td style="vertical-align: middle;"><?= word_limiter(esc(strip_tags($trend->descriptionView)) ?: '', 40) ?></td>
                            <td style="vertical-align: middle;" class="text-center">
                                <a href="<?= route_to('luxury.trends.edit', $trend->id) ?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Editar</a>
                                <button class="btn btn-sm btn-danger btnRemoveTrend" data-id="<?= $trend->id ?>"><i class="fa fa-trash"></i> Eliminar</button>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
                <tfoot>

                </tfoot>
            </table>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('assets/libs/adminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<!-- DataTables -->
<script src="<?= base_url('assets/libs/adminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/adminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    $(document).ready(function() {
        let baseUrl = '<?= base_url() ?>'

        $('#tableTrends').DataTable({
            'pageLength': 4,
            "lengthMenu": [[4, 8, 25, 50, -1], [4, 8, 25, 50, "Todos"]],
            language: {
                url: '<?= base_url('assets/libs/spanish-datatables.json') ?>'
            }
        });

        $("#tableTrends").on("click", ".btnRemoveTrend", function(e) {
            e.preventDefault();
            e.stopPropagation();
            let id = $(this).data('id');
            Swal.fire({
                title: '¿Desea eliminar este tema del mes?',
                text: "Usted esta a un paso de eliminar un registro en el sistema",
                icon: 'warning',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showCancelButton: true,
                confirmButtonText: 'Si, deseo continuar',
                cancelButtonText: 'No, cancelar',
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire({
                        title: "Procesando solicitud",
                        text: "Espere un momento por favor.",
                        imageUrl: baseUrl + "/assets/images/loader.gif",
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                    });
                    setTimeout(function() {
                        $.ajax({
                            url: baseUrl + '/luxury/trends/destroy',
                            type: 'POST',
                            data: {id},
                            headers: {'X-Requested-With': 'XMLHttpRequest'},
                            dataType: 'json',
                        }).done(function(data, textStatus, jqXHR) {
                            if (data.code == 200){
                                Swal.fire("Proceso terminado!", data.message, "success");
                                $("#trend-" + id).fadeOut(200);
                            } else {
                                Swal.fire("Lo sentimos", data.message, "error");
                            }
                        }).fail(function(jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR.responseText)
                            Swal.fire("Error", "Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.", "error");
                        });
                    }, 1000);
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire('Proceso cancelado', 'No se realizaron modificaciones en el sistema', 'info')
                }
            })
        })
    })
</script>
<?= $this->endSection() ?>
