<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Micrositios Luxury Hall | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Luxury Hall Lifestyle Center<br><small style="padding-left: 0;">Esta sección le permite registrar un micrositio sobre el sitio web de Luxury Hall</small></h1>
<ol class="breadcrumb" >
    <li><a href="<?= base_url('/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?= base_url('/luxury/dashboard') ?>">Luxury Hall</a></li>
    <li><a href="<?= base_url('/luxury/microsites') ?>">Micrositios</a></li>
    <li class="active">Registrar Micrositio</li>
</ol>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="box">
    <form action="<?= route_to('luxury.microsites.store') ?>" method="post" novalidate id="formMicrositeCreate" enctype="multipart/form-data">
        <div class="box-header">
            <h3 class="box-title">Registrar Micrositio</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="form-title">Titulo <span class="text-red">*</span>
                            <span class="help">Tip: BuenFin 2021</span></label></label>
                        <input type="text" name="title" id="form-title" class="form-control" placeholder="Titulo principal del micrositio" autofocus required>
                    </div>
                    <div class="form-group">
                        <label for="form-link">Slug <span class="text-red">*</span>
                            <span class="help">Tip: URL que apuntará a este micrositio (evite el uso de caracteres especiales)
                                <br>Ejemplo: https://luxuryhall.com.mx/<strong style="margin-left: 0;">slug</strong>
                            </span>
                        </label>
                        <input type="text" name="slug" id="form-slug" class="form-control" placeholder="Ingrese el slug asociado al micrositio" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label for="form-startdate">Fecha de inicio <span class="text-red">*</span>
                            <span class="help">Tip: Momento a partir del cuál el micrositio será visible en el sitio</span></label>
                        <input type="datetime-local" name="start_date" id="form-startdate" class="form-control" step="1" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label for="form-enddate">Fecha de cierre <span class="text-red">*</span>
                            <span class="help">Tip: Momento a partir del cuál el micrositio dejará de ser visible en el sitio</span></label>
                        <input type="datetime-local" name="end_date" id="form-enddate" class="form-control" step="1" autocomplete="off" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="form-image">Imagen <span class="text-red">*</span>
                            <span class="help">Tip: La imagen que muestra el contenido del micrositio <strong>Formato JPG de 1200x...</strong></span></label>
                        <input id="form-image" name="image" type="file" class="file">
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <p style="float: left; margin:8px 0 0 0;"><span class="text-red" style="font-weight: bold;">*</span> Campos con información obligatoria</p>
            <button type="submit" class="btn btn-default pull-right"><span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> Registrar</button>
        </div>
    </form>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/css/fileinput.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/libs/fileinput/js/fileinput.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/js/locales/es.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    let baseUrl = '<?= base_url() ?>'

    $("#form-image").fileinput({
        'showUpload': false,
        'showRemove': false,
        'required': true,
        maxImageWidth: 1200,
        //maxImageHeight: 370,
        minImageWidth: 1200,
        //minImageHeight: 370,
        maxFileSize: 4096,
        'maxFileCount': 1,
        'language': 'es',
        'previewFileType': "image",
        'theme': "explorer",
        'allowedFileExtensions': ["jpg"],
        'validateInitialCount': true,
        msgFileRequired: 'El archivo de imagen es requerido',
        msgSizeTooLarge: 'La imagen supera el tamaño permitido de <b>{maxSize} kilobytes.</b><br>Recomendamos optimizarla para Web',
        msgInvalidFileExtension: 'Extensión de archivo no válida, solo se admiten <b>{extensions}</b>',
        msgImageWidthSmall: 'El ancho de imagen debe ser de al menos <b>{size} px.</b>',
        msgImageHeightSmall: 'La altura de imagen debe ser de al menos <b>{size} px.</b>',
        msgImageWidthLarge: 'El ancho de imagen no debe exceder los <b>{size} px.</b>',
        msgImageHeightLarge: 'El alto de imagen no debe exceder los <b>{size} px.</b>'
    })

    $(document).ready(function() {
        $("#formMicrositeCreate").submit(function(e) {
            e.preventDefault();
            e.stopPropagation();

            // Detener el proceso si existe uno o más errores con los archivos de imagen
            if ($(".file-input.theme-explorer").hasClass("has-error")) return false;

            let formData = new FormData($(this)[0]);
            Swal.fire({
                title: '¿Desea registrar este micrositio?',
                text: "Usted esta a un paso de realizar un registro en el sistema",
                icon: 'warning',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showCancelButton: true,
                confirmButtonText: 'Si, deseo continuar',
                cancelButtonText: 'No, cancelar',
                //reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {
                    $(".message-error").hide();
                    $(".input-error").removeClass('input-error')

                    Swal.fire({
                        title: "Procesando solicitud",
                        text: "Espere un momento por favor.",
                        imageUrl: baseUrl + "/assets/images/loader.gif",
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                    });
                    setTimeout(function() {
                        $.ajax({
                            url: baseUrl + '/luxury/microsites/store',
                            type: 'POST',
                            data: formData,
                            contentType: false,
                            processData: false,
                            headers: {'X-Requested-With': 'XMLHttpRequest'},
                            dataType: 'json',
                        }).done(function(data, textStatus, jqXHR) {
                            if (data.code == 200) {
                                // Proceso exitoso
                                Swal.fire({title: "Proceso terminado", html: data.message, icon: "success"});
                                $("#formMicrositeCreate")[0].reset();
                            } else if (data.code == 400) {
                                // Error
                                Swal.fire("Lo sentimos", data.message, "error");
                            } else {
                                // Problemas de Validación
                                Swal.fire("Error de validación", data.message, "error");
                                $.each(data.validation, function(key, value) {
                                    $(`*[name='${key}']`).addClass('input-error').parent().append(`<span class="message-error"><i class="fa fa-exclamation-circle"></i> ${value}</span>`)
                                })
                            }
                        }).fail(function(jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR.responseText)
                            Swal.fire("Error", "Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.", "error");
                        });
                    }, 1000);
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire('Proceso cancelado', 'No se realizaron modificaciones en el sistema', 'info');
                }
            })
        })
    })
</script>
<?= $this->endSection() ?>
