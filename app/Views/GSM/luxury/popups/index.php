<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Popups Luxury Hall | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Luxury Hall Always in the Spotlight<br><small style="padding-left: 0;">Esta sección le permite gestionar los popups registrados sobre el sitio web de Luxury Hall Always in the Spotlight</small></h1>
<ol class="breadcrumb" >
    <li><a href="<?= base_url('/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?= base_url('/luxury/dashboard') ?>">Luxury Hall</a></li>
    <li class="active">Popups Publicados</li>
</ol>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="box">
    <div class="box-container">
        <div class="box-header">
            <h3 class="box-title">Popups Publicados</h3>
            <a href="<?= route_to('luxury.popups.create') ?>" class="btn btn-secondary"><i class="fa fa-plus-circle icon"></i> Registrar popup</a>
        </div>
        <div class="box-body">
            <table id="tablePopups" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th width="100">Imagen</th>
                        <th>Titulo</th>
                        <th class="text-center">Fecha de inicio</th>
                        <th class="text-center">Fecha de cierre</th>
                        <th class="text-center">Disponibilidad</th>
                        <th width="70" class="text-center">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($popups as $index => $popup): ?>
                        <tr id="popup-<?= $popup->id ?>">
                            <td style="vertical-align: middle;">
                                <img src="http://luxuryhall.com.mx/<?= esc($popup->image_desktop) ?>" alt="popup" class="image-responsive" width="180" title="Popup: <?= esc($popup->title) ?>">
                            </td>
                            <td style="vertical-align: middle;"><?= esc($popup->title) ?></td>
                            <td class="text-center" style="vertical-align: middle;"><?= esc($popup->start_date) ?></td>
                            <td class="text-center" style="vertical-align: middle;"><?= esc($popup->end_date) ?></td>
                            <td class="text-center" style="vertical-align: middle;"><?= date('U', strtotime(esc($popup->end_date))) > time() && $popup->enabled ? '<small class="label bg-green">Activado</small>' : '<small class="label bg-red">Desactivado</small>' ?></td>
                            <td style="vertical-align: middle;" class="text-center">
                                <a href="<?= route_to('luxury.popups.edit', $popup->id) ?>" class="btn btn-sm btn-warning">
                                    <i class="fa fa-edit"></i> Editar
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
                <tfoot>

                </tfoot>
            </table>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('assets/libs/adminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<!-- DataTables -->
<script src="<?= base_url('assets/libs/adminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/adminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    $(document).ready(function() {
        let baseUrl = '<?= base_url() ?>'

        $('#tablePopups').DataTable({
            'pageLength': 2,
            "lengthMenu": [[2, 8, 25, 50, -1], [2, 8, 25, 50, "Todos"]],
            language: {
                url: '<?= base_url('assets/libs/spanish-datatables.json') ?>'
            }
        });

        $("#tablePopups").on("click", ".btnRemovePopup", function(e) {
            e.preventDefault();
            e.stopPropagation();
            let id = $(this).data('id');
            Swal.fire({
                title: '¿Desea eliminar este popup?',
                text: "Usted esta a un paso de eliminar un registro en el sistema",
                icon: 'warning',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showCancelButton: true,
                confirmButtonText: 'Si, deseo continuar',
                cancelButtonText: 'No, cancelar',
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire({
                        title: "Procesando solicitud",
                        text: "Espere un momento por favor.",
                        imageUrl: baseUrl + "/assets/images/loader.gif",
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                    });
                    setTimeout(function() {
                        $.ajax({
                            url: baseUrl + '/luxury/popups/destroy/' + id,
                            type: 'POST',
                            headers: {'X-Requested-With': 'XMLHttpRequest'},
                            dataType: 'json',
                        }).done(function(data, textStatus, jqXHR) {
                            if (data.code == 200){
                                Swal.fire("Proceso terminado!", data.message, "success");
                                $("#popup-" + id + " .label.bg-green").fadeOut().delay(400).queue(function(next) {
                                    $(this).removeClass('bg-green').addClass('bg-red').text('Desactivado').fadeIn();
                                    next();
                                })
                            } else {
                                Swal.fire("Lo sentimos", data.message, "error");
                            }
                        }).fail(function(jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR.responseText)
                            Swal.fire("Error", "Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.", "error");
                        });
                    }, 1000)
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire('Proceso cancelado', 'No se realizaron modificaciones en el sistema', 'info')
                }
            })
        })
    })
</script>
<?= $this->endSection() ?>
