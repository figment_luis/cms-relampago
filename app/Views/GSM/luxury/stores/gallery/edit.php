<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Tiendas Luxury Hall | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Luxury Hall Always in the Spotlight<br><small style="padding-left: 0;">Esta sección le permite editar la galería de tienda sobre el sitio web de Luxury Hall</small></h1>
<ol class="breadcrumb" >
    <li><a href="<?= base_url('/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?= base_url('/luxury/dashboard') ?>">Luxury Hall</a></li>
    <li><a href="<?= base_url('/luxury/stores') ?>">Tiendas</a></li>
    <li><a href="<?= base_url('/luxury/stores/edit/'. $store->id) ?>"><?= $store->name ?></a></li>
    <li class="active">Editar Galería</li>
</ol>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="box">
    <div class="box-container">
        <div class="box-header">
            <h3 class="box-title">Editar Galería</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="form-title">Titulo <span class="text-red">*</span>
                        <span class="help">Tip: La tienda que actualmente se encuentra en estado de edición</span></label>
                        <input type="text" name="title" id="form-title" class="form-control" value="<?= esc($store->name) ?>" placeholder="Nombre de tienda" autocomplete="off" autofocus required readonly disabled>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="form-imagesweb">Galería Web y App <span class="text-red">*</span>
                        <span class="help">Tip: Conjunto de imágenes que representan a esta tienda <strong>Formato JPG de 590x448</strong></span></label>
                        <input id="form-imagesweb" name="images_web[]" type="file" class="file" multiple data-preview-file-type="text" >
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<link rel="stylesheet" href="<?= base_url('assets/libs/adminLTE//bower_components/select2/dist/css/select2.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/css/fileinput.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/libs/adminLTE/bower_components/select2/dist/js/select2.full.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/js/fileinput.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/js/locales/es.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    let baseUrl = '<?= base_url() ?>'
    let storeId = '<?= $store->id ?>'

    // La información ya viene escapada desde el Backend
    let imagesPHPWeb = '<?= json_encode($galleryWeb) ?>'

    let imagesWeb = JSON.parse(imagesPHPWeb)

    let imagesPreviewWeb = []
    let imagesPreviewConfigWeb = []

    for (let image of imagesWeb) {
        imagesPreviewWeb.push('http://luxuryhall.com.mx/assets/img/store_galleries/' + storeId + '/' + image)
    }

    for (let image of imagesWeb) {
        imagesPreviewConfigWeb.push({
            "caption": "Imagen publicada en tienda ",
            "width": "70px",
            "key" : image,
            "url": baseUrl + "/luxury/galleryweb/destroy/" + storeId + "/" + image,
        });
    }
    
    let $elFileInputWeb = $("#form-imagesweb");

    $elFileInputWeb.fileinput({
        'showUpload': false,
        'showRemove': false,
        'required': true,
        'uploadAsync': true,
        'uploadUrl': baseUrl + "/luxury/galleryweb/update/" + storeId,
        maxImageWidth: 590,
        maxImageHeight: 448,
        minImageWidth: 590,
        minImageHeight: 448,
        maxFileSize: 500,
        'maxFileCount': 5,
        'language': 'es',
        'previewFileType': "image",
        'theme': "explorer",
        'allowedFileExtensions': ["jpg"],
        'validateInitialCount': true,
        'overwriteInitial': false,
        'initialPreviewAsData': true, // identify if you are sending preview data only and not the raw markup
        'initialPreviewFileType': 'image',
        'initialPreview': imagesPreviewWeb,
        'initialPreviewConfig': imagesPreviewConfigWeb,
        msgFileRequired: 'El archivo de imagen es requerido',
        msgSizeTooLarge: 'La archivo supera el tamaño permitido de <b>{maxSize} kilobytes.</b>',
        msgInvalidFileExtension: 'Extensión de archivo no válida, solo se admiten <b>{extensions}</b>',
        msgImageWidthSmall: 'El ancho de imagen debe ser de al menos <b>{size} px.</b>',
        msgImageHeightSmall: 'La altura de imagen debe ser de al menos <b>{size} px.</b>',
        msgImageWidthLarge: 'El ancho de imagen no debe exceder los <b>{size} px.</b>',
        msgImageHeightLarge: 'El alto de imagen no debe exceder los <b>{size} px.</b>',
    }).on("filebatchselected", function(event, files) {
        $elFileInputWeb.fileinput("upload");
    }).on('fileuploaderror', function(event, data, msg) {
        console.log(msg);
    });
</script>
<?= $this->endSection() ?>
