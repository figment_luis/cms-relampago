<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Luxury Hall | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Luxury Hall Always in the Spotlight<br><small style="padding-left: 0;">Panel de Control Administrativo</small></h1>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="dashboard dashboard--luxuryhall">
  <h3 class="dashboard__title">Luxury Hall<span class="dashboard__slogan">Grupo Sordo Madaleno</span></h3>
  <div class="dashboard__body">
      <ul class="dashboard__menu">
          <li class="dashboard__item dashboard__item--square">
              <a href="<?= route_to('luxury.news.index') ?>" class="dashboard__link dashboard__link--square">
                  <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                  Novedades
              </a>
              <p class="dashboard__description">Registre y gestione las novedades publicadas en sitio Web.</p>
          </li>
          <li class="dashboard__item dashboard__item--square">
              <a href="<?= route_to('luxury.promotions.index') ?>" class="dashboard__link dashboard__link--square">
                  <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                  Promociones
              </a>
              <p class="dashboard__description">Registre y gestione las promociones publicadas en sitio Web.</p>
          </li>
          <li class="dashboard__item dashboard__item--square">
              <a href="<?= route_to('luxury.slides.index') ?>" class="dashboard__link dashboard__link--square">
                  <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                  Slides
              </a>
              <p class="dashboard__description">Registre y gestione los slides publicados en sitio Web.</p>
          </li>
          <li class="dashboard__item dashboard__item--square">
              <a href="<?= route_to('luxury.popups.index') ?>" class="dashboard__link dashboard__link--square">
                  <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                  Popups
              </a>
              <p class="dashboard__description">Registre y gestione los popups publicados en sitio Web.</p>
          </li>
          <li class="dashboard__item dashboard__item--square">
              <a href="<?= route_to('luxury.stores.index') ?>" class="dashboard__link dashboard__link--square">
                  <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                  Tiendas
              </a>
              <p class="dashboard__description">Registre y gestione las tiendas publicadas en sitio Web.</p>
          </li>
          <li class="dashboard__item dashboard__item--square">
              <a href="<?= route_to('luxury.microsites.index') ?>" class="dashboard__link dashboard__link--square">
                  <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                  Micrositios
              </a>
              <p class="dashboard__description">Registre y gestione los micrositios publicados en sitio Web.</p>
          </li>
          <li class="dashboard__item dashboard__item--square">
              <a href="<?= route_to('luxury.trends.index') ?>" class="dashboard__link dashboard__link--square">
                  <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                  Temas del Mes
              </a>
              <p class="dashboard__description">Registre y gestione los temas del mes publicados en la App.</p>
          </li>
          <!--li class="dashboard__item dashboard__item--square">
              <a href="<?= route_to('luxury.news.index') ?>" class="dashboard__link dashboard__link--square">
                  <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                  Publicaciones
              </a>
              <p class="dashboard__description">Registre y gestione las publicaciones registradas en sitio Web y App.</p>
          </li>
          <li class="dashboard__item dashboard__item--square">
              <a href="<?= route_to('luxury.trends.index') ?>" class="dashboard__link dashboard__link--square">
                  <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                  Temas del Mes
              </a>
              <p class="dashboard__description">Registre y gestione los temas del mes publicados en la App.</p>
          </li>
          <li class="dashboard__item dashboard__item--square">
              <a href="<?= route_to('luxury.slides.index') ?>" class="dashboard__link dashboard__link--square">
                  <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                  Slides
              </a>
              <p class="dashboard__description">Registre y gestione los slides publicados en sitio Web.</p>
          </li>
          <li class="dashboard__item dashboard__item--square">
              <a href="<?= route_to('luxury.popups.index') ?>" class="dashboard__link dashboard__link--square">
                  <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                  Popups
              </a>
              <p class="dashboard__description">Registre y gestione los popups publicados en sitio Web.</p>
          </li>
          <li class="dashboard__item dashboard__item--square">
              <a href="<?= route_to('luxury.stores.index') ?>" class="dashboard__link dashboard__link--square">
                  <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                  Tiendas
              </a>
              <p class="dashboard__description">Registre y gestione las tiendas publicadas en sitio Web y App.</p>
          </li-->
      </ul>
  </div>
</div>
<?= $this->endSection() ?>
