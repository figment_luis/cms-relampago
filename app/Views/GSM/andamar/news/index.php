<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Novedades Andamar | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Andamar Lifestyle Center<br><small style="padding-left: 0;">Esta sección le permite gestionar las novedades registradas sobre el sitio web de Andamar</small></h1>
<ol class="breadcrumb" >
    <li><a href="<?= base_url('/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?= base_url('/andamar/dashboard') ?>">Andamar</a></li>
    <li class="active">Novedades Publicadas</li>
</ol>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="box box-danger">
    <div class="box-container">
        <div class="box-header">
            <h3 class="box-title">Novedades Publicadas</h3>
            <a href="<?= route_to('andamar.news.create') ?>" class="btn btn-secondary"><i class="fa fa-plus-circle icon"></i> Registrar novedad</a>
        </div>
        <div class="box-body">
            <table id="tableNews" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th width="60">Portada</th>
                        <th width="100">Titulo</th>
                        <th>Descripción</th>
                        <th width="80" class="text-center">Fecha</th>
                        <th width="130" class="text-center">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($news as $index => $new): ?>
                        <tr id="new-<?= $new->id ?>">
                            <td><img src="https://andamar.com.mx/<?= esc($new->img) ?>" alt="Portada" title="Portada <?= esc($new->title) ?>" class="img-responsive" width="80"></td>
                            <td style="vertical-align: middle;"><?= esc($new->title) ?></td>
                            <td style="vertical-align: middle;"><?= esc($new->content) ?></td>
                            <td style="vertical-align: middle;" class="text-center"><?= ucfirst(strftime('%B %Y', strtotime($new->created_at))) ?></td>
                            <td style="vertical-align: middle;" class="text-center">
                                <a href="<?= route_to('andamar.news.edit', $new->id) ?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Editar</a>
                                <button class="btn btn-sm btn-danger btnRemoveNew" data-id="<?= $new->id ?>"><i class="fa fa-trash"></i> Eliminar</button>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
                <tfoot>

                </tfoot>
            </table>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('assets/libs/adminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<!-- DataTables -->
<script src="<?= base_url('assets/libs/adminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/adminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    $(document).ready(function() {
        let baseUrl = '<?= base_url() ?>'

        $('#tableNews').DataTable({
            'pageLength': 4,
            "lengthMenu": [[4, 8, 25, 50, -1], [4, 8, 25, 50, "Todos"]],
            language: {
                url: '<?= base_url('assets/libs/spanish-datatables.json') ?>'
            }
        });

        $("#tableNews").on("click", ".btnRemoveNew", function(e) {
            e.preventDefault();
            e.stopPropagation();
            let id = $(this).data('id');
            Swal.fire({
                title: '¿Desea eliminar esta novedad?',
                text: "Usted esta a un paso de eliminar un registro en el sistema",
                icon: 'warning',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showCancelButton: true,
                confirmButtonText: 'Si, deseo continuar',
                cancelButtonText: 'No, cancelar',
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire({
                        title: "Procesando solicitud",
                        text: "Espere un momento por favor.",
                        imageUrl: baseUrl + "/assets/images/loader.gif",
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                    });
                    setTimeout(function() {
                        $.ajax({
                            url: baseUrl + '/andamar/news/destroy',
                            type: 'POST',
                            data: {id},
                            headers: {'X-Requested-With': 'XMLHttpRequest'},
                            dataType: 'json',
                        }).done(function(data, textStatus, jqXHR) {
                            if (data.code == 200){
                                Swal.fire("Proceso terminado!", data.message, "success");
                                $("#new-" + id).fadeOut(200);
                            } else {
                                Swal.fire("Lo sentimos", data.message, "error");
                            }
                        }).fail(function(jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR.responseText)
                            Swal.fire("Error", "Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.", "error");
                        });
                    }, 1000);
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire('Proceso cancelado', 'No se realizaron modificaciones en el sistema', 'info')
                }
            })
        })
    })
</script>
<?= $this->endSection() ?>
