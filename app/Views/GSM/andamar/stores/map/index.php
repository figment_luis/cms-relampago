<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Tiendas Luxury Hall | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Luxury Hall LifeStyle Center<br><small style="padding-left: 0;">Esta sección le permite posicionar una tienda  registradas sobre el sitio web de Luxury Hall</small></h1>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="box">
    <div class="box-container">
        <div class="box-header">
            <h3 class="box-title">Tienda Seleccionada</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="form-name">Nombre
                        <span class="help">Tip: Zara Home</span></label>
                        <input type="text" name="name" id="form-name" class="form-control" value="<?= esc($store->name) ?>" placeholder="Nombre de tienda" disabled readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="form-location">Ubicación
                        <span class="help">Tip: Piso(s) dónde se localiza esta tienda</span></label>
                        <input type="text" name="location" id="form-location" class="form-control" value="<?= esc($locations[$store->location]) ?>" placeholder="Ubicación en Centro Comercial" disabled readonly>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="form-url">Sitio Web
                        <span class="help">Tip: Sitio Web oficial de esta tienda</span></label>
                        <input type="url" name="url" id="form-url" class="form-control" value="<?= esc($store->url) ?>" placeholder="Sitio web oficial de tienda" disabled readonly>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 pt-5 mb-5">
                    <div class="map-container">
                        <div id="mapplic"></div> <!-- Map -->
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <a href="#" id="btnGuardarUbicacionMapa" class="btn btn-default pull-right"><span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> Guardar</a>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('assets/libs/mapplic/mapplic.css') ?>">
<style>
    /*#mapa-container {
        position: relative;
        margin: 1.5rem auto 0;
    }
    #mapa {
        display: block;
    }
    #currentPin {
        animation: mymove .2s infinite alternate;
    }
    #currentPin, #newPin {
        position: absolute;
        background-color: yellow;
        box-shadow: 0 0 10px 2px white;
        border-radius: 50%;
        width: 8px;
        height: 8px;
        cursor: pointer;
        display: none;
    }
    #newPin {
        display: none;
    }
    @keyframes mymove {
        from {background-color: yellow;}
        to {background-color: red;}
    }
    @keyframes mymovenew {
        from {background-color: yellow;}
        to {background-color: orange;}
    }*/
    .mapplic-levels {
        display: none;
    }
</style>
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>

<script src="<?= base_url('assets/libs/mapplic/jquery.mousewheel.js') ?>"></script>
<script src="<?= base_url('assets/libs/mapplic/mapplic.js') ?>"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    let baseUrl = '<?= base_url() ?>'
    var css = '.mapplic-filtered svg [id^=landmark] > * {opacity: 1 !important; }';
    /*$('#mainMap').mapplic({   //Initializate the Map
        source: baseUrl + '/assets/mapas/andamar/map.json',
        height: 'auto',
        minimap: false,
        sidebar: true,
        hovertip: true,
        developer: false,
        maxscale: 3
    });*/


    let mapplic = $('#mapplic').mapplic({
        source: baseUrl + '/assets/mapas/andamar/original.json',
        customcss: css,
        //height: 'auto',
        sidebar: false,
        height: 450,
        search: false,
        searchdescription: true,
        searcheverywhere: true,
        minimap: false,
        marker: 'hidden',
        fillcolor: true,
        fullscreen: false,
        developer: true,
        thumbholder: true,
        maxscale: 3,
        hovertip: true,
    });
    if(true) {

        //Se agregaron dos lineas de codigo a Mapplic linea 1987 y 1988 para realizar esta funcion
        setTimeout(() => {
            /*$('.map-nav a').removeClass('active');
            $('.map-nav a').each(function () {
                if ($(this).attr('select-opt') == $('.map-title').attr('floor')) {
                    $(this).addClass('active');
                }
            });*/

            let mapZoom = document.querySelector('button.mapplic-zoomin-button');
            mapZoom.click();
            //mapZoom.click();
        }, 500);

        let target = document.querySelector('h2.map-title');

        /*let observer = new MutationObserver((mutationList) => {
            mutationList.forEach((mutation) => {
                if (mutation.type === 'attributes') {
                    $('.map-nav a').removeClass('active');
                    $('.map-nav a').each(function () {
                        if ($(this).attr('select-opt') == $('.map-title').attr('floor')) {
                            $(this).addClass('active');
                        }
                    });
                }
            });
        });
        var config = {attributes: true, childList: true, characterData: true};
        observer.observe(target, config);*/

        /*let selection = $(this).attr('select-opt');
        let self = mapplic.data('mapplic');
        self.hideLocation();
        self.switchLevel(selection);*/

        //Change Select Menu of Mapplic Map
       /* $('.map-nav a').click(function (e) {
            e.preventDefault();

            $('.map-nav a').removeClass('active');
            $(this).addClass('active');


            let selection = $(this).attr('select-opt');
            let self = mapplic.data('mapplic');
            self.hideLocation();
            self.switchLevel(selection);
        });*/
    }
    /*$(document).ready(function() {
        let baseUrl = '<?= base_url() ?>'
        let storeID = '<?= $store->id ?>'
        let storeName = '<?= $store->name ?>'
        let location = '<?= $store->location ?>'

        let $btnGuardarUbicacionMapa = $("#btnGuardarUbicacionMapa")
        
        let currentX = ''
        let currentY = ''
        
        let positionX
        let positionY
        // Listado de mapas disponibles en centro comercial
        const maps = ["https://luxuryhall.com.mx/assets/img/planta_baja.png", "https://luxuryhall.com.mx/assets/img/planta_alta.png"]
        // Tamaño de imagen para cada uno de los mapas
        const sizesMap = [[768, 493], [777, 487]];
        // Nombre de cada conjunto de áreas de mapa
        const namesMap = ["planta_baja_map", "planta_alta_map"]

        $("#mapa").attr({
            src: maps[location],
            usemap: "#" + namesMap[location]
        }).one("load", function() {
            // Las coordenadas en App se encuentran bajo una escala de 1:1 (regla de tres).
            // Sin embargo, en App se tiene un desfase (negativo) de 10px con respecto al eje Y
            $("#currentPin").css({
                left: (sizesMap[location][0] * currentX) - 0,
                top: (sizesMap[location][1] * currentY) + 10,
                display: 'block',
                transform: 'translate(-50%, -50%)',
                
            }).parent().css({
                width: sizesMap[location][0],
                height: sizesMap[location][1]
            })
        });

        // Leer el contenido del archivo de mapa descargado previamente por el controlador de esta vista
        $.get(`${baseUrl}/assets/temp/luxuryMap.php`, function( data ) {
            // Convertir la data en un array de nodos DOM (No JQuery)
            const parsed = $.parseHTML(data);
            // Localizar las áreas de mapa asociadas con el mapa descargado
            mapAreas = $(parsed).filter(`map[name=${namesMap[location]}]`)
            $(".box-body").append(mapAreas);
        }, 'text');

        // Establecer las nuevas coordenadas y área para posicionar la tienda en Mapa (Directorio)
        $(".box-body").on("click", "area.lxmap", function(event) {
            event.preventDefault();
            const rect = document.querySelector("#mapa").getBoundingClientRect()
            const newX = event.clientX - rect.left
            const newY = event.clientY - rect.top

            // La posición en Y (vertical) se encuentra desfasada 10px en App.
            positionX = (newX + 0)  / sizesMap[location][0];
            positionY = (newY - 10) / sizesMap[location][1];
            selectedArea = $(this);

            $("#newPin").css({
                left: newX,
                top: newY,
                display: 'block',
                transform: 'translate(-50%, -50%)',
            });
            $btnGuardarUbicacionMapa.attr('disabled', false).css('pointer-events', 'auto');
        })

        // Solo se permite guardar la ubicación en mapa si se ha seleccionado una nueva ubicación
        $btnGuardarUbicacionMapa.attr('disabled', true).css('pointer-events', 'none');

        $btnGuardarUbicacionMapa.click(function(e) {
            e.preventDefault();
            e.stopPropagation();

            /* Enviar al server: 
                App: Posición (x,y), 
                Web: Referencias del área de mapa seleccionada (id, coords) 
                     y el nuevo nombre de tienda que se proyectará en dicha área
            -----

            let formData = new FormData();
            formData.append('pos_x', positionX);
            formData.append('pos_y', positionY);
            formData.append('old_store', selectedArea.attr('id'))
            formData.append('new_store', storeName)
            formData.append('coords', selectedArea.attr('coords'))

            Swal.fire({
                title: '¿Desea guardar la nueva ubicación en Mapa?',
                text: "Usted esta a un paso de realizar una actualización en el sistema",
                icon: 'warning',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showCancelButton: true,
                confirmButtonText: 'Si, deseo continuar',
                cancelButtonText: 'No, cancelar',
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire({
                        title: "Procesando solicitud",
                        text: "Espere un momento por favor.",
                        imageUrl: baseUrl + "/assets/images/loader.gif",
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                    });
                    setTimeout(function() {
                        $.ajax({
                            url: `${baseUrl}/luxury/stores/map/update/${storeID}`,
                            type: 'POST',
                            data: formData,
                            contentType: false,
                            processData: false,
                            headers: {'X-Requested-With': 'XMLHttpRequest'},
                            dataType: 'json',
                        }).done(function(data, textStatus, jqXHR) {
                            if (data.code == 200){
                                Swal.fire("Proceso terminado!", data.message, "success");
                                // Volver a desactivar el botón de guardar
                                $btnGuardarUbicacionMapa.attr('disabled', true).css('pointer-events', 'none');
                                $("#currentPin").hide();
                                $("#newPin").css({
                                    animation: 'mymovenew .2s infinite alternate'
                                });
                                /*  Volver a cargar áreas de mapa actualizadas. 
                                    De no hacer esto el usuario puede seguir trabajando en esta vista, pero, al tratar de guardar, los datos (map-areas) estarían desfazados.
                                ---
                               /* $.get(`${baseUrl}/assets/temp/luxuryMap.php`, function( data ) {
                                    const parsed = $.parseHTML(data);
                                    result = $(parsed).filter(`map[name=${namesMap[location]}]`)
                                    // Remover el conjunto de áreas de mapa anteriores y colocar los recientes
                                    $(".box-body").children().last().remove();
                                    $(".box-body").append(result);
                                }, 'html');
                            } else {
                                Swal.fire("Lo sentimos", data.message, "error");
                            }
                        }).fail(function(jqXHR, textStatus, errorThrown) {
                            Swal.fire("Error", "Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.", "error");
                        });
                    }, 1000)
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire('Proceso cancelado', 'No se realizaron modificaciones en el sistema', 'info')
                }
            })
        })

        
    })*/
</script>
<?= $this->endSection() ?>
