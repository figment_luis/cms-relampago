<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Boletines The Harbor Mérida | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Boletines - The Harbor Mérida<br><small style="padding-left: 0;">Esta sección le permite editar un boletín sobre el sitio Web de The Harbor Mérida</small></h1>
<ol class="breadcrumb" >
    <li><a href="<?= base_url('/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?= base_url('/harbor/dashboard') ?>">The Harbor Mérida</a></li>
    <li><a href="<?= base_url('/harbor/newsletters') ?>">Boletines</a></li>
    <li class="active">Editar Boletín</li>
</ol>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="box">
    <form action="<?= route_to('harbor.newsletters.update', $newsletter->id) ?>" method="post" novalidate id="formNewsletterEdit" enctype="multipart/form-data">
        <div class="box-header">
            <h3 class="box-title">Formulario de Edición - Boletín The Harbor Mérida</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="form-title">Titulo <span class="text-red">*</span>
                        <span class="help">Tip: Las mejores ofertas sólo en el BuenFin</span></label></label>
                        <input type="text" name="title" id="form-title" class="form-control" placeholder="Titulo principal del boletín" value="<?= esc($newsletter->title) ?>" required>
                    </div>
                    <div class="form-group">
                        <label for="form-date">Fecha de publicación
                        <span class="help">Tip: Mayo <?= date('Y') ?></span></label>
                        <input type="text" name="date" id="form-date" class="form-control" placeholder="Fecha de publicación del boletín" value="<?= esc($newsletter->date) ?>" readonly disabled>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="form-link">Link <span class="text-red">*</span>
                        <span class="help">Tip: URL que apunta al recurso de boletín</span></label>
                        <input type="url" id="form-link" name="link" class="form-control" placeholder="URL asociada al boletín" value="<?= esc($newsletter->url) ?>">
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <p style="float: left; margin:8px 0 0 0;"><span class="text-red" style="font-weight: bold;">*</span> Campos con información obligatoria</p>
            <button type="submit" class="btn btn-default pull-right"><span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> Actualizar</button>
        </div>
    </form>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/css/fileinput.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/libs/fileinput/js/fileinput.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/js/locales/es.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    let baseUrl = '<?= base_url() ?>'

    $(document).ready(function() {
        $("#formNewsletterEdit").submit(function(e) {
            e.preventDefault();
            e.stopPropagation();

            // Detener el proceso si existe uno o más errores con los archivos de imagen
            if ($(".file-input.theme-explorer").hasClass("has-error")) return false;

            let formData = new FormData($(this)[0]);
            Swal.fire({
                title: '¿Desea actualizar este boletín?',
                text: "Usted esta a un paso de realizar una actualización en el sistema",
                icon: 'warning',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showCancelButton: true,
                confirmButtonText: 'Si, deseo continuar',
                cancelButtonText: 'No, cancelar',
            }).then((result) => {
                if (result.isConfirmed) {
                    $(".message-error").hide();
                    $(".input-error").removeClass('input-error')

                    Swal.fire({
                        title: "Procesando solicitud",
                        text: "Espere un momento por favor.",
                        imageUrl: baseUrl + "/assets/images/loader.gif",
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                    });
                    setTimeout(function() {
                        $.ajax({
                            url: baseUrl + '/harbor/newsletters/update/' + '<?= $newsletter->id ?>',
                            type: 'POST',
                            data: formData,
                            contentType: false,
                            processData: false,
                            headers: {'X-Requested-With': 'XMLHttpRequest'},
                            dataType: 'json',
                        }).done(function(data, textStatus, jqXHR) {
                            if (data.code == 200) {
                                // Proceso exitoso
                                Swal.fire("Proceso terminado", data.message, "success");
                            } else if (data.code == 400 || data.code == 500) {
                                // Error
                                Swal.fire("Lo sentimos", data.message, "error");
                            } else {
                                // Problemas de Validación
                                Swal.fire("Error de validación", data.message, "error");
                                $.each(data.validation, function(key, value) {
                                    $(`*[name='${key}']`).addClass('input-error').parent().append(`<span class="message-error"><i class="fa fa-exclamation-circle"></i> ${value}</span>`)
                                })
                            }
                        }).fail(function(jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR.responseText)
                            Swal.fire("Error", "Se detectó un error crítico en el sistema, favor de contactar al área de IT.", "error");
                        });
                    }, 1000);
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire('Proceso cancelado', 'No se realizaron modificaciones en el sistema', 'info');
                }
            })
        })
    })
</script>
<?= $this->endSection() ?>
