<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Micrositios Calle Corazón | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Calle Corazón Lifestyle Center<br><small style="padding-left: 0;">Esta sección le permite gestionar los micrositios registradas sobre el sitio web de Calle Corazón</small></h1>
<ol class="breadcrumb" >
    <li><a href="<?= base_url('/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?= base_url('/corazon/dashboard') ?>">Calle Corazón</a></li>
    <li class="active">Micrositios Publicados</li>
</ol>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="box box-danger">
    <div class="box-container">
        <div class="box-header">
            <h3 class="box-title">Micrositios Publicados</h3>
            <a href="<?= route_to('corazon.microsites.create') ?>" class="btn btn-secondary"><i class="fa fa-plus-circle icon"></i> Registrar micrositio</a>
        </div>
        <div class="box-body">
            <table id="tableMicrosites" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th width="100">Titulo</th>
                        <th>URL</th>
                        <th class="text-center">Fecha de inicio</th>
                        <th class="text-center">Fecha de cierre</th>
                        <th class="text-center">Disponibilidad</th>
                        <th width="130" class="text-center">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($microsites as $index => $microsite): ?>
                        <tr id="microsite-<?= $microsite->id ?>">
                            <td style="vertical-align: middle;"><?= esc($microsite->title) ?></td>
                            <td style="vertical-align: middle;"><a href="https://callecorazon.com/<?= esc($microsite->slug) ?>" target="_blank">https://callecorazon.com/<?= esc($microsite->slug) ?></a></td>
                            <td class="text-center" style="vertical-align: middle;"><?= esc($microsite->start_date) ?></td>
                            <td class="text-center" style="vertical-align: middle;"><?= esc($microsite->end_date) ?></td>
                            <td class="text-center" style="vertical-align: middle;"><?= date('U', strtotime(esc($microsite->end_date))) > time() && $microsite->enabled ? '<small class="label bg-green">Activado</small>' : '<small class="label bg-red">Desactivado</small>' ?></td>
                            <td style="vertical-align: middle;" class="text-center microsite-actions">
                                <?php if($microsite->enabled): ?>
                                    <a href="<?= route_to('corazon.microsites.edit', $microsite->id) ?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Editar</a>
                                    <button class="btn btn-sm btn-danger btnRemoveMicrosite" data-id="<?= $microsite->id ?>"><i class="fa fa-trash"></i> Eliminar</button>
                                <?php else: ?>
                                    <button class="btn btn-sm btn-success btnActivateMicrosite" style="width: 145px;" data-id="<?= $microsite->id ?>"><i class="fa fa-recycle"></i> Activar</button>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
                <tfoot>

                </tfoot>
            </table>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('assets/libs/adminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<!-- DataTables -->
<script src="<?= base_url('assets/libs/adminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/adminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    $(document).ready(function() {
        let baseUrl = '<?= base_url() ?>'

        $('#tableMicrosites').DataTable({
            'pageLength': 4,
            "lengthMenu": [[4, 8, 25, 50, -1], [4, 8, 25, 50, "Todos"]],
            language: {
                url: '<?= base_url('assets/libs/spanish-datatables.json') ?>'
            }
        });

        $("#tableMicrosites").on("click", ".btnRemoveMicrosite", function(e) {
            e.preventDefault();
            e.stopPropagation();
            let id = $(this).data('id');
            Swal.fire({
                title: '¿Desea eliminar este micrositio?',
                text: "Usted esta a un paso de eliminar un registro en el sistema",
                icon: 'warning',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showCancelButton: true,
                confirmButtonText: 'Si, deseo continuar',
                cancelButtonText: 'No, cancelar',
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire({
                        title: "Procesando solicitud",
                        text: "Espere un momento por favor.",
                        imageUrl: baseUrl + "/assets/images/loader.gif",
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                    });
                    setTimeout(function() {
                        $.ajax({
                            url: baseUrl + '/corazon/microsites/destroy',
                            type: 'POST',
                            data: {id},
                            headers: {'X-Requested-With': 'XMLHttpRequest'},
                            dataType: 'json',
                        }).done(function(data, textStatus, jqXHR) {
                            if (data.code == 200){
                                Swal.fire("Proceso terminado!", data.message, "success");
                                $(`#microsite-${id} .microsite-actions`).html(`
                                    <button class="btn btn-sm btn-success btnActivateMicrosite" style="width: 145px;" data-id="${id}"><i class="fa fa-recycle"></i> Activar</button>
                                `)
                               //$("#microsite-" + id).fadeOut(200);
                            } else {
                                Swal.fire("Lo sentimos", data.message, "error");
                            }
                        }).fail(function(jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR.responseText)
                            Swal.fire("Error", "Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.", "error");
                        });
                    }, 1000);
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire('Proceso cancelado', 'No se realizaron modificaciones en el sistema', 'info')
                }
            })
        })

        $("#tableMicrosites").on("click", ".btnActivateMicrosite", function(e) {
            e.preventDefault();
            e.stopPropagation();
            let id = $(this).data('id');
            Swal.fire({
                title: '¿Desea activar este micrositio?',
                text: "Usted esta a un paso de modificar un registro en el sistema",
                icon: 'warning',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showCancelButton: true,
                confirmButtonText: 'Si, deseo continuar',
                cancelButtonText: 'No, cancelar',
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire({
                        title: "Procesando solicitud",
                        text: "Espere un momento por favor.",
                        imageUrl: baseUrl + "/assets/images/loader.gif",
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                    });
                    setTimeout(function() {
                        $.ajax({
                            url: baseUrl + '/corazon/microsites/activate/' + id,
                            type: 'POST',
                            headers: {'X-Requested-With': 'XMLHttpRequest'},
                            dataType: 'json',
                        }).done(function(data, textStatus, jqXHR) {
                            if (data.code == 200){
                                Swal.fire("Proceso terminado!", data.message, "success");
                                //$("#store-" + id).fadeOut(200);
                                $(`#microsite-${id} .microsite-actions`).html(`
                                    <a href="${baseUrl}/corazon/microsites/edit/${id}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Editar</a>
                                    <button class="btn btn-sm btn-danger btnRemoveMicrosite" data-id="${id}"><i class="fa fa-trash"></i> Eliminar</button>
                                `)
                            } else {
                                Swal.fire("Lo sentimos", data.message, "error");
                            }
                        }).fail(function(jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR.responseText)
                            Swal.fire("Error", "Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.", "error");
                        });
                    }, 1000);
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire('Proceso cancelado', 'No se realizaron modificaciones en el sistema', 'info')
                }
            })
        })
    })
</script>
<?= $this->endSection() ?>
