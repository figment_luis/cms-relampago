<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Tiendas Altavista 147 | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Tiendas - Altavista 147<br><small style="padding-left: 0;">Esta sección le permite registrar una tienda sobre el sitio web de Altavista 147</small></h1>
<ol class="breadcrumb" >
    <li><a href="<?= base_url('/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?= base_url('/altavista/dashboard') ?>">Altavista 147</a></li>
    <li><a href="<?= base_url('/altavista/stores') ?>">Tiendas</a></li>
    <li class="active">Registrar Tienda</li>
</ol>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="box">
    <form action="<?= route_to('altavista.stores.store') ?>" method="post" novalidate id="formStoresCreate" enctype="multipart/form-data">
        <div class="box-header">
            <h3 class="box-title">Registrar Tienda</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="form-name">Nombre <span class="text-red">*</span>
                        <span class="help">Tip: Zara Home</span></label>
                        <input type="text" name="name" id="form-name" class="form-control" placeholder="Nombre de tienda" autocomplete="off" autofocus required>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="form-phone">Teléfono
                        <span class="help">Tip: Teléfono de 10 dígitos - (552) 1020300</span></label>
                        <input type="text" name="phone" id="form-phone" class="form-control" placeholder="Teléfono de atención a clientes" autocomplete="off" data-inputmask="'mask': '(999) 9999999'" data-mask>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="form-phone2">Teléfono Alternativo
                        <span class="help">Tip: Teléfono de 10 dígitos - (722) 1020300</span></label>
                        <input type="text" name="phone2" id="form-phone2" class="form-control" placeholder="Teléfono alternativo de atención a clientes" autocomplete="off" data-inputmask="'mask': '(999) 9999999'" data-mask>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="form-hourhand">Horarios <span class="text-red">*</span>
                        <span class="help">Tip: Lunes a Domingo de 10:00am. a 20:00hrs.</span></label>
                        <input type="text" name="hourhand" id="form-hourhand" class="form-control" placeholder="Horarios de atención a clientes" autocomplete="off" required>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="form-url">Sitio Web
                        <span class="help">Tip: Sitio Web oficial de esta tienda</span></label>
                        <input type="url" name="url" id="form-url" class="form-control" placeholder="Sitio web oficial de tienda" autocomplete="off">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="form-email">Email
                        <span class="help">Tip: Correo electrónico de contacto</span></label>
                        <input type="email" name="correo" id="form-email" class="form-control" placeholder="Correo electrónico de atención a clientes" autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="form-keywords">Etiquetas <span class="text-red">*</span>
                        <span class="help">Tip: ¿Qué comercializa esta tienda?</span></label>
                        <input type="text" name="keywords" id="form-keywords" class="form-control" placeholder="ropa, moda, tecnología, fragancias, comida" autocomplete="off" required>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="form-location">Ubicación en Plaza Comercial <span class="text-red">*</span>
                        <span class="help">Tip: Piso o nivel dónde se localiza esta tienda</span></label>
                        <select name="location" id="form-location" class="form-control" required>
                            <option value="" selected disabled>Seleccione una ubicación</option>
                            <?php foreach($locations as $index => $location): ?>
                                <option value="<?= esc($location->name) ?>"><?= esc($location->name) ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="form-category">Categoría <span class="text-red">*</span>
                        <span class="help">Tip: Categoría asociada a esta tienda</span></label>
                        <select name="category" id="form-category" class="form-control" required>
                            <option value="" selected disabled>Seleccione una categoría</option>
                            <?php foreach($categories as $index => $category): ?>
                                <option value="<?= $category->id ?>"><?= esc($category->name) ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="form-subcategories">Subcategorías <span class="text-red">*</span>
                        <span class="help">Tip: Subcategorías asociadas a esta tienda</span></label>
                        <select name="subcategories[]" multiple required id="form-subcategories" class="form-control select-subcategories" data-placeholder="Seleccione una o más subcategorías" style="width: 100%;">
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="form-description">Descripción <span class="text-red">*</span>
                        <span class="help">Tip: Contenido detallado acerca de esta tienda</span></label>
                        <textarea name="description" id="form-description" class="form-control" rows="11" placeholder="Descripción detallada de la tienda" autocomplete="off" required></textarea>
                    </div>
                    <div class="form-group">
                        <label>Disponible en Thor2Go
                        <span class="help">Tip: Esta tienda ofrece servicio de entregas a domicilio</span></label>
                        <br>
                        <div class="checkbox-group required">
                            <label style="margin-right: 1.5rem;">
                                <input type="checkbox" name="thor2go" class="flat-red">&nbsp; Aceptar
                            </label>  
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    
                    <div class="form-group">
                        <label for="form-qr">Código QR
                        <span class="help">Tip: Código QR asociado a esta tienda <strong>Formato PNG de 200x200</strong></span></label>
                        <input id="form-qr" name="qr" type="file" class="file" data-preview-file-type="text" >
                    </div>
                    <div class="form-group">
                        <label for="form-gallery">Galería <span class="text-red">*</span>
                        <span class="help">Tip: Conjunto de imágenes asociadas a esta tienda <strong>Formato JPG de 420x440</strong></span></label>
                        <input id="form-gallery" name="images[]" type="file" class="file" multiple data-preview-file-type="text" >
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <p style="float: left; margin:8px 0 0 0;"><span class="text-red" style="font-weight: bold;">*</span> Campos con información obligatoria</p>
            <button type="submit" class="btn btn-default pull-right"><span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> Registrar</button>
        </div>
    </form>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<link rel="stylesheet" href="<?= base_url('assets/libs/adminLTE//bower_components/select2/dist/css/select2.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/css/fileinput.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/libs/adminLTE/plugins/iCheck/all.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/libs/adminLTE/plugins/iCheck/icheck.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/adminLTE/bower_components/select2/dist/js/select2.full.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/adminLTE/plugins/input-mask/jquery.inputmask.js') ?>"></script>
<script src="<?= base_url('assets/libs/adminLTE/plugins/input-mask/jquery.inputmask.extensions.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/js/fileinput.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/js/locales/es.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    let baseUrl = '<?= base_url() ?>'

    $("#form-qr").fileinput({
        'showUpload': false,
        'showRemove': false,
        maxImageWidth: 200,
        maxImageHeight: 200,
        minImageWidth: 200,
        minImageHeight: 200,
        maxFileSize: 500,
        'maxFileCount': 1,
        'language': 'es',
        'previewFileType': "image",
        'theme': "explorer",
        'allowedFileExtensions': ["png"],
        'validateInitialCount': true,
        msgFileRequired: 'El archivo de imagen es requerido',
        msgSizeTooLarge: 'La archivo supera el tamaño permitido de <b>{maxSize} kilobytes.</b>',
        msgInvalidFileExtension: 'Extensión de archivo no válida, solo se admiten <b>{extensions}</b>',
        msgImageWidthSmall: 'El ancho de imagen debe ser de al menos <b>{size} px.</b>',
        msgImageHeightSmall: 'La altura de imagen debe ser de al menos <b>{size} px.</b>',
        msgImageWidthLarge: 'El ancho de imagen no debe exceder los <b>{size} px.</b>',
        msgImageHeightLarge: 'El alto de imagen no debe exceder los <b>{size} px.</b>'
    });

    $("#form-gallery").fileinput({
        'showUpload': false,
        'showRemove': false,
        'required': true,
        maxImageWidth: 420,
        maxImageHeight: 440,
        minImageWidth: 420,
        minImageHeight: 440,
        maxFileSize: 500,
        'maxFileCount': 3,
        'language': 'es',
        'previewFileType': "image",
        'theme': "explorer",
        'allowedFileExtensions': ["jpg"],
        'validateInitialCount': true,
        msgFileRequired: 'El archivo de imagen es requerido',
        msgSizeTooLarge: 'La archivo supera el tamaño permitido de <b>{maxSize} kilobytes.</b>',
        msgInvalidFileExtension: 'Extensión de archivo no válida, solo se admiten <b>{extensions}</b>',
        msgImageWidthSmall: 'El ancho de imagen debe ser de al menos <b>{size} px.</b>',
        msgImageHeightSmall: 'La altura de imagen debe ser de al menos <b>{size} px.</b>',
        msgImageWidthLarge: 'El ancho de imagen no debe exceder los <b>{size} px.</b>',
        msgImageHeightLarge: 'El alto de imagen no debe exceder los <b>{size} px.</b>'
    });

    $(document).ready(function() {
        $('input[type="checkbox"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass   : 'iradio_flat-green'
        });

        $(".select2").select2();
        $('.select-subcategories').select2()

        $("[data-mask]").inputmask();

        // Recuperar el listado de subcategorías asociadas con la categoría seleccionada
        let $selectSubcategories = $("#form-subcategories")
        $selectSubcategories.prop('disabled', true)
        $("#form-category").on('change', function(e) {
            $selectSubcategories.empty();
            $selectSubcategories.prop('disabled', true)
            $.ajax({
                url: baseUrl + '/altavista/stores/subcategories/' + this.value,
                type: 'GET',
                headers: {'X-Requested-With': 'XMLHttpRequest'},
                dataType: 'json',
            }).done(function(data, textStatus, jqXHR) {
                if (data.code == 200) {
                    $selectSubcategories.prop('disabled', false)
                    // Proceso exitoso: Llenar el control correspondiente al listado de subcategorías
                    $.each(data.result, function(key, val) {
                        $selectSubcategories.append(new Option(val.subcat_name, val.id))
                    })
                } else if (data.code == 400) {
                    // Se ha seleccionado la categoría Servicios. (Los servicios no tienen subcategorías)
                    Swal.fire("Aviso importante", data.message, "warning");
                    $selectSubcategories.append(new Option('Servicios', 100, true, true)).trigger('change')
                } else {
                    // Problemas en el servidor: Limpiar el control correspondiente al listado de subcategorías
                    Swal.fire("Error de validación", data.message, "error");
                }
            }).fail(function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText)
                Swal.fire("Error", "Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.", "error");
            });
        });

        $("#formStoresCreate").submit(function(e) {
            e.preventDefault();
            e.stopPropagation();

            // Detener el proceso si existe uno o más errores con los archivos de imagen
            if ($(".file-input.theme-explorer").hasClass("has-error")) return false;

            let formData = new FormData($(this)[0]);
            // Si el valor de la subcategoría es 100, debemos adjuntarla manualmente en el FormData (Problemas con Select2)
            if ($(".select-subcategories").val().includes('100')) {
                formData.append('subcategories[]', $(".select-subcategories").val())
            }

            Swal.fire({
                title: '¿Desea registrar esta tienda?',
                text: "Usted esta a un paso de realizar una actualización en el sistema",
                icon: 'warning',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showCancelButton: true,
                confirmButtonText: 'Si, deseo continuar',
                cancelButtonText: 'No, cancelar',
            }).then((result) => {
                if (result.isConfirmed) {
                    $(".message-error").hide();
                    $(".input-error").removeClass('input-error')

                    Swal.fire({
                        title: "Procesando solicitud",
                        text: "Espere un momento por favor.",
                        imageUrl: baseUrl + "/assets/images/loader.gif",
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                    });
                    setTimeout(function(){
                        $.ajax({
                            url: baseUrl + '/altavista/stores/store',
                            type: 'POST',
                            data: formData,
                            contentType: false,
                            processData: false,
                            headers: {'X-Requested-With': 'XMLHttpRequest'},
                            dataType: 'json',
                        }).done(function(data, textStatus, jqXHR) {
                            if (data.code == 200) {
                                // Proceso exitoso
                                Swal.fire("Proceso terminado", data.message, "success");
                                $("#formStoresCreate")[0].reset();
                                $(".select2").val(null).trigger("change");
                                $(".select-subcategories").val(null).trigger("change");
                                $('input[type="checkbox"].flat-red').iCheck('update');
                            } else if (data.code == 400) {
                                // Error
                                Swal.fire("Lo sentimos", data.message, "error");
                            } else {
                                // Problemas de Validación
                                Swal.fire("Error de validación", data.message, "error");
                                $.each(data.validation, function(key, value) {
                                    if (key == 'subcategories.*') {
                                        $(`*[name='subcategories[]']`).addClass('input-error').parent().append(`<span class="message-error"><i class="fa fa-exclamation-circle"></i> ${value}</span>`)
                                    }

                                    $(`*[name='${key}']`).addClass('input-error').parent().append(`<span class="message-error"><i class="fa fa-exclamation-circle"></i> ${value}</span>`)
                                })
                            }
                        }).fail(function(jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR.responseText)
                            Swal.fire("Error", "Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.", "error");
                        });
                    }, 1000);
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire('Proceso cancelado', 'No se realizaron modificaciones en el sistema', 'info');
                }
            })
        })
    })
</script>
<?= $this->endSection() ?>
