<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Marina Puerto Cancún | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Marina Puerto Cancún<br><small style="padding-left: 0;">Panel de Control Administrativo</small></h1>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="dashboard dashboard--marina">
  <h3 class="dashboard__title">Marina Puerto Cancún<span class="dashboard__slogan">Grupo Thor Urbana</span></h3>
  <div class="dashboard__body">
      <ul class="dashboard__menu">
          <li class="dashboard__item dashboard__item--square">
              <a href="<?= route_to('marina.news.index') ?>" class="dashboard__link dashboard__link--square">
                  <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                  Novedades
              </a>
              <p class="dashboard__description">Registre y gestione las novedades publicadas en sitio Web.</p>
          </li>
          <li class="dashboard__item dashboard__item--square">
              <a href="<?= route_to('marina.events.index') ?>" class="dashboard__link dashboard__link--square">
                  <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                  Eventos
              </a>
              <p class="dashboard__description">Registre y gestione los eventos publicadas en sitio Web.</p>
          </li>
          <li class="dashboard__item dashboard__item--square">
              <a href="<?= route_to('marina.popups.index') ?>" class="dashboard__link dashboard__link--square">
                  <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                  Popups
              </a>
              <p class="dashboard__description">Registre y gestione los popups publicados en sitio Web.</p>
          </li>
          <li class="dashboard__item dashboard__item--square">
              <a href="<?= route_to('marina.newsletters.index') ?>" class="dashboard__link dashboard__link--square">
                  <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                  Boletines
              </a>
              <p class="dashboard__description">Registre y gestione los boletines publicados en sitio Web.</p>
          </li>
          <li class="dashboard__item dashboard__item--square">
              <a href="<?= route_to('marina.stores.index') ?>" class="dashboard__link dashboard__link--square">
                  <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                  Tiendas
              </a>
              <p class="dashboard__description">Registre y gestione las tiendas publicadas en sitio Web.</p>
          </li>
          <li class="dashboard__item dashboard__item--square">
              <a href="<?= route_to('marina.microsites.index') ?>" class="dashboard__link dashboard__link--square">
                  <i class="fa fa-arrow-circle-right dashboard__icon"></i>
                  Micrositios
              </a>
              <p class="dashboard__description">Registre y gestione los micrositios publicados en sitio Web.</p>
          </li>
      </ul>
  </div>
</div>
<?= $this->endSection() ?>
