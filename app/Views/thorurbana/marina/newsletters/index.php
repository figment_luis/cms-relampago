<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Boletines Marina Puerto Cancún | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Boletines - Marina Puerto Cancún<br><small style="padding-left: 0;">Esta sección le permite gestionar los boletines registrados sobre el sitio Web de Marina Puerto Cancún</small></h1>
<ol class="breadcrumb" >
    <li><a href="<?= base_url('/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?= base_url('/marina/dashboard') ?>">Marina Puerto Cancún</a></li>
    <li class="active">Boletines Publicados</li>
</ol>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="box box-danger">
    <div class="box-container">
        <div class="box-header">
            <h3 class="box-title">Boletines Publicados</h3>
            <a href="<?= route_to('marina.newsletters.create') ?>" class="btn btn-secondary"><i class="fa fa-plus-circle icon"></i> Registrar boletín</a>
        </div>
        <div class="box-body">
            <table id="tableNewsletter" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th width="100">Titulo</th>
                        <th>URL</th>
                        <th>Disponibilidad</th>
                        <th width="120">Mes de Publicación</th>
                        <th width="130" class="text-center">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($newsletters as $index => $newsletter): ?>
                        <tr id="nl-<?= $newsletter->id ?>">
                            <td style="vertical-align: middle;"><?= esc($newsletter->title) ?></td>
                            <td style="vertical-align: middle;"><a href="<?= esc($newsletter->url) ?>" title="<?= esc($newsletter->title) ?>" target="_blank"><?= esc($newsletter->url) ?></a></td>
                            <td class="text-center" style="vertical-align: middle;"><?= esc($newsletter->enabled) ? '<small class="label bg-green">Activado</small>' : '<small class="label bg-red">Desactivado</small>' ?></td>
                            <td style="vertical-align: middle;" class="text-center"><?= esc($newsletter->date) ?></td>
                            <td style="vertical-align: middle;" class="text-center">
                                <?php if(esc($newsletter->enabled)): ?>
                                    <a href="<?= route_to('marina.newsletters.edit', $newsletter->id) ?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Editar</a>
                                    <button class="btn btn-sm btn-danger btnRemoveNewletter" data-id="<?= $newsletter->id ?>"><i class="fa fa-trash"></i> Eliminar</button>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
                <tfoot>

                </tfoot>
            </table>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('assets/libs/adminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<!-- DataTables -->
<script src="<?= base_url('assets/libs/adminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/adminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    $(document).ready(function() {
        let baseUrl = '<?= base_url() ?>'

        $('#tableNewsletter').DataTable({
            'pageLength': 8,
            "lengthMenu": [[8, 15, 25, 50, -1], [8, 15, 25, 50, "Todos"]],
            "order": [],
            language: {
                url: '<?= base_url('assets/libs/spanish-datatables.json') ?>'
            }
        });

        $("#tableNewsletter").on("click", ".btnRemoveNewletter", function(e) {
            e.preventDefault();
            e.stopPropagation();
            let id = $(this).data('id');
            Swal.fire({
                title: '¿Desea eliminar este boletín?',
                text: "Usted esta a un paso de eliminar un registro en el sistema",
                icon: 'warning',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showCancelButton: true,
                confirmButtonText: 'Si, deseo continuar',
                cancelButtonText: 'No, cancelar',
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire({
                        title: "Procesando solicitud",
                        text: "Espere un momento por favor.",
                        imageUrl: baseUrl + "/assets/images/loader.gif",
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                    });
                    setTimeout(function() {
                        $.ajax({
                            url: baseUrl + '/marina/newsletters/destroy',
                            type: 'POST',
                            data: {id},
                            headers: {'X-Requested-With': 'XMLHttpRequest'},
                            dataType: 'json',
                        }).done(function(data, textStatus, jqXHR) {
                            if (data.code == 200){
                                Swal.fire("Proceso terminado!", data.message, "success");
                                $("#nl-" + id + " .label.bg-green").fadeOut().delay(400).queue(function(next) {
                                    $(this).removeClass('bg-green').addClass('bg-red').text('Desactivado').fadeIn();
                                    next();
                                })
                            } else {
                                Swal.fire("Lo sentimos", data.message, "error");
                            }
                        }).fail(function(jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR.responseText)
                            Swal.fire("Error", "Se detectó un error crítico en el sistema, favor de contactar al área de IT.", "error");
                        });
                    }, 1000);
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire('Proceso cancelado', 'No se realizaron modificaciones en el sistema', 'info')
                }
            })
        })
    })
</script>
<?= $this->endSection() ?>
