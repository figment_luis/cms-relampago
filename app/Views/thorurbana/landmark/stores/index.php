<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Tiendas Landmark Guadalajara | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Tiendas - Landmark Guadalajara<br><small style="padding-left: 0;">Esta sección le permite gestionar las tiendas registradas sobre el sitio web de Landmark Guadalajara</small></h1>
<ol class="breadcrumb" >
    <li><a href="<?= base_url('/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?= base_url('/landmark/dashboard') ?>">Landmark Guadalajara</a></li>
    <li class="active">Tiendas Publicadas</li>
</ol>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="box">
    <div class="box-container">
        <div class="box-header">
            <h3 class="box-title">Tiendas Publicadas</h3>
            <a href="<?= route_to('landmark.stores.create') ?>" class="btn btn-secondary"><i class="fa fa-plus-circle icon"></i> Registrar tienda</a>
        </div>
        <div class="box-body">
            <table id="tableStores" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th width="150">Tienda</th>
                        <th width="120" >Ubicación en Plaza</th>
                        <th>Descripción</th>
                        <th width="150" class="text-center">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($stores as $index => $store): ?>
                        <tr id="store-<?= $store->id ?>">
                            <td style="vertical-align: middle;"><?= esc($store->name) ?></td>
                            <td style="vertical-align: middle;">
                                <i class="fa fa-map-pin bg-1"></i> <?= esc($store->map_location) ?>
                            </td>
                            <td style="vertical-align: middle;"><?= esc(word_limiter($store->description, 30, '...')) ?></td>
                            <td style="vertical-align: middle;" class="text-center">
                                <a href="<?= route_to('landmark.stores.edit', $store->id) ?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Editar</a>
                                <button class="btn btn-sm btn-danger btnRemoveStore" data-id="<?= $store->id ?>"><i class="fa fa-trash"></i> Eliminar</button>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
                <tfoot>

                </tfoot>
            </table>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('assets/libs/adminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<!-- DataTables -->
<script src="<?= base_url('assets/libs/adminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/adminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    $(document).ready(function() {
        let baseUrl = '<?= base_url() ?>'

        $('#tableStores').DataTable({
            'pageLength': 5,
            "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "Todos"]],
            language: {
                url: '<?= base_url('assets/libs/spanish-datatables.json') ?>'
            }
        });

        $("#tableStores").on("click", ".btnRemoveStore", function(e) {
            e.preventDefault();
            e.stopPropagation();
            let id = $(this).data('id');
            Swal.fire({
                title: '¿Desea eliminar esta tienda?',
                text: "Usted esta a un paso de eliminar un registro en el sistema",
                icon: 'warning',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showCancelButton: true,
                confirmButtonText: 'Si, deseo continuar',
                cancelButtonText: 'No, cancelar',
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire({
                        title: "Procesando solicitud",
                        text: "Espere un momento por favor.",
                        imageUrl: baseUrl + "/assets/images/loader.gif",
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                    });
                    setTimeout(function() {
                        $.ajax({
                            url: baseUrl + '/landmark/stores/destroy/' + id,
                            type: 'POST',
                            headers: {'X-Requested-With': 'XMLHttpRequest'},
                            dataType: 'json',
                        }).done(function(data, textStatus, jqXHR) {
                            if (data.code == 200){
                                Swal.fire("Proceso terminado!", data.message, "success");
                                $("#store-" + id).fadeOut(200);
                            } else {
                                Swal.fire("Lo sentimos", data.message, "error");
                            }
                        }).fail(function(jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR.responseText)
                            Swal.fire("Error", "Se detectó un error crítico en el sistema, favor de contactar al área de Soporte Técnico.", "error");
                        });
                    }, 1000)
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire('Proceso cancelado', 'No se realizaron modificaciones en el sistema', 'info')
                }
            })
        })
    })
</script>
<?= $this->endSection() ?>
