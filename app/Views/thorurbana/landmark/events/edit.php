<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Eventos Landmark Guadalajara | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Eventos - Landmark Guadalajara<br><small style="padding-left: 0;">Esta sección le permite editar un evento sobre el sitio web de Landmark Guadalajara</small></h1>
<ol class="breadcrumb" >
    <li><a href="<?= base_url('/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?= base_url('/landmark/dashboard') ?>">Landmark Guadalajara</a></li>
    <li><a href="<?= base_url('/landmark/events') ?>">Eventos</a></li>
    <li class="active">Editar Evento</li>
</ol>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="box">
    <form action="<?= route_to('landmark.events.update', $event->id) ?>" method="post" novalidate id="formEventEdit" enctype="multipart/form-data">
        <div class="box-header">
            <h3 class="box-title">Formulario de Edición - Eventos Landmark Guadalajara</h3>
            <a href="<?= route_to('landmark.events.gallery.edit', $event->id) ?>" class="btn btn-secondary btn-secondary--gallery"><i class="fa fa-plus-circle icon"></i> Editar galería</a>
        </div>
        <div class="box-body">
            <div class="row">
                
                <div class="col-md-6">
                <div class="form-group">
                        <label for="form-title">Titulo <span class="text-red">*</span>
                        <span class="help">Tip: ¡Muestra tu actitud Town Square!</span></label></label>
                        <input type="text" name="title" id="form-title" class="form-control" placeholder="Titulo principal del evento" value="<?= esc($event->name) ?>" required>
                    </div>
                    <div class="form-group">
                        <label for="form-description">Descripción <span class="text-red">*</span>
                        <span class="help">Tip: Contenido detallado acerca de este evento</span></label>
                        <textarea name="description" id="form-description" class="form-control" rows="7" placeholder="Descripción detallada del evento" required><?= $event->event_description ?></textarea>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="form-image">Vista Previa
                        <span class="help">Tip: La imagen que representa este evento</label>
                        <img src="https://thelandmarkguadalajara.com/assets/images/events/<?= esc($event->id) ?>/<?= esc($event->slug) ?> 1.jpg" class="img-responsive" >
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <p style="float: left; margin:8px 0 0 0;"><span class="text-red" style="font-weight: bold;">*</span> Campos con información obligatoria</p>
            <button type="submit" class="btn btn-default pull-right"><span class="glyphicon glyphicon-floppy-save" aria-hidden="true"></span> Actualizar</button>
        </div>
    </form>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/css/fileinput.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/libs/fileinput/js/fileinput.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/js/locales/es.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    let baseUrl = '<?= base_url() ?>'

    $(document).ready(function() {
        $("#formEventEdit").submit(function(e) {
            e.preventDefault();
            e.stopPropagation();

            // Detener el proceso si existe uno o más errores con los archivos de imagen
            if ($(".file-input.theme-explorer").hasClass("has-error")) return false;

            let formData = new FormData($(this)[0]);
            Swal.fire({
                title: '¿Desea actualizar este evento?',
                text: "Usted esta a un paso de realizar una actualización en el sistema",
                icon: 'warning',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                showCancelButton: true,
                confirmButtonText: 'Si, deseo continuar',
                cancelButtonText: 'No, cancelar',
            }).then((result) => {
                if (result.isConfirmed) {
                    $(".message-error").hide();
                    $(".input-error").removeClass('input-error')

                    Swal.fire({
                        title: "Procesando solicitud",
                        text: "Espere un momento por favor.",
                        imageUrl: baseUrl + "/assets/images/loader.gif",
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                    });
                    setTimeout(function() {
                        $.ajax({
                            url: baseUrl + '/landmark/events/update/' + '<?= $event->id ?>',
                            type: 'POST',
                            data: formData,
                            contentType: false,
                            processData: false,
                            headers: {'X-Requested-With': 'XMLHttpRequest'},
                            dataType: 'json',
                        }).done(function(data, textStatus, jqXHR) {
                            if (data.code == 200) {
                                // Proceso exitoso
                                Swal.fire("Proceso terminado", data.message, "success");
                            } else if (data.code == 400 || data.code == 500) {
                                // Error
                                Swal.fire("Lo sentimos", data.message, "error");
                            } else {
                                // Problemas de Validación
                                Swal.fire("Error de validación", data.message, "error");
                                $.each(data.validation, function(key, value) {
                                    $(`*[name='${key}']`).addClass('input-error').parent().append(`<span class="message-error"><i class="fa fa-exclamation-circle"></i> ${value}</span>`)
                                })
                            }
                        }).fail(function(jqXHR, textStatus, errorThrown) {
                            console.log(jqXHR.responseText)
                            Swal.fire("Error", "Se detectó un error crítico en el sistema, favor de contactar al área de IT.", "error");
                        });
                    }, 1000);
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire('Proceso cancelado', 'No se realizaron modificaciones en el sistema', 'info');
                }
            })
        })
    })
</script>
<?= $this->endSection() ?>
