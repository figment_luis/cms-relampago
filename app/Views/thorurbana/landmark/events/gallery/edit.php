<?= $this->extend('layouts/app') ?>

<!-- Titulo de página -->
<?= $this->section('title') ?>
Eventos Landmark Guadalajara | <?= getenv('project_title'); ?>
<?= $this->endSection() ?>

<?= $this->section('content-header') ?>
<h1>Eventos - Landmark Guadalajara<br><small style="padding-left: 0;">Esta sección le permite editar la galería de imagenes asociadas con un evento sobre el sitio web de Landmark Guadalajara</small></h1>
<ol class="breadcrumb" >
    <li><a href="<?= base_url('/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?= base_url('/landmark/dashboard') ?>">Landmark Guadalajara</a></li>
    <li><a href="<?= base_url('/landmark/events') ?>">Eventos</a></li>
    <li><a href="<?= base_url('/landmark/events/edit/'. $event->id) ?>"><?= $event->name ?></a></li>
    <li class="active">Editar Galería</li>
</ol>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="box">
    <div class="box-container">
        <div class="box-header">
            <h3 class="box-title">Editar Galería</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="form-title">Titulo <span class="text-red">*</span>
                        <span class="help">Tip: ¡Muestra tu actitud Town Square!</span></label>
                        <input type="text" name="title" id="form-title" class="form-control" value="<?= esc($event->name) ?>" placeholder="Titulo principal del evento" autocomplete="off" autofocus required readonly disabled>
                    </div>
                    <div class="form-group">
                        <label for="form-gallery">Galería <span class="text-red">*</span>
                        <span class="help">Tip: Conjunto de imágenes alusivas a este evento <strong>Formato JPG de 820x440</strong></span></label>
                        <input id="form-gallery" name="images[]" type="file" class="file" multiple data-preview-file-type="text" >
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('styles') ?>
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/css/fileinput.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.css') ?>">
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="<?= base_url('assets/libs/fileinput/js/fileinput.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/themes/explorer/theme.min.js') ?>"></script>
<script src="<?= base_url('assets/libs/fileinput/js/locales/es.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

<script>
    let baseUrl = '<?= base_url() ?>'
    let storeId = '<?= $event->id ?>'
    let slug = '<?= $event->slug ?>'

    let imagesPHP = '<?= json_encode($currentImages) ?>'
    let images = JSON.parse(imagesPHP)

    let imagesPreview = [];
    let imagesPreviewConfig = []

    for (let image of images) {
        imagesPreview.push('https://thelandmarkguadalajara.com/assets/images/events/' + storeId + '/' + image)
    }

    for (let image of images) {

        imagesPreviewConfig.push({
            "caption": image.includes('1.jpg') ? '<b>Imagen Principal</b><br> (si elimina esta imagen, la siguiente que suba será considerada como imagen principal)' : image,
            "width": "70px",
            "key" : image,
            "url": baseUrl + "/landmark/events/gallery/destroy/" + storeId + "/" + image,
        });
    }

    let $elFileInput = $("#form-gallery");

    $elFileInput.fileinput({
        'showUpload': false,
        'showRemove': false,
        'required': true,
        'uploadAsync': true,
        'uploadUrl': baseUrl + "/landmark/events/gallery/update/" + storeId + "/" + slug,
        maxImageWidth: 820,
        maxImageHeight: 440,
        minImageWidth: 820,
        minImageHeight: 440,
        maxFileSize: 500,
        'maxFileCount': 5,
        'language': 'es',
        'previewFileType': "image",
        'theme': "explorer",
        'allowedFileExtensions': ["jpg"],
        'msgFileRequired': 'Seleccione uno o más archivos de tipo imagen para subir',
        'validateInitialCount': true,
        'overwriteInitial': false,
        'initialPreviewAsData': true, // identify if you are sending preview data only and not the raw markup
        'initialPreviewFileType': 'image',
        'initialPreview': imagesPreview,
        'initialPreviewConfig': imagesPreviewConfig,
        msgFileRequired: 'El archivo de imagen es requerido',
        msgSizeTooLarge: 'La archivo supera el tamaño permitido de <br>{maxSize} kilobytes.</br>',
        msgInvalidFileExtension: 'Extensión de archivo no válida, solo se admiten <b>{extensions}</b>',
        msgImageWidthSmall: 'El ancho de imagen debe ser de al menos <b>{size} px.</b>',
        msgImageHeightSmall: 'La altura de imagen debe ser de al menos <b>{size} px.</b>',
        msgImageWidthLarge: 'El ancho de imagen no debe exceder los <b>{size} px.</b>',
        msgImageHeightLarge: 'El alto de imagen no debe exceder los <b>{size} px.</b>',
    }).on("filebatchselected", function(event, files) {
        $elFileInput.fileinput("upload");
    }).on('fileuploaded', function(event, previewId, index, fileId) {
        
    });
</script>
<?= $this->endSection() ?>
