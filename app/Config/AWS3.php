<?php
namespace Config;

use CodeIgniter\Config\BaseConfig;

class AWS3 extends BaseConfig
{

    public $config = [];

    public function __construct()
    {
        /*
        |--------------------------------------------------------------------------
        | Use SSL
        |--------------------------------------------------------------------------
        |
        | Run this over HTTP or HTTPS. HTTPS (SSL) is more secure but can cause problems
        | on incorrectly configured servers.
        |
        */
        $this->config['use_ssl'] = FALSE;

        /*
        |--------------------------------------------------------------------------
        | Verify Peer
        |--------------------------------------------------------------------------
        |
        | Enable verification of the HTTPS (SSL) certificate against the local CA
        | certificate store.
        |
        */
        $this->config['verify_peer'] = FALSE;

        /*
        |--------------------------------------------------------------------------
        | Access Key
        |--------------------------------------------------------------------------
        |
        | Your Amazon S3 access key.
        |
        */
        $this->config['access_key'] = 'AKIAJSNCCI2Q6WDMAEOQ';

        /*
        |--------------------------------------------------------------------------
        | Secret Key
        |--------------------------------------------------------------------------
        |
        | Your Amazon S3 Secret Key.
        |
        */
        $this->config['secret_key'] = 'sxuu7q39khU0IT5AfWQE158Q0ZOnPdnhnkcKm4uB';

        /*
        |--------------------------------------------------------------------------
        | Use Enviroment?
        |--------------------------------------------------------------------------
        |
        | Get Settings from enviroment instead of this file?
        | Used as best-practice on Heroku
        |
        */
        $this->config['get_from_enviroment'] = FALSE;

        /*
        |--------------------------------------------------------------------------
        | Access Key Name
        |--------------------------------------------------------------------------
        |
        | Name for access key in enviroment
        |
        */
        $this->config['access_key_envname'] = 'S3_KEY';

        /*
        |--------------------------------------------------------------------------
        | Access Key Name
        |--------------------------------------------------------------------------
        |
        | Name for access key in enviroment
        |
        */
        $this->config['secret_key_envname'] = 'S3_SECRET';
    }

}
