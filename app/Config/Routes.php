<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(false);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'LoginController::login');

$routes->get('/dashboard', 'Dashboard::index', ['filter' => 'auth:admin,thor,gsm']);

/********************************************************************
*
*  Rutas -- Antara
*
********************************************************************/
$routes->group('antara', ['namespace' => 'App\Controllers\GSM\Antara', 'filter' => 'auth:admin,gsm'], function($routes) {
    $routes->get('dashboard', 'Dashboard::index', ['as' => 'antara.dashboard']);
    // Novedades
    $routes->get('news', 'News::index', ['as' => 'antara.news.index']);
    $routes->get('news/create', 'News::create', ['as' => 'antara.news.create']);
    $routes->post('news/store', 'News::store', ['as' => 'antara.news.store']);
    $routes->post('news/destroy', 'News::destroy', ['as' => 'antara.news.destroy']);
    $routes->get('news/edit/(:segment)', 'News::edit/$1', ['as' => 'antara.news.edit']);
    $routes->post('news/update/(:segment)', 'News::update/$1', ['as' => 'antara.news.update']);
    // Promociones
    $routes->get('promotions', 'Promotions::index', ['as' => 'antara.promotions.index']);
    $routes->get('promotions/create', 'Promotions::create', ['as' => 'antara.promotions.create']);
    $routes->post('promotions/store', 'Promotions::store', ['as' => 'antara.promotions.store']);
    $routes->post('promotions/destroy', 'Promotions::destroy', ['as' => 'antara.promotions.destroy']);
    $routes->get('promotions/edit/(:segment)', 'Promotions::edit/$1', ['as' => 'antara.promotions.edit']);
    $routes->post('promotions/update/(:segment)', 'Promotions::update/$1', ['as' => 'antara.promotions.update']);
    // Slides
    $routes->get('slides', 'Slides::index', ['as' => 'antara.slides.index']);
    $routes->get('slides/create', 'Slides::create', ['as' => 'antara.slides.create']);
    $routes->post('slides/store', 'Slides::store', ['as' => 'antara.slides.store']);
    $routes->get('slides/edit/(:segment)', 'Slides::edit/$1', ['as' => 'antara.slides.edit']);
    $routes->post('slides/update/(:segment)', 'Slides::update/$1', ['as' => 'antara.slides.update']);
    $routes->post('slides/destroy/(:segment)', 'Slides::destroy/$1', ['as' => 'antara.slides.destroy']);
    $routes->get('slides/orderbyposition', 'Slides::orderSlidesByPosition', ['as' => 'antara.slides.orderbyposition']);
    $routes->post('slides/updateposition/(:segment)', 'Slides::updateSlidesPosition/$1');
    // Popups
    $routes->get('popups', 'Popups::index', ['as' => 'antara.popups.index']);
    $routes->get('popups/create', 'Popups::create', ['as' => 'antara.popups.create']);
    $routes->post('popups/store', 'Popups::store', ['as' => 'antara.popups.store']);
    $routes->get('popups/edit/(:segment)', 'Popups::edit/$1', ['as' => 'antara.popups.edit']);
    $routes->post('popups/update/(:segment)', 'Popups::update/$1', ['as' => 'antara.popups.update']);
    $routes->post('popups/destroy/(:segment)', 'Popups::destroy/$1', ['as' => 'antara.popups.destroy']);
    // Stores
    $routes->get('stores', 'Stores::index', ['as' => 'antara.stores.index']);
    $routes->get('stores/create', 'Stores::create', ['as' => 'antara.stores.create']);
    $routes->post('stores/store', 'Stores::store', ['as' => 'antara.stores.store']);
    $routes->get('stores/edit/(:segment)', 'Stores::edit/$1', ['as' => 'antara.stores.edit']);
    $routes->post('stores/update/(:segment)', 'Stores::update/$1', ['as' => 'antara.stores.update']);
    $routes->post('stores/destroy/(:segment)', 'Stores::destroy/$1', ['as' => 'antara.stores.destroy']);
    // Gallery Stores
    $routes->get('gallery/edit/(:segment)', 'Stores::galleryEdit/$1', ['as' => 'antara.gallery.edit']);
    $routes->post('gallery/destroy/(:segment)/(:segment)', 'Stores::galleryDestroy/$1/$2');
    $routes->post('gallery/update/(:segment)/', 'Stores::galleryUpdate/$1');
    // Subcategories
    $routes->get('stores/subcategories/(:segment)', 'Stores::getSubcategoriesByCategoryId/$1');
    // Micrositios
    $routes->get('microsites', 'Microsites::index', ['as' => 'antara.microsites.index']);
    $routes->get('microsites/create', 'Microsites::create', ['as' => 'antara.microsites.create']);
    $routes->post('microsites/store', 'Microsites::store', ['as' => 'antara.microsites.store']);
    $routes->post('microsites/destroy', 'Microsites::destroy', ['as' => 'antara.microsites.destroy']);
    $routes->post('microsites/activate/(:segment)', 'Microsites::activate/$1', ['as' => 'antara.microsites.activate']);
    $routes->get('microsites/edit/(:segment)', 'Microsites::edit/$1', ['as' => 'antara.microsites.edit']);
    $routes->post('microsites/update/(:segment)', 'Microsites::update/$1', ['as' => 'antara.microsites.update']);
});

/********************************************************************
*
*  Rutas -- Andamar
*
********************************************************************/
$routes->group('andamar', ['namespace' => 'App\Controllers\GSM\Andamar', 'filter' => 'auth:admin,gsm'], function($routes) {
    $routes->get('dashboard', 'Dashboard::index', ['as' => 'andamar.dashboard']);
    // Novedades
    $routes->get('news', 'News::index', ['as' => 'andamar.news.index']);
    $routes->get('news/create', 'News::create', ['as' => 'andamar.news.create']);
    $routes->post('news/store', 'News::store', ['as' => 'andamar.news.store']);
    $routes->post('news/destroy', 'News::destroy', ['as' => 'andamar.news.destroy']);
    $routes->get('news/edit/(:segment)', 'News::edit/$1', ['as' => 'andamar.news.edit']);
    $routes->post('news/update/(:segment)', 'News::update/$1', ['as' => 'andamar.news.update']);
    // Promociones
    $routes->get('promotions', 'Promotions::index', ['as' => 'andamar.promotions.index']);
    $routes->get('promotions/create', 'Promotions::create', ['as' => 'andamar.promotions.create']);
    $routes->post('promotions/store', 'Promotions::store', ['as' => 'andamar.promotions.store']);
    $routes->post('promotions/destroy', 'Promotions::destroy', ['as' => 'andamar.promotions.destroy']);
    $routes->get('promotions/edit/(:segment)', 'Promotions::edit/$1', ['as' => 'andamar.promotions.edit']);
    $routes->post('promotions/update/(:segment)', 'Promotions::update/$1', ['as' => 'andamar.promotions.update']);
    // Slides
    $routes->get('slides', 'Slides::index', ['as' => 'andamar.slides.index']);
    $routes->get('slides/create', 'Slides::create', ['as' => 'andamar.slides.create']);
    $routes->post('slides/store', 'Slides::store', ['as' => 'andamar.slides.store']);
    $routes->get('slides/edit/(:segment)', 'Slides::edit/$1', ['as' => 'andamar.slides.edit']);
    $routes->post('slides/update/(:segment)', 'Slides::update/$1', ['as' => 'andamar.slides.update']);
    $routes->post('slides/destroy/(:segment)', 'Slides::destroy/$1', ['as' => 'andamar.slides.destroy']);
    $routes->get('slides/orderbyposition', 'Slides::orderSlidesByPosition', ['as' => 'andamar.slides.orderbyposition']);
    $routes->post('slides/updateposition/(:segment)', 'Slides::updateSlidesPosition/$1');
    // Popups
    $routes->get('popups', 'Popups::index', ['as' => 'andamar.popups.index']);
    $routes->get('popups/create', 'Popups::create', ['as' => 'andamar.popups.create']);
    $routes->post('popups/store', 'Popups::store', ['as' => 'andamar.popups.store']);
    $routes->get('popups/edit/(:segment)', 'Popups::edit/$1', ['as' => 'andamar.popups.edit']);
    $routes->post('popups/update/(:segment)', 'Popups::update/$1', ['as' => 'andamar.popups.update']);
    $routes->post('popups/destroy/(:segment)', 'Popups::destroy/$1', ['as' => 'andamar.popups.destroy']);
    // Stores
    $routes->get('stores', 'Stores::index', ['as' => 'andamar.stores.index']);
    $routes->get('stores/create', 'Stores::create', ['as' => 'andamar.stores.create']);
    $routes->post('stores/store', 'Stores::store', ['as' => 'andamar.stores.store']);
    $routes->get('stores/edit/(:segment)', 'Stores::edit/$1', ['as' => 'andamar.stores.edit']);
    $routes->post('stores/update/(:segment)', 'Stores::update/$1', ['as' => 'andamar.stores.update']);
    $routes->post('stores/destroy/(:segment)', 'Stores::destroy/$1', ['as' => 'andamar.stores.destroy']);
    // Maps
    $routes->get('stores/map/(:segment)', 'Stores::mapEdit/$1', ['as' => 'andamar.stores.mapedit']);
    $routes->post('stores/map/update/(:segment)', 'Stores::mapUpdate/$1');
    // Micrositios
    $routes->get('microsites', 'Microsites::index', ['as' => 'andamar.microsites.index']);
    $routes->get('microsites/create', 'Microsites::create', ['as' => 'andamar.microsites.create']);
    $routes->post('microsites/store', 'Microsites::store', ['as' => 'andamar.microsites.store']);
    $routes->post('microsites/destroy', 'Microsites::destroy', ['as' => 'andamar.microsites.destroy']);
    $routes->post('microsites/activate/(:segment)', 'Microsites::activate/$1', ['as' => 'andamar.microsites.activate']);
    $routes->get('microsites/edit/(:segment)', 'Microsites::edit/$1', ['as' => 'andamar.microsites.edit']);
    $routes->post('microsites/update/(:segment)', 'Microsites::update/$1', ['as' => 'andamar.microsites.update']);
});

/********************************************************************
*
*  Rutas -- Antea
*
********************************************************************/
$routes->group('antea', ['namespace' => 'App\Controllers\GSM\Antea', 'filter' => 'auth:admin,gsm'], function($routes) {
    $routes->get('dashboard', 'Dashboard::index', ['as' => 'antea.dashboard']);
    // Novedades
    $routes->get('news', 'News::index', ['as' => 'antea.news.index']);
    $routes->get('news/create', 'News::create', ['as' => 'antea.news.create']);
    $routes->post('news/store', 'News::store', ['as' => 'antea.news.store']);
    $routes->post('news/destroy', 'News::destroy', ['as' => 'antea.news.destroy']);
    $routes->get('news/edit/(:segment)', 'News::edit/$1', ['as' => 'antea.news.edit']);
    $routes->post('news/update/(:segment)', 'News::update/$1', ['as' => 'antea.news.update']);
    // Promociones
    $routes->get('promotions', 'Promotions::index', ['as' => 'antea.promotions.index']);
    $routes->get('promotions/create', 'Promotions::create', ['as' => 'antea.promotions.create']);
    $routes->post('promotions/store', 'Promotions::store', ['as' => 'antea.promotions.store']);
    $routes->post('promotions/destroy', 'Promotions::destroy', ['as' => 'antea.promotions.destroy']);
    $routes->get('promotions/edit/(:segment)', 'Promotions::edit/$1', ['as' => 'antea.promotions.edit']);
    $routes->post('promotions/update/(:segment)', 'Promotions::update/$1', ['as' => 'antea.promotions.update']);
    // Slides
    $routes->get('slides', 'Slides::index', ['as' => 'antea.slides.index']);
    $routes->get('slides/create', 'Slides::create', ['as' => 'antea.slides.create']);
    $routes->post('slides/store', 'Slides::store', ['as' => 'antea.slides.store']);
    $routes->get('slides/edit/(:segment)', 'Slides::edit/$1', ['as' => 'antea.slides.edit']);
    $routes->post('slides/update/(:segment)', 'Slides::update/$1', ['as' => 'antea.slides.update']);
    $routes->post('slides/destroy/(:segment)', 'Slides::destroy/$1', ['as' => 'antea.slides.destroy']);
    $routes->get('slides/orderbyposition', 'Slides::orderSlidesByPosition', ['as' => 'antea.slides.orderbyposition']);
    $routes->post('slides/updateposition/(:segment)', 'Slides::updateSlidesPosition/$1');
    // Popups
    $routes->get('popups', 'Popups::index', ['as' => 'antea.popups.index']);
    $routes->get('popups/create', 'Popups::create', ['as' => 'antea.popups.create']);
    $routes->post('popups/store', 'Popups::store', ['as' => 'antea.popups.store']);
    $routes->get('popups/edit/(:segment)', 'Popups::edit/$1', ['as' => 'antea.popups.edit']);
    $routes->post('popups/update/(:segment)', 'Popups::update/$1', ['as' => 'antea.popups.update']);
    $routes->post('popups/destroy/(:segment)', 'Popups::destroy/$1', ['as' => 'antea.popups.destroy']);
    // Stores
    $routes->get('stores', 'Stores::index', ['as' => 'antea.stores.index']);
    $routes->get('stores/create', 'Stores::create', ['as' => 'antea.stores.create']);
    $routes->post('stores/store', 'Stores::store', ['as' => 'antea.stores.store']);
    $routes->get('stores/edit/(:segment)', 'Stores::edit/$1', ['as' => 'antea.stores.edit']);
    $routes->post('stores/update/(:segment)', 'Stores::update/$1', ['as' => 'antea.stores.update']);
    $routes->post('stores/destroy/(:segment)', 'Stores::destroy/$1', ['as' => 'antea.stores.destroy']);
    // Gallery Stores
    $routes->get('gallery/edit/(:segment)', 'Stores::galleryEdit/$1', ['as' => 'antea.gallery.edit']);
    $routes->post('gallery/destroy/(:segment)/(:segment)', 'Stores::galleryDestroy/$1/$2');
    $routes->post('gallery/update/(:segment)/', 'Stores::galleryUpdate/$1');
    // Subcategories
    $routes->get('stores/subcategories/(:segment)', 'Stores::getSubcategoriesByCategoryId/$1');
    // Maps
    $routes->get('stores/map/(:segment)', 'Stores::mapEdit/$1', ['as' => 'antea.stores.mapedit']);
    $routes->post('stores/map/update/(:segment)', 'Stores::mapUpdate/$1');
    // Micrositios
    $routes->get('microsites', 'Microsites::index', ['as' => 'antea.microsites.index']);
    $routes->get('microsites/create', 'Microsites::create', ['as' => 'antea.microsites.create']);
    $routes->post('microsites/store', 'Microsites::store', ['as' => 'antea.microsites.store']);
    $routes->post('microsites/destroy', 'Microsites::destroy', ['as' => 'antea.microsites.destroy']);
    $routes->post('microsites/activate/(:segment)', 'Microsites::activate/$1', ['as' => 'antea.microsites.activate']);
    $routes->get('microsites/edit/(:segment)', 'Microsites::edit/$1', ['as' => 'antea.microsites.edit']);
    $routes->post('microsites/update/(:segment)', 'Microsites::update/$1', ['as' => 'antea.microsites.update']);
});

/********************************************************************
*
*  Rutas -- Angelópolis Puebla
*
********************************************************************/
$routes->group('angelopolis', ['namespace' => 'App\Controllers\GSM\Angelopolis', 'filter' => 'auth:admin,gsm'], function($routes) {
    $routes->get('dashboard', 'Dashboard::index', ['as' => 'angelopolis.dashboard']);
    // Novedades
    $routes->get('news', 'News::index', ['as' => 'angelopolis.news.index']);
    $routes->get('news/create', 'News::create', ['as' => 'angelopolis.news.create']);
    $routes->post('news/store', 'News::store', ['as' => 'angelopolis.news.store']);
    $routes->post('news/destroy', 'News::destroy', ['as' => 'angelopolis.news.destroy']);
    $routes->get('news/edit/(:segment)', 'News::edit/$1', ['as' => 'angelopolis.news.edit']);
    $routes->post('news/update/(:segment)', 'News::update/$1', ['as' => 'angelopolis.news.update']);
    // Promociones
    $routes->get('promotions', 'Promotions::index', ['as' => 'angelopolis.promotions.index']);
    $routes->get('promotions/create', 'Promotions::create', ['as' => 'angelopolis.promotions.create']);
    $routes->post('promotions/store', 'Promotions::store', ['as' => 'angelopolis.promotions.store']);
    $routes->post('promotions/destroy', 'Promotions::destroy', ['as' => 'angelopolis.promotions.destroy']);
    $routes->get('promotions/edit/(:segment)', 'Promotions::edit/$1', ['as' => 'angelopolis.promotions.edit']);
    $routes->post('promotions/update/(:segment)', 'Promotions::update/$1', ['as' => 'angelopolis.promotions.update']);
    // Temas del Mes
    $routes->get('trends', 'Trends::index', ['as' => 'angelopolis.trends.index']);
    $routes->get('trends/create', 'Trends::create', ['as' => 'angelopolis.trends.create']);
    $routes->post('trends/store', 'Trends::store', ['as' => 'angelopolis.trends.store']);
    $routes->post('trends/destroy', 'Trends::destroy', ['as' => 'angelopolis.trends.destroy']);
    $routes->get('trends/edit/(:segment)', 'Trends::edit/$1', ['as' => 'angelopolis.trends.edit']);
    $routes->post('trends/update/(:segment)', 'Trends::update/$1', ['as' => 'angelopolis.trends.update']);
    $routes->post('trends/uploadImage', 'Trends::uploadImage');
    // Slides
    $routes->get('slides', 'Slides::index', ['as' => 'angelopolis.slides.index']);
    $routes->get('slides/create', 'Slides::create', ['as' => 'angelopolis.slides.create']);
    $routes->post('slides/store', 'Slides::store', ['as' => 'angelopolis.slides.store']);
    $routes->get('slides/edit/(:segment)', 'Slides::edit/$1', ['as' => 'angelopolis.slides.edit']);
    $routes->post('slides/update/(:segment)', 'Slides::update/$1', ['as' => 'angelopolis.slides.update']);
    $routes->post('slides/destroy/(:segment)', 'Slides::destroy/$1', ['as' => 'angelopolis.slides.destroy']);
    $routes->get('slides/orderbyposition', 'Slides::orderSlidesByPosition', ['as' => 'angelopolis.slides.orderbyposition']);
    $routes->post('slides/updateposition/(:segment)', 'Slides::updateSlidesPosition/$1');
    // Popups
    $routes->get('popups', 'Popups::index', ['as' => 'angelopolis.popups.index']);
    $routes->get('popups/create', 'Popups::create', ['as' => 'angelopolis.popups.create']);
    $routes->post('popups/store', 'Popups::store', ['as' => 'angelopolis.popups.store']);
    $routes->get('popups/edit/(:segment)', 'Popups::edit/$1', ['as' => 'angelopolis.popups.edit']);
    $routes->post('popups/update/(:segment)', 'Popups::update/$1', ['as' => 'angelopolis.popups.update']);
    $routes->post('popups/destroy/(:segment)', 'Popups::destroy/$1', ['as' => 'angelopolis.popups.destroy']);
    // Stores
    $routes->get('stores', 'Stores::index', ['as' => 'angelopolis.stores.index']);
    $routes->get('stores/create', 'Stores::create', ['as' => 'angelopolis.stores.create']);
    $routes->post('stores/store', 'Stores::store', ['as' => 'angelopolis.stores.store']);
    $routes->get('stores/edit/(:segment)', 'Stores::edit/$1', ['as' => 'angelopolis.stores.edit']);
    $routes->post('stores/update/(:segment)', 'Stores::update/$1', ['as' => 'angelopolis.stores.update']);
    $routes->post('stores/destroy/(:segment)', 'Stores::destroy/$1', ['as' => 'angelopolis.stores.destroy']);
    // Gallery Stores
    $routes->get('gallery/edit/(:segment)', 'Stores::galleryEdit/$1', ['as' => 'angelopolis.gallery.edit']);
    $routes->post('gallery/destroy/(:segment)/(:segment)', 'Stores::galleryDestroy/$1/$2');
    $routes->post('gallery/update/(:segment)/', 'Stores::galleryUpdate/$1');
    // Subcategories
    $routes->get('stores/subcategories/(:segment)', 'Stores::getSubcategoriesByCategoryId/$1');
    // Maps
    $routes->get('stores/map/(:segment)', 'Stores::mapEdit/$1', ['as' => 'angelopolis.stores.mapedit']);
    $routes->post('stores/map/update/(:segment)', 'Stores::mapUpdate/$1');
    // Micrositios
    $routes->get('microsites', 'Microsites::index', ['as' => 'angelopolis.microsites.index']);
    $routes->get('microsites/create', 'Microsites::create', ['as' => 'angelopolis.microsites.create']);
    $routes->post('microsites/store', 'Microsites::store', ['as' => 'angelopolis.microsites.store']);
    $routes->post('microsites/destroy', 'Microsites::destroy', ['as' => 'angelopolis.microsites.destroy']);
    $routes->post('microsites/activate/(:segment)', 'Microsites::activate/$1', ['as' => 'angelopolis.microsites.activate']);
    $routes->get('microsites/edit/(:segment)', 'Microsites::edit/$1', ['as' => 'angelopolis.microsites.edit']);
    $routes->post('microsites/update/(:segment)', 'Microsites::update/$1', ['as' => 'angelopolis.microsites.update']);
});

/********************************************************************
*
*  Rutas -- Luxury Hall
*
********************************************************************/
$routes->group('luxury', ['namespace' => 'App\Controllers\GSM\Luxury', 'filter' => 'auth:admin,gsm'], function($routes) {
    $routes->get('dashboard', 'Dashboard::index', ['as' => 'luxury.dashboard']);
    // Novedades
    $routes->get('news', 'News::index', ['as' => 'luxury.news.index']);
    $routes->get('news/create', 'News::create', ['as' => 'luxury.news.create']);
    $routes->post('news/store', 'News::store', ['as' => 'luxury.news.store']);
    $routes->post('news/destroy', 'News::destroy', ['as' => 'luxury.news.destroy']);
    $routes->get('news/edit/(:segment)', 'News::edit/$1', ['as' => 'luxury.news.edit']);
    $routes->post('news/update/(:segment)', 'News::update/$1', ['as' => 'luxury.news.update']);
    // Promociones
    $routes->get('promotions', 'Promotions::index', ['as' => 'luxury.promotions.index']);
    $routes->get('promotions/create', 'Promotions::create', ['as' => 'luxury.promotions.create']);
    $routes->post('promotions/store', 'Promotions::store', ['as' => 'luxury.promotions.store']);
    $routes->post('promotions/destroy', 'Promotions::destroy', ['as' => 'luxury.promotions.destroy']);
    $routes->get('promotions/edit/(:segment)', 'Promotions::edit/$1', ['as' => 'luxury.promotions.edit']);
    $routes->post('promotions/update/(:segment)', 'Promotions::update/$1', ['as' => 'luxury.promotions.update']);
    // Temas del Mes
    $routes->get('trends', 'Trends::index', ['as' => 'luxury.trends.index']);
    $routes->get('trends/create', 'Trends::create', ['as' => 'luxury.trends.create']);
    $routes->post('trends/store', 'Trends::store', ['as' => 'luxury.trends.store']);
    $routes->post('trends/destroy', 'Trends::destroy', ['as' => 'luxury.trends.destroy']);
    $routes->get('trends/edit/(:segment)', 'Trends::edit/$1', ['as' => 'luxury.trends.edit']);
    $routes->post('trends/update/(:segment)', 'Trends::update/$1', ['as' => 'luxury.trends.update']);
    $routes->post('trends/uploadImage', 'Trends::uploadImage');
    // Slides
    $routes->get('slides', 'Slides::index', ['as' => 'luxury.slides.index']);
    $routes->get('slides/create', 'Slides::create', ['as' => 'luxury.slides.create']);
    $routes->post('slides/store', 'Slides::store', ['as' => 'luxury.slides.store']);
    $routes->get('slides/edit/(:segment)', 'Slides::edit/$1', ['as' => 'luxury.slides.edit']);
    $routes->post('slides/update/(:segment)', 'Slides::update/$1', ['as' => 'luxury.slides.update']);
    $routes->post('slides/destroy/(:segment)', 'Slides::destroy/$1', ['as' => 'luxury.slides.destroy']);
    $routes->get('slides/orderbyposition', 'Slides::orderSlidesByPosition', ['as' => 'luxury.slides.orderbyposition']);
    $routes->post('slides/updateposition/(:segment)', 'Slides::updateSlidesPosition/$1');
    // Popups
    $routes->get('popups', 'Popups::index', ['as' => 'luxury.popups.index']);
    $routes->get('popups/create', 'Popups::create', ['as' => 'luxury.popups.create']);
    $routes->post('popups/store', 'Popups::store', ['as' => 'luxury.popups.store']);
    $routes->get('popups/edit/(:segment)', 'Popups::edit/$1', ['as' => 'luxury.popups.edit']);
    $routes->post('popups/update/(:segment)', 'Popups::update/$1', ['as' => 'luxury.popups.update']);
    $routes->post('popups/destroy/(:segment)', 'Popups::destroy/$1', ['as' => 'luxury.popups.destroy']);
    // Stores
    $routes->get('stores', 'Stores::index', ['as' => 'luxury.stores.index']);
    $routes->get('stores/create', 'Stores::create', ['as' => 'luxury.stores.create']);
    $routes->post('stores/store', 'Stores::store', ['as' => 'luxury.stores.store']);
    $routes->get('stores/edit/(:segment)', 'Stores::edit/$1', ['as' => 'luxury.stores.edit']);
    $routes->post('stores/update/(:segment)', 'Stores::update/$1', ['as' => 'luxury.stores.update']);
    $routes->post('stores/destroy/(:segment)', 'Stores::destroy/$1', ['as' => 'luxury.stores.destroy']);
    // Gallery Web Stores
    $routes->get('gallery/edit/(:segment)', 'Stores::galleryEdit/$1', ['as' => 'luxury.gallery.edit']);
    $routes->post('galleryweb/destroy/(:segment)/(:segment)', 'Stores::galleryWebDestroy/$1/$2');
    $routes->post('galleryweb/update/(:segment)/', 'Stores::galleryWebUpdate/$1');
    // Subcategories
    $routes->get('stores/subcategories/(:segment)', 'Stores::getSubcategoriesByCategoryId/$1');
    // Maps
    $routes->get('stores/map/(:segment)', 'Stores::mapEdit/$1', ['as' => 'luxury.stores.mapedit']);
    $routes->post('stores/map/update/(:segment)', 'Stores::mapUpdate/$1');
    // Micrositios
    $routes->get('microsites', 'Microsites::index', ['as' => 'luxury.microsites.index']);
    $routes->get('microsites/create', 'Microsites::create', ['as' => 'luxury.microsites.create']);
    $routes->post('microsites/store', 'Microsites::store', ['as' => 'luxury.microsites.store']);
    $routes->post('microsites/destroy', 'Microsites::destroy', ['as' => 'luxury.microsites.destroy']);
    $routes->post('microsites/activate/(:segment)', 'Microsites::activate/$1', ['as' => 'luxury.microsites.activate']);
    $routes->get('microsites/edit/(:segment)', 'Microsites::edit/$1', ['as' => 'luxury.microsites.edit']);
    $routes->post('microsites/update/(:segment)', 'Microsites::update/$1', ['as' => 'luxury.microsites.update']);
});

/********************************************************************
*
*  Rutas -- Plan de lealtad Addicted
*
********************************************************************/
$routes->group('addicted', ['namespace' => 'App\Controllers\Loyalty\Addicted', 'filter' => 'auth:admin,gsm'], function($routes) {
    $routes->get('dashboard', 'Dashboard::index', ['as' => 'addicted.dashboard']);
    // Premios
    $routes->get('awards', 'Awards::index', ['as' => 'addicted.awards.index']);
    $routes->get('awards/create', 'Awards::create', ['as' => 'addicted.awards.create']);
    $routes->post('awards/store', 'Awards::store', ['as' => 'addicted.awards.store']);
    $routes->get('awards/edit/(:segment)', 'Awards::edit/$1', ['as' => 'addicted.awards.edit']);
    $routes->post('awards/update/(:segment)', 'Awards::update/$1', ['as' => 'addicted.awards.update']);
    $routes->post('awards/destroy/(:segment)', 'Awards::destroy/$1', ['as' => 'addicted.awards.destroy']);
    // Descuentos
    $routes->get('discounts', 'Discounts::index', ['as' => 'addicted.discounts.index']);
    $routes->get('discounts/create', 'Discounts::create', ['as' => 'addicted.discounts.create']);
    $routes->post('discounts/store', 'Discounts::store', ['as' => 'addicted.discounts.store']);
    $routes->get('discounts/edit/(:segment)', 'Discounts::edit/$1', ['as' => 'addicted.discounts.edit']);
    $routes->post('discounts/update/(:segment)', 'Discounts::update/$1', ['as' => 'addicted.discounts.update']);
    $routes->post('discounts/destroy/(:segment)', 'Discounts::destroy/$1', ['as' => 'addicted.discounts.destroy']);
    // Beneficios
    $routes->get('benefits', 'Benefits::index', ['as' => 'addicted.benefits.index']);
    $routes->get('benefits/create', 'Benefits::create', ['as' => 'addicted.benefits.create']);
    $routes->post('benefits/store', 'Benefits::store', ['as' => 'addicted.benefits.store']);
    $routes->get('benefits/edit/(:segment)', 'Benefits::edit/$1', ['as' => 'addicted.benefits.edit']);
    $routes->post('benefits/update/(:segment)', 'Benefits::update/$1', ['as' => 'addicted.benefits.update']);
    $routes->post('benefits/destroy/(:segment)', 'Benefits::destroy/$1', ['as' => 'addicted.benefits.destroy']);
    // Experiencias
    $routes->get('experiences', 'Experiences::index', ['as' => 'addicted.experiences.index']);
    $routes->get('experiences/create', 'Experiences::create', ['as' => 'addicted.experiences.create']);
    $routes->post('experiences/store', 'Experiences::store', ['as' => 'addicted.experiences.store']);
    $routes->get('experiences/edit/(:segment)', 'Experiences::edit/$1', ['as' => 'addicted.experiences.edit']);
    $routes->post('experiences/update/(:segment)', 'Experiences::update/$1', ['as' => 'addicted.experiences.update']);
    $routes->post('experiences/destroy/(:segment)', 'Experiences::destroy/$1', ['as' => 'addicted.experiences.destroy']);
    // Tiendas
    $routes->get('stores', 'Stores::index', ['as' => 'addicted.stores.index']);
    $routes->get('stores/create', 'Stores::create', ['as' => 'addicted.stores.create']);
    $routes->post('stores/store', 'Stores::store', ['as' => 'addicted.stores.store']);
    $routes->get('stores/edit/(:segment)', 'Stores::edit/$1', ['as' => 'addicted.stores.edit']);
    $routes->post('stores/update/(:segment)', 'Stores::update/$1', ['as' => 'addicted.stores.update']);
    $routes->post('stores/destroy/(:segment)', 'Stores::destroy/$1', ['as' => 'addicted.stores.destroy']);
    $routes->post('stores/activate/(:segment)', 'Stores::activate/$1', ['as' => 'addicted.stores.activate']);
    // Popups
    $routes->get('popups', 'Popups::index', ['as' => 'addicted.popups.index']);
    $routes->get('popups/create', 'Popups::create', ['as' => 'addicted.popups.create']);
    $routes->post('popups/store', 'Popups::store', ['as' => 'addicted.popups.store']);
    $routes->get('popups/edit/(:segment)', 'Popups::edit/$1', ['as' => 'addicted.popups.edit']);
    $routes->post('popups/update/(:segment)', 'Popups::update/$1', ['as' => 'addicted.popups.update']);
    $routes->post('popups/destroy/(:segment)', 'Popups::destroy/$1', ['as' => 'addicted.popups.destroy']);
});

/********************************************************************
*
*  Rutas -- Plan de lealtad Limitless
*
********************************************************************/
$routes->group('limitless', ['namespace' => 'App\Controllers\Loyalty\Limitless', 'filter' => 'auth:admin,gsm'], function($routes) {
    $routes->get('dashboard', 'Dashboard::index', ['as' => 'limitless.dashboard']);
    // Premios
    $routes->get('awards', 'Awards::index', ['as' => 'limitless.awards.index']);
    $routes->get('awards/create', 'Awards::create', ['as' => 'limitless.awards.create']);
    $routes->post('awards/store', 'Awards::store', ['as' => 'limitless.awards.store']);
    $routes->get('awards/edit/(:segment)', 'Awards::edit/$1', ['as' => 'limitless.awards.edit']);
    $routes->post('awards/update/(:segment)', 'Awards::update/$1', ['as' => 'limitless.awards.update']);
    $routes->post('awards/destroy/(:segment)', 'Awards::destroy/$1', ['as' => 'limitless.awards.destroy']);
    // Beneficios
    $routes->get('benefits', 'Benefits::index', ['as' => 'limitless.benefits.index']);
    $routes->get('benefits/create', 'Benefits::create', ['as' => 'limitless.benefits.create']);
    $routes->post('benefits/store', 'Benefits::store', ['as' => 'limitless.benefits.store']);
    $routes->get('benefits/edit/(:segment)', 'Benefits::edit/$1', ['as' => 'limitless.benefits.edit']);
    $routes->post('benefits/update/(:segment)', 'Benefits::update/$1', ['as' => 'limitless.benefits.update']);
    $routes->post('benefits/destroy/(:segment)', 'Benefits::destroy/$1', ['as' => 'limitless.benefits.destroy']);
    // Tiendas
    $routes->get('stores', 'Stores::index', ['as' => 'limitless.stores.index']);
    $routes->get('stores/create', 'Stores::create', ['as' => 'limitless.stores.create']);
    $routes->post('stores/store', 'Stores::store', ['as' => 'limitless.stores.store']);
    $routes->get('stores/edit/(:segment)', 'Stores::edit/$1', ['as' => 'limitless.stores.edit']);
    $routes->post('stores/update/(:segment)', 'Stores::update/$1', ['as' => 'limitless.stores.update']);
    $routes->post('stores/destroy/(:segment)', 'Stores::destroy/$1', ['as' => 'limitless.stores.destroy']);
    $routes->post('stores/activate/(:segment)', 'Stores::activate/$1', ['as' => 'limitless.stores.activate']);
    // Popups
    $routes->get('popups', 'Popups::index', ['as' => 'limitless.popups.index']);
    $routes->get('popups/create', 'Popups::create', ['as' => 'limitless.popups.create']);
    $routes->post('popups/store', 'Popups::store', ['as' => 'limitless.popups.store']);
    $routes->get('popups/edit/(:segment)', 'Popups::edit/$1', ['as' => 'limitless.popups.edit']);
    $routes->post('popups/update/(:segment)', 'Popups::update/$1', ['as' => 'limitless.popups.update']);
    $routes->post('popups/destroy/(:segment)', 'Popups::destroy/$1', ['as' => 'limitless.popups.destroy']);
});

/********************************************************************
*
*  Rutas -- Plan de lealtad PearlAndamar
*
********************************************************************/
$routes->group('pearlandamar', ['namespace' => 'App\Controllers\Loyalty\Pearlandamar', 'filter' => 'auth:admin,gsm'], function($routes) {
    $routes->get('dashboard', 'Dashboard::index', ['as' => 'pearlandamar.dashboard']);
    // Premios
    $routes->get('awards', 'Awards::index', ['as' => 'pearlandamar.awards.index']);
    $routes->get('awards/create', 'Awards::create', ['as' => 'pearlandamar.awards.create']);
    $routes->post('awards/store', 'Awards::store', ['as' => 'pearlandamar.awards.store']);
    $routes->get('awards/edit/(:segment)', 'Awards::edit/$1', ['as' => 'pearlandamar.awards.edit']);
    $routes->post('awards/update/(:segment)', 'Awards::update/$1', ['as' => 'pearlandamar.awards.update']);
    $routes->post('awards/destroy/(:segment)', 'Awards::destroy/$1', ['as' => 'pearlandamar.awards.destroy']);
    // Gallery Premios
    $routes->get('awards/gallery/edit/(:segment)', 'Awards::editGallery/$1', ['as' => 'pearlandamar.gallery.edit']);
    $routes->post('awards/gallery/destroy/(:segment)/(:segment)', 'Awards::destroyGallery/$1/$2', ['as' => 'pearlandamar.gallery.destroy']);
    $routes->post('awards/gallery/update/(:segment)/', 'Awards::updateGallery/$1', ['as' => 'pearlandamar.gallery.update']);
    // Beneficios
    $routes->get('benefits', 'Benefits::index', ['as' => 'pearlandamar.benefits.index']);
    $routes->get('benefits/create', 'Benefits::create', ['as' => 'pearlandamar.benefits.create']);
    $routes->post('benefits/store', 'Benefits::store', ['as' => 'pearlandamar.benefits.store']);
    $routes->get('benefits/edit/(:segment)', 'Benefits::edit/$1', ['as' => 'pearlandamar.benefits.edit']);
    $routes->post('benefits/update/(:segment)', 'Benefits::update/$1', ['as' => 'pearlandamar.benefits.update']);
    $routes->post('benefits/destroy/(:segment)', 'Benefits::destroy/$1', ['as' => 'pearlandamar.benefits.destroy']);
    // Tiendas
    $routes->get('stores', 'Stores::index', ['as' => 'pearlandamar.stores.index']);
    $routes->get('stores/create', 'Stores::create', ['as' => 'pearlandamar.stores.create']);
    $routes->post('stores/store', 'Stores::store', ['as' => 'pearlandamar.stores.store']);
    $routes->get('stores/edit/(:segment)', 'Stores::edit/$1', ['as' => 'pearlandamar.stores.edit']);
    $routes->post('stores/update/(:segment)', 'Stores::update/$1', ['as' => 'pearlandamar.stores.update']);
    $routes->post('stores/destroy/(:segment)', 'Stores::destroy/$1', ['as' => 'pearlandamar.stores.destroy']);
    $routes->post('stores/activate/(:segment)', 'Stores::activate/$1', ['as' => 'pearlandamar.stores.activate']);
    // Popups
    $routes->get('popups', 'Popups::index', ['as' => 'pearlandamar.popups.index']);
    $routes->get('popups/create', 'Popups::create', ['as' => 'pearlandamar.popups.create']);
    $routes->post('popups/store', 'Popups::store', ['as' => 'pearlandamar.popups.store']);
    $routes->get('popups/edit/(:segment)', 'Popups::edit/$1', ['as' => 'pearlandamar.popups.edit']);
    $routes->post('popups/update/(:segment)', 'Popups::update/$1', ['as' => 'pearlandamar.popups.update']);
    $routes->post('popups/destroy/(:segment)', 'Popups::destroy/$1', ['as' => 'pearlandamar.popups.destroy']);
});

/********************************************************************
*
*  Rutas -- Plan de lealtad Rewards
*
********************************************************************/
$routes->group('rewards', ['namespace' => 'App\Controllers\Loyalty\Rewards', 'filter' => 'auth:admin,gsm'], function($routes) {
    $routes->get('dashboard', 'Dashboard::index', ['as' => 'rewards.dashboard']);
    // Premios
    $routes->get('awards', 'Awards::index', ['as' => 'rewards.awards.index']);
    $routes->get('awards/create', 'Awards::create', ['as' => 'rewards.awards.create']);
    $routes->post('awards/store', 'Awards::store', ['as' => 'rewards.awards.store']);
    $routes->get('awards/edit/(:segment)', 'Awards::edit/$1', ['as' => 'rewards.awards.edit']);
    $routes->post('awards/update/(:segment)', 'Awards::update/$1', ['as' => 'rewards.awards.update']);
    $routes->post('awards/destroy/(:segment)', 'Awards::destroy/$1', ['as' => 'rewards.awards.destroy']);
    // Gallery Premios
    $routes->get('awards/gallery/edit/(:segment)', 'Awards::editGallery/$1', ['as' => 'rewards.gallery.edit']);
    $routes->post('awards/gallery/destroy/(:segment)/(:segment)', 'Awards::destroyGallery/$1/$2', ['as' => 'rewards.gallery.destroy']);
    $routes->post('awards/gallery/update/(:segment)/', 'Awards::updateGallery/$1', ['as' => 'rewards.gallery.update']);
    // Beneficios
    $routes->get('benefits', 'Benefits::index', ['as' => 'rewards.benefits.index']);
    $routes->get('benefits/create', 'Benefits::create', ['as' => 'rewards.benefits.create']);
    $routes->post('benefits/store', 'Benefits::store', ['as' => 'rewards.benefits.store']);
    $routes->get('benefits/edit/(:segment)', 'Benefits::edit/$1', ['as' => 'rewards.benefits.edit']);
    $routes->post('benefits/update/(:segment)', 'Benefits::update/$1', ['as' => 'rewards.benefits.update']);
    $routes->post('benefits/destroy/(:segment)', 'Benefits::destroy/$1', ['as' => 'rewards.benefits.destroy']);
    // Tiendas
    $routes->get('stores', 'Stores::index', ['as' => 'rewards.stores.index']);
    $routes->get('stores/create', 'Stores::create', ['as' => 'rewards.stores.create']);
    $routes->post('stores/store', 'Stores::store', ['as' => 'rewards.stores.store']);
    $routes->get('stores/edit/(:segment)', 'Stores::edit/$1', ['as' => 'rewards.stores.edit']);
    $routes->post('stores/update/(:segment)', 'Stores::update/$1', ['as' => 'rewards.stores.update']);
    $routes->post('stores/destroy/(:segment)', 'Stores::destroy/$1', ['as' => 'rewards.stores.destroy']);
    $routes->post('stores/activate/(:segment)', 'Stores::activate/$1', ['as' => 'rewards.stores.activate']);
    // Popups
    $routes->get('popups', 'Popups::index', ['as' => 'rewards.popups.index']);
    $routes->get('popups/create', 'Popups::create', ['as' => 'rewards.popups.create']);
    $routes->post('popups/store', 'Popups::store', ['as' => 'rewards.popups.store']);
    $routes->get('popups/edit/(:segment)', 'Popups::edit/$1', ['as' => 'rewards.popups.edit']);
    $routes->post('popups/update/(:segment)', 'Popups::update/$1', ['as' => 'rewards.popups.update']);
    $routes->post('popups/destroy/(:segment)', 'Popups::destroy/$1', ['as' => 'rewards.popups.destroy']);
});

/********************************************************************
*
*  Rutas -- Plan de lealtad One
*
********************************************************************/
$routes->group('one', ['namespace' => 'App\Controllers\Loyalty\One', 'filter' => 'auth:admin,gsm'], function($routes) {
    $routes->get('dashboard', 'Dashboard::index', ['as' => 'one.dashboard']);
    // Premios
    $routes->get('awards', 'Awards::index', ['as' => 'one.awards.index']);
    $routes->get('awards/create', 'Awards::create', ['as' => 'one.awards.create']);
    $routes->post('awards/store', 'Awards::store', ['as' => 'one.awards.store']);
    $routes->get('awards/edit/(:segment)', 'Awards::edit/$1', ['as' => 'one.awards.edit']);
    $routes->post('awards/update/(:segment)', 'Awards::update/$1', ['as' => 'one.awards.update']);
    $routes->post('awards/destroy/(:segment)', 'Awards::destroy/$1', ['as' => 'one.awards.destroy']);
    // Beneficios
    $routes->get('benefits', 'Benefits::index', ['as' => 'one.benefits.index']);
    $routes->get('benefits/create', 'Benefits::create', ['as' => 'one.benefits.create']);
    $routes->post('benefits/store', 'Benefits::store', ['as' => 'one.benefits.store']);
    $routes->get('benefits/edit/(:segment)', 'Benefits::edit/$1', ['as' => 'one.benefits.edit']);
    $routes->post('benefits/update/(:segment)', 'Benefits::update/$1', ['as' => 'one.benefits.update']);
    $routes->post('benefits/destroy/(:segment)', 'Benefits::destroy/$1', ['as' => 'one.benefits.destroy']);
    // Descuentos
    $routes->get('discounts', 'Discounts::index', ['as' => 'one.discounts.index']);
    $routes->get('discounts/create', 'Discounts::create', ['as' => 'one.discounts.create']);
    $routes->post('discounts/store', 'Discounts::store', ['as' => 'one.discounts.store']);
    $routes->get('discounts/edit/(:segment)', 'Discounts::edit/$1', ['as' => 'one.discounts.edit']);
    $routes->post('discounts/update/(:segment)', 'Discounts::update/$1', ['as' => 'one.discounts.update']);
    $routes->post('discounts/destroy/(:segment)', 'Discounts::destroy/$1', ['as' => 'one.discounts.destroy']);
    // Tiendas
    $routes->get('stores', 'Stores::index', ['as' => 'one.stores.index']);
    $routes->get('stores/create', 'Stores::create', ['as' => 'one.stores.create']);
    $routes->post('stores/store', 'Stores::store', ['as' => 'one.stores.store']);
    $routes->get('stores/edit/(:segment)', 'Stores::edit/$1', ['as' => 'one.stores.edit']);
    $routes->post('stores/update/(:segment)', 'Stores::update/$1', ['as' => 'one.stores.update']);
    $routes->post('stores/destroy/(:segment)', 'Stores::destroy/$1', ['as' => 'one.stores.destroy']);
    $routes->post('stores/activate/(:segment)', 'Stores::activate/$1', ['as' => 'one.stores.activate']);
    // Popups
    $routes->get('popups', 'Popups::index', ['as' => 'one.popups.index']);
    $routes->get('popups/create', 'Popups::create', ['as' => 'one.popups.create']);
    $routes->post('popups/store', 'Popups::store', ['as' => 'one.popups.store']);
    $routes->get('popups/edit/(:segment)', 'Popups::edit/$1', ['as' => 'one.popups.edit']);
    $routes->post('popups/update/(:segment)', 'Popups::update/$1', ['as' => 'one.popups.update']);
    $routes->post('popups/destroy/(:segment)', 'Popups::destroy/$1', ['as' => 'one.popups.destroy']);
});

/********************************************************************
*
*  Rutas -- Usuarios
*
********************************************************************/
$routes->group('users', ['namespace' => 'App\Controllers', 'filter' => 'auth:admin'], function($routes) {
    $routes->get('create', 'RegisterController::create', ['as' => 'users.create']);
    $routes->post('store', 'RegisterController::store', ['as' => 'users.store']);
});

/********************************************************************
 *
 *  Rutas -- Logs
 *
 ********************************************************************/
$routes->group('logs', ['namespace' => 'App\Controllers', 'filter' => 'auth:admin'], function($routes) {
    $routes->get('/', 'LogController::index', ['as' => 'logs.index']);
});

$routes->group('login', ['namespace' => 'App\Controllers'], function($routes) {
    $routes->post('auth', 'LoginController::auth', ['as' => 'login.auth']);
    // Verificar por que solo los usuarios logeados pueden deslogarse (separarlo fuera del grupo)
    $routes->post('logout', 'LoginController::logout', ['as' => 'login.logout']);
});



/********************************************************************
*
*  Rutas -- Townsqueare Metepec
*
********************************************************************/
$routes->group('townsquare', ['namespace' => 'App\Controllers\Thorurbana\Townsquare', 'filter' => 'auth:admin,thor'], function($routes) {
    $routes->get('dashboard', 'Dashboard::index', ['as' => 'townsquare.dashboard']);
    // Novedades
    $routes->get('news', 'News::index', ['as' => 'townsquare.news.index']);
    $routes->get('news/create', 'News::create', ['as' => 'townsquare.news.create']);
    $routes->post('news/store', 'News::store', ['as' => 'townsquare.news.store']);
    $routes->post('news/destroy', 'News::destroy', ['as' => 'townsquare.news.destroy']);
    $routes->get('news/edit/(:segment)', 'News::edit/$1', ['as' => 'townsquare.news.edit']);
    $routes->post('news/update/(:segment)', 'News::update/$1', ['as' => 'townsquare.news.update']);
    // Eventos
    $routes->get('events', 'Events::index', ['as' => 'townsquare.events.index']);
    $routes->get('events/create', 'Events::create', ['as' => 'townsquare.events.create']);
    $routes->post('events/store', 'Events::store', ['as' => 'townsquare.events.store']);
    $routes->post('events/destroy', 'Events::destroy', ['as' => 'townsquare.events.destroy']);
    $routes->get('events/edit/(:segment)', 'Events::edit/$1', ['as' => 'townsquare.events.edit']);
    $routes->post('events/update/(:segment)', 'Events::update/$1', ['as' => 'townsquare.events.update']);
    // Gallería Eventos
    $routes->get('events/gallery/edit/(:segment)', 'Events::editGallery/$1', ['as' => 'townsquare.events.gallery.edit']);
    $routes->post('events/gallery/destroy/(:segment)/(:segment)', 'Events::destroyGallery/$1/$2', ['as' => 'townsquare.events.gallery.destroy']);
    $routes->post('events/gallery/update/(:segment)/(:segment)', 'Events::updateGallery/$1/$2', ['as' => 'townsquare.events.gallery.update']);
    // Popups
    $routes->get('popups', 'Popups::index', ['as' => 'townsquare.popups.index']);
    $routes->get('popups/create', 'Popups::create', ['as' => 'townsquare.popups.create']);
    $routes->post('popups/store', 'Popups::store', ['as' => 'townsquare.popups.store']);
    $routes->get('popups/edit/(:segment)', 'Popups::edit/$1', ['as' => 'townsquare.popups.edit']);
    $routes->post('popups/update/(:segment)', 'Popups::update/$1', ['as' => 'townsquare.popups.update']);
    $routes->post('popups/destroy/(:segment)', 'Popups::destroy/$1', ['as' => 'townsquare.popups.destroy']);
    // Stores
    $routes->get('stores', 'Stores::index', ['as' => 'townsquare.stores.index']);
    $routes->get('stores/create', 'Stores::create', ['as' => 'townsquare.stores.create']);
    $routes->post('stores/store', 'Stores::store', ['as' => 'townsquare.stores.store']);
    $routes->get('stores/edit/(:segment)', 'Stores::edit/$1', ['as' => 'townsquare.stores.edit']);
    $routes->post('stores/update/(:segment)', 'Stores::update/$1', ['as' => 'townsquare.stores.update']);
    $routes->post('stores/destroy/(:segment)', 'Stores::destroy/$1', ['as' => 'townsquare.stores.destroy']);
    // Gallery Stores
    $routes->get('stores/gallery/edit/(:segment)', 'Stores::editGallery/$1', ['as' => 'townsquare.stores.gallery.edit']);
    $routes->post('stores/gallery/destroy/(:segment)/(:segment)', 'Stores::destroyGallery/$1/$2');
    $routes->post('stores/gallery/update/(:segment)/(:segment)', 'Stores::updateGallery/$1/$2');
    // Subcategories
    $routes->get('stores/subcategories/(:segment)', 'Stores::getSubcategoriesByCategoryId/$1');
    // Micrositios
    $routes->get('microsites', 'Microsites::index', ['as' => 'townsquare.microsites.index']);
    $routes->get('microsites/create', 'Microsites::create', ['as' => 'townsquare.microsites.create']);
    $routes->post('microsites/store', 'Microsites::store', ['as' => 'townsquare.microsites.store']);
    $routes->post('microsites/destroy', 'Microsites::destroy', ['as' => 'townsquare.microsites.destroy']);
    $routes->post('microsites/activate/(:segment)', 'Microsites::activate/$1', ['as' => 'townsquare.microsites.activate']);
    $routes->get('microsites/edit/(:segment)', 'Microsites::edit/$1', ['as' => 'townsquare.microsites.edit']);
    $routes->post('microsites/update/(:segment)', 'Microsites::update/$1', ['as' => 'townsquare.microsites.update']);
});

/********************************************************************
*
*  Rutas -- The Harbor Mérida
*
********************************************************************/
$routes->group('harbor', ['namespace' => 'App\Controllers\Thorurbana\Harbor', 'filter' => 'auth:admin,thor'], function($routes) {
    $routes->get('dashboard', 'Dashboard::index', ['as' => 'harbor.dashboard']);
    // Novedades
    $routes->get('news', 'News::index', ['as' => 'harbor.news.index']);
    $routes->get('news/create', 'News::create', ['as' => 'harbor.news.create']);
    $routes->post('news/store', 'News::store', ['as' => 'harbor.news.store']);
    $routes->post('news/destroy', 'News::destroy', ['as' => 'harbor.news.destroy']);
    $routes->get('news/edit/(:segment)', 'News::edit/$1', ['as' => 'harbor.news.edit']);
    $routes->post('news/update/(:segment)', 'News::update/$1', ['as' => 'harbor.news.update']);
    // Eventos
    $routes->get('events', 'Events::index', ['as' => 'harbor.events.index']);
    $routes->get('events/create', 'Events::create', ['as' => 'harbor.events.create']);
    $routes->post('events/store', 'Events::store', ['as' => 'harbor.events.store']);
    $routes->post('events/destroy', 'Events::destroy', ['as' => 'harbor.events.destroy']);
    $routes->get('events/edit/(:segment)', 'Events::edit/$1', ['as' => 'harbor.events.edit']);
    $routes->post('events/update/(:segment)', 'Events::update/$1', ['as' => 'harbor.events.update']);
    // Gallería Eventos
    $routes->get('events/gallery/edit/(:segment)', 'Events::editGallery/$1', ['as' => 'harbor.events.gallery.edit']);
    $routes->post('events/gallery/destroy/(:segment)/(:segment)', 'Events::destroyGallery/$1/$2', ['as' => 'harbor.events.gallery.destroy']);
    $routes->post('events/gallery/update/(:segment)/(:segment)', 'Events::updateGallery/$1/$2', ['as' => 'harbor.events.gallery.update']);
    // Popups
    $routes->get('popups', 'Popups::index', ['as' => 'harbor.popups.index']);
    $routes->get('popups/create', 'Popups::create', ['as' => 'harbor.popups.create']);
    $routes->post('popups/store', 'Popups::store', ['as' => 'harbor.popups.store']);
    $routes->get('popups/edit/(:segment)', 'Popups::edit/$1', ['as' => 'harbor.popups.edit']);
    $routes->post('popups/update/(:segment)', 'Popups::update/$1', ['as' => 'harbor.popups.update']);
    $routes->post('popups/destroy/(:segment)', 'Popups::destroy/$1', ['as' => 'harbor.popups.destroy']);
    // Stores
    $routes->get('stores', 'Stores::index', ['as' => 'harbor.stores.index']);
    $routes->get('stores/create', 'Stores::create', ['as' => 'harbor.stores.create']);
    $routes->post('stores/store', 'Stores::store', ['as' => 'harbor.stores.store']);
    $routes->get('stores/edit/(:segment)', 'Stores::edit/$1', ['as' => 'harbor.stores.edit']);
    $routes->post('stores/update/(:segment)', 'Stores::update/$1', ['as' => 'harbor.stores.update']);
    $routes->post('stores/destroy/(:segment)', 'Stores::destroy/$1', ['as' => 'harbor.stores.destroy']);
    // Gallery Stores
    $routes->get('stores/gallery/edit/(:segment)', 'Stores::editGallery/$1', ['as' => 'harbor.stores.gallery.edit']);
    $routes->post('stores/gallery/destroy/(:segment)/(:segment)', 'Stores::destroyGallery/$1/$2');
    $routes->post('stores/gallery/update/(:segment)/(:segment)', 'Stores::updateGallery/$1/$2');
    // Subcategories
    $routes->get('stores/subcategories/(:segment)', 'Stores::getSubcategoriesByCategoryId/$1');
    // Boletines
    $routes->get('newsletters', 'Newsletter::index', ['as' => 'harbor.newsletters.index']);
    $routes->get('newsletters/create', 'Newsletter::create', ['as' => 'harbor.newsletters.create']);
    $routes->post('newsletters/store', 'Newsletter::store', ['as' => 'harbor.newsletters.store']);
    $routes->get('newsletters/edit/(:segment)', 'Newsletter::edit/$1', ['as' => 'harbor.newsletters.edit']);
    $routes->post('newsletters/update/(:segment)', 'Newsletter::update/$1', ['as' => 'harbor.newsletters.update']);
    $routes->post('newsletters/destroy/', 'Newsletter::destroy', ['as' => 'harbor.newsletters.destroy']);
    // Micrositios
    $routes->get('microsites', 'Microsites::index', ['as' => 'harbor.microsites.index']);
    $routes->get('microsites/create', 'Microsites::create', ['as' => 'harbor.microsites.create']);
    $routes->post('microsites/store', 'Microsites::store', ['as' => 'harbor.microsites.store']);
    $routes->post('microsites/destroy', 'Microsites::destroy', ['as' => 'harbor.microsites.destroy']);
    $routes->post('microsites/activate/(:segment)', 'Microsites::activate/$1', ['as' => 'harbor.microsites.activate']);
    $routes->get('microsites/edit/(:segment)', 'Microsites::edit/$1', ['as' => 'harbor.microsites.edit']);
    $routes->post('microsites/update/(:segment)', 'Microsites::update/$1', ['as' => 'harbor.microsites.update']);
});

/********************************************************************
*
*  Rutas -- Marina Puerto Cancún
*
********************************************************************/
$routes->group('marina', ['namespace' => 'App\Controllers\Thorurbana\Marina', 'filter' => 'auth:admin,thor'], function($routes) {
    $routes->get('dashboard', 'Dashboard::index', ['as' => 'marina.dashboard']);
    // Novedades
    $routes->get('news', 'News::index', ['as' => 'marina.news.index']);
    $routes->get('news/create', 'News::create', ['as' => 'marina.news.create']);
    $routes->post('news/store', 'News::store', ['as' => 'marina.news.store']);
    $routes->post('news/destroy', 'News::destroy', ['as' => 'marina.news.destroy']);
    $routes->get('news/edit/(:segment)', 'News::edit/$1', ['as' => 'marina.news.edit']);
    $routes->post('news/update/(:segment)', 'News::update/$1', ['as' => 'marina.news.update']);
    // Eventos
    $routes->get('events', 'Events::index', ['as' => 'marina.events.index']);
    $routes->get('events/create', 'Events::create', ['as' => 'marina.events.create']);
    $routes->post('events/store', 'Events::store', ['as' => 'marina.events.store']);
    $routes->post('events/destroy', 'Events::destroy', ['as' => 'marina.events.destroy']);
    $routes->get('events/edit/(:segment)', 'Events::edit/$1', ['as' => 'marina.events.edit']);
    $routes->post('events/update/(:segment)', 'Events::update/$1', ['as' => 'marina.events.update']);
    // Gallería Eventos
    $routes->get('events/gallery/edit/(:segment)', 'Events::editGallery/$1', ['as' => 'marina.events.gallery.edit']);
    $routes->post('events/gallery/destroy/(:segment)/(:segment)', 'Events::destroyGallery/$1/$2', ['as' => 'marina.events.gallery.destroy']);
    $routes->post('events/gallery/update/(:segment)/(:segment)', 'Events::updateGallery/$1/$2', ['as' => 'marina.events.gallery.update']);
    // Popups
    $routes->get('popups', 'Popups::index', ['as' => 'marina.popups.index']);
    $routes->get('popups/create', 'Popups::create', ['as' => 'marina.popups.create']);
    $routes->post('popups/store', 'Popups::store', ['as' => 'marina.popups.store']);
    $routes->get('popups/edit/(:segment)', 'Popups::edit/$1', ['as' => 'marina.popups.edit']);
    $routes->post('popups/update/(:segment)', 'Popups::update/$1', ['as' => 'marina.popups.update']);
    $routes->post('popups/destroy/(:segment)', 'Popups::destroy/$1', ['as' => 'marina.popups.destroy']);
    // Stores
    $routes->get('stores', 'Stores::index', ['as' => 'marina.stores.index']);
    $routes->get('stores/create', 'Stores::create', ['as' => 'marina.stores.create']);
    $routes->post('stores/store', 'Stores::store', ['as' => 'marina.stores.store']);
    $routes->get('stores/edit/(:segment)', 'Stores::edit/$1', ['as' => 'marina.stores.edit']);
    $routes->post('stores/update/(:segment)', 'Stores::update/$1', ['as' => 'marina.stores.update']);
    $routes->post('stores/destroy/(:segment)', 'Stores::destroy/$1', ['as' => 'marina.stores.destroy']);
    // Gallery Stores
    $routes->get('stores/gallery/edit/(:segment)', 'Stores::editGallery/$1', ['as' => 'marina.stores.gallery.edit']);
    $routes->post('stores/gallery/destroy/(:segment)/(:segment)', 'Stores::destroyGallery/$1/$2');
    $routes->post('stores/gallery/update/(:segment)/(:segment)', 'Stores::updateGallery/$1/$2');
    // Subcategories
    $routes->get('stores/subcategories/(:segment)', 'Stores::getSubcategoriesByCategoryId/$1');
    // Boletines
    $routes->get('newsletters', 'Newsletter::index', ['as' => 'marina.newsletters.index']);
    $routes->get('newsletters/create', 'Newsletter::create', ['as' => 'marina.newsletters.create']);
    $routes->post('newsletters/store', 'Newsletter::store', ['as' => 'marina.newsletters.store']);
    $routes->get('newsletters/edit/(:segment)', 'Newsletter::edit/$1', ['as' => 'marina.newsletters.edit']);
    $routes->post('newsletters/update/(:segment)', 'Newsletter::update/$1', ['as' => 'marina.newsletters.update']);
    $routes->post('newsletters/destroy/', 'Newsletter::destroy', ['as' => 'marina.newsletters.destroy']);
    // Micrositios
    $routes->get('microsites', 'Microsites::index', ['as' => 'marina.microsites.index']);
    $routes->get('microsites/create', 'Microsites::create', ['as' => 'marina.microsites.create']);
    $routes->post('microsites/store', 'Microsites::store', ['as' => 'marina.microsites.store']);
    $routes->post('microsites/destroy', 'Microsites::destroy', ['as' => 'marina.microsites.destroy']);
    $routes->post('microsites/activate/(:segment)', 'Microsites::activate/$1', ['as' => 'marina.microsites.activate']);
    $routes->get('microsites/edit/(:segment)', 'Microsites::edit/$1', ['as' => 'marina.microsites.edit']);
    $routes->post('microsites/update/(:segment)', 'Microsites::update/$1', ['as' => 'marina.microsites.update']);
});

/********************************************************************
*
*  Rutas -- Calle Corazón
*
********************************************************************/
$routes->group('corazon', ['namespace' => 'App\Controllers\Thorurbana\Corazon', 'filter' => 'auth:admin,thor'], function($routes) {
    $routes->get('dashboard', 'Dashboard::index', ['as' => 'corazon.dashboard']);
    // Novedades
    $routes->get('news', 'News::index', ['as' => 'corazon.news.index']);
    $routes->get('news/create', 'News::create', ['as' => 'corazon.news.create']);
    $routes->post('news/store', 'News::store', ['as' => 'corazon.news.store']);
    $routes->post('news/destroy', 'News::destroy', ['as' => 'corazon.news.destroy']);
    $routes->get('news/edit/(:segment)', 'News::edit/$1', ['as' => 'corazon.news.edit']);
    $routes->post('news/update/(:segment)', 'News::update/$1', ['as' => 'corazon.news.update']);
    // Eventos
    $routes->get('events', 'Events::index', ['as' => 'corazon.events.index']);
    $routes->get('events/create', 'Events::create', ['as' => 'corazon.events.create']);
    $routes->post('events/store', 'Events::store', ['as' => 'corazon.events.store']);
    $routes->post('events/destroy', 'Events::destroy', ['as' => 'corazon.events.destroy']);
    $routes->get('events/edit/(:segment)', 'Events::edit/$1', ['as' => 'corazon.events.edit']);
    $routes->post('events/update/(:segment)', 'Events::update/$1', ['as' => 'corazon.events.update']);
    // Gallería Eventos
    $routes->get('events/gallery/edit/(:segment)', 'Events::editGallery/$1', ['as' => 'corazon.events.gallery.edit']);
    $routes->post('events/gallery/destroy/(:segment)/(:segment)', 'Events::destroyGallery/$1/$2', ['as' => 'corazon.events.gallery.destroy']);
    $routes->post('events/gallery/update/(:segment)/(:segment)', 'Events::updateGallery/$1/$2', ['as' => 'corazon.events.gallery.update']);
    // Popups
    $routes->get('popups', 'Popups::index', ['as' => 'corazon.popups.index']);
    $routes->get('popups/create', 'Popups::create', ['as' => 'corazon.popups.create']);
    $routes->post('popups/store', 'Popups::store', ['as' => 'corazon.popups.store']);
    $routes->get('popups/edit/(:segment)', 'Popups::edit/$1', ['as' => 'corazon.popups.edit']);
    $routes->post('popups/update/(:segment)', 'Popups::update/$1', ['as' => 'corazon.popups.update']);
    $routes->post('popups/destroy/(:segment)', 'Popups::destroy/$1', ['as' => 'corazon.popups.destroy']);
    // Stores
    $routes->get('stores', 'Stores::index', ['as' => 'corazon.stores.index']);
    $routes->get('stores/create', 'Stores::create', ['as' => 'corazon.stores.create']);
    $routes->post('stores/store', 'Stores::store', ['as' => 'corazon.stores.store']);
    $routes->get('stores/edit/(:segment)', 'Stores::edit/$1', ['as' => 'corazon.stores.edit']);
    $routes->post('stores/update/(:segment)', 'Stores::update/$1', ['as' => 'corazon.stores.update']);
    $routes->post('stores/destroy/(:segment)', 'Stores::destroy/$1', ['as' => 'corazon.stores.destroy']);
    // Gallery Stores
    $routes->get('stores/gallery/edit/(:segment)', 'Stores::editGallery/$1', ['as' => 'corazon.stores.gallery.edit']);
    $routes->post('stores/gallery/destroy/(:segment)/(:segment)', 'Stores::destroyGallery/$1/$2');
    $routes->post('stores/gallery/update/(:segment)/(:segment)', 'Stores::updateGallery/$1/$2');
    // Subcategories
    $routes->get('stores/subcategories/(:segment)', 'Stores::getSubcategoriesByCategoryId/$1');
    // Boletines
    $routes->get('newsletters', 'Newsletter::index', ['as' => 'corazon.newsletters.index']);
    $routes->get('newsletters/create', 'Newsletter::create', ['as' => 'corazon.newsletters.create']);
    $routes->post('newsletters/store', 'Newsletter::store', ['as' => 'corazon.newsletters.store']);
    $routes->get('newsletters/edit/(:segment)', 'Newsletter::edit/$1', ['as' => 'corazon.newsletters.edit']);
    $routes->post('newsletters/update/(:segment)', 'Newsletter::update/$1', ['as' => 'corazon.newsletters.update']);
    $routes->post('newsletters/destroy/', 'Newsletter::destroy', ['as' => 'corazon.newsletters.destroy']);
    // Micrositios
    $routes->get('microsites', 'Microsites::index', ['as' => 'corazon.microsites.index']);
    $routes->get('microsites/create', 'Microsites::create', ['as' => 'corazon.microsites.create']);
    $routes->post('microsites/store', 'Microsites::store', ['as' => 'corazon.microsites.store']);
    $routes->post('microsites/destroy', 'Microsites::destroy', ['as' => 'corazon.microsites.destroy']);
    $routes->post('microsites/activate/(:segment)', 'Microsites::activate/$1', ['as' => 'corazon.microsites.activate']);
    $routes->get('microsites/edit/(:segment)', 'Microsites::edit/$1', ['as' => 'corazon.microsites.edit']);
    $routes->post('microsites/update/(:segment)', 'Microsites::update/$1', ['as' => 'corazon.microsites.update']);
});

/********************************************************************
*
*  Rutas -- Landmark Guadalajara
*
********************************************************************/
$routes->group('landmark', ['namespace' => 'App\Controllers\Thorurbana\Landmark', 'filter' => 'auth:admin,thor'], function($routes) {
    $routes->get('dashboard', 'Dashboard::index', ['as' => 'landmark.dashboard']);
    // Novedades
    $routes->get('news', 'News::index', ['as' => 'landmark.news.index']);
    $routes->get('news/create', 'News::create', ['as' => 'landmark.news.create']);
    $routes->post('news/store', 'News::store', ['as' => 'landmark.news.store']);
    $routes->post('news/destroy', 'News::destroy', ['as' => 'landmark.news.destroy']);
    $routes->get('news/edit/(:segment)', 'News::edit/$1', ['as' => 'landmark.news.edit']);
    $routes->post('news/update/(:segment)', 'News::update/$1', ['as' => 'landmark.news.update']);
    // Eventos
    $routes->get('events', 'Events::index', ['as' => 'landmark.events.index']);
    $routes->get('events/create', 'Events::create', ['as' => 'landmark.events.create']);
    $routes->post('events/store', 'Events::store', ['as' => 'landmark.events.store']);
    $routes->post('events/destroy', 'Events::destroy', ['as' => 'landmark.events.destroy']);
    $routes->get('events/edit/(:segment)', 'Events::edit/$1', ['as' => 'landmark.events.edit']);
    $routes->post('events/update/(:segment)', 'Events::update/$1', ['as' => 'landmark.events.update']);
    // Gallería Eventos
    $routes->get('events/gallery/edit/(:segment)', 'Events::editGallery/$1', ['as' => 'landmark.events.gallery.edit']);
    $routes->post('events/gallery/destroy/(:segment)/(:segment)', 'Events::destroyGallery/$1/$2', ['as' => 'landmark.events.gallery.destroy']);
    $routes->post('events/gallery/update/(:segment)/(:segment)', 'Events::updateGallery/$1/$2', ['as' => 'landmark.events.gallery.update']);
    // Popups
    $routes->get('popups', 'Popups::index', ['as' => 'landmark.popups.index']);
    $routes->get('popups/create', 'Popups::create', ['as' => 'landmark.popups.create']);
    $routes->post('popups/store', 'Popups::store', ['as' => 'landmark.popups.store']);
    $routes->get('popups/edit/(:segment)', 'Popups::edit/$1', ['as' => 'landmark.popups.edit']);
    $routes->post('popups/update/(:segment)', 'Popups::update/$1', ['as' => 'landmark.popups.update']);
    $routes->post('popups/destroy/(:segment)', 'Popups::destroy/$1', ['as' => 'landmark.popups.destroy']);
    // Stores
    $routes->get('stores', 'Stores::index', ['as' => 'landmark.stores.index']);
    $routes->get('stores/create', 'Stores::create', ['as' => 'landmark.stores.create']);
    $routes->post('stores/store', 'Stores::store', ['as' => 'landmark.stores.store']);
    $routes->get('stores/edit/(:segment)', 'Stores::edit/$1', ['as' => 'landmark.stores.edit']);
    $routes->post('stores/update/(:segment)', 'Stores::update/$1', ['as' => 'landmark.stores.update']);
    $routes->post('stores/destroy/(:segment)', 'Stores::destroy/$1', ['as' => 'landmark.stores.destroy']);
    // Gallery Stores
    $routes->get('stores/gallery/edit/(:segment)', 'Stores::editGallery/$1', ['as' => 'landmark.stores.gallery.edit']);
    $routes->post('stores/gallery/destroy/(:segment)/(:segment)', 'Stores::destroyGallery/$1/$2');
    $routes->post('stores/gallery/update/(:segment)/(:segment)', 'Stores::updateGallery/$1/$2');
    // Subcategories
    $routes->get('stores/subcategories/(:segment)', 'Stores::getSubcategoriesByCategoryId/$1');
    // Micrositios
    $routes->get('microsites', 'Microsites::index', ['as' => 'landmark.microsites.index']);
    $routes->get('microsites/create', 'Microsites::create', ['as' => 'landmark.microsites.create']);
    $routes->post('microsites/store', 'Microsites::store', ['as' => 'landmark.microsites.store']);
    $routes->post('microsites/destroy', 'Microsites::destroy', ['as' => 'landmark.microsites.destroy']);
    $routes->post('microsites/activate/(:segment)', 'Microsites::activate/$1', ['as' => 'landmark.microsites.activate']);
    $routes->get('microsites/edit/(:segment)', 'Microsites::edit/$1', ['as' => 'landmark.microsites.edit']);
    $routes->post('microsites/update/(:segment)', 'Microsites::update/$1', ['as' => 'landmark.microsites.update']);
});

/********************************************************************
*
*  Rutas -- Altavista 147
*
********************************************************************/
$routes->group('altavista', ['namespace' => 'App\Controllers\Thorurbana\Altavista', 'filter' => 'auth:admin,thor'], function($routes) {
    $routes->get('dashboard', 'Dashboard::index', ['as' => 'altavista.dashboard']);
    // Novedades
    $routes->get('news', 'News::index', ['as' => 'altavista.news.index']);
    $routes->get('news/create', 'News::create', ['as' => 'altavista.news.create']);
    $routes->post('news/store', 'News::store', ['as' => 'altavista.news.store']);
    $routes->post('news/destroy', 'News::destroy', ['as' => 'altavista.news.destroy']);
    $routes->get('news/edit/(:segment)', 'News::edit/$1', ['as' => 'altavista.news.edit']);
    $routes->post('news/update/(:segment)', 'News::update/$1', ['as' => 'altavista.news.update']);
    // Eventos
    $routes->get('events', 'Events::index', ['as' => 'altavista.events.index']);
    $routes->get('events/create', 'Events::create', ['as' => 'altavista.events.create']);
    $routes->post('events/store', 'Events::store', ['as' => 'altavista.events.store']);
    $routes->post('events/destroy', 'Events::destroy', ['as' => 'altavista.events.destroy']);
    $routes->get('events/edit/(:segment)', 'Events::edit/$1', ['as' => 'altavista.events.edit']);
    $routes->post('events/update/(:segment)', 'Events::update/$1', ['as' => 'altavista.events.update']);
    // Gallería Eventos
    $routes->get('events/gallery/edit/(:segment)', 'Events::editGallery/$1', ['as' => 'altavista.events.gallery.edit']);
    $routes->post('events/gallery/destroy/(:segment)/(:segment)', 'Events::destroyGallery/$1/$2', ['as' => 'altavista.events.gallery.destroy']);
    $routes->post('events/gallery/update/(:segment)/(:segment)', 'Events::updateGallery/$1/$2', ['as' => 'altavista.events.gallery.update']);
    // Popups
    $routes->get('popups', 'Popups::index', ['as' => 'altavista.popups.index']);
    $routes->get('popups/create', 'Popups::create', ['as' => 'altavista.popups.create']);
    $routes->post('popups/store', 'Popups::store', ['as' => 'altavista.popups.store']);
    $routes->get('popups/edit/(:segment)', 'Popups::edit/$1', ['as' => 'altavista.popups.edit']);
    $routes->post('popups/update/(:segment)', 'Popups::update/$1', ['as' => 'altavista.popups.update']);
    $routes->post('popups/destroy/(:segment)', 'Popups::destroy/$1', ['as' => 'altavista.popups.destroy']);
    // Stores
    $routes->get('stores', 'Stores::index', ['as' => 'altavista.stores.index']);
    $routes->get('stores/create', 'Stores::create', ['as' => 'altavista.stores.create']);
    $routes->post('stores/store', 'Stores::store', ['as' => 'altavista.stores.store']);
    $routes->get('stores/edit/(:segment)', 'Stores::edit/$1', ['as' => 'altavista.stores.edit']);
    $routes->post('stores/update/(:segment)', 'Stores::update/$1', ['as' => 'altavista.stores.update']);
    $routes->post('stores/destroy/(:segment)', 'Stores::destroy/$1', ['as' => 'altavista.stores.destroy']);
    // Gallery Stores
    $routes->get('stores/gallery/edit/(:segment)', 'Stores::editGallery/$1', ['as' => 'altavista.stores.gallery.edit']);
    $routes->post('stores/gallery/destroy/(:segment)/(:segment)', 'Stores::destroyGallery/$1/$2');
    $routes->post('stores/gallery/update/(:segment)/(:segment)', 'Stores::updateGallery/$1/$2');
    // Subcategories
    $routes->get('stores/subcategories/(:segment)', 'Stores::getSubcategoriesByCategoryId/$1');
});


/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
