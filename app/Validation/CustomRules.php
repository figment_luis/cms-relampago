<?php
namespace App\Validation;

class CustomRules 
{

    // Regla para validar fechas de inicio (popups)
    public function startdateValidation(string $str, string $fields, array $data) 
    {
        // Verificar que la fecha de inicio sea mayor o igual al dia de hoy
        if ( date('U', strtotime($data['start_date'])) >= time() ) {
            return true;
        } else {
            return false;
        }
    }

    // Regla para validar fechas de cierre (popups)
    public function enddateValidation(string $str, string $fields, array $data) 
    {
        // Verificar que la fecha de inicio sea mayor o igual al dia de hoy
        if ( date('U', strtotime($data['end_date'])) > date('U', strtotime($data['start_date'])) ) {
            return true;
        } else {
            return false;
        }
    }

    // Regla para validar fechas de cierre (popups)
    public function enddateValidationSlide(string $str, string $fields, array $data) 
    {
        // Verificar que la fecha de inicio sea mayor o igual al dia de hoy
        if ( date('U', strtotime($data['end_date'])) > date('U', strtotime(date('Y-m-d H:i:s'))) ) {
            return true;
        } else {
            return false;
        }
    }

    // Regla para validar fechas de cierre actualizadas (popups)
    public function endupdatedateValidation(string $str, string $fields, array $data) 
    {
        // Verificar que la fecha de inicio sea mayor o igual al dia de hoy
        if ( date('U', strtotime($data['end_date'])) >= time() && 
             date('U', strtotime($data['end_date'])) > date('U', strtotime($data['start_date'])) ) {
            return true;
        } else {
            return false;
        }
    }

}