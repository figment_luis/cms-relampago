<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateLogsTable extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id' => [
				'type' 				=> 'INT',
				'unsigned' 			=> TRUE,
				'auto_increment' 	=> TRUE,
			],
			'description' => [
				'type' 				=> 'VARCHAR',
				'constraint' 		=> 200,
			],
			'user_id' => [
                'type' => 'INT',
                'unsigned' => true,
                'null' => true,
            ],
			'created_at' => [
				'type' => 'DATETIME',
			],
		]);

		$this->forge->addPrimaryKey('id');
		$this->forge->addForeignKey('user_id', 'users', 'id', 'CASCADE', 'SET NULL');
		$this->forge->createTable('logs');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('logs');
	}
}
