<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateAreasTable extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id' => [
				'type' 				=> 'INT',
				'unsigned' 			=> TRUE,
				'auto_increment' 	=> TRUE,
			],
			'name' => [
				'type' 				=> 'VARCHAR',
				'constraint' 		=> 30,
				'unique' 			=> TRUE,
			],
			'created_at' => [
				'type' => 'DATETIME',
			],
			'updated_at' => [
				'type' => 'DATETIME',
				'null' => TRUE,
			],
			'deleted_at' => [
				'type' => 'DATETIME',
				'null' => TRUE,
			]
		]);

		$this->forge->addPrimaryKey('id');
		$this->forge->createTable('areas');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('areas');
	}
}
