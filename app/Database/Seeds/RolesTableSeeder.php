<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class RolesTableSeeder extends Seeder
{
	public function run()
	{
		$roles = [
            [
                'name' => 'admin',
                'display_name' => 'Administrador de Sistema',
                'created_at' => '2021-05-10 12:00:00'
            ],
            [
                'name' => 'user',
                'display_name' => 'Operador de Sistema',
                'created_at' => '2021-05-10 12:00:00'
			],
        ];

        $builder = $this->db->table('roles');
        $builder->insertBatch($roles);
	}
}
