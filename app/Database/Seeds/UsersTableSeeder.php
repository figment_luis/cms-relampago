<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class UsersTableSeeder extends Seeder
{
	public function run()
	{
		$users = [
            [
                'first_name'    => 'Luis Antonio',
                'last_name'     => 'Rojas Carbellido',
				'email'         => 'luis.figment@gmail.com',
				'password'      => password_hash('123456789', PASSWORD_BCRYPT),
                'area_id'       => 1,
				'role_id'       => 1,
                'created_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'first_name'    => 'Marco Antonio',
                'last_name'     => 'Salazar',
				'email'         => 'marco.figment@gmail.com',
				'password'      => password_hash('123456789', PASSWORD_BCRYPT),
                'area_id'       => 1,
				'role_id'       => 1,
                'created_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'first_name'    => 'Jorge Eduardo',
                'last_name'     => 'Aguilar Díaz',
				'email'         => 'jorge.aguilar.figment@gmail.com',
				'password'      => password_hash('123456789', PASSWORD_BCRYPT),
                'area_id'       => 1,
				'role_id'       => 1,
                'created_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'first_name'    => 'Mauricio Alejandro',
                'last_name'     => 'Hernández Gómez',
				'email'         => 'mauricio.figment@gmail.com',
				'password'      => password_hash('123456789', PASSWORD_BCRYPT),
                'area_id'       => 1,
				'role_id'       => 1,
                'created_at'    => date('Y-m-d H:i:s'),
            ],
            [
                'first_name'    => 'Alejandro',
                'last_name'     => 'González Reyes',
				'email'         => 'alejandro.figment@gmail.com',
				'password'      => password_hash('123456789', PASSWORD_BCRYPT),
                'area_id'       => 1,
				'role_id'       => 1,
                'created_at'    => date('Y-m-d H:i:s'),
            ],
        ];

        $builder = $this->db->table('users');
        $builder->insertBatch($users);
	}
}
