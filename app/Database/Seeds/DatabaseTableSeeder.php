<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class DatabaseTableSeeder extends Seeder
{
	public function run()
	{
		$this->call('AreasTableSeeder');
		$this->call('RolesTableSeeder');
		$this->call('UsersTableSeeder');
	}
}
