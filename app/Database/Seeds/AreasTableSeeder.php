<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class AreasTableSeeder extends Seeder
{
	public function run()
	{
		$areas = [
            [
                'name' 			=> 'Development',
                'created_at' 	=> '2021-05-10 12:00:00'
            ],
            [
                'name' 			=> 'Design',
                'created_at' 	=> '2021-05-10 12:00:00'
			],
			[
                'name' 			=> 'Social Media',
                'created_at' 	=> '2021-05-10 12:00:00'
			],
        ];

        $builder = $this->db->table('areas');
        $builder->insertBatch($areas);
	}
}
