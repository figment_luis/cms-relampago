<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\UserModel;
use App\Models\RoleModel;
use App\Models\AreaModel;

class RegisterController extends Controller {

    public function create() {
        $roleModel = new RoleModel();
        $areaModel = new AreaModel();

        $roles = $roleModel->findAll();
        $areas = $areaModel->findAll();
        
        return view('users/register', compact('roles', 'areas'));
    }

    public function store() {
        $rules = [
            'firstname' => [
                'label' => 'nombre del usuario',
                'rules' => 'trim|required|min_length[3]|max_length[20]',
                'errors' => [
                    'required' => 'El {field} es requerido',
                    'min_length' => 'El {field} debe tener al menos {param} caracteres',
                    'max_length' => 'El {field} no debe exceder los {param} caracteres'
                ]
            ],
            'lastname' => [
                'label' => 'apellidos del usuario',
                'rules' => 'trim|required|min_length[5]|max_length[30]',
                'errors' => [
                    'required' => 'Los {field} son un dato requerido',
                    'min_length' => 'Los {field} deben tener al menos {param} caracteres',
                    'max_length' => 'Los {field} no deben exceder los {param} caracteres'
                ]
            ],
            'email' => [
                'label' => 'correo electrónico',
                'rules' => 'trim|required|valid_email|is_unique[users.email]',
                'errors' => [
                    'required' => 'El {field} es requerido',
                    'valid_email' => 'El {field} proporcionado no parece ser un dato válido',
                    'is_unique' => 'El {field} proporcionado ya se encuentra en uso'
                ]
            ],
            'role' => [
                'label' => 'rol de usuario',
                'rules' => 'required|numeric',
                'errors' => [
                    'required' => 'El {field} es un dato requerido',
                    'numeric' => 'El {field} debe ser un valor numérico'
                ]
            ],
            'area' => [
                'label' => 'area de usuario',
                'rules' => 'required|numeric',
                'errors' => [
                    'required' => 'El {field} es un dato requerido',
                    'numeric' => 'El {field} debe ser un valor numérico'
                ]
            ],
            'password' => [
                'label' => 'contraseña del usuario',
                'rules' => 'required|min_length[8]|max_length[16]',
                'errors' => [
                    'required' => 'La {field} es un dato requerido',
                    'min_length' => 'La {field} debe tener al menos {param} caracteres',
                    'max_length' => 'La {field} no debe exceder los {param} caracteres'
                ]
            ],
            'confirmpassword' => [
                'label' => 'confirmación de la contraseña',
                'rules' => 'matches[password]',
                'errors' => [
                    'matches' => 'La {field} parece ser un dato diferente',
                ]
            ]
        ];

        $input = $this->validate($rules);

        $response = [];

        if (!$input) {
            $response["status"]     = "invalid";
            $response["code"]       = 406;
            $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
            $response['validation'] = $this->validator->getErrors();

            return json_encode($response);
        }

        // Almacenar información en base de datos
        $userModel = new UserModel();

        $result = $userModel->insert([
            'first_name' => $this->request->getPost('firstname'),
            'last_name'  => $this->request->getPost('lastname'),
            'email'      => $this->request->getPost('email'),
            'password'   => password_hash($this->request->getPost('password'), PASSWORD_BCRYPT),
            'area_id'    => $this->request->getPost('area'),
            'role_id'    => $this->request->getPost('role'),
            'created_at' => date('Y-m-d H:i:s'),
        ]);

        // Salida de resultados
        if ($result !== 0) {
            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "El usuario se registró satisfactoriamente en el sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible registrar el usuario en el sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }

}