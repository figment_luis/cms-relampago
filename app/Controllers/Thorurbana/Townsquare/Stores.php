<?php
namespace App\Controllers\Thorurbana\Townsquare;
use App\Controllers\BaseController;
use App\Models\Thorurbana\Townsquare\StoresModel;
use App\Models\LogModel;

class Stores extends BaseController
{

    public function index()
    {
        try {
            helper('text');
            $storesModel = new StoresModel();
            $stores      = $storesModel->getAllStores()->getResult();

            return view('thorurbana/townsquare/stores/index', compact('stores'));
        } catch (\Exception $e) {
            $message['site'] = "Town Square Metepec";
            $message['section'] = "Listar Tiendas";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function create()
    {
        try {
            $storesModel    = new StoresModel();
            $categories     = $storesModel->getCategories()->getResult();
            $locations      = $storesModel->getLocations()->getResult();
            // El listado de subcategorías se obtiene mediante ajax con base a la categoría selecciona en el formulario
            
            return view('thorurbana/townsquare/stores/create', compact('categories', 'locations'));
        } catch (\Exception $e) {
            $message['site'] = "Town Square Metepec";
            $message['section'] = "Registrar Tienda";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function store()
    {
        try {
            // Validación de datos
            $rules = [
                'name' => [
                    'label' => 'Nombre',
                    'rules' => 'trim|required|min_length[3]|max_length[50]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'phone' => [
                    'label' => 'Teléfono',
                    'rules' => 'trim|permit_empty|min_length[14]|max_length[14]',
                    'errors' => [
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'Los {field} no deben exceder los {param} caracteres'
                    ]
                ],
                'phone2' => [
                    'label' => 'Teléfono Alternativo',
                    'rules' => 'trim|permit_empty|min_length[14]|max_length[14]',
                    'errors' => [
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'Los {field} no deben exceder los {param} caracteres'
                    ]
                ],
                'whatsapp' => [
                    'label' => 'WhatsApp',
                    'rules' => 'trim|permit_empty|min_length[15]|max_length[15]',
                    'errors' => [
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'Los {field} no deben exceder los {param} caracteres'
                    ]
                ],
                'hourhand' => [
                    'label' => 'Horarios',
                    'rules' => 'trim|required|min_length[3]|max_length[150]',
                    'errors' => [
                        'required' => 'Los {field} son un dato requerido',
                        'min_length' => 'Los {field} deben tener al menos {param} caracteres',
                        'max_length' => 'Los {field} no deben exceder los {param} caracteres'
                    ]
                ],
                'url' => [
                    'label' => 'Sitio Web',
                    'rules' => 'trim|permit_empty|valid_url',
                    'errors' => [
                        'valid_url' => 'El {field} no contiene una URL válida'
                    ]
                ],
                'correo' => [
                    'label' => 'Email',
                    'rules' => 'trim|permit_empty|valid_email',
                    'errors' => [
                        'valid_email' => 'El {field} parece ser un dato no válido',
                    ]
                ],
                'keywords' => [
                    'label' => 'Etiquetas',
                    'rules' => 'trim|required|min_length[10]',
                    'errors' => [
                        'required' => 'Las {field} son un dato requerido',
                        'min_length' => 'Las {field} deben tener al menos {param} caracteres'
                    ]
                ],
                'location' => [
                    'label' => 'Ubicación',
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                    ]
                ],
                'category' => [
                    'label' => 'Categoría',
                    'rules' => 'required|numeric',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                        'numeric' => 'La {field} debe ser un valor numérico'
                    ]
                ],
                'description' => [
                    'label' => 'Descripción',
                    'rules' => 'trim|required|min_length[20]|max_length[2000]',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                        'min_length' => 'La {field} debe tener al menos {param} caracteres',
                        'max_length' => 'La {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'subcategories.*' => [
                    'label' => 'Subcategorías',
                    'rules' => 'trim|required|numeric',
                    'errors' => [
                        'required' => 'Las {field} son un dato requerido',
                        'numeric' => 'Las {field} deben ser un valor numérico',
                    ]
                ],
                'url_booking' => [
                    'label' => 'Link de reservación',
                    'rules' => 'trim|permit_empty|valid_url',
                    'errors' => [
                        'valid_url' => 'El {field} no contiene una URL válida'
                    ]
                ],
            ];

            $input = $this->validate($rules);

            $response = [];

            if (!$input) {
                $response["status"]     = "invalid";
                $response["code"]       = 406;
                $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
                $response['validation'] = $this->validator->getErrors();

                return json_encode($response);
            }

            // Almacenar información en la base de datos
            $name           = $this->request->getPost('name');
            $description    = $this->request->getPost('description');
            $email          = $this->request->getPost('correo');
            $url            = $this->request->getPost('url');
            $thor2go        = $this->request->getPost('thor2go');
            $phone          = $this->request->getPost('phone');
            $phone_ext      = $this->request->getPost('phone_ext');
            $phone2         = $this->request->getPost('phone2');
            $phone2_ext     = $this->request->getPost('phone2_ext');
            $hourhand       = $this->request->getPost('hourhand');
            $keywords       = $this->request->getPost('keywords');
            $location       = $this->request->getPost('location');
            $category       = $this->request->getPost('category');
            $subcategories  = $this->request->getPost('subcategories');
            $qrcode         = $this->request->getFile('qr');
            $galleryFiles   = $this->request->getFiles();
            $whatsapp       = $this->request->getPost('whatsapp');
            $pets_friendly  = $this->request->getPost('pets_friendly');
            $url_booking    = $this->request->getPost('url_booking');

            $storesModel    = new StoresModel();
            $result         = $storesModel->createStore($name, $description, $email, $url, $thor2go, $phone, $phone2, $hourhand, $keywords, $location, $category, $subcategories, $qrcode, $galleryFiles, $phone_ext, $phone2_ext, $whatsapp, $pets_friendly, $url_booking);

            // Salida de resultados
            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($name, "Town Square Metepec", "Alta en Tienda");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "La tienda se registró satisfactoriamente en el sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible registrar la tienda en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    public function edit($id)
    {
        try {
            $storesModel            = new StoresModel();
            $store                  = $storesModel->getStore($id)->getRow();
            $currentCategory        = $storesModel->getCategoryStore($id)->getRow();
            $currentSubcategories   = $storesModel->getSubCategoriesStore($id)->getResult();
            
            $categories             = $storesModel->getCategories()->getResult();
            // Obtener solo el listado de subcategorías relacionadas con la categoría actual
            $subcategories          = $storesModel->getSubcategoriesByCategoryId($currentCategory->category_id)->getResult();

            $locations =            $storesModel->getLocations()->getResult();
            
            return view('thorurbana/townsquare/stores/edit', compact('store', 'currentCategory', 'currentSubcategories', 'categories', 'subcategories', 'locations'));
        } catch (\Exception $e) {
            $message['site'] = "Town Square Metepec";
            $message['section'] = "Editar Tienda";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function update($id)
    {
        try {
            // Validación de datos
            $rules = [
                'name' => [
                    'label' => 'Nombre',
                    'rules' => 'trim|required|min_length[3]|max_length[50]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'phone' => [
                    'label' => 'Teléfono',
                    'rules' => 'trim|permit_empty|min_length[14]|max_length[14]',
                    'errors' => [
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'Los {field} no deben exceder los {param} caracteres'
                    ]
                ],
                'phone2' => [
                    'label' => 'Teléfono Alternativo',
                    'rules' => 'trim|permit_empty|min_length[14]|max_length[14]',
                    'errors' => [
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'Los {field} no deben exceder los {param} caracteres'
                    ]
                ],
                'whatsapp' => [
                    'label' => 'WhatsApp',
                    'rules' => 'trim|permit_empty|min_length[15]|max_length[15]',
                    'errors' => [
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'Los {field} no deben exceder los {param} caracteres'
                    ]
                ],
                'hourhand' => [
                    'label' => 'Horarios',
                    'rules' => 'trim|required|min_length[3]|max_length[150]',
                    'errors' => [
                        'required' => 'Los {field} son un dato requerido',
                        'min_length' => 'Los {field} deben tener al menos {param} caracteres',
                        'max_length' => 'Los {field} no deben exceder los {param} caracteres'
                    ]
                ],
                'url' => [
                    'label' => 'Sitio Web',
                    'rules' => 'trim|permit_empty|valid_url',
                    'errors' => [
                        'valid_url' => 'El {field} no contiene una URL válida'
                    ]
                ],
                'correo' => [
                    'label' => 'Email',
                    'rules' => 'trim|permit_empty|valid_email',
                    'errors' => [
                        'valid_email' => 'El {field} parece ser un dato no válido',
                    ]
                ],
                'keywords' => [
                    'label' => 'Etiquetas',
                    'rules' => 'trim|required|min_length[10]',
                    'errors' => [
                        'required' => 'Las {field} son un dato requerido',
                        'min_length' => 'Las {field} deben tener al menos {param} caracteres'
                    ]
                ],
                'location' => [
                    'label' => 'Ubicación',
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                    ]
                ],
                'category' => [
                    'label' => 'Categoría',
                    'rules' => 'required|numeric',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                        'numeric' => 'La {field} debe ser un valor numérico'
                    ]
                ],
                'description' => [
                    'label' => 'Descripción',
                    'rules' => 'trim|required|min_length[20]|max_length[2000]',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                        'min_length' => 'La {field} debe tener al menos {param} caracteres',
                        'max_length' => 'La {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'subcategories.*' => [
                    'label' => 'Subcategorías',
                    'rules' => 'trim|required|numeric',
                    'errors' => [
                        'required' => 'Las {field} son un dato requerido',
                        'numeric' => 'Las {field} deben ser un valor numérico',
                    ]
                ],
                'url_booking' => [
                    'label' => 'Link de reservación',
                    'rules' => 'trim|permit_empty|valid_url',
                    'errors' => [
                        'valid_url' => 'El {field} no contiene una URL válida'
                    ]
                ],
            ];

            $input = $this->validate($rules);

            $response = [];

            if (!$input) {
                $response["status"]     = "invalid";
                $response["code"]       = 406;
                $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
                $response['validation'] = $this->validator->getErrors();

                return json_encode($response);
            }

            // Almacenar información en la base de datos
            $name           = $this->request->getPost('name');
            $slug           = $this->request->getPost('slug');
            $description    = $this->request->getPost('description');
            $email          = $this->request->getPost('correo');
            $url            = $this->request->getPost('url');
            $thor2go        = $this->request->getPost('thor2go');
            $phone          = $this->request->getPost('phone');
            $phone2         = $this->request->getPost('phone2');
            $hourhand       = $this->request->getPost('hourhand');
            $keywords       = $this->request->getPost('keywords');
            $location       = $this->request->getPost('location');
            $category       = $this->request->getPost('category');
            $subcategories  = $this->request->getPost('subcategories');
            $qrcode         = $this->request->getFile('qr');
            $phone_ext      = $this->request->getPost('phone_ext');
            $phone2_ext     = $this->request->getPost('phone2_ext');
            $whatsapp       = $this->request->getPost('whatsapp');
            $pets_friendly  = $this->request->getPost('pets_friendly');
            $url_booking    = $this->request->getPost('url_booking');

            $storesModel    = new StoresModel();
            $result         = $storesModel->editStore($id, $name, $slug, $description, $email, $url, $thor2go, $phone, $phone2, $hourhand, $keywords, $location, $category, $subcategories, $qrcode, $phone_ext, $phone2_ext, $whatsapp, $pets_friendly, $url_booking);

            // Salida de resultados
            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($name, "Town Square Metepec", "Modificación en Tienda");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "La tienda se actualizó satisfactoriamente en el sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible actualizar la tienda en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    public function editGallery($id) {
        try {
            $storesModel   = new StoresModel();
            $store         = $storesModel->getStore($id)->getRow();
            $currentImages = esc($storesModel->getGalleryStore($id));

            return view('thorurbana/townsquare/stores/gallery/edit', compact('store', 'currentImages'));
        } catch (\Exception $e) {
            $message['site'] = "Town Square Metepec";
            $message['section'] = "Editar Galería de Tienda";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function destroyGallery($id, $image) {
        try {
            $response = [];
            $storesModel = new StoresModel();
            $result = $storesModel->removeGallery($id, $image);
            $myName = $storesModel->getStore($id)->getRow()->name;

            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog("Baja en Galería - " . $myName, "Town Square Metepec", "Baja en Tienda [Galería]");

                $response["success"] = "Imagen eliminada satisfactoriamente del sistema";
            } else {
                $response["error"] = "No fue posible eliminar la imagen del sistema, favor de intentar más tarde";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $message['site'] = "Town Square Metepec";
            $message['section'] = "Editar Galería de Tienda";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function updateGallery($id, $slug) {
        try {
            $images = $this->request->getFiles();

            $response = [];
            $storesModel = new StoresModel();
            $result = $storesModel->updateGallery($id, $slug, $images);
            $myName = $storesModel->getStore($id)->getRow()->name;

            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog("Modificación en Galería - " . $myName, "Town Square Metepec", "Modificación en Tienda [Galería]");
                
                $response = $result;
            } else {
                $response["error"] = "No fue posible registrar la imagen en el sistema, favor de intentar más tarde";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $message['site'] = "Town Square Metepec";
            $message['section'] = "Editar Galería de Tienda";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function destroy($id)
    {
        try {
            $response = [];
            $storesModel = new StoresModel();
            $result = $storesModel->removeStore($id);
            $myName = $storesModel->getStore($id)->getRow()->name;

            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($myName, "Town Square Metepec", "Baja en Tienda");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "La tienda se eliminó satisfactoriamente del sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible eliminar la tienda del sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    public function getSubcategoriesByCategoryId($id)
    {
        // Localizar las subcategorías relacionadas con el ID de categoría seleccionado
        try {
            $response = [];
            $storesModel = new StoresModel();
            $result = $storesModel->getSubcategoriesByCategoryId($id)->getResult();
            if ($result) {
                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["result"]     = $result;
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No existen subcategorías asociadas con la categoría seleccionada. Sin embargo, puede continuar con el proceso de registro y/o actualización.";
            }

            return json_encode($response);
        } catch(\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

}
