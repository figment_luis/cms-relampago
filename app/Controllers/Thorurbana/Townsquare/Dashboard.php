<?php
namespace App\Controllers\Thorurbana\Townsquare;
use App\Controllers\BaseController;

class Dashboard extends BaseController
{

    public function index()
    {
        return view('thorurbana/townsquare/dashboard');
    }

}
