<?php
namespace App\Controllers\Thorurbana\Harbor;
use App\Controllers\BaseController;
use App\Models\Thorurbana\Harbor\NewsletterModel;
use App\Models\LogModel;

class Newsletter extends BaseController
{

    public function index()
    {
        try {
            $newsModel = new NewsletterModel();
            $newsletters = $newsModel->getAllNewsletters()->getResult();
            //dd($newsletters);
            return view('thorurbana/harbor/newsletters/index', compact('newsletters'));
        } catch (\Exception $e) {
            $message['site'] = "The Harbor Mérida";
            $message['section'] = "Listar Boletín";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function create()
    {
        return view('thorurbana/harbor/newsletters/create');
    }

    public function store()
    {
        try {
            // Validación de datos
            $rules = [
                'title' => [
                    'label' => 'Titulo de boletín',
                    'rules' => 'trim|required|min_length[3]|max_length[50]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'link' => [
                    'label' => 'Link de boletín',
                    'rules' => 'trim|required|valid_url',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'valid_url' => 'El {field} no es una URL válida'
                    ]
                ],
            ];

            $input = $this->validate($rules);

            $response = [];

            if (!$input) {
                $response["status"]     = "invalid";
                $response["code"]       = 406;
                $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
                $response['validation'] = $this->validator->getErrors();

                return json_encode($response);
            }

            // Almacenar información en la base de datos
            $title       = $this->request->getPost('title');
            $link        = $this->request->getPost('link');

            $newsletterModel = new NewsletterModel();
            $result = $newsletterModel->createNewsletter($title, $link);

            // Salida de resultados
            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($title, "The Harbor Mérida", "Alta en Boletín");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "El boletín se registró satisfactoriamente en el sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible registrar el boletín en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    public function edit($id)
    {
        try {
            $newsletterModel = new NewsletterModel();
            $newsletter = $newsletterModel->getNewsletter($id)->getRow();

            return view('thorurbana/harbor/newsletters/edit', compact('newsletter'));
        } catch (\Exception $e) {
            $message['site'] = "The Harbor Mérida";
            $message['section'] = "Editar Boletín";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function update($id)
    {
        try {
            // Validación de datos
            $rules = [
                'title' => [
                    'label' => 'Titulo de boletín',
                    'rules' => 'trim|required|min_length[3]|max_length[50]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'link' => [
                    'label' => 'Link de boletín',
                    'rules' => 'trim|required|valid_url',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'valid_url' => 'El {field} no es una URL válida'
                    ]
                ],
            ];

            $input = $this->validate($rules);

            $response = [];

            if (!$input) {
                $response["status"]     = "invalid";
                $response["code"]       = 406;
                $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
                $response['validation'] = $this->validator->getErrors();

                return json_encode($response);
            }

            // Almacenar información en la base de datos
            $title       = $this->request->getPost('title');
            $link        = $this->request->getPost('link');

            $newsletterModel = new NewsletterModel();
            $result = $newsletterModel->editNewsletter($id, $title, $link);

            // Salida de resultados
            if ($result) {

                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($title, "The Harbor Mérida", "Modificación en Boletín");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "El boletín se actualizó satisfactoriamente en el sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible actualizar el boletín en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    public function destroy()
    {
        try {
            $id = $this->request->getPost('id');

            $response = [];
            $newsletterModel = new NewsletterModel();
            $result = $newsletterModel->removeNewsletter($id);
            $myTitle = $newsletterModel->getNewsletter($id)->getRow()->title;

            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($myTitle, "The Harbor Mérida", "Baja en Boletín");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "El boletín se eliminó satisfactoriamente del sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible eliminar el boletín en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

}
