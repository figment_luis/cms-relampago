<?php
namespace App\Controllers\Thorurbana\Harbor;
use App\Controllers\BaseController;

class Dashboard extends BaseController
{

    public function index()
    {
        return view('thorurbana/harbor/dashboard');
    }

}
