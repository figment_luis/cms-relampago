<?php
namespace App\Controllers\Thorurbana\Landmark;
use App\Controllers\BaseController;

class Dashboard extends BaseController
{

    public function index()
    {
        return view('thorurbana/landmark/dashboard');
    }

}
