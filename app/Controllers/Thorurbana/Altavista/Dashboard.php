<?php
namespace App\Controllers\Thorurbana\Altavista;
use App\Controllers\BaseController;

class Dashboard extends BaseController
{

    public function index()
    {
        return view('thorurbana/altavista/dashboard');
    }

}
