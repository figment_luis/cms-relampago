<?php
namespace App\Controllers\Thorurbana\Corazon;
use App\Controllers\BaseController;
use App\Models\Thorurbana\Corazon\MicrositesModel;
use App\Models\LogModel;

class Microsites extends BaseController
{

    public function index()
    {
        try {
            $micrositesModel = new MicrositesModel();
            $microsites = $micrositesModel->getAllMicrosites()->getResult();
            return view('thorurbana/corazon/microsites/index', compact('microsites'));
        } catch (\Exception $e) {
            $message['site'] = "Calle Corazón";
            $message['section'] = "Listar Micrositios";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function create()
    {
        return view('thorurbana/corazon/microsites/create');
    }

    public function store()
    {
        try {
            // Validación de datos
            $rules = [
                'title' => [
                    'label' => 'Titulo de micrositio',
                    'rules' => 'trim|required|min_length[3]|max_length[60]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'slug' => [
                    'label' => 'Slug de micrositio',
                    'rules' => 'trim|required|min_length[3]|max_length[60]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'start_date' => [
                    'label' => 'Fecha de inicio',
                    'rules' => 'trim|required|startdateValidation[start_date]',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                        'startdateValidation' => 'La {field} debe ser igual o posterior al momento actual'
                    ]
                ],
                'end_date' => [
                    'label' => 'Fecha de cierre',
                    'rules' => 'trim|required|enddateValidation[end_date, start_date]',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                        'enddateValidation' => 'La {field} debe ser posterior a la fecha de inicio'
                    ]
                ],
            ];

            $input = $this->validate($rules);

            $response = [];

            if (!$input) {
                $response["status"]     = "invalid";
                $response["code"]       = 406;
                $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
                $response['validation'] = $this->validator->getErrors();

                return json_encode($response);
            }

            // Almacenar información en la base de datos
            $title          = $this->request->getPost('title');
            $slug           = $this->request->getPost('slug');
            $start_date     = $this->request->getPost('start_date');
            $end_date       = $this->request->getPost('end_date');
            $image          = $this->request->getFile('image');

            $micrositesModel = new MicrositesModel();
            $result = $micrositesModel->createMicrosite($title, $slug, $start_date, $end_date, $image);

            // Salida de resultados
            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($title, "Calle Corazón", "Alta en Micrositio");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "El micrositio se registró satisfactoriamente en el sistema. <br><br> <strong>https://callecorazon.com/" . mb_url_title($slug, '-', TRUE) . "</strong>";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible registrar el micrositio en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    public function edit($id)
    {
        try {
            $micrositesModel = new MicrositesModel();
            $microsite = $micrositesModel->getMicrosite($id)->getRow();
            return view('thorurbana/corazon/microsites/edit', compact('microsite'));
        } catch (\Exception $e) {
            $message['site'] = "Calle Corazón";
            $message['section'] = "Editar Micrositio";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function update($id)
    {
        try {
            // Validación de datos
            $rules = [
                'title' => [
                    'label' => 'Titulo de micrositio',
                    'rules' => 'trim|required|min_length[3]|max_length[60]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'slug' => [
                    'label' => 'Slug de micrositio',
                    'rules' => 'trim|required|min_length[3]|max_length[60]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'start_date' => [
                    'label' => 'Fecha de inicio',
                    'rules' => 'trim|required',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                    ]
                ],
                'end_date' => [
                    'label' => 'Fecha de cierre',
                    'rules' => 'trim|required|endupdatedateValidation[end_date, start_date]',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                        'endupdatedateValidation' => 'La {field} debe ser posterior al momento actual y a la fecha de inicio'
                    ]
                ],
            ];

            $input = $this->validate($rules);

            $response = [];

            if (!$input) {
                $response["status"]     = "invalid";
                $response["code"]       = 406;
                $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
                $response['validation'] = $this->validator->getErrors();

                return json_encode($response);
            }

            // Almacenar información en la base de datos
            $title          = $this->request->getPost('title');
            $slug           = $this->request->getPost('slug');
            $start_date     = $this->request->getPost('start_date');
            $end_date       = $this->request->getPost('end_date');
            $image          = $this->request->getFile('image');
            $enabled        = $this->request->getPost('enabled');

            $micrositesModel = new MicrositesModel();
            $result = $micrositesModel->editMicrosite($id, $title, $slug, $start_date, $end_date, $enabled, $image);

            // Salida de resultados
            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($title, "Calle Corazón", "Modificación en Micrositio");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "El micrositio se actualizó satisfactoriamente en el sistema. <br><br> <strong>https://callecorazon.com/" . mb_url_title($slug, '-', TRUE) . "</strong>";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible actualizar el micrositio en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    public function destroy()
    {
        try {
            $id = $this->request->getPost('id');

            $response = [];
            $micrositesModel = new MicrositesModel();
            $result = $micrositesModel->removeMicrosite($id);
            $myTitle = $micrositesModel->getMicrosite($id)->getRow()->title;

            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($myTitle, "Calle Corazón", "Baja en Micrositio");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "El micrositio se eliminó satisfactoriamente del sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible eliminar el micrositio en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    public function activate($id)
    {
        $response = [];
        $micrositesModel = new MicrositesModel();
        $result = $micrositesModel->activateMicrosite($id);
        $myTitle = $micrositesModel->getMicrosite($id)->getRow()->title;

        if ($result) {
            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog($myTitle, "Calle Corazón", "Activación en Micrositio");

            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "El micrositio se activo satisfactoriamente del sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible activar el micrositio en el sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }
    
}