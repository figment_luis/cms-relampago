<?php
namespace App\Controllers\Thorurbana\Corazon;
use App\Controllers\BaseController;

class Dashboard extends BaseController
{

    public function index()
    {
        return view('thorurbana/corazon/dashboard');
    }

}
