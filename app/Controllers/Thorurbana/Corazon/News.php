<?php
namespace App\Controllers\Thorurbana\Corazon;
use App\Controllers\BaseController;
use App\Models\Thorurbana\Corazon\NewsModel;
use App\Models\LogModel;

class News extends BaseController
{

    public function index()
    {
        try {
            $newsModel = new NewsModel();
            $news = $newsModel->getAllNews()->getResult();

            return view('thorurbana/corazon/news/index', compact('news'));
        } catch (\Exception $e) {
            $message['site'] = "Calle Corazón";
            $message['section'] = "Listar Novedades";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function create()
    {
        return view('thorurbana/corazon/news/create');
    }

    public function store()
    {
        try {
            // Validación de datos
            $rules = [
                'title' => [
                    'label' => 'Titulo de novedad',
                    'rules' => 'trim|required|min_length[3]|max_length[80]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'link' => [
                    'label' => 'Link de novedad',
                    'rules' => 'trim|permit_empty|valid_url',
                    'errors' => [
                        'valid_url' => 'El {field} no es una URL válida'
                    ]
                ],
                'description' => [
                    'label' => 'Descripción de novedad',
                    'rules' => 'trim|required|min_length[20]|max_length[1000]',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                        'min_length' => 'La {field} debe tener al menos {param} caracteres',
                        'max_length' => 'La {field} no debe exceder los {param} caracteres'
                    ]
                ],
            ];

            $input = $this->validate($rules);

            $response = [];

            if (!$input) {
                $response["status"]     = "invalid";
                $response["code"]       = 406;
                $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
                $response['validation'] = $this->validator->getErrors();

                return json_encode($response);
            }

            // Almacenar información en la base de datos
            $title       = $this->request->getPost('title');
            $link        = $this->request->getPost('link');
            $image       = $this->request->getFile('image');
            $description = strip_tags($this->request->getPost('description'));

            $newsModel = new NewsModel();
            $result = $newsModel->createNews($title, $link, $image, $description);

            // Salida de resultados
            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($title, "Calle Corazón", "Alta en Novedad");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "La novedad se registró satisfactoriamente en el sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible registrar la novedad en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    public function edit($id)
    {
        try {
            $newsModel = new NewsModel();
            $news = $newsModel->getNews($id)->getRow();

            return view('thorurbana/corazon/news/edit', compact('news'));
        } catch (\Exception $e) {
            $message['site'] = "Calle Corazón";
            $message['section'] = "Editar Novedad";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function update($id)
    {
        try {
            // Validación de datos
            $rules = [
                'title' => [
                    'label' => 'Titulo de novedad',
                    'rules' => 'trim|required|min_length[3]|max_length[80]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'link' => [
                    'label' => 'Link de novedad',
                    'rules' => 'trim|permit_empty|valid_url',
                    'errors' => [
                        'valid_url' => 'El {field} no es una URL válida'
                    ]
                ],
                'description' => [
                    'label' => 'Descripción de novedad',
                    'rules' => 'trim|required|min_length[20]|max_length[1000]',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                        'min_length' => 'La {field} debe tener al menos {param} caracteres',
                        'max_length' => 'La {field} no debe exceder los {param} caracteres'
                    ]
                ],
            ];

            $input = $this->validate($rules);

            $response = [];

            if (!$input) {
                $response["status"]     = "invalid";
                $response["code"]       = 406;
                $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
                $response['validation'] = $this->validator->getErrors();

                return json_encode($response);
            }

            // Almacenar información en la base de datos
            $title       = $this->request->getPost('title');
            $link        = $this->request->getPost('link');
            $image       = $this->request->getFile('image');
            $description = strip_tags($this->request->getPost('description'));

            $newsModel = new NewsModel();
            $result = $newsModel->editNews($id, $title, $link, $image, $description);

            // Salida de resultados
            if ($result) {

                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($title, "Calle Corazón", "Modificación en Novedad");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "La novedad se actualizó satisfactoriamente en el sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible actualizar la novedad en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    public function destroy()
    {
        try {
            $id = $this->request->getPost('id');

            $response = [];
            $newsModel = new NewsModel();
            $result = $newsModel->removeNews($id);
            $myTitle = $newsModel->getNews($id)->getRow()->title;

            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($myTitle, "Calle Corazón", "Baja en Novedad");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "La novedad se eliminó satisfactoriamente del sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible eliminar la novedad en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

}
