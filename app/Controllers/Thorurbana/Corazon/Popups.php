<?php
namespace App\Controllers\Thorurbana\Corazon;
use App\Controllers\BaseController;
use App\Models\Thorurbana\Corazon\PopupsModel;
use App\Models\LogModel;

class Popups extends BaseController
{

    public function index()
    {
        try {
            $popupsModel = new PopupsModel();
            $popups = $popupsModel->getAllPopups()->getResult();

            return view('thorurbana/corazon/popups/index', compact('popups'));
        } catch (\Exception $e) {
            $message['site'] = "Calle Corazón";
            $message['section'] = "Listar Popups";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function create()
    {
        return view('thorurbana/corazon/popups/create');
    }

    public function store()
    {
        try {
            // Validación de datos
            $rules = [
                'title' => [
                    'label' => 'Titulo de popup',
                    'rules' => 'trim|required|min_length[3]|max_length[80]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'start_date' => [
                    'label' => 'Fecha de inicio',
                    'rules' => 'trim|required|startdateValidation[start_date]',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                        'startdateValidation' => 'La {field} debe ser igual o posterior al momento actual'
                    ]
                ],
                'end_date' => [
                    'label' => 'Fecha de cierre',
                    'rules' => 'trim|required|enddateValidation[end_date, start_date]',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                        'enddateValidation' => 'La {field} debe ser posterior a la fecha de inicio'
                    ]
                ],
                'link' => [
                    'label' => 'Link asociado al popup',
                    'rules' => 'trim|permit_empty|valid_url',
                    'errors' => [
                        'valid_url' => 'El {field} no es una URL válida'
                    ]
                ],
            ];

            $input = $this->validate($rules);

            $response = [];

            if (!$input) {
                $response["status"]     = "invalid";
                $response["code"]       = 406;
                $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
                $response['validation'] = $this->validator->getErrors();

                return json_encode($response);
            }

            // Almacenar información en la base de datos
            $title          = $this->request->getPost('title');
            $start_date     = $this->request->getPost('start_date');
            $end_date       = $this->request->getPost('end_date');
            $link           = $this->request->getPost('link');
            $image_desktop  = $this->request->getFile('image_desktop');
            $image_mobile   = $this->request->getFile('image_mobile');

            $popupsModel = new PopupsModel();
            $result      = $popupsModel->createPopup($title, $start_date, $end_date, $link, $image_desktop, $image_mobile);

            // Salida de resultados
            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($title, "Calle Corazón", "Alta en Popup");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "El popup se registró satisfactoriamente en el sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible registrar el popup en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    public function edit($id)
    {
        try {
            $popupsModel = new PopupsModel();
            $popup = $popupsModel->getPopup($id)->getRow();

            return view('thorurbana/corazon/popups/edit', compact('popup'));
        } catch (\Exception $e) {
            $message['site'] = "Calle Corazón";
            $message['section'] = "Editar Popup";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function update($id)
    {
        try {
            // Validación de datos
            $rules = [
                'title' => [
                    'label' => 'Titulo de popup',
                    'rules' => 'trim|required|min_length[3]|max_length[80]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'start_date' => [
                    'label' => 'Fecha de inicio',
                    'rules' => 'trim|required',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                    ]
                ],
                'end_date' => [
                    'label' => 'Fecha de cierre',
                    'rules' => 'trim|required|endupdatedateValidation[end_date, start_date]',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                        'endupdatedateValidation' => 'La {field} debe ser posterior al momento actual y a la fecha de inicio'
                    ]
                ],
                'link' => [
                    'label' => 'Link asociado al popup',
                    'rules' => 'trim|permit_empty|valid_url',
                    'errors' => [
                        'valid_url' => 'El {field} no es una URL válida'
                    ]
                ],
            ];

            $input = $this->validate($rules);

            $response = [];

            if (!$input) {
                $response["status"]     = "invalid";
                $response["code"]       = 406;
                $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
                $response['validation'] = $this->validator->getErrors();

                return json_encode($response);
            }

            // Almacenar información en la base de datos
            $title          = $this->request->getPost('title');
            $start_date     = $this->request->getPost('start_date');
            $end_date       = $this->request->getPost('end_date');
            $link           = $this->request->getPost('link');
            $enabled        = $this->request->getPost('enabled');
            $image_desktop  = $this->request->getFile('image_desktop');
            $image_mobile   = $this->request->getFile('image_mobile');

            $popupsModel = new PopupsModel();
            $result = $popupsModel->editPopup($id, $title, $start_date, $end_date, $link, $enabled, $image_desktop, $image_mobile);

            // Salida de resultados
            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($title, "Calle Corazón", "Modificación en Popup");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "El popup se actualizó satisfactoriamente en el sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible actualizar el popup en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    // public function destroy($id)
    // {
    //     $response    = [];
    //     $popupsModel = new PopupsModel();
    //     $result      = $popupsModel->removePopup($id);

    //     if ($result) {

    //         // Registro de Log en sistema
    //         $logModel = new LogModel();
    //         $logModel->createLog("Baja en Popup - Town Square Metepec");

    //         $response["status"]     = "success";
    //         $response["code"]       = 200;
    //         $response["message"]    = "La novedad se eliminó satisfactoriamente del sistema.";
    //     } else {
    //         $response["status"]     = "error";
    //         $response["code"]       = 400;
    //         $response["message"]    = "No fue posible eliminar la novedad en el sistema, favor de intentar mas tarde.";
    //     }

    //     return json_encode($response);
    // }

}
