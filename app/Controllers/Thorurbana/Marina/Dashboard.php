<?php
namespace App\Controllers\Thorurbana\Marina;
use App\Controllers\BaseController;

class Dashboard extends BaseController
{

    public function index()
    {
        return view('thorurbana/marina/dashboard');
    }

}
