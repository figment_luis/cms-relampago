<?php
namespace App\Controllers\Thorurbana\Marina;
use App\Controllers\BaseController;
use App\Models\Thorurbana\Marina\EventsModel;
use App\Models\LogModel;

class Events extends BaseController
{

    public function index()
    {
        try {
            $eventsModel = new EventsModel();
            $events = $eventsModel->getAllEvents()->getResult();

            return view('thorurbana/marina/events/index', compact('events'));
        } catch (\Exception $e) {
            $message['site'] = "Marina Puerto Cancún";
            $message['section'] = "Listar Eventos";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
        
    }

    public function create()
    {
        return view('thorurbana/marina/events/create');
    }

    public function store()
    {
        try {
            // Validación de datos
            $rules = [
                'title' => [
                    'label' => 'Titulo del evento',
                    'rules' => 'trim|required|min_length[3]|max_length[80]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'description' => [
                    'label' => 'Descripción del evento',
                    'rules' => 'trim|required|min_length[20]|max_length[1000]',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                        'min_length' => 'La {field} debe tener al menos {param} caracteres',
                        'max_length' => 'La {field} no debe exceder los {param} caracteres'
                    ]
                ],
            ];

            $input = $this->validate($rules);

            $response = [];

            if (!$input) {
                $response["status"]     = "invalid";
                $response["code"]       = 406;
                $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
                $response['validation'] = $this->validator->getErrors();

                return json_encode($response);
            }

            // Almacenar información en la base de datos
            $title          = $this->request->getPost('title');
            $description    = nl2br(strip_tags($this->request->getPost('description')));
            $galleryFiles   = $this->request->getFiles();

            $eventsModel = new EventsModel();
            $result = $eventsModel->createEvent($title, $description, $galleryFiles);

            // Salida de resultados
            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($title, "Marina Puerto Cancún", "Alta en Evento");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "El evento se registró satisfactoriamente en el sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible registrar el evento en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    public function edit($id)
    {
        try {
            $eventsModel = new EventsModel();
            $event = $eventsModel->getEvent($id)->getRow();
            $event->event_description = strip_tags(str_replace(['<br />', '<br>', '<br/>'], '&#13;', $event->event_description));

            return view('thorurbana/marina/events/edit', compact('event'));
        } catch (\Exception $e) {
            $message['site'] = "Marina Puerto Cancún";
            $message['section'] = "Editar Evento";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function editGallery($id) 
    {
        try {
            $eventsModel   = new EventsModel();
            $event         = $eventsModel->getEvent($id)->getRow();
            $currentImages = esc($eventsModel->getGalleryEvent($id));

            return view('thorurbana/marina/events/gallery/edit', compact('event', 'currentImages'));
        } catch (\Exception $e) {
            $message['site'] = "Marina Puerto Cancún";
            $message['section'] = "Editar Galería de Evento";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function update($id)
    {
        try {
            // Validación de datos
            $rules = [
                'title' => [
                    'label' => 'Titulo del evento',
                    'rules' => 'trim|required|min_length[3]|max_length[80]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'description' => [
                    'label' => 'Descripción del evento',
                    'rules' => 'trim|required|min_length[20]|max_length[1000]',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                        'min_length' => 'La {field} debe tener al menos {param} caracteres',
                        'max_length' => 'La {field} no debe exceder los {param} caracteres'
                    ]
                ],
            ];

            $input = $this->validate($rules);

            $response = [];

            if (!$input) {
                $response["status"]     = "invalid";
                $response["code"]       = 406;
                $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
                $response['validation'] = $this->validator->getErrors();

                return json_encode($response);
            }

            // Almacenar información en la base de datos
            $title       = $this->request->getPost('title');
            $description = nl2br(strip_tags($this->request->getPost('description')));

            $eventsModel = new EventsModel();
            $result = $eventsModel->editEvent($id, $title, $description);

            // Salida de resultados
            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($title, "Marina Puerto Cancún", "Modificación en Evento");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "El evento se actualizó satisfactoriamente en el sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible actualizar el evento en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    public function updateGallery($id, $slug)
    {
        try {
            $images = $this->request->getFiles();

            $response = [];
            $eventsModel = new EventsModel();
            $result = $eventsModel->updateGalleryEvent($id, $slug, $images);
            $myTitle = $eventsModel->getEvent($id)->getRow()->name;
    
            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog("Baja en Galería - " . $myTitle, "Marina Puerto Cancún", "Modificación en Evento [Galería]");
    
                $response = $result;
            } else {
                $response["error"] = "No fue posible registrar la imagen en el sistema, favor de intentar más tarde";
            }
    
            return json_encode($response);
        } catch (\Exception $e) {
            $message['site'] = "Marina Puerto Cancún";
            $message['section'] = "Editar Galería de Evento";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function destroy()
    {
        try {
            $id = $this->request->getPost('id');

            $response = [];
            $eventsModel = new EventsModel();
            $result = $eventsModel->removeEvent($id);
            $myTitle = $eventsModel->getEvent($id)->getRow()->name;

            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($myTitle, "Marina Puerto Cancún", "Baja en Evento");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "El evento se eliminó satisfactoriamente del sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible eliminar el evento en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    public function destroyGallery($id, $image)
    {
        try {
            $response = [];
            $eventsModel = new EventsModel();
            $result = $eventsModel->removeGalleryEvent($id, $image);
            $myTitle = $eventsModel->getEvent($id)->getRow()->name;

            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog("Baja en Galería - " . $myTitle, "Marina Puerto Cancún", "Baja en Evento [Galería]");

                $response["success"] = "Imagen eliminada satisfactoriamente del sistema";
            } else {
                $response["error"] = "No fue posible eliminar la imagen del sistema, favor de intentar más tarde";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

}
