<?php
namespace App\Controllers\GSM\Antea;
use App\Controllers\BaseController;

class Dashboard extends BaseController
{

    public function index()
    {
        return view('GSM/antea/dashboard');
    }

}
