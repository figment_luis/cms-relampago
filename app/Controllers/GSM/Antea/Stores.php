<?php
namespace App\Controllers\GSM\Antea;
use App\Controllers\BaseController;
use App\Models\GSM\Antea\StoresModel;
use App\Models\LogModel;

class Stores extends BaseController
{

    public function index()
    {
        try {
            helper('text');
            $storesModel = new StoresModel();
            $stores      = $storesModel->getAllStores()->getResult();
            $levels      = $storesModel->getLevels()->getResult();
            return view('GSM/antea/stores/index', compact('stores', 'levels'));
        } catch (\Exception $e) {
            $message['site'] = "Antea";
            $message['section'] = "Listar Tiendas";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function create()
    {
        try {
            $storesModel    = new StoresModel();
            $categories     = $storesModel->getCategories()->getResult();
            $levels         = $storesModel->getLevels()->getResult();
            return view('GSM/antea/stores/create', compact('categories', 'levels'));
        } catch (\Exception $e) {
            $message['site'] = "Antea";
            $message['section'] = "Registrar Tienda";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function store()
    {
        try {
            // Validación de datos
            $rules = [
                'name' => [
                    'label' => 'Nombre de Tienda',
                    'rules' => 'trim|required|min_length[3]|max_length[50]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'phone' => [
                    'label' => 'Teléfono(s)',
                    'rules' => 'trim|permit_empty|min_length[10]|max_length[23]',
                    'errors' => [
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'Los {field} no deben exceder los {param} caracteres'
                    ]
                ],
                'hourhand' => [
                    'label' => 'Horarios',
                    'rules' => 'trim|required|min_length[3]|max_length[150]',
                    'errors' => [
                        'required' => 'Los {field} son un dato requerido',
                        'min_length' => 'Los {field} deben tener al menos {param} caracteres',
                        'max_length' => 'Los {field} no deben exceder los {param} caracteres'
                    ]
                ],
                'url' => [
                    'label' => 'Sitio Web',
                    'rules' => 'trim|permit_empty|valid_url',
                    'errors' => [
                        'valid_url' => 'El {field} no contiene una URL válida'
                    ]
                ],
                'shorturl' => [
                    'label' => 'URL Amigable',
                    'rules' => 'trim|required_with[url]',
                    'errors' => [
                        'valid_url' => 'La {field} no contiene una URL válida',
                        'required_with' => 'La {field} es un dato requerido'
                    ]
                ],
                'correo' => [
                    'label' => 'Email',
                    'rules' => 'trim|permit_empty|valid_email',
                    'errors' => [
                        'valid_email' => 'El {field} parece ser un dato no válido',
                    ]
                ],
                'location' => [
                    'label' => 'Ubicación en Plaza',
                    'rules' => 'required|numeric',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                        'numeric' => 'La {field} debe ser un valor numérico'
                    ]
                ],
                'category' => [
                    'label' => 'Categoría',
                    'rules' => 'required|numeric',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                        'numeric' => 'La {field} debe ser un valor numérico'
                    ]
                ],
                'keywords' => [
                    'label' => 'Etiquetas',
                    'rules' => 'trim|required|min_length[10]',
                    'errors' => [
                        'required' => 'Las {field} son un dato requerido',
                        'min_length' => 'Las {field} deben tener al menos {param} caracteres'
                    ]
                ],
                'description' => [
                    'label' => 'Descripción de Tienda',
                    'rules' => 'trim|required|min_length[20]|max_length[2000]',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                        'min_length' => 'La {field} debe tener al menos {param} caracteres',
                        'max_length' => 'La {field} no debe exceder los {param} caracteres'
                    ]
                ],
            ];

            $input = $this->validate($rules);

            $response = [];

            if (!$input) {
                $response["status"]     = "invalid";
                $response["code"]       = 406;
                $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
                $response['validation'] = $this->validator->getErrors();

                return json_encode($response);
            }

            // Almacenar información en la base de datos
            $name           = $this->request->getPost('name');
            $description    = $this->request->getPost('description');
            $email          = $this->request->getPost('correo');
            $url            = $this->request->getPost('url');
            $shorturl       = $this->request->getPost('shorturl');
            $phone          = $this->request->getPost('phone');
            $hourhand       = $this->request->getPost('hourhand');
            $keywords       = $this->request->getPost('keywords');
            $location       = $this->request->getPost('location');
            $category       = $this->request->getPost('category');
            $logotipo       = $this->request->getFile('logotipo');
            $cover          = $this->request->getFile('cover');

            $storesModel    = new StoresModel();
            $result         = $storesModel->createStore($name, $description, $email, $url, $shorturl, $phone, $hourhand, $keywords, $location, $category, $logotipo, $cover);

            // Salida de resultados
            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($name, "Antea", "Alta en Tienda");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "La tienda se registró satisfactoriamente en el sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible registrar la tienda en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    public function edit($id)
    {
        try {
            $storesModel            = new StoresModel();
            $store                  = $storesModel->getStore($id)->getRow();
            $currentCategory        = $storesModel->getCategoryStore($id)->getRow();
            $categories             = $storesModel->getCategories()->getResult();
            $imageStore             = $storesModel->getImageStore($id);
            $levels                 = $storesModel->getLevels()->getResult();
            return view('GSM/antea/stores/edit', compact('store', 'currentCategory', 'categories', 'imageStore', 'levels'));
        } catch (\Exception $e) {
            $message['site'] = "Antea";
            $message['section'] = "Editar Tienda";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function update($id)
    {
        try {
            // Validación de datos
            $rules = [
                'name' => [
                    'label' => 'Nombre',
                    'rules' => 'trim|required|min_length[3]|max_length[50]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'phone' => [
                    'label' => 'Teléfono(s)',
                    'rules' => 'trim|permit_empty|min_length[10]|max_length[23]',
                    'errors' => [
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'Los {field} no deben exceder los {param} caracteres'
                    ]
                ],
                'hourhand' => [
                    'label' => 'Horarios',
                    'rules' => 'trim|required|min_length[3]|max_length[150]',
                    'errors' => [
                        'required' => 'Los {field} son un dato requerido',
                        'min_length' => 'Los {field} deben tener al menos {param} caracteres',
                        'max_length' => 'Los {field} no deben exceder los {param} caracteres'
                    ]
                ],
                'url' => [
                    'label' => 'Sitio Web',
                    'rules' => 'trim|permit_empty|valid_url',
                    'errors' => [
                        'valid_url' => 'El {field} no contiene una URL válida'
                    ]
                ],
                'shorturl' => [
                    'label' => 'URL Amigable',
                    'rules' => 'trim|required_with[url]',
                    'errors' => [
                        'valid_url' => 'La {field} no contiene una URL válida',
                        'required_with' => 'La {field} es un dato requerido'
                    ]
                ],
                'correo' => [
                    'label' => 'Email',
                    'rules' => 'trim|permit_empty|valid_email',
                    'errors' => [
                        'valid_email' => 'El {field} parece ser un dato no válido',
                    ]
                ],
                'location' => [
                    'label' => 'Ubicación',
                    'rules' => 'required|numeric',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                        'numeric' => 'La {field} debe ser un valor numérico'
                    ]
                ],
                'category' => [
                    'label' => 'Categoría',
                    'rules' => 'required|numeric',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                        'numeric' => 'La {field} debe ser un valor numérico'
                    ]
                ],
                'keywords' => [
                    'label' => 'Etiquetas',
                    'rules' => 'trim|required|min_length[10]',
                    'errors' => [
                        'required' => 'Las {field} son un dato requerido',
                        'min_length' => 'Las {field} deben tener al menos {param} caracteres'
                    ]
                ],
                'description' => [
                    'label' => 'Descripción',
                    'rules' => 'trim|required|min_length[20]|max_length[2000]',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                        'min_length' => 'La {field} debe tener al menos {param} caracteres',
                        'max_length' => 'La {field} no debe exceder los {param} caracteres'
                    ]
                ],
            ];

            $input = $this->validate($rules);

            $response = [];

            if (!$input) {
                $response["status"]     = "invalid";
                $response["code"]       = 406;
                $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
                $response['validation'] = $this->validator->getErrors();

                return json_encode($response);
            }

            // Almacenar información en la base de datos
            $name           = $this->request->getPost('name');
            $description    = $this->request->getPost('description');
            $email          = $this->request->getPost('correo');
            $url            = $this->request->getPost('url');
            $shorturl       = $this->request->getPost('shorturl');
            $phone          = $this->request->getPost('phone');
            $hourhand       = $this->request->getPost('hourhand');
            $keywords       = $this->request->getPost('keywords');
            $location       = $this->request->getPost('location');
            $category       = $this->request->getPost('category');
            $logotipo       = $this->request->getFile('logotipo');
            $cover          = $this->request->getFile('cover');

            $storesModel    = new StoresModel();
            $result         = $storesModel->editStore($id, $name, $description, $email, $url, $shorturl, $phone, $hourhand, $keywords, $location, $category, $logotipo, $cover);

            // Salida de resultados
            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($name, "Antea", "Modificación en Tienda");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "La tienda se actualizó satisfactoriamente en el sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible actualizar la tienda en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    public function destroy($id)
    {
        try {
            $response = [];
            $storesModel = new StoresModel();
            $result = $storesModel->removeStore($id);
            $myName = $storesModel->getStore($id)->getRow()->name;

            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($myName, "Antea", "Baja en Tienda");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "La tienda se eliminó satisfactoriamente del sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible eliminar la tienda del sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    public function mapEdit($id)
    {
        try {
            $storesModel            = new StoresModel();
            $store                  = $storesModel->getStore($id)->getRow();
            $locations              = ['', 'Terrazas', 'Planta Baja', 'Primer Nivel', 'Segundo Nivel', 'Tercer Nivel', 'Sotano 02'];
            $mapNameLevels = ['', 'S2', 'PB', 'N1', 'N2', 'N3', 'sotano_2'];
            $level = $mapNameLevels[$store->location];
            
            // Solicitar descargar el mapa desde el sitio Web (vista de directorio PHP)
            // El objetivo es obtener una copia de las áreas de mapa (más recientes) para proyectarlas en la vista del CMS
            $storesModel->mapDownload($mapNameLevels[$store->location]);
            return view('GSM/antea/stores/map/index', compact('store', 'locations', 'level'));
        } catch (\Exception $e) {
            $message['site'] = "Antea";
            $message['section'] = "Editar Localización de Tienda en Mapa (Directorio)";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function mapUpdate($id) 
    {
        try {
            $x              = $this->request->getPost('pos_x');
            $y              = $this->request->getPost('pos_y');
            $newStore       = $this->request->getPost('new_store');
            $oldStore       = $this->request->getPost('old_store');
            $coords         = $this->request->getPost('coords');
            // En archivos de mapa separados, es importante enviar el nivel (piso) de tienda
            $location       = $this->request->getPost('location');
            $storesModel    = new StoresModel();    
            // Guardar coordenadas de tienda - Directorio App
            $resultCoordsMap = $storesModel->editMap($id, [$x, $y]);
            // Guardar referencia a MapArea - Directorio Web
            $resultWriteArea = $storesModel->writeMap($newStore, $oldStore, $coords, $location);

            // Salida de resultados
            if ($resultCoordsMap && $resultWriteArea) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog("Modificación en Mapa - Antea");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "El mapa se actualizó satisfactoriamente en el sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible actualizar el mapa en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

}
