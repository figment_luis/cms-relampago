<?php
namespace App\Controllers\GSM\Antara;
use App\Controllers\BaseController;

class Dashboard extends BaseController
{

    public function index()
    {
        return view('GSM/antara/dashboard');
    }

}
