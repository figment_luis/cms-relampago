<?php
namespace App\Controllers\GSM\Antara;
use App\Controllers\BaseController;
use App\Models\GSM\Antara\NewsModel;
use App\Models\GSM\Antara\StoresModel;
use App\Models\LogModel;

class News extends BaseController
{

    public function index()
    {
        try {
            setlocale(LC_TIME, 'es_ES.UTF-8');
            $newsModel = new NewsModel();
            $news = $newsModel->getAllNews()->getResult();
            return view('GSM/antara/news/index', compact('news'));
        } catch (\Exception $e) {
            $message['site'] = "Antara";
            $message['section'] = "Listar Novedades";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function create()
    {
        return view('GSM/antara/news/create');
    }

    public function store()
    {
        try {
            // Validación de datos
            $rules = [
                'title' => [
                    'label' => 'Titulo de novedad',
                    'rules' => 'trim|required|min_length[3]|max_length[80]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'subtitle' => [
                    'label' => 'Subtitulo de novedad',
                    'rules' => 'trim|required|min_length[3]|max_length[80]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'link' => [
                    'label' => 'Link de novedad',
                    'rules' => 'trim|permit_empty|valid_url',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'valid_url' => 'El {field} no es una URL válida'
                    ]
                ],
                'description' => [
                    'label' => 'Descripción de novedad',
                    'rules' => 'trim|required|min_length[20]|max_length[1000]',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                        'min_length' => 'La {field} debe tener al menos {param} caracteres',
                        'max_length' => 'La {field} no debe exceder los {param} caracteres'
                    ]
                ],
            ];

            $input = $this->validate($rules);

            $response = [];

            if (!$input) {
                $response["status"]     = "invalid";
                $response["code"]       = 406;
                $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
                $response['validation'] = $this->validator->getErrors();

                return json_encode($response);
            }

            // Almacenar información en la base de datos
            $title      = $this->request->getPost('title');
            $subtitle   = $this->request->getPost('subtitle');
            $url        = $this->request->getPost('link');
            $image      = $this->request->getFile('image');
            $content    = $this->request->getPost('description');

            $newsModel = new NewsModel();
            $result = $newsModel->createNews($title, $subtitle, $url, $image, $content);

            // Salida de resultados
            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($title, "Antara", "Alta en Novedad");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "La novedad se registró satisfactoriamente en el sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible registrar la novedad en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    public function edit($id)
    {
        try {
            $newsModel = new NewsModel();
            $news = $newsModel->getNews($id)->getRow();
            return view('GSM/antara/news/edit', compact('news'));
        } catch (\Exception $e) {
            $message['site'] = "Antara";
            $message['section'] = "Editar Novedad";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function update($id)
    {
        try {
            // Validación de datos
            $rules = [
                'title' => [
                    'label' => 'Titulo de novedad',
                    'rules' => 'trim|required|min_length[3]|max_length[80]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'subtitle' => [
                    'label' => 'Subtitulo de novedad',
                    'rules' => 'trim|required|min_length[3]|max_length[80]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'link' => [
                    'label' => 'Link de novedad',
                    'rules' => 'trim|permit_empty|valid_url',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'valid_url' => 'El {field} no es una URL válida'
                    ]
                ],
                'description' => [
                    'label' => 'Descripción de novedad',
                    'rules' => 'trim|required|min_length[20]|max_length[1000]',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                        'min_length' => 'La {field} debe tener al menos {param} caracteres',
                        'max_length' => 'La {field} no debe exceder los {param} caracteres'
                    ]
                ],
            ];

            $input = $this->validate($rules);

            $response = [];

            if (!$input) {
                $response["status"]     = "invalid";
                $response["code"]       = 406;
                $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
                $response['validation'] = $this->validator->getErrors();

                return json_encode($response);
            }

            // Almacenar información en la base de datos
            $title      = $this->request->getPost('title');
            $subtitle   = $this->request->getPost('subtitle');
            $url        = $this->request->getPost('link');
            $image      = $this->request->getFile('image');
            $content    = $this->request->getPost('description');

            $newsModel = new NewsModel();
            $result = $newsModel->editNews($id, $title, $subtitle, $url, $image, $content);

            // Salida de resultados
            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($title, "Antara", "Modificación en Novedad");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "La novedad se actualizó satisfactoriamente en el sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible actualizar la novedad en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    public function destroy()
    {
        try {
            $id = $this->request->getPost('id');

            $response = [];
            $newsModel = new NewsModel();
            $result = $newsModel->removeNews($id);
            $myTitle = $newsModel->getNews($id)->getRow()->title;

            if ($result) {
                $logModel = new LogModel();
                $logModel->createLog($myTitle, "Antara", "Baja en Novedad");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "La novedad se eliminó satisfactoriamente del sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible eliminar la novedad en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

}
