<?php
namespace App\Controllers\GSM\Luxury;
use App\Controllers\BaseController;
use App\Models\GSM\Luxury\PromotionsModel;
use App\Models\LogModel;

class Promotions extends BaseController
{

    public function index()
    {
        try {
            setlocale(LC_TIME, 'es_ES.UTF-8');
            $promotionsModel = new PromotionsModel();
            $promotions = $promotionsModel->getAllPromotions()->getResult();
            return view('GSM/luxury/promotions/index', compact('promotions'));
        } catch (\Exception $e) {
            $message['site'] = "Luxury Hall";
            $message['section'] = "Listar Promociones";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function create()
    {
        return view('GSM/luxury/promotions/create');
    }

    public function store()
    {
        try {
            // Validación de datos
            $rules = [
                'title' => [
                    'label' => 'Titulo de promoción',
                    'rules' => 'trim|required|min_length[3]|max_length[80]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                /*'link' => [
                    'label' => 'Link asociado a la promoción',
                    'rules' => 'trim|permit_empty|valid_url',
                    'errors' => [
                        'valid_url' => 'El {field} no es una URL válida'
                    ]
                ],*/
                'description' => [
                    'label' => 'Descripción de promoción',
                    'rules' => 'trim|required|min_length[20]|max_length[1000]',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                        'min_length' => 'La {field} debe tener al menos {param} caracteres',
                        'max_length' => 'La {field} no debe exceder los {param} caracteres'
                    ]
                ],
            ];

            $input = $this->validate($rules);

            $response = [];

            if (!$input) {
                $response["status"]     = "invalid";
                $response["code"]       = 406;
                $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
                $response['validation'] = $this->validator->getErrors();

                return json_encode($response);
            }

            // Almacenar información en la base de datos
            $title      = $this->request->getPost('title');
            //$url        = $this->request->getPost('link');
            $image      = $this->request->getFile('image');
            $subtitle   = $this->request->getPost('description');

            $promotionsModel = new PromotionsModel();
            $result = $promotionsModel->createPromotion($title, $subtitle, '', $image);

            // Salida de resultados
            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($title, "Luxury Hall", "Alta en Promoción");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "La promoción se registró satisfactoriamente en el sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible registrar la promoción en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    public function edit($id)
    {
        try {
            $promotionsModel = new PromotionsModel();
            $promotion = $promotionsModel->getPromotion($id)->getRow();
            // Retirar los tags <br /> del contenido de la descripción para mostrar correctamente la información en el textarea
            $promotion->description = str_replace(['<br />', '<br>', '<br/>'], '', $promotion->subtitle);
            return view('GSM/luxury/promotions/edit', compact('promotion'));
        } catch (\Exception $e) {
            $message['site'] = "Luxury Hall";
            $message['section'] = "Editar Promoción";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function update($id)
    {
        try {
            // Validación de datos
            $rules = [
                'title' => [
                    'label' => 'Titulo de promoción',
                    'rules' => 'trim|required|min_length[3]|max_length[80]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                /*'link' => [
                    'label' => 'Link asociado a la promoción',
                    'rules' => 'trim|permit_empty|valid_url',
                    'errors' => [
                        'valid_url' => 'El {field} no es una URL válida'
                    ]
                ],*/
                'description' => [
                    'label' => 'Descripción de promoción',
                    'rules' => 'trim|required|min_length[20]|max_length[1000]',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                        'min_length' => 'La {field} debe tener al menos {param} caracteres',
                        'max_length' => 'La {field} no debe exceder los {param} caracteres'
                    ]
                ],
            ];

            $input = $this->validate($rules);

            $response = [];

            if (!$input) {
                $response["status"]     = "invalid";
                $response["code"]       = 406;
                $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
                $response['validation'] = $this->validator->getErrors();

                return json_encode($response);
            }

            // Almacenar información en la base de datos
            $title      = $this->request->getPost('title');
            //$url        = $this->request->getPost('link');
            $image      = $this->request->getFile('image');
            $subtitle   = $this->request->getPost('description');

            $promotionsModel = new PromotionsModel();
            $result = $promotionsModel->editPromotion($id, $title, $subtitle, '', $image);

            // Salida de resultados
            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($title, "Luxury Hall", "Modificación en Promoción");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "La promoción se actualizó satisfactoriamente en el sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible actualizar la promoción en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    public function destroy()
    {
        try {
            $id = $this->request->getPost('id');

            $response = [];
            $promotionsModel = new PromotionsModel();
            $result = $promotionsModel->removePromotion($id);
            $myTitle = $promotionsModel->getPromotion($id)->getRow()->title;

            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($myTitle, "Luxury Hall", "Baja en Promoción");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "La promoción se eliminó satisfactoriamente del sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible eliminar la promoción en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }
    
}