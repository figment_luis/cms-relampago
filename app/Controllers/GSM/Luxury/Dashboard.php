<?php
namespace App\Controllers\GSM\Luxury;
use App\Controllers\BaseController;

class Dashboard extends BaseController
{

    public function index()
    {
        return view('GSM/luxury/dashboard');
    }

}
