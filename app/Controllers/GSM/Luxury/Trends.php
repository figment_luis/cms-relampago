<?php
namespace App\Controllers\GSM\Luxury;
use App\Controllers\BaseController;
use App\Models\GSM\Luxury\TrendsModel;
use App\Models\GSM\Luxury\StoresModel;
use App\Models\LogModel;

class Trends extends BaseController
{

    public function index()
    {
        try {
            helper('text');
            $trendsModel = new TrendsModel();
            $trends = $trendsModel->getAllTrends()->getResult();
            return view('GSM/luxury/trends/index', compact('trends'));
        } catch (\Exception $e) {
            $message['site'] = "Luxury Hall";
            $message['section'] = "Listar Temas del Mes";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function create()
    {
        try {
            return view('GSM/luxury/trends/create');
        } catch (\Exception $e) {
            $message['site'] = "Luxury Hall";
            $message['section'] = "Registrar Tema del Mes";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function store()
    {
        try {
            // Validación de datos
            $rules = [
                'title' => [
                    'label' => 'Titulo de tema',
                    'rules' => 'trim|required|min_length[10]|max_length[100]', // 30
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'subtitle' => [
                    'label' => 'Subtitulo de tema',
                    'rules' => 'trim|permit_empty|min_length[10]|max_length[100]', // 30
                    'errors' => [
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'link_external' => [
                    'label' => 'Link externo de tema',
                    'rules' => 'trim|permit_empty|valid_url',
                    'errors' => [
                        'valid_url' => 'El {field} no es una URL válida'
                    ]
                ],
                'content' => [
                    'label' => 'Contenido de tema',
                    'rules' => 'trim|required|min_length[80]|max_length[10000]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
            ];

            $input = $this->validate($rules);

            $response = [];

            if (!$input) {
                $response["status"]     = "invalid";
                $response["code"]       = 406;
                $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
                $response['validation'] = $this->validator->getErrors();

                return json_encode($response);
            }

            // Almacenar información en la base de datos
            $title          = $this->request->getPost('title');
            $subtitle       = $this->request->getPost('subtitle');
            $cover          = $this->request->getFile('cover');
            $link_external  = $this->request->getPost('link_external');
            $content        = $this->request->getPost('content');
            $content        = str_replace(['<figure class="image">', '</figure>'], ['<p>', '</p>'], $content);
            
            $trendsModel = new TrendsModel();
            $result = $trendsModel->createTrend($title, $subtitle, $cover, $content, $link_external);

            // Salida de resultados
            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($title, "Luxury Hall", "Alta en Tema del Mes");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "El tema de mes se registró satisfactoriamente en el sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible registrar el tema de mes en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    public function edit($id)
    {
        try {
            $trendsModel = new TrendsModel();
            $trend = $trendsModel->getTrend($id)->getRow();

            return view('GSM/luxury/trends/edit', compact('trend'));
        } catch (\Exception $e) {
            $message['site'] = "Luxury Hall";
            $message['section'] = "Editar Tema del Mes";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function update($id)
    {
        try {
            // Validación de datos
            $rules = [
                'title' => [
                    'label' => 'Titulo de tema',
                    'rules' => 'trim|required|min_length[10]|max_length[100]', // 30
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'subtitle' => [
                    'label' => 'Subtitulo de tema',
                    'rules' => 'trim|permit_empty|min_length[10]|max_length[100]', // 30
                    'errors' => [
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'link_external' => [
                    'label' => 'Link externo de tema',
                    'rules' => 'trim|permit_empty|valid_url',
                    'errors' => [
                        'valid_url' => 'El {field} no es una URL válida'
                    ]
                ],
                'content' => [
                    'label' => 'Contenido de tema',
                    'rules' => 'trim|required|min_length[80]|max_length[10000]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
            ];

            $input = $this->validate($rules);

            $response = [];

            if (!$input) {
                $response["status"]     = "invalid";
                $response["code"]       = 406;
                $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
                $response['validation'] = $this->validator->getErrors();

                return json_encode($response);
            }

            // Almacenar información en la base de datos
            $title          = $this->request->getPost('title');
            $subtitle       = $this->request->getPost('subtitle');
            $cover          = $this->request->getFile('cover');
            $link_external  = $this->request->getPost('link_external');
            $content        = $this->request->getPost('content');
            $content        = str_replace(['<figure class="image">', '</figure>'], ['<p>', '</p>'], $content);

            $trendsModel = new TrendsModel();
            $result = $trendsModel->editTrend($id, $title, $subtitle, $cover, $content, $link_external);

            // Salida de resultados
            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($title, "Luxury Hall", "Modificación en Tema del Mes");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "El tema de mes se actualizó satisfactoriamente en el sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible actualizar el tema de mes en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    public function destroy()
    {
        try {
            $id = $this->request->getPost('id');

            $response = [];
            $trendsModel = new TrendsModel();
            $result = $trendsModel->removeTrend($id);
            $myTitle = $trendsModel->getTrend($id)->getRow()->title;

            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($myTitle, "Luxury Hall", "Baja en Tema del Mes");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "El tema de mes se eliminó satisfactoriamente del sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible eliminar el tema de mes en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    // Método que se invoca cada vez que se registra una imagen sobre el contenido principal de la publicación (CKEditor)
    public function uploadImage() {
        try {
            // Validación de datos
            $rules = [
                'upload' => [
                    'label' => 'Imagen de contenido',
                    'rules' => 'max_size[upload,500]|max_dims[upload,350,350]|ext_in[upload,jpg]',
                    'errors' => [
                        'ext_in' => 'La {field} debe ser en formato jpg',
                        'max_size' => 'La {field} excede el tamaño permitido de 500kb.',
                        'max_dims' => 'La {field} no debe exceder los 350x350'
                    ]
                ]
            ];

            $input = $this->validate($rules);

            $response = [];

            if (!$input) {
                $response["res"] = false;
                $response["msg"] = $this->validator->getErrors()['upload'];

                return json_encode($response);
            }

            // Almacenar imagen en el servidor FTP
            $image = $this->request->getFile('upload');
            $trendsModel = new TrendsModel();
            $result = $trendsModel->uploadImage($image);

            if ($result) {
                // El modelo retorna un arreglo con datos que espera CKEditor para proyectar la imagen sobre el editor de contenido
                $response = $result;
            } else {
                $response["code"] = 500;
                $response["res"] = false;
                $response["msg"] = "No fue posible registrar la imagen en el sistema, favor de intentar más tarde";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            // Respuesta de error decorada para el componente CKEditor (No hace uso de Sweet Alert)
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["res"]     = false;
            $response["msg"]     = $e->getMessage();
            return json_encode($response);
        }
    }

}
