<?php
namespace App\Controllers\GSM\Angelopolis;
use App\Controllers\BaseController;

class Dashboard extends BaseController
{

    public function index()
    {
        return view('GSM/angelopolis/dashboard');
    }

}
