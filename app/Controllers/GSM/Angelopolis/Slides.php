<?php
namespace App\Controllers\GSM\Angelopolis;
use App\Controllers\BaseController;
use App\Models\GSM\Angelopolis\SlidesModel;
use App\Models\LogModel;

class Slides extends BaseController
{

    public function index()
    {
        try {
            $slidesModel = new SlidesModel();
            $slides = $slidesModel->getAllSlides()->getResult();
            return view('GSM/angelopolis/slides/index', compact('slides'));
        } catch (\Exception $e) {
            $message['site'] = "Angelópolis";
            $message['section'] = "Listar Slides";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function create()
    {
        return view('GSM/angelopolis/slides/create');
    }

    public function store()
    {
        try {
            // Validación de datos
            $rules = [
                'title' => [
                    'label' => 'Titulo del slide',
                    'rules' => 'trim|required|min_length[3]|max_length[50]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'end_date' => [
                    'label' => 'Fecha de cierre',
                    'rules' => "trim|permit_empty|enddateValidationSlide[end_date]",
                    'errors' => [
                        'enddateValidationSlide' => 'La {field} debe ser posterior a la fecha actual'
                    ]
                ],
            ];

            $input = $this->validate($rules);

            $response = [];

            if (!$input) {
                $response["status"]     = "invalid";
                $response["code"]       = 406;
                $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
                $response['validation'] = $this->validator->getErrors();

                return json_encode($response);
            }

            // Almacenar información en la base de datos
            $title      = $this->request->getPost('title');
            $end_date   = $this->request->getPost('end_date');
            $image      = $this->request->getFile('image');

            $slidesModel = new SlidesModel();
            $result = $slidesModel->createSlide($title, $end_date, $image);

            // Salida de resultados
            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($title, "Angelópolis", "Alta en Slide");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "El slide se registró satisfactoriamente en el sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible registrar el slide en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    public function edit($id)
    {
        try {
            $slidesModel = new SlidesModel();
            $slide = $slidesModel->getSlide($id)->getRow();
            return view('GSM/angelopolis/slides/edit', compact('slide'));
        } catch (\Exception $e) {
            $message['site'] = "Angelópolis";
            $message['section'] = "Editar Slide";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function update($id)
    {
        try {
            // Validación de datos
            $rules = [
                'title' => [
                    'label' => 'Titulo del slide',
                    'rules' => 'trim|required|min_length[3]|max_length[50]',
                    'errors' => [
                        'required' => 'El {field} es un dato requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'end_date' => [
                    'label' => 'Fecha de cierre',
                    'rules' => "trim|permit_empty|enddateValidationSlide[end_date]",
                    'errors' => [
                        'enddateValidationSlide' => 'La {field} debe ser posterior a la fecha actual'
                    ]
                ],
            ];

            $input = $this->validate($rules);

            $response = [];

            if (!$input) {
                $response["status"]     = "invalid";
                $response["code"]       = 406;
                $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
                $response['validation'] = $this->validator->getErrors();

                return json_encode($response);
            }

            // Almacenar información en la base de datos
            $title      = $this->request->getPost('title');
            $end_date   = $this->request->getPost('end_date');
            $image      = $this->request->getFile('image');

            $slidesModel = new SlidesModel();
            $result = $slidesModel->editSlide($id, $title, $end_date, $image);

            // Salida de resultados
            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($title, "Angelópolis", "Modificación en Slide");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "El slide se actualizó satisfactoriamente en el sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible actualizar el slide en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    public function destroy($id)
    {
        try {
            $response = [];
            $slidesModel = new SlidesModel();
            $result = $slidesModel->removeSlide($id);
            $myTitle = $slidesModel->getSlide($id)->getRow()->title;

            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($myTitle, "Angelópolis", "Baja en Slide");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "El slide se eliminó satisfactoriamente del sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible eliminar el slide del sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    public function orderSlidesByPosition() {
        try {
            $slidesModel = new SlidesModel();
            $slides = $slidesModel->getSlidesOrderByPosition()->getResult();
            return  view('GSM/angelopolis/slides/order', compact('slides'));
        } catch (\Exception $e) {
            $message['site'] = "Angelópolis";
            $message['section'] = "Ordenar Slides";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function updateSlidesPosition($list) {
        try {
            $response = [];
            $slidesModel = new SlidesModel();
            $result = $slidesModel->updateSlidesPosition($list);
            if ($result) {
                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog("Ordenar Slides", "Angelópolis", "Modificación Posición Slides");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "La nueva posición de los slides se actualizó satisfactoriamente del sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible actualizar la nueva posicón de los slide en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }

    }

}
