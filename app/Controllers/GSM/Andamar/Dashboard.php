<?php
namespace App\Controllers\GSM\Andamar;
use App\Controllers\BaseController;

class Dashboard extends BaseController
{

    public function index()
    {
        return view('GSM/andamar/dashboard');
    }

}
