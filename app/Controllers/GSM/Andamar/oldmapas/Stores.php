<?php
namespace App\Controllers\GSM\Andamar;
use App\Controllers\BaseController;
use App\Models\GSM\Andamar\StoresModel;
use App\Models\LogModel;

class Stores extends BaseController
{

    public function index()
    {
        helper('text');

        $storesModel = new StoresModel();
        $stores      = $storesModel->getAllStores()->getResult();
        $levels      = $storesModel->getLevels()->getResult();

        return view('GSM/andamar/stores/index', compact('stores', 'levels'));
    }

    public function create()
    {
        $storesModel    = new StoresModel();
        $categories     = $storesModel->getCategories()->getResult();
        $mapLocations   = $storesModel->getMapLocations()->getResult();
        $levels         = $storesModel->getLevels()->getResult();

        return view('GSM/andamar/stores/create', compact('categories', 'mapLocations', 'levels'));
    }

    public function store()
    {
        // Validación de datos
        $rules = [
            'name' => [
                'label' => 'Nombre de Tienda',
                'rules' => 'trim|required|min_length[3]|max_length[50]',
                'errors' => [
                    'required' => 'El {field} es un dato requerido',
                    'min_length' => 'El {field} debe tener al menos {param} caracteres',
                    'max_length' => 'El {field} no debe exceder los {param} caracteres'
                ]
            ],
            'phone' => [
                'label' => 'Teléfono(s)',
                'rules' => 'trim|permit_empty|min_length[10]|max_length[23]',
                'errors' => [
                    'min_length' => 'El {field} debe tener al menos {param} caracteres',
                    'max_length' => 'Los {field} no deben exceder los {param} caracteres'
                ]
            ],
            'hourhand' => [
                'label' => 'Horarios',
                'rules' => 'trim|required|min_length[3]|max_length[150]',
                'errors' => [
                    'required' => 'Los {field} son un dato requerido',
                    'min_length' => 'Los {field} deben tener al menos {param} caracteres',
                    'max_length' => 'Los {field} no deben exceder los {param} caracteres'
                ]
            ],
            'url' => [
                'label' => 'Sitio Web',
                'rules' => 'trim|permit_empty|valid_url',
                'errors' => [
                    'valid_url' => 'El {field} no contiene una URL válida'
                ]
            ],
            'shorturl' => [
                'label' => 'URL Amigable',
                'rules' => 'trim|required_with[url]',
                'errors' => [
                    'valid_url' => 'La {field} no contiene una URL válida',
                    'required_with' => 'La {field} es un dato requerido'
                ]
            ],
            'email' => [
                'label' => 'Email',
                'rules' => 'trim|permit_empty|valid_email',
                'errors' => [
                    'valid_email' => 'El {field} parece ser un dato no válido',
                ]
            ],
            'location' => [
                'label' => 'Ubicación en Plaza',
                'rules' => 'required|numeric',
                'errors' => [
                    'required' => 'La {field} es un dato requerido',
                    'numeric' => 'La {field} debe ser un valor numérico'
                ]
            ],
            'category' => [
                'label' => 'Categoría',
                'rules' => 'required|numeric',
                'errors' => [
                    'required' => 'La {field} es un dato requerido',
                    'numeric' => 'La {field} debe ser un valor numérico'
                ]
            ],
            'keywords' => [
                'label' => 'Etiquetas',
                'rules' => 'trim|required|min_length[10]',
                'errors' => [
                    'required' => 'Las {field} son un dato requerido',
                    'min_length' => 'Las {field} deben tener al menos {param} caracteres'
                ]
            ],
            'map' => [
                'label' => 'Ubicación en Mapa',
                'rules' => 'required',
                'errors' => [
                    'required' => 'La {field} es un dato requerido',
                ]
            ],
            'description' => [
                'label' => 'Descripción de Tienda',
                'rules' => 'trim|required|min_length[20]|max_length[2000]',
                'errors' => [
                    'required' => 'La {field} es un dato requerido',
                    'min_length' => 'La {field} debe tener al menos {param} caracteres',
                    'max_length' => 'La {field} no debe exceder los {param} caracteres'
                ]
            ],
        ];

        $input = $this->validate($rules);

        $response = [];

        if (!$input) {
            $response["status"]     = "invalid";
            $response["code"]       = 406;
            $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
            $response['validation'] = $this->validator->getErrors();

            return json_encode($response);
        }

        // Almacenar información en la base de datos
        $name           = $this->request->getPost('name');
        $description    = $this->request->getPost('description');
        $email          = $this->request->getPost('email');
        $url            = $this->request->getPost('url');
        $shorturl       = $this->request->getPost('shorturl');
        $phone          = $this->request->getPost('phone');
        $hourhand       = $this->request->getPost('hourhand');
        $keywords       = $this->request->getPost('keywords');
        $location       = $this->request->getPost('location');
        $category       = $this->request->getPost('category');
        $map            = $this->request->getPost('map');
        $logotipo       = $this->request->getFile('logotipo');
        $cover          = $this->request->getFile('cover');

        $storesModel    = new StoresModel();
        $result         = $storesModel->createStore($name, $description, $email, $url, $shorturl, $phone, $hourhand, $keywords, $location, $category, $map, $logotipo, $cover);

        // Salida de resultados
        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog("Alta en Tienda - Andamar");

            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "La tienda se registró satisfactoriamente en el sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible registrar la tienda en el sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }

    public function edit($id)
    {
        $storesModel            = new StoresModel();
        $store                  = $storesModel->getStore($id)->getRow();
        $currentCategory        = $storesModel->getCategoryStore($id)->getRow();
        $mapLocations           = $storesModel->getMapLocations()->getResult();
        $categories             = $storesModel->getCategories()->getResult();
        $locations              = ['Terrazas', 'Planta baja', 'Primer nivel', 'Segundo nivel', 'Tercer nivel', 'Sótano 2'];
        $imageStore             = $storesModel->getImageStore($id);

        return view('GSM/andamar/stores/edit', compact('store', 'currentCategory', 'categories', 'locations', 'mapLocations', 'imageStore'));
    }

    public function update($id)
    {
        // Validación de datos
        $rules = [
            'name' => [
                'label' => 'Nombre',
                'rules' => 'trim|required|min_length[3]|max_length[50]',
                'errors' => [
                    'required' => 'El {field} es un dato requerido',
                    'min_length' => 'El {field} debe tener al menos {param} caracteres',
                    'max_length' => 'El {field} no debe exceder los {param} caracteres'
                ]
            ],
            'phone' => [
                'label' => 'Teléfono(s)',
                'rules' => 'trim|permit_empty|min_length[10]|max_length[23]',
                'errors' => [
                    'min_length' => 'El {field} debe tener al menos {param} caracteres',
                    'max_length' => 'Los {field} no deben exceder los {param} caracteres'
                ]
            ],
            'hourhand' => [
                'label' => 'Horarios',
                'rules' => 'trim|required|min_length[3]|max_length[150]',
                'errors' => [
                    'required' => 'Los {field} son un dato requerido',
                    'min_length' => 'Los {field} deben tener al menos {param} caracteres',
                    'max_length' => 'Los {field} no deben exceder los {param} caracteres'
                ]
            ],
            'url' => [
                'label' => 'Sitio Web',
                'rules' => 'trim|permit_empty|valid_url',
                'errors' => [
                    'valid_url' => 'El {field} no contiene una URL válida'
                ]
            ],
            'shorturl' => [
                'label' => 'URL Amigable',
                'rules' => 'trim|required_with[url]',
                'errors' => [
                    'valid_url' => 'La {field} no contiene una URL válida',
                    'required_with' => 'La {field} es un dato requerido'
                ]
            ],
            'email' => [
                'label' => 'Email',
                'rules' => 'trim|permit_empty|valid_email',
                'errors' => [
                    'valid_email' => 'El {field} parece ser un dato no válido',
                ]
            ],
            'location' => [
                'label' => 'Ubicación',
                'rules' => 'required|numeric',
                'errors' => [
                    'required' => 'La {field} es un dato requerido',
                    'numeric' => 'La {field} debe ser un valor numérico'
                ]
            ],
            'category' => [
                'label' => 'Categoría',
                'rules' => 'required|numeric',
                'errors' => [
                    'required' => 'La {field} es un dato requerido',
                    'numeric' => 'La {field} debe ser un valor numérico'
                ]
            ],
            'keywords' => [
                'label' => 'Etiquetas',
                'rules' => 'trim|required|min_length[10]',
                'errors' => [
                    'required' => 'Las {field} son un dato requerido',
                    'min_length' => 'Las {field} deben tener al menos {param} caracteres'
                ]
            ],
            'map' => [
                'label' => 'Ubicación en Mapa',
                'rules' => 'required',
                'errors' => [
                    'required' => 'La {field} es un dato requerido',
                ]
            ],
            'description' => [
                'label' => 'Descripción',
                'rules' => 'trim|required|min_length[20]|max_length[2000]',
                'errors' => [
                    'required' => 'La {field} es un dato requerido',
                    'min_length' => 'La {field} debe tener al menos {param} caracteres',
                    'max_length' => 'La {field} no debe exceder los {param} caracteres'
                ]
            ],
        ];

        $input = $this->validate($rules);

        $response = [];

        if (!$input) {
            $response["status"]     = "invalid";
            $response["code"]       = 406;
            $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
            $response['validation'] = $this->validator->getErrors();

            return json_encode($response);
        }

        // Almacenar información en la base de datos
        $name           = $this->request->getPost('name');
        $description    = $this->request->getPost('description');
        $email          = $this->request->getPost('email');
        $url            = $this->request->getPost('url');
        $shorturl       = $this->request->getPost('shorturl');
        $phone          = $this->request->getPost('phone');
        $hourhand       = $this->request->getPost('hourhand');
        $keywords       = $this->request->getPost('keywords');
        $location       = $this->request->getPost('location');
        $category       = $this->request->getPost('category');
        $map            = $this->request->getPost('map');
        $logotipo       = $this->request->getFile('logotipo');
        $cover          = $this->request->getFile('cover');

        $storesModel    = new StoresModel();
        $result         = $storesModel->editStore($id, $name, $description, $email, $url, $shorturl, $phone, $hourhand, $keywords, $location, $category, $map, $logotipo, $cover);

        // Salida de resultados
        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog("Modificación en Tienda - Andamar");

            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "La tienda se actualizó satisfactoriamente en el sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible actualizar la tienda en el sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }

    public function galleryEdit($id) {
        $storesModel   = new StoresModel();
        $store         = $storesModel->getStore($id)->getRow();
        $currentImages = esc($storesModel->getGalleryStore($id));

        return view('GSM/andamar/stores/gallery/edit', compact('store', 'currentImages'));
    }

    /*public function galleryDestroy($id, $image) {
        $response = [];
        $storesModel = new StoresModel();
        $result = $storesModel->removeGallery($id, $image);

        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog("Baja en Tienda [Galería] - Andamar");

            $response["success"] = "Imagen eliminada satisfactoriamente del sistema";
        } else {
            $response["error"] = "No fue posible eliminar la imagen del sistema, favor de intentar más tarde";
        }

        return json_encode($response);
    }*/

    /*public function galleryUpdate($id) {
        $images = $this->request->getFiles();

        $response = [];
        $storesModel = new StoresModel();
        $result = $storesModel->updateGallery($id, $images);

        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog("Modificación en Tienda [Galería] - Andamar");

            //$response["success"] = "Imagen registrada satisfactoriamente del sistema";
            $response = $result;
            //$response["initialPreviewConfig"] = $result
        } else {
            $response["error"] = "No fue posible registrar la imagen en el sistema, favor de intentar más tarde";
        }

        return json_encode($response);
    }*/

    public function destroy($id)
    {
        $response = [];
        $storesModel = new StoresModel();
        $result = $storesModel->removeStore($id);

        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog("Baja en Tienda - Andamar");

            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "La tienda se eliminó satisfactoriamente del sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible eliminar la tienda del sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }

}
