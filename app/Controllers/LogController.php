<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\LogModel;

class LogController extends Controller {

    public function index() {
        $logModel = new LogModel();

        $logs = $logModel->getAllLogs();
        
        return view('pages/logs', compact('logs'));
    }

}