<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\UserModel;

class LoginController extends Controller {

    private $encrypter;

    public function __construct()
    {
        $this->encrypter = \Config\Services::encrypter();
        helper('cookie');
    }

    public function login() {
        if ($this->userIsLogged()) {
            return redirect('dashboard');
        }

        return view('users/login');
    }

    public function auth() {
        $rules = [
            'email' => [
                'label' => 'correo electrónico',
                'rules' => 'trim|required|valid_email',
                'errors' => [
                    'required' => 'El {field} es requerido',
                    'valid_email' => 'El {field} no parece ser un dato válido',
                ]
            ],
            'password' => [
                'label' => 'contraseña del usuario',
                'rules' => 'required',
                'errors' => [
                    'required' => 'La {field} es un dato requerido',
                ]
            ]
        ];

        $input      = $this->validate($rules);
        $response   = [];

        if (!$input) {
            $response["status"]     = "invalid";
            $response["code"]       = 406;
            $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos.";
            $response['validation'] = $this->validator->getErrors();

            return json_encode($response);
        }

        // Almacenar información en base de datos
        $email = $this->request->getPost('email');
        $password = $this->request->getPost('password');
        $rememberme = $this->request->getPost('rememberme') ? intval($this->request->getPost('rememberme')) : 0;

        $user = $this->authenticateUser($email, $password);

        if ($user) {
            $this->setLoggedUser($user);
            $this->setCookieUser($rememberme, $user['email']);
            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "Felicidades, los datos de acceso son correctos.";
            return json_encode($response);
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "Los datos de acceso son incorrectos.";
            return json_encode($response);
        }
        return json_encode($response);
    }

    public function logout() {
        $session = session();

        delete_cookie('email');
        $session->destroy();
        // Investigar por que la cookies siguen existiendo si redirecciono en este punto
        // Pero si muestro la vista estas se eliminan
        return view('users/login');
    }

    private function setCookieUser($rememberme, $email)
    {
        $expires = 0;
        $email = $this->encrypter->encrypt($email);
        if ($rememberme)
            $expires = strtotime("+1 year");
        set_cookie('email', $email, $expires);
    }

    private function setLoggedUser($user)
    {
        $roleModel = model('RoleModel');
        $role = $roleModel->find($user['role_id']);

        $sessionData = [
            'user_id'       => $user['id'],
            'user_fullname' => $user['first_name'] . ' ' . $user['last_name'],
            'user_email'    => $user['email'],
            'role'          => $role->name,
            'logged_in'     => TRUE
        ];
        session()->set($sessionData);
    }

    public function authenticateUser($email, $password)
    {
        $userModel = new UserModel();
        $user = $userModel->where('email', $email)->first();
        if (!$user)
            return false;

        $validPassword = password_verify($password, $user['password']);
        if (!$validPassword)
            return false;

        return $user;
    }

    private function userIsLogged()
    {
        $userModel = model('UserModel');
        $roleModel = model('RoleModel');

        if (!session()->get('logged_in')) {
            if (get_cookie('email')) {
                $email = $this->encrypter->decrypt(get_cookie('email'));
                $user  = $userModel->where('email', $email)->first();
                if ($user) {
                    $role = $roleModel->find($user['role_id']);
                    $sessionData = [
                        'user_id'       => $user['id'],
                        'user_fullname' => $user['first_name'] . ' ' . $user['last_name'],
                        'user_email'    => $user['email'],
                        'role'          => $role->name,
                        'logged_in'     => TRUE
                    ];
                    session()->set($sessionData);
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
        return true;
    }

}