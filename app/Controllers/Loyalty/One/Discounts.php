<?php
namespace App\Controllers\Loyalty\One;
use App\Controllers\BaseController;
use App\Models\Loyalty\One\DiscountsModel;
use App\Models\LogModel;

class Discounts extends BaseController
{

    public function index()
    {
        $discountsModel = new DiscountsModel();
        $discounts = $discountsModel->getAllDiscounts()->getResult();

        return view('loyalty/one/discounts/index', compact('discounts'));
    }

    public function create()
    {
        return view('loyalty/one/discounts/create');
    }

    public function store()
    {
        // Validación de datos
        $rules = [
            'title' => [
                'label' => 'Titulo asociado al descuento',
                'rules' => 'trim|required|min_length[2]|max_length[80]',
                'errors' => [
                    'required' => 'El {field} es un dato requerido',
                    'min_length' => 'El {field} debe tener al menos {param} caracteres',
                    'max_length' => 'El {field} no debe exceder los {param} caracteres'
                ]
            ],
            'subtitle' => [
                'label' => 'Subtitulo asociado al descuento',
                'rules' => 'trim|required|min_length[3]|max_length[80]',
                'errors' => [
                    'required' => 'El {field} es un dato requerido',
                    'min_length' => 'El {field} debe tener al menos {param} caracteres',
                    'max_length' => 'El {field} no debe exceder los {param} caracteres'
                ]
            ],
            'description' => [
                'label' => 'Descripción asociada al descuento',
                'rules' => 'trim|required|min_length[20]|max_length[1000]',
                'errors' => [
                    'required' => 'La {field} es un dato requerido',
                    'min_length' => 'La {field} debe tener al menos {param} caracteres',
                    'max_length' => 'La {field} no debe exceder los {param} caracteres'
                ]
            ],
        ];

        $input = $this->validate($rules);

        $response = [];

        if (!$input) {
            $response["status"]     = "invalid";
            $response["code"]       = 406;
            $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
            $response['validation'] = $this->validator->getErrors();

            return json_encode($response);
        }

        // Almacenar información en la base de datos
        $title              = $this->request->getPost('title');
        $subtitle           = $this->request->getPost('subtitle');
        $description        = nl2br(strip_tags($this->request->getPost('description')));
        $image              = $this->request->getFile('image');
        
        $discountsModel = new DiscountsModel();
        $result = $discountsModel->createDiscount($title, $subtitle, $description, $image);

        // Salida de resultados
        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog($title, "One", "Alta en Descuento");

            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "El descuento se registró satisfactoriamente en el sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible registrar el descuento en el sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }

    public function edit($id)
    {
        $discountsModel = new DiscountsModel();
        $discount = $discountsModel->getDiscount($id)->getRow();
        // Retirar tags <br /> del contenido a mostrar en el textarea
        $discount->description = strip_tags(str_replace(['<br />', '<br>', '<br/>'], '&#13;', $discount->description));

        return view('loyalty/one/discounts/edit', compact('discount'));
    }

    public function update($id)
    {
        // Validación de datos
        $rules = [
            'title' => [
                'label' => 'Titulo asociado al descuento',
                'rules' => 'trim|required|min_length[2]|max_length[80]',
                'errors' => [
                    'required' => 'El {field} es un dato requerido',
                    'min_length' => 'El {field} debe tener al menos {param} caracteres',
                    'max_length' => 'El {field} no debe exceder los {param} caracteres'
                ]
            ],
            'subtitle' => [
                'label' => 'Subtitulo asociado al descuento',
                'rules' => 'trim|required|min_length[3]|max_length[80]',
                'errors' => [
                    'required' => 'El {field} es un dato requerido',
                    'min_length' => 'El {field} debe tener al menos {param} caracteres',
                    'max_length' => 'El {field} no debe exceder los {param} caracteres'
                ]
            ],
            'description' => [
                'label' => 'Descripción asociada al descuento',
                'rules' => 'trim|required|min_length[20]|max_length[1000]',
                'errors' => [
                    'required' => 'La {field} es un dato requerido',
                    'min_length' => 'La {field} debe tener al menos {param} caracteres',
                    'max_length' => 'La {field} no debe exceder los {param} caracteres'
                ]
            ],
        ];

        $input = $this->validate($rules);

        $response = [];

        if (!$input) {
            $response["status"]     = "invalid";
            $response["code"]       = 406;
            $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
            $response['validation'] = $this->validator->getErrors();

            return json_encode($response);
        }

        // Almacenar información en la base de datos
        $title              = $this->request->getPost('title');
        $subtitle           = $this->request->getPost('subtitle');
        $description        = nl2br(strip_tags($this->request->getPost('description')));
        $image              = $this->request->getFile('image');

        $discountsModel = new DiscountsModel();
        $result = $discountsModel->editDiscount($id, $title, $subtitle, $description, $image);

        // Salida de resultados
        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog($title, "One", "Modificación en Descuento");

            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "El descuento se actualizó satisfactoriamente en el sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible actualizar el descuento en el sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }

    public function destroy($id)
    {
        $response = [];
        $discountsModel = new DiscountsModel();
        $result = $discountsModel->removeDiscount($id);
        $myTitle = $discountsModel->getDiscount($id)->getRow()->name;

        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog($myTitle, "One", "Baja en Descuento");

            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "El descuento se eliminó satisfactoriamente del sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible eliminar el descuento en el sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }
}