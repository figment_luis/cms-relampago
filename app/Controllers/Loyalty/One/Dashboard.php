<?php
namespace App\Controllers\Loyalty\One;
use App\Controllers\BaseController;

class Dashboard extends BaseController
{

    public function index()
    {
        return view('loyalty/one/dashboard');
    }

}
