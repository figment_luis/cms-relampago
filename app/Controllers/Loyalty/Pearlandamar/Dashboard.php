<?php
namespace App\Controllers\Loyalty\Pearlandamar;
use App\Controllers\BaseController;

class Dashboard extends BaseController
{

    public function index()
    {
        return view('loyalty/pearlandamar/dashboard');
    }

}
