<?php
namespace App\Controllers\Loyalty\Pearlandamar;
use App\Controllers\BaseController;
use App\Models\Loyalty\Pearlandamar\StoresModel;
use App\Models\LogModel;

class Stores extends BaseController
{

    public function index()
    {
        $storesModel = new StoresModel();
        $stores = $storesModel->getAllStores()->getResult();
        $groups = $storesModel->getAllGroups()->getResult();

        return view('loyalty/pearlandamar/stores/index', compact('stores', 'groups'));
    }

    public function create()
    {
        $storesModel = new StoresModel();
        $groups = $storesModel->getAllGroups()->getResult();
        return view('loyalty/pearlandamar/stores/create', compact('groups'));
    }

    public function store()
    {
        // Validación de datos
        $rules = [
            'title' => [
                'label' => 'Titulo de tienda',
                'rules' => 'required',
                'errors' => [
                    'required' => 'El {field} es requerido'
                ]
            ],
            'multiple' => [
                'label' => 'Multiplicador',
                'rules' => 'required|numeric',
                'errors' => [
                    'required' => 'El {field} es requerido',
                    'numeric' => 'El {field} debe ser un valor numérico'
                ]
            ],
            'group' => [
                'label' => 'Grupo comercial',
                'rules' => 'required|numeric',
                'errors' => [
                    'required' => 'El {field} es requerido',
                    'numeric' => 'El {field} debe ser un valor numérico'
                ]
            ],
        ];

        $input = $this->validate($rules);

        $response = [];

        if (!$input) {
            $response["status"]     = "invalid";
            $response["code"]       = 406;
            $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
            $response['validation'] = $this->validator->getErrors();

            return json_encode($response);
        }

        // Almacenar información en base de datos
        $title              = $this->request->getPost('title');
        $multiple           = $this->request->getPost('multiple');
        $group              = $this->request->getPost('group');
        
        $storesModel = new StoresModel();
        $result = $storesModel->createStore($title, $multiple, $group);

        // Salida de resultados
        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog($title, "Pearl Andamar", "Alta en Tienda");

            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "La tienda se registró satisfactoriamente en el sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible registrar la tienda en el sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }

    public function edit($id)
    {
        $storesModel = new StoresModel();
        $store = $storesModel->getStore($id)->getRow();
        $groups = $storesModel->getAllGroups()->getResult();

        return view('loyalty/pearlandamar/stores/edit', compact('store', 'groups'));
    }

    public function update($id)
    {
        // Validación de datos
        $rules = [
            'title' => [
                'label' => 'Titulo de tienda',
                'rules' => 'trim|required|min_length[3]|max_length[40]',
                'errors' => [
                    'required' => 'El {field} es un dato requerido',
                    'min_length' => 'El {field} debe tener al menos {param} caracteres',
                    'max_length' => 'El {field} no debe exceder los {param} caracteres'
                ]
            ],
            'multiple' => [
                'label' => 'Multiplicador',
                'rules' => 'required|numeric',
                'errors' => [
                    'required' => 'El {field} es un dato requerido',
                    'numeric' => 'El {field} debe ser un valor numérico'
                ]
            ],
            'group' => [
                'label' => 'Grupo comercial',
                'rules' => 'required|numeric',
                'errors' => [
                    'required' => 'El {field} es un dato requerido',
                    'numeric' => 'El {field} debe ser un valor numérico'
                ]
            ],
        ];

        $input = $this->validate($rules);

        $response = [];

        if (!$input) {
            $response["status"]     = "invalid";
            $response["code"]       = 406;
            $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
            $response['validation'] = $this->validator->getErrors();

            return json_encode($response);
        }

        // Almacenar información en base de datos
        $title      = $this->request->getPost('title');
        $multiple   = $this->request->getPost('multiple');
        $group      = $this->request->getPost('group');

        $storesModel = new StoresModel();
        $result = $storesModel->editStore($id, $title, $multiple, $group);

        // Salida de resultados
        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog($title, "Pearl Andamar", "Modificación en Tienda");

            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "La tienda se actualizó satisfactoriamente en el sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible actualizar la tienda en el sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }

    public function destroy($id)
    {
        $response = [];
        $storesModel = new StoresModel();
        $result = $storesModel->removeStore($id);
        $myName = $storesModel->getStore($id)->getRow()->name;

        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog($myName, "Pearl Andamar", "Baja en Tienda");

            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "La tienda se eliminó satisfactoriamente del sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible eliminar la tienda en el sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }

    public function activate($id)
    {
        $response = [];
        $storesModel = new StoresModel();
        $result = $storesModel->activateStore($id);
        $myName = $storesModel->getStore($id)->getRow()->name;

        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog($myName, "Pearl Andamar", "Activación en Tienda");

            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "La tienda se activo satisfactoriamente del sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible activar la tienda en el sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }

}