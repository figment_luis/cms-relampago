<?php
namespace App\Controllers\Loyalty\Rewards;
use App\Controllers\BaseController;

class Dashboard extends BaseController
{

    public function index()
    {
        return view('loyalty/rewards/dashboard');
    }

}
