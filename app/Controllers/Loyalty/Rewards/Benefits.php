<?php
namespace App\Controllers\Loyalty\Rewards;
use App\Controllers\BaseController;
use App\Models\Loyalty\Rewards\BenefitsModel;
use App\Models\LogModel;

class Benefits extends BaseController
{

    public function index()
    {
        $benefitsModel = new BenefitsModel();
        $benefits = $benefitsModel->getAllBenefits()->getResult();

        return view('loyalty/rewards/benefits/index', compact('benefits'));
    }

    public function create()
    {
        return view('loyalty/rewards/benefits/create');
    }

    public function store()
    {
        // Validación de datos
        $rules = [
            'title' => [
                'label' => 'Titulo del beneficio',
                'rules' => 'trim|required|min_length[3]|max_length[80]',
                'errors' => [
                    'required' => 'El {field} es requerido',
                    'min_length' => 'El {field} debe tener al menos {param} caracteres',
                    'max_length' => 'El {field} no debe exceder los {param} caracteres'
                ]
            ],
            'stock' => [
                'label' => 'Stock inicial del beneficio',
                'rules' => 'trim|required|numeric|is_natural_no_zero',
                'errors' => [
                    'required' => 'El {field} es requerido',
                    'numeric' => 'El {field} debe ser un valor numérico',
                    'is_natural_no_zero' => 'El {field} debe ser un valor numérico entero mayor a cero'
                ]
            ],
            'description' => [
                'label' => 'Descripción del beneficio',
                'rules' => 'trim|required|min_length[20]|max_length[1000]',
                'errors' => [
                    'required' => 'La {field} es un dato requerido',
                    'min_length' => 'La {field} debe tener al menos {param} caracteres',
                    'max_length' => 'La {field} no debe exceder los {param} caracteres'
                ]
            ],
        ];

        $input = $this->validate($rules);

        $response = [];

        if (!$input) {
            $response["status"]     = "invalid";
            $response["code"]       = 406;
            $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
            $response['validation'] = $this->validator->getErrors();

            return json_encode($response);
        }

        // Almacenar información en base de datos
        $title              = $this->request->getPost('title');
        $stock              = $this->request->getPost('stock');
        $description        = nl2br(strip_tags($this->request->getPost('description')));
        $image              = $this->request->getFile('image');
        $cards              = $this->request->getPost('cards');
        
        $benefitsModel = new BenefitsModel();
        $result = $benefitsModel->createBenefit($title, $stock, $description, $cards, $image);

        // Salida de resultados
        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog($title, "Rewards", "Alta en Beneficio");

            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "El beneficio se registró satisfactoriamente en el sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible registrar el beneficio en el sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }

    public function edit($id)
    {
        $benefitsModel  = new BenefitsModel();
        $benefit        = $benefitsModel->getBenefit($id)->getRow();
        $currentCards   = $benefitsModel->getCardsBenefit($id)->getResultArray();
        // Retirar tags <br /> del contenido a mostrar en el textarea
        $benefit->description = strip_tags(str_replace(['<br />', '<br>', '<br/>'], '&#13;', $benefit->description));

        return view('loyalty/rewards/benefits/edit', compact('benefit', 'currentCards'));
    }

    public function update($id)
    {
        // Validación de datos
        $rules = [
            'title' => [
                'label' => 'Titulo del beneficio',
                'rules' => 'trim|required|min_length[3]|max_length[80]',
                'errors' => [
                    'required' => 'El {field} es un dato requerido',
                    'min_length' => 'El {field} debe tener al menos {param} caracteres',
                    'max_length' => 'El {field} no debe exceder los {param} caracteres'
                ]
            ],
            'stock' => [
                'label' => 'Stock real del beneficio',
                'rules' => 'trim|required|numeric|is_natural',
                'errors' => [
                    'required' => 'El {field} es un dato requerido',
                    'numeric' => 'El {field} debe ser un valor numérico',
                    'is_natural' => 'El {field} debe ser un valor numérico entero positivo'
                ]
            ],
            'description' => [
                'label' => 'Descripción del beneficio',
                'rules' => 'trim|required|min_length[20]|max_length[1000]',
                'errors' => [
                    'required' => 'La {field} es un dato requerido',
                    'min_length' => 'La {field} debe tener al menos {param} caracteres',
                    'max_length' => 'La {field} no debe exceder los {param} caracteres'
                ]
            ],
        ];

        $input = $this->validate($rules);

        $response = [];

        if (!$input) {
            $response["status"]     = "invalid";
            $response["code"]       = 406;
            $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
            $response['validation'] = $this->validator->getErrors();

            return json_encode($response);
        }

        // Almacenar información en base de datos
        $title              = $this->request->getPost('title');
        $stock              = $this->request->getPost('stock');
        $description        = nl2br(strip_tags($this->request->getPost('description')));
        $image              = $this->request->getFile('image');
        $cards              = $this->request->getPost('cards');

        $benefitsModel = new BenefitsModel();
        $result = $benefitsModel->editBenefit($id, $title, $stock, $description, $cards, $image);

        // Salida de resultados
        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog($title, "Rewards", "Modificación en Beneficio");

            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "El beneficio se actualizó satisfactoriamente en el sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible actualizar el beneficio en el sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }

    public function destroy($id)
    {
        $response = [];
        $benefitsModel = new BenefitsModel();
        $result = $benefitsModel->removeBenefit($id);
        $myTitle = $benefitsModel->getBenefit($id)->getRow()->name;

        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog($myTitle, "Rewards", "Baja en Beneficio");

            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "El beneficio se eliminó satisfactoriamente del sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible eliminar el beneficio en el sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }
}