<?php
namespace App\Controllers\Loyalty\Rewards;
use App\Controllers\BaseController;
use App\Models\Loyalty\Rewards\AwardsModel;
use App\Models\LogModel;

class Awards extends BaseController
{

    public function index()
    {
        $awardsModel = new AwardsModel();
        $awards = $awardsModel->getAllAwards()->getResult();

        return view('loyalty/rewards/awards/index', compact('awards'));
    }

    public function create()
    {
        return view('loyalty/rewards/awards/create');
    }

    public function store()
    {
        // Validación de datos
        $rules = [
            'title' => [
                'label' => 'Nombre de certificado',
                'rules' => 'trim|required|min_length[3]|max_length[80]',
                'errors' => [
                    'required' => 'El {field} es requerido',
                    'min_length' => 'El {field} debe tener al menos {param} caracteres',
                    'max_length' => 'El {field} no debe exceder los {param} caracteres'
                ]
            ],
            'subtitle' => [
                'label' => 'Subtitulo de certificado',
                'rules' => 'trim|required|min_length[3]|max_length[40]',
                'errors' => [
                    'required' => 'El {field} es requerido',
                    'min_length' => 'El {field} debe tener al menos {param} caracteres',
                    'max_length' => 'El {field} no debe exceder los {param} caracteres'
                ]
            ],
            'points' => [
                'label' => 'Puntos de certificado',
                'rules' => 'trim|required|numeric|is_natural_no_zero',
                'errors' => [
                    'required' => 'Los {field} son requeridos',
                    'numeric' => 'Los {field} deben ser un valor numérico',
                    'is_natural_no_zero' => 'Los {field} debe ser un valor numérico entero mayor a cero'
                ]
            ],
            'stock' => [
                'label' => 'Stock inicial de certificado',
                'rules' => 'trim|required|numeric|is_natural_no_zero',
                'errors' => [
                    'required' => 'El {field} es requerido',
                    'numeric' => 'El {field} debe ser un valor numérico',
                    'is_natural_no_zero' => 'El {field} debe ser un valor numérico entero mayor a cero'
                ]
            ],
            'description' => [
                'label' => 'Descripción de certificado',
                'rules' => 'trim|required|min_length[20]|max_length[1000]',
                'errors' => [
                    'required' => 'La {field} es un dato requerido',
                    'min_length' => 'La {field} debe tener al menos {param} caracteres',
                    'max_length' => 'La {field} no debe exceder los {param} caracteres'
                ]
            ],
        ];

        $input = $this->validate($rules);

        $response = [];

        if (!$input) {
            $response["status"]     = "invalid";
            $response["code"]       = 406;
            $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
            $response['validation'] = $this->validator->getErrors();

            return json_encode($response);
        }

        // Almacenar información en base de datos
        $title              = $this->request->getPost('title');
        $subtitle           = $this->request->getPost('subtitle');
        $points             = $this->request->getPost('points');
        $stock              = $this->request->getPost('stock');
        $description        = nl2br(strip_tags($this->request->getPost('description')));
        $image              = $this->request->getFile('image');
        $galleryFiles       = $this->request->getFiles();
        
        $awardsModel = new AwardsModel();
        $result = $awardsModel->createAward($title, $subtitle, $points, $stock, $description, $image, $galleryFiles);

        // Salida de resultados
        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog($title, "Rewards", "Alta en Certificado");

            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "El certificado se registró satisfactoriamente en el sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible registrar el certificado en el sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }

    public function edit($id)
    {
        $awardsModel = new AwardsModel();
        $award = $awardsModel->getAward($id)->getRow();
        // Retirar tags <br /> del contenido a mostrar en el textarea
        $award->description = strip_tags(str_replace(['<br />', '<br>', '<br/>'], '&#13;', $award->description));

        return view('loyalty/rewards/awards/edit', compact('award'));
    }

    public function update($id)
    {
        // Validación de datos
        $rules = [
            'title' => [
                'label' => 'Nombre de certificado',
                'rules' => 'trim|required|min_length[3]|max_length[80]',
                'errors' => [
                    'required' => 'El {field} es un dato requerido',
                    'min_length' => 'El {field} debe tener al menos {param} caracteres',
                    'max_length' => 'El {field} no debe exceder los {param} caracteres'
                ]
            ],
            'subtitle' => [
                'label' => 'Subtitulo de certificado',
                'rules' => 'trim|required|min_length[3]|max_length[40]',
                'errors' => [
                    'required' => 'El {field} es un dato requerido',
                    'min_length' => 'El {field} debe tener al menos {param} caracteres',
                    'max_length' => 'El {field} no debe exceder los {param} caracteres'
                ]
            ],
            'points' => [
                'label' => 'Puntos de certificado',
                'rules' => 'trim|required|numeric|is_natural_no_zero',
                'errors' => [
                    'required' => 'Los {field} son un dato requerido',
                    'numeric' => 'Los {field} deben ser un valor numérico',
                    'is_natural_no_zero' => 'Los {field} debe ser un valor numérico entero mayor a cero'
                ]
            ],
            'stock' => [
                'label' => 'Stock inicial de certificado',
                'rules' => 'trim|required|numeric|is_natural',
                'errors' => [
                    'required' => 'El {field} es un dato requerido',
                    'numeric' => 'El {field} debe ser un valor numérico',
                    'is_natural' => 'El {field} debe ser un valor numérico entero positivo'
                ]
            ],
            'description' => [
                'label' => 'Descripción de certificado',
                'rules' => 'trim|required|min_length[20]|max_length[1000]',
                'errors' => [
                    'required' => 'La {field} es un dato requerido',
                    'min_length' => 'La {field} debe tener al menos {param} caracteres',
                    'max_length' => 'La {field} no debe exceder los {param} caracteres'
                ]
            ],
        ];

        $input = $this->validate($rules);

        $response = [];

        if (!$input) {
            $response["status"]     = "invalid";
            $response["code"]       = 406;
            $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
            $response['validation'] = $this->validator->getErrors();

            return json_encode($response);
        }

        // Almacenar información en base de datos
        $title              = $this->request->getPost('title');
        $subtitle           = $this->request->getPost('subtitle');
        $points             = $this->request->getPost('points');
        $stock              = $this->request->getPost('stock');
        $description        = nl2br(strip_tags($this->request->getPost('description')));
        $image              = $this->request->getFile('image');

        $awardsModel = new AwardsModel();
        $result = $awardsModel->editAward($id, $title, $subtitle, $points, $stock, $description, $image);

        // Salida de resultados
        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog($title, "Rewards", "Modificación en Certificado");

            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "El certificado se actualizó satisfactoriamente en el sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible actualizar el certificado en el sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }

    public function destroy($id)
    {
        $response = [];
        $awardsModel = new AwardsModel();
        $result = $awardsModel->removeAward($id);
        $myTitle = $awardsModel->getAward($id)->getRow()->name;

        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog($myTitle, "Rewards", "Baja en Certificado");

            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "El certificado se eliminó satisfactoriamente del sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible eliminar el certificado en el sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }

    public function editGallery($id) {
        $awardsModel = new AwardsModel();
        $award = $awardsModel->getAward($id)->getRow();

        return view('loyalty/rewards/awards/gallery/edit', compact('award'));
    }

    public function destroyGallery($id, $image) {
        $response = [];
        $awardsModel = new AwardsModel();
        $result = $awardsModel->destroyGallery($id, $image);
        $myTitle = $awardsModel->getAward($id)->getRow()->name;

        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog("Baja en Galería - " . $myTitle, "Rewards", "Baja en Certificado [Galería]");

            $response["success"] = "Imagen eliminada satisfactoriamente del sistema";
        } else {
            $response["error"] = "No fue posible eliminar la imagen del sistema, favor de intentar más tarde";
        }

        return json_encode($response);
    }

    public function updateGallery($id) {
        $galleryFiles = $this->request->getFiles();

        $response = [];
        $awardsModel = new AwardsModel();
        $result = $awardsModel->updateGallery($id, $galleryFiles);
        $myTitle = $awardsModel->getAward($id)->getRow()->name;

        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog("Modificación en Galería - " . $myTitle, "Rewards", "Modificación en Certificado [Galería]");

            $response = $result;
        } else {
            $response["error"] = "No fue posible registrar la imagen en el sistema, favor de intentar más tarde";
        }

        return json_encode($response);
    }
}