<?php
namespace App\Controllers\Loyalty\Addicted;
use App\Controllers\BaseController;
use App\Models\Loyalty\Addicted\StoresModel;
use App\Models\LogModel;

class Stores extends BaseController
{

    public function index()
    {
        $storesModel = new StoresModel();
        $stores = $storesModel->getAllStores()->getResult();

        return view('loyalty/addicted/stores/index', compact('stores'));
    }

    public function create()
    {
        $storesModel = new StoresModel();
        return view('loyalty/addicted/stores/create');
    }

    public function store()
    {
        // Validación de datos
        $rules = [
            'title' => [
                'label' => 'Titulo de tienda',
                'rules' => 'required',
                'errors' => [
                    'required' => 'El {field} es requerido'
                ]
            ],
        ];

        $input = $this->validate($rules);

        $response = [];

        if (!$input) {
            $response["status"]     = "invalid";
            $response["code"]       = 406;
            $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
            $response['validation'] = $this->validator->getErrors();

            return json_encode($response);
        }

        // Almacenar información en base de datos
        $title = $this->request->getPost('title');
        
        $storesModel = new StoresModel();
        $result = $storesModel->createStore($title);

        // Salida de resultados
        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog($title, "Addicted", "Alta en Tienda");

            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "La tienda se registró satisfactoriamente en el sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible registrar la tienda en el sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }

    public function edit($id)
    {
        $storesModel = new StoresModel();
        $store = $storesModel->getStore($id)->getRow();

        return view('loyalty/addicted/stores/edit', compact('store'));
    }

    public function update($id)
    {
        // Validación de datos
        $rules = [
            'title' => [
                'label' => 'Titulo de tienda',
                'rules' => 'trim|required|min_length[3]|max_length[40]',
                'errors' => [
                    'required' => 'El {field} es un dato requerido',
                    'min_length' => 'El {field} debe tener al menos {param} caracteres',
                    'max_length' => 'El {field} no debe exceder los {param} caracteres'
                ]
            ],
        ];

        $input = $this->validate($rules);

        $response = [];

        if (!$input) {
            $response["status"]     = "invalid";
            $response["code"]       = 406;
            $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
            $response['validation'] = $this->validator->getErrors();

            return json_encode($response);
        }

        // Almacenar información en base de datos
        $title = $this->request->getPost('title');

        $storesModel = new StoresModel();
        $result = $storesModel->editStore($id, $title);

        // Salida de resultados
        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog($title, "Addicted", "Modificación en Tienda");

            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "La tienda se actualizó satisfactoriamente en el sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible actualizar la tienda en el sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }

    public function destroy($id)
    {
        $response = [];
        $storesModel = new StoresModel();
        $result = $storesModel->removeStore($id);
        $myName = $storesModel->getStore($id)->getRow()->name;

        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog($myName, "Addicted", "Baja en Tienda");

            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "La tienda se eliminó satisfactoriamente del sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible eliminar la tienda en el sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }

    public function activate($id)
    {
        $response = [];
        $storesModel = new StoresModel();
        $result = $storesModel->activateStore($id);
        $myName = $storesModel->getStore($id)->getRow()->name;

        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog($myName, "Addicted", "Activación en Tienda");

            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "La tienda se activo satisfactoriamente del sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible activar la tienda en el sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }

}