<?php
namespace App\Controllers\Loyalty\Addicted;
use App\Controllers\BaseController;
use App\Models\Loyalty\Addicted\ExperiencesModel;
use App\Models\LogModel;

class Experiences extends BaseController
{

    public function index()
    {
        $experiencesModel = new ExperiencesModel();
        $experiences = $experiencesModel->getAllExperiences()->getResult();

        return view('loyalty/addicted/experiences/index', compact('experiences'));
    }

    public function create()
    {
        return view('loyalty/addicted/experiences/create');
    }

    public function store()
    {
        // Validación de datos
        $rules = [
            'title' => [
                'label' => 'Titulo asociado a la experiencia',
                'rules' => 'trim|required|min_length[3]|max_length[80]',
                'errors' => [
                    'required' => 'El {field} es un dato requerido',
                    'min_length' => 'El {field} debe tener al menos {param} caracteres',
                    'max_length' => 'El {field} no debe exceder los {param} caracteres'
                ]
            ],
            'points' => [
                'label' => 'Puntos asociados a la experiencia',
                'rules' => 'trim|required|numeric|is_natural_no_zero',
                'errors' => [
                    'required' => 'Los {field} son un dato requerido',
                    'numeric' => 'Los {field} deben ser un valor numérico',
                    'is_natural_no_zero' => 'Los {field} deben ser un valor numérico entero mayor a cero'
                ]
            ],
            'link' => [
                'label' => 'Link asociado a la experiencia',
                'rules' => 'trim|permit_empty|valid_url',
                'errors' => [
                    'valid_url' => 'El {field} debe ser una URL válida (http:// | https://)'
                ]
            ],
            'description' => [
                'label' => 'Descripción asociada a la experiencia',
                'rules' => 'trim|required|min_length[20]|max_length[1000]',
                'errors' => [
                    'required' => 'La {field} es un dato requerido',
                    'min_length' => 'La {field} debe tener al menos {param} caracteres',
                    'max_length' => 'La {field} no debe exceder los {param} caracteres'
                ]
            ],
        ];

        $input = $this->validate($rules);

        $response = [];

        if (!$input) {
            $response["status"]     = "invalid";
            $response["code"]       = 406;
            $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
            $response['validation'] = $this->validator->getErrors();

            return json_encode($response);
        }

        // Almacenar información en la base de datos
        $title              = $this->request->getPost('title');
        $points             = $this->request->getPost('points');
        $link               = $this->request->getPost('link');
        $description        = nl2br(strip_tags($this->request->getPost('description')));
        $image              = $this->request->getFile('image');
        
        $experiencesModel = new ExperiencesModel();
        $result = $experiencesModel->createExperience($title, $points, $link, $description, $image);

        // Salida de resultados
        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog($title, "Addicted", "Alta en Experiencia");

            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "La experiencia se registró satisfactoriamente en el sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible registrar la experiencia en el sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }

    public function edit($id)
    {
        $experiencesModel = new ExperiencesModel();
        $experience = $experiencesModel->getExperience($id)->getRow();
        // Retirar tags <br /> del contenido a mostrar en el textarea
        $experience->description = strip_tags(str_replace(['<br />', '<br>', '<br/>'], '&#13;', $experience->description));

        return view('loyalty/addicted/experiences/edit', compact('experience'));
    }

    public function update($id)
    {
        // Validación de datos
        $rules = [
            'title' => [
                'label' => 'Titulo asociado a la experiencia',
                'rules' => 'trim|required|min_length[3]|max_length[80]',
                'errors' => [
                    'required' => 'El {field} es un dato requerido',
                    'min_length' => 'El {field} debe tener al menos {param} caracteres',
                    'max_length' => 'El {field} no debe exceder los {param} caracteres'
                ]
            ],
            'points' => [
                'label' => 'Puntos asociados a la experiencia',
                'rules' => 'trim|required|numeric|is_natural_no_zero',
                'errors' => [
                    'required' => 'Los {field} son un dato requerido',
                    'numeric' => 'Los {field} deben ser un valor numérico',
                    'is_natural_no_zero' => 'Los {field} deben ser un valor numérico entero mayor a cero'
                ]
            ],
            'link' => [
                'label' => 'Link asociado a la experiencia',
                'rules' => 'trim|permit_empty|valid_url',
                'errors' => [
                    'valid_url' => 'El {field} debe ser una URL válida (http:// | https://)'
                ]
            ],
            'description' => [
                'label' => 'Descripción asociada a la experiencia',
                'rules' => 'trim|required|min_length[20]|max_length[1000]',
                'errors' => [
                    'required' => 'La {field} es un dato requerido',
                    'min_length' => 'La {field} debe tener al menos {param} caracteres',
                    'max_length' => 'La {field} no debe exceder los {param} caracteres'
                ]
            ],
        ];

        $input = $this->validate($rules);

        $response = [];

        if (!$input) {
            $response["status"]     = "invalid";
            $response["code"]       = 406;
            $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
            $response['validation'] = $this->validator->getErrors();

            return json_encode($response);
        }

        // Almacenar información en la base de datos
        $title              = $this->request->getPost('title');
        $points             = $this->request->getPost('points');
        $link               = $this->request->getPost('link');
        $description        = nl2br(strip_tags($this->request->getPost('description')));
        $image              = $this->request->getFile('image');

        $experiencesModel = new ExperiencesModel();
        $result = $experiencesModel->editExperience($id, $title, $points, $link, $description, $image);

        // Salida de resultados
        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog($title, "Addicted", "Modificación en Experiencia");

            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "La experiencia se actualizó satisfactoriamente en el sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible actualizar la experiencia en el sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }

    public function destroy($id)
    {
        $response = [];
        $experiencesModel = new ExperiencesModel();
        $result = $experiencesModel->removeExperience($id);
        $myTitle = $experiencesModel->getExperience($id)->getRow()->title;

        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog($myTitle, "Addicted", "Baja en Experiencia");

            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "La experiencia se eliminó satisfactoriamente del sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible eliminar la experiencia en el sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }
}