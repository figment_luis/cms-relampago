<?php
namespace App\Controllers\Loyalty\Addicted;
use App\Controllers\BaseController;

class Dashboard extends BaseController
{

    public function index()
    {
        return view('loyalty/addicted/dashboard');
    }

}
