<?php
namespace App\Controllers\Loyalty\Addicted;
use App\Controllers\BaseController;
use App\Models\Loyalty\Addicted\AwardsModel;
use App\Models\LogModel;

class Awards extends BaseController
{

    public function index()
    {
        $awardsModel = new AwardsModel();
        $awards = $awardsModel->getAllAwards()->getResult();

        return view('loyalty/addicted/awards/index', compact('awards'));
    }

    public function create()
    {
        return view('loyalty/addicted/awards/create');
    }

    public function store()
    {
        // Validación de datos
        $rules = [
            'title' => [
                'label' => 'Nombre de certificado',
                'rules' => 'trim|required|min_length[3]|max_length[80]',
                'errors' => [
                    'required' => 'El {field} es requerido',
                    'min_length' => 'El {field} debe tener al menos {param} caracteres',
                    'max_length' => 'El {field} no debe exceder los {param} caracteres'
                ]
            ],
            'points' => [
                'label' => 'Puntos de certificado',
                'rules' => 'trim|required|numeric|is_natural_no_zero',
                'errors' => [
                    'required' => 'Los {field} son requeridos',
                    'numeric' => 'Los {field} deben ser un valor numérico',
                    'is_natural_no_zero' => 'Los {field} debe ser un valor numérico entero mayor a cero'
                ]
            ],
            'price' => [
                'label' => 'Precio de certificado',
                'rules' => 'trim|required|numeric|greater_than[0]',
                'errors' => [
                    'required' => 'El {field} es requerido',
                    'numeric' => 'El {field} debe ser un valor numérico',
                    'greater_than' => 'El {field} debe ser un valor numérico mayor a cero'
                ]
            ],
            'description' => [
                'label' => 'Descripción de certificado',
                'rules' => 'trim|required|min_length[20]|max_length[1000]',
                'errors' => [
                    'required' => 'La {field} es un dato requerido',
                    'min_length' => 'La {field} debe tener al menos {param} caracteres',
                    'max_length' => 'La {field} no debe exceder los {param} caracteres'
                ]
            ],
            'link' => [
                'label' => 'Link de certificado',
                'rules' => 'trim|permit_empty|valid_url',
                'errors' => [
                    'valid_url' => 'El {field} no es una URL válida'
                ]
            ],
        ];

        $input = $this->validate($rules);

        $response = [];

        if (!$input) {
            $response["status"]     = "invalid";
            $response["code"]       = 406;
            $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
            $response['validation'] = $this->validator->getErrors();

            return json_encode($response);
        }

        // Almacenar información en la base de datos
        $title              = $this->request->getPost('title');
        $points             = $this->request->getPost('points');
        $price              = $this->request->getPost('price');
        $link               = $this->request->getPost('link');
        $description        = nl2br(strip_tags($this->request->getPost('description')));
        $image              = $this->request->getFile('image');
        
        $awardsModel = new AwardsModel();
        $result = $awardsModel->createAward($title, $points, $price, $link, $description, $image);

        // Salida de resultados
        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog($title, "Addicted", "Alta en Certificado");

            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "El certificado se registró satisfactoriamente en el sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible registrar el certificado en el sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }

    public function edit($id)
    {
        $awardsModel = new AwardsModel();
        $award = $awardsModel->getAward($id)->getRow();
        // Retirar tags <br /> del contenido a mostrar en el textarea
        $award->description = strip_tags(str_replace(['<br />', '<br>', '<br/>'], '&#13;', $award->description));

        return view('loyalty/addicted/awards/edit', compact('award'));
    }

    public function update($id)
    {
        // Validación de datos
        $rules = [
            'title' => [
                'label' => 'Nombre de certificado',
                'rules' => 'trim|required|min_length[3]|max_length[80]',
                'errors' => [
                    'required' => 'El {field} es un dato requerido',
                    'min_length' => 'El {field} debe tener al menos {param} caracteres',
                    'max_length' => 'El {field} no debe exceder los {param} caracteres'
                ]
            ],
            'points' => [
                'label' => 'Puntos de certificado',
                'rules' => 'trim|required|numeric|is_natural_no_zero',
                'errors' => [
                    'required' => 'Los {field} son un dato requerido',
                    'numeric' => 'Los {field} deben ser un valor numérico',
                    'is_natural_no_zero' => 'Los {field} debe ser un valor numérico entero mayor a cero'
                ]
            ],
            'price' => [
                'label' => 'Precio de certificado',
                'rules' => 'trim|required|numeric|greater_than[0]',
                'errors' => [
                    'required' => 'El {field} es un dato requerido',
                    'numeric' => 'El {field} debe ser un valor numérico',
                    'greater_than' => 'El {field} debe ser un valor numérico mayor a cero'
                ]
            ],
            'link' => [
                'label' => 'Link de certificado',
                'rules' => 'trim|permit_empty|valid_url',
                'errors' => [
                    'valid_url' => 'El {field} debe ser una URL válida (http:// | https://)'
                ]
            ],
            'description' => [
                'label' => 'Descripción de certificado',
                'rules' => 'trim|required|min_length[20]|max_length[1000]',
                'errors' => [
                    'required' => 'La {field} es un dato requerido',
                    'min_length' => 'La {field} debe tener al menos {param} caracteres',
                    'max_length' => 'La {field} no debe exceder los {param} caracteres'
                ]
            ],
        ];

        $input = $this->validate($rules);

        $response = [];

        if (!$input) {
            $response["status"]     = "invalid";
            $response["code"]       = 406;
            $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
            $response['validation'] = $this->validator->getErrors();

            return json_encode($response);
        }

        // Almacenar información en la base de datos
        $title              = $this->request->getPost('title');
        $points             = $this->request->getPost('points');
        $price              = $this->request->getPost('price');
        $link               = $this->request->getPost('link');
        $description        = nl2br(strip_tags($this->request->getPost('description')));
        $image              = $this->request->getFile('image');

        $awardsModel = new AwardsModel();
        $result = $awardsModel->editAward($id, $title, $points, $price, $link, $description, $image);

        // Salida de resultados
        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog($title, "Addicted", "Modificación en Certificado");

            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "El certificado se actualizó satisfactoriamente en el sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible actualizar el certificado en el sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }

    public function destroy($id)
    {
        $response = [];
        $awardsModel = new AwardsModel();
        $result = $awardsModel->removeAward($id);
        $myTitle = $awardsModel->getAward($id)->getRow()->title;

        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog($myTitle, "Addicted", "Baja en Certificado");

            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "El certificado se eliminó satisfactoriamente del sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible eliminar el certificado en el sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }
}