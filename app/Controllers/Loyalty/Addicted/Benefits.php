<?php
namespace App\Controllers\Loyalty\Addicted;
use App\Controllers\BaseController;
use App\Models\Loyalty\Addicted\BenefitsModel;
use App\Models\LogModel;

class Benefits extends BaseController
{

    public function index()
    {
        $benefitsModel = new BenefitsModel();
        $benefits = $benefitsModel->getAllBenefits()->getResult();

        return view('loyalty/addicted/benefits/index', compact('benefits'));
    }

    public function create()
    {
        return view('loyalty/addicted/benefits/create');
    }

    public function store()
    {
        // Validación de datos
        $rules = [
            'title' => [
                'label' => 'Titulo del beneficio',
                'rules' => 'trim|required|min_length[3]|max_length[80]',
                'errors' => [
                    'required' => 'El {field} es requerido',
                    'min_length' => 'El {field} debe tener al menos {param} caracteres',
                    'max_length' => 'El {field} no debe exceder los {param} caracteres'
                ]
            ],
            /*'points' => [
                'label' => 'Puntos del beneficio',
                'rules' => 'trim|required|numeric|is_natural_no_zero',
                'errors' => [
                    'required' => 'Los {field} son requeridos',
                    'numeric' => 'Los {field} deben ser un valor numérico',
                    'is_natural_no_zero' => 'Los {field} deben ser un valor numérico entero mayor a cero'
                ]
            ],*/
            'link' => [
                'label' => 'Link del beneficio',
                'rules' => 'trim|permit_empty|valid_url',
                'errors' => [
                    'valid_url' => 'El {field} debe ser una url válida (http:// | https://)',
                ]
            ],
            'description' => [
                'label' => 'Descripción del beneficio',
                'rules' => 'trim|required|min_length[20]|max_length[1000]',
                'errors' => [
                    'required' => 'La {field} es un dato requerido',
                    'min_length' => 'La {field} debe tener al menos {param} caracteres',
                    'max_length' => 'La {field} no debe exceder los {param} caracteres'
                ]
            ],
        ];

        $input = $this->validate($rules);

        $response = [];

        if (!$input) {
            $response["status"]     = "invalid";
            $response["code"]       = 406;
            $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
            $response['validation'] = $this->validator->getErrors();

            return json_encode($response);
        }

        // Almacenar información en base de datos
        $title              = $this->request->getPost('title');
        //$points             = $this->request->getPost('points');
        $points             = 100;
        $link               = $this->request->getPost('link');
        $description        = nl2br(strip_tags($this->request->getPost('description')));
        $image              = $this->request->getFile('image');
        
        $benefitsModel = new BenefitsModel();
        $result = $benefitsModel->createBenefit($title, $points, $link, $description, $image);

        // Salida de resultados
        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog($title, "Addicted", "Alta en Beneficio");

            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "El beneficio se registró satisfactoriamente en el sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible registrar el beneficio en el sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }

    public function edit($id)
    {
        $benefitsModel = new BenefitsModel();
        $benefit = $benefitsModel->getBenefit($id)->getRow();
        // Retirar tags <br /> del contenido a mostrar en el textarea
        $benefit->description = strip_tags(str_replace(['<br />', '<br>', '<br/>'], '&#13;', $benefit->description));

        return view('loyalty/addicted/benefits/edit', compact('benefit'));
    }

    public function update($id)
    {
        // Validación de datos
        $rules = [
            'title' => [
                'label' => 'Titulo del beneficio',
                'rules' => 'trim|required|min_length[3]|max_length[80]',
                'errors' => [
                    'required' => 'El {field} es requerido',
                    'min_length' => 'El {field} debe tener al menos {param} caracteres',
                    'max_length' => 'El {field} no debe exceder los {param} caracteres'
                ]
            ],
            /*'points' => [
                'label' => 'Puntos del beneficio',
                'rules' => 'trim|required|numeric|is_natural_no_zero',
                'errors' => [
                    'required' => 'Los {field} son requeridos',
                    'numeric' => 'Los {field} deben ser un valor numérico',
                    'is_natural_no_zero' => 'Los {field} deben ser un valor numérico entero mayor a cero'
                ]
            ],*/
            'link' => [
                'label' => 'Link del beneficio',
                'rules' => 'trim|permit_empty|valid_url',
                'errors' => [
                    'valid_url' => 'El {field} debe ser una url válida (http:// | https://)',
                ]
            ],
            'description' => [
                'label' => 'Descripción del beneficio',
                'rules' => 'trim|required|min_length[20]|max_length[1000]',
                'errors' => [
                    'required' => 'La {field} es un dato requerido',
                    'min_length' => 'La {field} debe tener al menos {param} caracteres',
                    'max_length' => 'La {field} no debe exceder los {param} caracteres'
                ]
            ],
        ];

        $input = $this->validate($rules);

        $response = [];

        if (!$input) {
            $response["status"]     = "invalid";
            $response["code"]       = 406;
            $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
            $response['validation'] = $this->validator->getErrors();

            return json_encode($response);
        }

        // Almacenar información en base de datos
        $title              = $this->request->getPost('title');
        //$points             = $this->request->getPost('points');
        $points             = 100;
        $link               = $this->request->getPost('link');
        $description        = nl2br(strip_tags($this->request->getPost('description')));
        $image              = $this->request->getFile('image');

        $benefitsModel = new BenefitsModel();
        $result = $benefitsModel->editBenefit($id, $title, $points, $link, $description, $image);

        // Salida de resultados
        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog($title, "Addicted", "Modificación en Beneficio");

            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "El beneficio se actualizó satisfactoriamente en el sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible actualizar el beneficio en el sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }

    public function destroy($id)
    {
        $response = [];
        $benefitsModel = new BenefitsModel();
        $result = $benefitsModel->removeBenefit($id);
        $myTitle = $benefitsModel->getBenefit($id)->getRow()->title;

        if ($result) {

            // Registro de Log en sistema
            $logModel = new LogModel();
            $logModel->createLog($myTitle, "Addicted", "Baja en Beneficio");

            $response["status"]     = "success";
            $response["code"]       = 200;
            $response["message"]    = "El beneficio se eliminó satisfactoriamente del sistema.";
        } else {
            $response["status"]     = "error";
            $response["code"]       = 400;
            $response["message"]    = "No fue posible eliminar el beneficio en el sistema, favor de intentar mas tarde.";
        }

        return json_encode($response);
    }
}