<?php
namespace App\Controllers\Loyalty\Limitless;
use App\Controllers\BaseController;

class Dashboard extends BaseController
{

    public function index()
    {
        return view('loyalty/limitless/dashboard');
    }

}
