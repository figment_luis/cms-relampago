<?php
namespace App\Controllers\Loyalty\Limitless;
use App\Controllers\BaseController;
use App\Models\Loyalty\Limitless\BenefitsModel;
use App\Models\LogModel;

class Benefits extends BaseController
{

    public function index()
    {
        try {
            $benefitsModel = new BenefitsModel();
            $benefits = $benefitsModel->getAllBenefits()->getResult();

            return view('loyalty/limitless/benefits/index', compact('benefits'));
        } catch (\Exception $e) {
            $message['site'] = "Limitless";
            $message['section'] = "Listar Beneficios";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function create()
    {
        return view('loyalty/limitless/benefits/create');
    }

    public function store()
    {
        try {
            // Validación de datos
            $rules = [
                'title' => [
                    'label' => 'Titulo del beneficio',
                    'rules' => 'trim|required|min_length[3]|max_length[80]',
                    'errors' => [
                        'required' => 'El {field} es requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'description' => [
                    'label' => 'Descripción del beneficio',
                    'rules' => 'trim|required|min_length[20]|max_length[1000]',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                        'min_length' => 'La {field} debe tener al menos {param} caracteres',
                        'max_length' => 'La {field} no debe exceder los {param} caracteres'
                    ]
                ],
            ];

            $input = $this->validate($rules);

            $response = [];

            if (!$input) {
                $response["status"]     = "invalid";
                $response["code"]       = 406;
                $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
                $response['validation'] = $this->validator->getErrors();

                return json_encode($response);
            }

            // Almacenar información en base de datos
            $title              = $this->request->getPost('title');
            $description        = nl2br(strip_tags($this->request->getPost('description')));
            $image              = $this->request->getFile('image');
            $cards              = $this->request->getPost('cards');

            $benefitsModel = new BenefitsModel();
            $result = $benefitsModel->createBenefit($title, $description, $cards, $image);

            // Salida de resultados
            if ($result) {

                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($title, "Limitless", "Alta en Beneficio");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "El beneficio se registró satisfactoriamente en el sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible registrar el beneficio en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    public function edit($id)
    {
        try {
            $benefitsModel  = new BenefitsModel();
            $benefit        = $benefitsModel->getBenefit($id)->getRow();
            $currentCards   = $benefitsModel->getCardsBenefit($id)->getResultArray();
            // Retirar tags <br /> del contenido a mostrar en el textarea
            $benefit->description = strip_tags(str_replace(['<br />', '<br>', '<br/>'], '&#13;', $benefit->description));

            return view('loyalty/limitless/benefits/edit', compact('benefit', 'currentCards'));
        } catch (\Exception $e) {
            $message['site'] = "Limitless";
            $message['section'] = "Editar Beneficio";
            $message['description'] = $e->getMessage();
            return view ('errors/cms', compact('message') );
        }
    }

    public function update($id)
    {
        try {
            // Validación de datos
            $rules = [
                'title' => [
                    'label' => 'Titulo del beneficio',
                    'rules' => 'trim|required|min_length[3]|max_length[80]',
                    'errors' => [
                        'required' => 'El {field} es requerido',
                        'min_length' => 'El {field} debe tener al menos {param} caracteres',
                        'max_length' => 'El {field} no debe exceder los {param} caracteres'
                    ]
                ],
                'description' => [
                    'label' => 'Descripción del beneficio',
                    'rules' => 'trim|required|min_length[20]|max_length[1000]',
                    'errors' => [
                        'required' => 'La {field} es un dato requerido',
                        'min_length' => 'La {field} debe tener al menos {param} caracteres',
                        'max_length' => 'La {field} no debe exceder los {param} caracteres'
                    ]
                ],
            ];

            $input = $this->validate($rules);

            $response = [];

            if (!$input) {
                $response["status"]     = "invalid";
                $response["code"]       = 406;
                $response["message"]    = "Estimado usuario, los datos ingresados son incorrectos, verifiquelos nuevamente.";
                $response['validation'] = $this->validator->getErrors();

                return json_encode($response);
            }

            // Almacenar información en base de datos
            $title              = $this->request->getPost('title');
            $description        = nl2br(strip_tags($this->request->getPost('description')));
            $image              = $this->request->getFile('image');
            $cards              = $this->request->getPost('cards');

            $benefitsModel = new BenefitsModel();
            $result = $benefitsModel->editBenefit($id, $title, $description, $cards, $image);

            // Salida de resultados
            if ($result) {

                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($title, "Limitless", "Modificación en Beneficio");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "El beneficio se actualizó satisfactoriamente en el sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible actualizar el beneficio en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }

    public function destroy($id)
    {
        try {
            $response = [];
            $benefitsModel = new BenefitsModel();
            $result = $benefitsModel->removeBenefit($id);
            $myTitle = $benefitsModel->getBenefit($id)->getRow()->name;

            if ($result) {

                // Registro de Log en sistema
                $logModel = new LogModel();
                $logModel->createLog($myTitle, "Limitless", "Baja en Beneficio");

                $response["status"]     = "success";
                $response["code"]       = 200;
                $response["message"]    = "El beneficio se eliminó satisfactoriamente del sistema.";
            } else {
                $response["status"]     = "error";
                $response["code"]       = 400;
                $response["message"]    = "No fue posible eliminar el beneficio en el sistema, favor de intentar mas tarde.";
            }

            return json_encode($response);
        } catch (\Exception $e) {
            $response = [];
            $response["status"]  = "error";
            $response["code"]    = 500;
            $response["message"] = $e->getMessage();
            return json_encode($response);
        }
    }
}