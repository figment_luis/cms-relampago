<?php
namespace App\Models\Loyalty\Pearlandamar;

use CodeIgniter\Model;
use App\Libraries\S3;

class AwardsModel extends Model
{
    public function __construct()
    {
        helper('cms');
    }

    public function getAward($id)
    {
        try {
            $this->db = db_connect('pearlandamarDB');
            $builder = $this->db->table('certificate');
            $query = $builder->where('id', $id)->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Rewards, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function getAllAwards()
    {
        try {
            $this->db = db_connect('pearlandamarDB');
            $builder = $this->db->table('certificate');
            $query = $builder->where('enabled', 1)->where('available', 1)->orderBy('name')->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Rewards, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function createAward($title, $subtitle, $points, $stock, $description, $image, $galleryFiles)
    {
        try {
            $data = [
                'name'                  => $title,
                'subtitle'              => $subtitle,
                'description'           => $description,
                'points'                => $points,
                'stock_inicial'         => $stock,
                'stock'                 => $stock,
                'enabled'               => 1,
                'available'             => 1,
                'isExperience'          => 0,
                'type'                  => 4,
                'created'               => date('Y-m-d H:i:s'),
            ];

            $this->db = db_connect('pearlandamarDB');
            $builder  = $this->db->table('certificate');

            $connectionFTP = ftp_connect('andamar.com.mx');

            if ($connectionFTP) {
                if (@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                    if ($galleryFiles) {
                        $fileNames = [];
                        foreach($galleryFiles['images'] as $item) {
                            if ($item->isValid() && !$item->hasMoved()) {
                                $itemName = $item->getRandomName();
                                if (ftp_put($connectionFTP, '/pearlandamar.com.mx/assets/imagenes_premios/' . $itemName, $item, FTP_BINARY)) {
                                    array_push($fileNames, 'http://pearlandamar.com.mx/assets/imagenes_premios/' . $itemName);
                                } else {
                                    ftp_close($connectionFTP);
                                    throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Rewards, favor de intentar más tarde.", 3);
                                }
                            } else {
                                ftp_close($connectionFTP);
                                return false;
                            }
                        }
                        
                        $data['img'] = implode('|', $fileNames);

                        if ($image->isValid() && !$image->hasMoved()) {
                            $fileName = $image->getRandomName();
                            if (ftp_put($connectionFTP, '/pearlandamar.com.mx/assets/imagenes_premios/img_internal/' . $fileName, $image, FTP_BINARY)) {
                                $data['img_mobile_internal'] = 'http://pearlandamar.com.mx/assets/imagenes_premios/img_internal/' . $fileName;
                                $query = $builder->insert($data);

                                ftp_close($connectionFTP);
                                return $query;
                            } else {
                                ftp_close($connectionFTP);
                                throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Rewards, favor de intentar más tarde.", 3);
                            }
                        } else {
                            ftp_close($connectionFTP);
                            return false;
                        }
                    } else {
                        ftp_close($connectionFTP);
                        return false;
                    }
                } else {
                    ftp_close($connectionFTP);
                    throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Rewards, favor de intentar más tarde.", 3);
                }
            } else {
                ftp_close($connectionFTP);
                throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Rewards, favor de intentar más tarde.", 3);
            }
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Rewards, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function editAward($id, $title, $subtitle, $points, $stock, $description, $image)
    {
        try {
            $connectionFTP = ftp_connect('andamar.com.mx');

            if ($connectionFTP) {
                if (@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                    $data = [
                        'name'          => $title,
                        'subtitle'      => $subtitle,
                        'description'   => $description,
                        'points'        => $points,
                        'stock'         => $stock,
                        'modified'       => date('Y-m-d H:i:s'),
                    ];
                
                    $this->db = db_connect('pearlandamarDB');
                    $builder  = $this->db->table('certificate');
                    $currentStock = $builder->select('stock')->where('id', $id)->get()->getRow();

                    if ($image->getError() === 4) {
                        $this->verifyStock($id, $currentStock->stock, $stock);
                        $query = $builder->where('id', $id)->update($data);

                        ftp_close($connectionFTP);
                        return $query;
                    } else {
                        if ($image->isValid() && !$image->hasMoved()) {
                            $fileName = $image->getRandomName();
                            if (ftp_put($connectionFTP, '/pearlandamar.com.mx/assets/imagenes_premios/img_internal/' . $fileName, $image, FTP_BINARY)) {
                                $this->verifyStock($id, $currentStock->stock, $stock);
                                $data['img_mobile_internal'] = 'http://pearlandamar.com.mx/assets/imagenes_premios/img_internal/' . $fileName;
                                $query = $builder->where('id', $id)->update($data);

                                ftp_close($connectionFTP);
                                return $query;
                            } else {
                                ftp_close($connectionFTP);
                                throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Rewards, favor de intentar más tarde.", 3);
                            }
                        } else {
                            ftp_close($connectionFTP);
                            return false;
                        }
                    }
                } else {
                    ftp_close($connectionFTP);
                    throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Rewards, favor de intentar más tarde.", 3);
                }
            } else {
                ftp_close($connectionFTP);
                throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Rewards, favor de intentar más tarde.", 3);
            }
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Rewards, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function removeAward($id)
    {
        try {
            $data = [
                'enabled' => 0,
                'available' => 0,
                'modified' => date('Y-m-d H:i:s'),
            ];

            $this->db = db_connect('pearlandamarDB');
            $builder = $this->db->table('certificate');
            $query = $builder->where('id', $id)->update($data);

            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Rewards, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function updateGallery($id, $galleryFiles) 
    {
        try {
            $connectionFTP = ftp_connect('andamar.com.mx');
            if ($connectionFTP) {
                if (@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                    if ($galleryFiles) {

                        $fileNames = [];
                        $preview = [];
                        $config = [];
                        foreach($galleryFiles['images'] as $item) {
                            if ($item->isValid() && !$item->hasMoved()) {
                                $itemName = $item->getRandomName();
                                if (ftp_put($connectionFTP, '/pearlandamar.com.mx/assets/imagenes_premios/' . $itemName, $item, FTP_BINARY)) {
                                    array_push($fileNames, 'http://pearlandamar.com.mx/assets/imagenes_premios/' . $itemName);
                                    $config[] = [
                                        'key'       => $itemName,
                                        'caption'   => 'Imagen publicada',
                                        'width'     => '70px',
                                        'url' => base_url() . '/pearlandamar/awards/gallery/destroy/'.$id.'/'.$itemName,
                                    ];
                                    array_push($preview, 'http://pearlandamar.com.mx/assets/imagenes_premios/'.$itemName);
                                } else {
                                    ftp_close($connectionFTP);
                                    throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Rewards, favor de intentar más tarde.", 3);
                                }
                            } else {
                                ftp_close($connectionFTP);
                                return false;
                            }
                        }

                        $award = $this->getAward($id)->getRow();
                        $currentImages = $award->img == "" ? [] : explode('|', $award->img);
                        $data['img'] = implode('|', array_merge($currentImages, $fileNames));
                        
                        $this->db = db_connect('pearlandamarDB');
                        $builder  = $this->db->table('certificate');
                        $query = $builder->where('id', $id)->update($data);

                        ftp_close($connectionFTP);
                        return ['initialPreview' => $preview, 'initialPreviewConfig' => $config, 'initialPreviewAsData' => true];
                            
                    } else {
                        ftp_close($connectionFTP);
                        return false;
                    }
                } else {
                    ftp_close($connectionFTP);
                    throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Rewards, favor de intentar más tarde.", 3);
                }
            } else {
                ftp_close($connectionFTP);
                throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Rewards, favor de intentar más tarde.", 3);
            }
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Rewards, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function destroyGallery($id, $image) {
        try {
            $connectionFTP = ftp_connect('andamar.com.mx');

            if ($connectionFTP) {
                if(@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                    if(@ftp_chdir($connectionFTP, '/pearlandamar.com.mx/assets/imagenes_premios/')) {
                        if (ftp_delete($connectionFTP, $image)) {
                            $award = $this->getAward($id)->getRow();
                            $images = explode('|', $award->img);
                            $fileNames = [];

                            foreach ($images as $item) {
                                if ($item !== 'http://pearlandamar.com.mx/assets/imagenes_premios/' . $image) {
                                    array_push($fileNames, $item);
                                }
                            }

                            $data['img'] = implode('|', $fileNames);

                            $this->db = db_connect('pearlandamarDB');
                            $builder  = $this->db->table('certificate');
                            $query = $builder->where('id', $id)->update($data);

                            ftp_close($connectionFTP);
                            return true;
                        } else {
                            ftp_close($connectionFTP);
                            throw new \Exception("Error al tratar de eliminar el recurso de imagen en el servidor FTP del proyecto Rewards, favor de intentar más tarde.", 3);
                        }
                    } else {
                        ftp_close($connectionFTP);
                        throw new \Exception("Error al tratar de localizar el directorio de imagenes en el servidor FTP del proyecto Rewards, favor de intentar más tarde.", 3);
                    }
                } else {
                    ftp_close($connectionFTP);
                    throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Rewards, favor de intentar más tarde.", 3);
                }
            } else {
                ftp_close($connectionFTP);
                throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Rewards, favor de intentar más tarde.", 3);
            }
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Rewards, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
        }
    }

    private function logStock($id, $stock, $type, $user)
    {
        try {
            $data = [
                'certificate_id' => $id,
                'username'   => $user,
                'created' => date('Y-m-d H:i:s')
            ];

            if ($type === 'decrease') {
                $data['stock_increased'] = 0;
                $data['stock_decreased']  = $stock;
            } else {
                $data['stock_increased'] = $stock;
                $data['stock_decreased']  = 0;
            }

            $this->db = db_connect('pearlandamarDB');
            $builder  = $this->db->table('certificate_log_stock');
            $query    = $builder->insert($data);
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto PearlAndamar, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    private function verifyStock($id, $currentStock, $newStock) {
        try {
            if ($currentStock != $newStock) {
                if ($currentStock > $newStock) {
                    $this->logStock($id, $currentStock - $newStock, 'decrease', session()->get('user_fullname'));
                } else {
                    $this->logStock($id, $newStock - $currentStock, 'increase', session()->get('user_fullname'));
                }
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }
}