<?php
namespace App\Models\Loyalty\Pearlandamar;

use CodeIgniter\Model;
use App\Libraries\S3;

class BenefitsModel extends Model
{
    public function __construct()
    {
        helper('cms');
    }

    public function getBenefit($id)
    {
        try {
            $this->db = db_connect('pearlandamarDB');
            $builder = $this->db->table('gift_type');
            $query = $builder->where('id', $id)->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto PearlAndamar, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function getAllBenefits()
    {
        try {
            $this->db = db_connect('pearlandamarDB');
            $builder = $this->db->table('gift_type');
            $query = $builder->where('enabled', 1)->where('available', 1)->orderBy('name')->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto PearlAndamar, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function getCardsBenefit($id)
    {
        try {
            $this->db = db_connect('pearlandamarDB');
            $builder = $this->db->table('gift_type_has_card_type');
            $query = $builder->where('gift_type_id', $id)->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto PearlAndamar, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function createBenefit($title, $stock, $description, $cards)
    {
        // TODO: Ajustar las excepciones si en el futuro se contempla nuevamente el registro de imagen
        try {
        // Posible actualización: Quizá no sea obligatoria la imagen, si se tiene una por defecto en el SFTP
        //if ($image->isValid() && !$image->hasMoved()) {

            //$connectionFTP = ftp_connect('andamar.com.mx');

            //if ($connectionFTP) {
                //if (ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                    //$fileName = $image->getRandomName();
                    //if (ftp_put($connectionFTP, '/pearlandamar.com.mx/assets/imagenes_beneficios/' . $fileName, $image, FTP_BINARY)) {
                        $this->db   = db_connect('pearlandamarDB');
                        $this->db->transBegin();

                        // Obtener el ultimo ID registrado en esta tabla. 
                        // No implementa id autoincrementable, esto se debe a que es una posible solución a un proyecto legado.
                        // Por tanto no puedo usar posteriormente $this->db->insertID()
                        $lastIdBenefit = $this->db->table('gift_type')->orderBy('id', 'desc')
                                                   ->limit(1)->get()->getRow();

                        $indexBenefit = $lastIdBenefit->id;
                        $dataBenefit = [
                            'id'                    => ++$indexBenefit,
                            'name'                  => $title,
                            'description'           => $description,
                            'stock'                 => $stock,
                            //'img'                   => 'http://pearlandamar.com.mx/assets/imagenes_beneficios/' . $fileName,
                            'enabled'               => 1,
                            'available'             => 1,
                            'created'               => date('Y-m-d H:i:s'),
                        ];
    
                        $this->db->table('gift_type')->insert($dataBenefit);

                        // Recuperar el ID del último registro relacionado.
                        $lastIdBenefitCards = $this->db->table('gift_type_has_card_type')->orderBy('id', 'desc')
                                                   ->limit(1)->get()->getRow();

                        $indexBenefitCards = $lastIdBenefitCards->id;
                        // Operaciones tabla pivote gift_type_has_card_type
                        // Un beneficio puede estar presente en más de una tarjeta - Blue/Silver/Gold
                        $dataBenefitCards = [];
                        foreach ($cards as $card) {
                            array_push($dataBenefitCards, [
                                'id'           => ++$indexBenefitCards,
                                'gift_type_id' => $indexBenefit,   // Beneficio registrado
                                'card_type_id' => $card,            // Tarjeta asociada
                            ]);
                        }
                        $this->db->table('gift_type_has_card_type')->insertBatch($dataBenefitCards);

                        // Confirmar operaciones en la base de datos
                        if ($this->db->transStatus() !== FALSE) {
                            $this->db->transCommit();
                            //ftp_close($connectionFTP);
                            return true;
                        } else {
                            $this->db->transRollback();
                            //ftp_close($connectionFTP);
                            return false;
                        }
                    //}
                //}
                //ftp_close($connectionFTP);
            //}
            
        //}

        //return false;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto PearlAndamar, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function editBenefit($id, $title, $stock, $description, $cards)
    {
        // TODO: Ajustar las excepciones si en el futuro se contempla nuevamente el registro de imagen
        try {
            //$connectionFTP = ftp_connect('andamar.com.mx');

            //if ($connectionFTP) {
                //if (ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                    $dataBenefit = [
                        'name'          => $title,
                        'description'   => $description,
                        'stock'         => $stock,
                        'modified'      => date('Y-m-d H:i:s'),
                    ];

                    $this->db = db_connect('pearlandamarDB');
                    $this->db->transBegin();
                
                    //if ($image->getError() === 4) {
                        $this->db->table('gift_type')->where('id', $id)->update($dataBenefit);

                        // Elimino todas las relaciones actuales y registro las nuevas
                        $this->db->table('gift_type_has_card_type')->where('gift_type_id', $id)->delete();

                        // Operaciones tabla pivote gift_type_has_card_type
                        // Un beneficio puede estar presente en más de una tarjeta - Silver/Gold/Black
                        // Al carecer de id autoincrementable, es necesario consultar el último id registrado
                        $lastIdBenefitCards = $this->db->table('gift_type_has_card_type')->orderBy('id', 'desc')
                                                ->limit(1)->get()->getRow();

                        $indexBenefitCards = $lastIdBenefitCards->id;

                        $dataBenefitCards = [];
                        foreach ($cards as $card) {
                            array_push($dataBenefitCards, [
                                'id'           => ++$indexBenefitCards,
                                'gift_type_id' => $id,      // Beneficio actualmente editado
                                'card_type_id' => $card,    // Tarjeta asociada
                            ]);
                        }
                        $this->db->table('gift_type_has_card_type')->insertBatch($dataBenefitCards);

                        // Confirmar operaciones en la base de datos
                        if ($this->db->transStatus() !== FALSE) {
                            $this->db->transCommit();
                            //ftp_close($connectionFTP);
                            return true;
                        } else {
                            $this->db->transRollback();
                            //ftp_close($connectionFTP);
                            return false;
                        }
                    /*} else {
                        if ($image->isValid() && !$image->hasMoved()) {
                            $fileName = $image->getRandomName();
                            if (ftp_put($connectionFTP, '/pearlandamar.com.mx/assets/imagenes_beneficios/' . $fileName, $image, FTP_BINARY)) {
                                $dataBenefit['img'] = 'http://pearlandamar.com.mx/assets/imagenes_beneficios/' . $fileName;
                                $this->db->table('gift_type')->where('id', $id)->update($dataBenefit);

                                // Elimino todas las relaciones actuales y registro las nuevas
                                $this->db->table('gift_type_has_card_type')->where('gift_type_id', $id)->delete();
                                // Operaciones tabla pivote gift_type_has_card_type
                                // Un beneficio puede estar presente en más de una tarjeta - Silver/Gold/Black
                                // Al carecer de id autoincrementable, es necesario consultar el último id registrado
                                $lastIdBenefitCards = $this->db->table('gift_type_has_card_type')->orderBy('id', 'desc')
                                                ->limit(1)->get()->getRow();

                                $indexBenefitCards = $lastIdBenefitCards->id;
                                $dataBenefitCards = [];
                                foreach ($cards as $card) {
                                    array_push($dataBenefitCards, [
                                        'id'           => ++$indexBenefitCards,
                                        'gift_type_id' => $id,      // Beneficio actualmente editado
                                        'card_type_id' => $card,    // Tarjeta asociada
                                    ]);
                                }
                                $this->db->table('gift_type_has_card_type')->insertBatch($dataBenefitCards);

                                // Confirmar operaciones en la base de datos
                                if ($this->db->transStatus() !== FALSE) {
                                    $this->db->transCommit();
                                    ftp_close($connectionFTP);
                                    return true;
                                } else {
                                    $this->db->transRollback();
                                    ftp_close($connectionFTP);
                                    return false;
                                }
                            }
                        }
                    }*/
                //}
                //ftp_close($connectionFTP);
            //}

            //return false;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto PearlAndamar, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function removeBenefit($id)
    {
        try {
            $data = [
                'enabled' => 0,
                'available' => 0,
                'modified' => date('Y-m-d H:i:s'),
            ];

            $this->db = db_connect('pearlandamarDB');
            $builder = $this->db->table('gift_type');
            $query = $builder->where('id', $id)->update($data);

            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto PearlAndamar, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }
}