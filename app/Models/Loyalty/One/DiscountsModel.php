<?php
namespace App\Models\Loyalty\One;

use CodeIgniter\Model;
use App\Libraries\S3;

class DiscountsModel extends Model
{
    public function __construct()
    {
        helper('cms');
    }

    public function getDiscount($id)
    {
        $this->db = db_connect('oneDB');
        $builder = $this->db->table('discounts');
        $query = $builder->where('id', $id)->get();
        return $query;
    }

    public function getAllDiscounts()
    {
        $this->db = db_connect('oneDB');
        $builder = $this->db->table('discounts');
        $query = $builder->where('enabled', 1)->where('available', 1)->orderBy('name')->get();
        return $query;
    }

    public function createDiscount($title, $subtitle, $description, $image)
    {
        // Posible actualización: Quizá no sea obligatoria la imagen, si se tiene una por defecto en el SFTP
        if ($image->isValid() && !$image->hasMoved()) {
            $connectionFTP = ftp_connect('192.169.176.159');

            if (@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                // activar modo pasivo FTP
                ftp_pasv($connectionFTP, true);
                $fileName = $image->getRandomName();

                if (ftp_put($connectionFTP, '/httpdocs/img/premios/' . $fileName, $image, FTP_BINARY)) {
                    $data = [
                        'name'          => $title,
                        'subtitle'      => $subtitle,
                        'description'   => $description,
                        'img'           => $fileName,
                        'enabled'       => 1,
                        'available'     => 1,
                        'created'       => date('Y-m-d H:i:s'),
                    ];

                    $this->db   = db_connect('oneDB');
                    $builder    = $this->db->table('discounts');
                    $query      = $builder->insert($data);

                    ftp_close($connectionFTP);
                    return $query;
                }
            }
        }

        return false;
    }

    public function editDiscount($id, $title, $subtitle, $description, $image)
    {
        $data = [
            'name'          => $title,
            'subtitle'      => $subtitle,
            'description'   => $description,
            'modified'      => date('Y-m-d H:i:s'),
        ];

        $this->db = db_connect('oneDB');
        $builder  = $this->db->table('discounts');

        if ($image->getError() === 4) {
            $query = $builder->where('id', $id)->update($data);
            return $query;
        } else {
            if ($image->isValid() && !$image->hasMoved()) {
                $connectionFTP = ftp_connect('192.169.176.159');
                if (@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                    // activar modo pasivo FTP
                    ftp_pasv($connectionFTP, true);
                    $fileName = $image->getRandomName();
                    if (ftp_put($connectionFTP, '/httpdocs/img/premios/' . $fileName, $image, FTP_BINARY)) {
                        $data['img'] = $fileName;
                        $query = $builder->where('id', $id)->update($data);

                        ftp_close($connectionFTP);
                        return $query;
                    }
                }
            }
        }

        return false;
    }

    public function removeDiscount($id)
    {
        $data = [
            'enabled' => 0,
            'available' => 0,
            'modified' => date('Y-m-d H:i:s'),
        ];

        $this->db = db_connect('oneDB');
        $builder = $this->db->table('discounts');
        $query = $builder->where('id', $id)->update($data);

        return $query;
    }
}