<?php
namespace App\Models\Loyalty\One;

use CodeIgniter\Model;
use App\Libraries\S3;

class StoresModel extends Model
{
    public function __construct()
    {
        helper('cms');
    }

    public function getStore($id)
    {
        $this->db = db_connect('oneDB');
        $builder = $this->db->table('store');
        $query = $builder->where('id', $id)->get();
        return $query;
    }

    public function getAllGroups()
    {
        $this->db = db_connect('oneDB');
        $builder = $this->db->table('group');
        $query = $builder->where('enabled', 1)->where('available', 1)->get();
        return $query;
    }

    public function getAllStores()
    {
        $this->db = db_connect('oneDB');
        $builder = $this->db->table('store');
        // $query = $builder->where('enabled', 1)->where('available', 1)->orderBy('name')->get();
        $query = $builder->orderBy('name')->get();
        return $query;
    }

    public function createStore($title, $multiple, $group)
    {
        
        $data = [
            'name'          => $title,
            'multipler'     => $multiple,
            'group_id'      => $group,
            'enabled'       => 1,
            'available'     => 1,
            'created'       => date('Y-m-d H:i:s'),
        ];

        $this->db   = db_connect('oneDB');
        $builder    = $this->db->table('store');
        $query      = $builder->insert($data);
        
        return $query;

    }

    public function editStore($id, $title, $multiple, $group)
    {
        
        $data = [
            'name'      => $title,
            'multipler' => $multiple,
            'group_id'  => $group,
            'modified'  => date('Y-m-d H:i:s'),
        ];
            
        $this->db   = db_connect('oneDB');
        $builder    = $this->db->table('store');
        $query      = $builder->where('id', $id)->update($data);
        
        return $query;
        
    }

    public function removeStore($id)
    {
        $data = [
            'enabled' => 0,
            'available' => 0,
            'modified' => date('Y-m-d H:i:s'),
        ];

        $this->db = db_connect('oneDB');
        $builder = $this->db->table('store');
        $query = $builder->where('id', $id)->update($data);

        return $query;
    }

    public function activateStore($id)
    {
        $data = [
            'enabled'   => 1,
            'available' => 1,
            'modified'  => date('Y-m-d H:i:s'),
        ];

        $this->db = db_connect('oneDB');
        $builder = $this->db->table('store');
        $query = $builder->where('id', $id)->update($data);

        return $query;
    }
}