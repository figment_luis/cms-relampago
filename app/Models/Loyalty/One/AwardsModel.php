<?php
namespace App\Models\Loyalty\One;

use CodeIgniter\Model;
use App\Libraries\S3;

class AwardsModel extends Model
{
    public function __construct()
    {
        helper('cms');
    }

    public function getAward($id)
    {
        $this->db = db_connect('oneDB');
        $builder = $this->db->table('certificate');
        $query = $builder->where('id', $id)->get();
        return $query;
    }

    public function getAllAwards()
    {
        $this->db = db_connect('oneDB');
        $builder = $this->db->table('certificate');
        $query = $builder->where('enabled', 1)->where('available', 1)->orderBy('name')->get();
        return $query;
    }

    public function createAward($title, $subtitle, $points, $stock, $description, $image)
    {
        // Posible actualización: Quizá no sea obligatoria la imagen, si se tiene una por defecto en el SFTP
        if ($image->isValid() && !$image->hasMoved()) {
            $connectionFTP = ftp_connect('192.169.176.159');

            if (@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                // activar modo pasivo FTP
                ftp_pasv($connectionFTP, true);
                $fileName = $image->getRandomName();

                if (ftp_put($connectionFTP, '/httpdocs/img/premios/' . $fileName, $image, FTP_BINARY)) {
                    $data = [
                        'name'                  => $title,
                        'subtitle'              => $subtitle,
                        'description'           => $description,
                        'points'                => $points,
                        'stock'                 => $stock,
                        'img'                   => $fileName,
                        'img_mobile_internal'   => 'http://www.luxuryone.com.mx/img/premios/' . $fileName,
                        'max_month'             => 3,
                        'enabled'               => 1,
                        'available'             => 1,
                        'isExperience'          => 0,
                        'type'                  => 2,
                        'created'               => date('Y-m-d H:i:s'),
                    ];

                    $this->db   = db_connect('oneDB');
                    $builder    = $this->db->table('certificate');
                    $query      = $builder->insert($data);

                    ftp_close($connectionFTP);
                    return $query;
                }
            }
        }

        return false;
    }

    public function editAward($id, $title, $subtitle, $points, $stock, $description, $image)
    {
        $data = [
            'name'          => $title,
            'subtitle'      => $subtitle,
            'description'   => $description,
            'points'        => $points,
            'stock'         => $stock,
            'modified'      => date('Y-m-d H:i:s'),
        ];

        $this->db = db_connect('oneDB');
        $builder  = $this->db->table('certificate');

        if ($image->getError() === 4) {
            $query = $builder->where('id', $id)->update($data);
            return $query;
        } else {
            if ($image->isValid() && !$image->hasMoved()) {
                $connectionFTP = ftp_connect('192.169.176.159');
                if (@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                    // activar modo pasivo FTP
                    ftp_pasv($connectionFTP, true);
                    $fileName = $image->getRandomName();

                    if (ftp_put($connectionFTP, '/httpdocs/img/premios/' . $fileName, $image, FTP_BINARY)) {
                        $data['img'] = $fileName;
                        $data['img_mobile_internal'] = 'http://www.luxuryone.com.mx/img/premios/' . $fileName;
                        $query = $builder->where('id', $id)->update($data);

                        ftp_close($connectionFTP);
                        return $query;
                    }
                }
            }
        }

        return false;
    }

    public function removeAward($id)
    {
        $data = [
            'enabled' => 0,
            'available' => 0,
            'modified' => date('Y-m-d H:i:s'),
        ];

        $this->db = db_connect('oneDB');
        $builder = $this->db->table('certificate');
        $query = $builder->where('id', $id)->update($data);

        return $query;
    }

}