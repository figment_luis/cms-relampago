<?php
namespace App\Models\Loyalty\One;

use CodeIgniter\Model;
use App\Libraries\S3;

class BenefitsModel extends Model
{
    public function __construct()
    {
        helper('cms');
    }

    public function getBenefit($id)
    {
        $this->db = db_connect('oneDB');
        $builder = $this->db->table('gift_type');
        $query = $builder->where('id', $id)->get();
        return $query;
    }

    public function getAllBenefits()
    {
        $this->db = db_connect('oneDB');
        $builder = $this->db->table('gift_type');
        $query = $builder->where('enabled', 1)->where('available', 1)->orderBy('name')->get();
        return $query;
    }

    public function getCardsBenefit($id)
    {
        $this->db = db_connect('oneDB');
        $builder = $this->db->table('gift_type_has_card_type');
        $query = $builder->where('gift_type_id', $id)->get();
        return $query;
    }

    public function createBenefit($title, $stock, $description, $cards, $image)
    {
        // Posible actualización: Quizá no sea obligatoria la imagen, si se tiene una por defecto en el SFTP
        if ($image->isValid() && !$image->hasMoved()) {
            $connectionFTP = ftp_connect('192.169.176.159');

            if (@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                // activar modo pasivo FTP
                ftp_pasv($connectionFTP, true);
                $fileName = $image->getRandomName();

                if (ftp_put($connectionFTP, '/httpdocs/img/beneficios/' . $fileName, $image, FTP_BINARY)) {
                    $this->db = db_connect('oneDB');
                    $this->db->transBegin();

                    // Obtener el ultimo ID registrado en esta tabla.
                    // No implementa id autoincrementable, esto se debe a que es una posible solución a un proyecto legado.
                    // Por tanto no puedo usar posteriormente $this->db->insertID()
                    $lastIdBenefit = $this->db->table('gift_type')->orderBy('id', 'desc')
                        ->limit(1)->get()->getRow();

                    $indexBenefit = $lastIdBenefit->id;
                    $dataBenefit = [
                        'id'                    => ++$indexBenefit,
                        'name'                  => $title,
                        'description'           => $description,
                        'stock'                 => $stock,
                        'img'                   => $fileName,
                        'enabled'               => 1,
                        'available'             => 1,
                        'created'               => date('Y-m-d H:i:s'),
                    ];

                    $this->db->table('gift_type')->insert($dataBenefit);

                    // Recuperar el ID del último registro relacionado.
                    $lastIdBenefitCards = $this->db->table('gift_type_has_card_type')->orderBy('id', 'desc')
                        ->limit(1)->get()->getRow();

                    $indexBenefitCards = $lastIdBenefitCards->id;

                    // Operaciones tabla pivote gift_type_has_card_type
                    // Un beneficio puede estar presente en más de una tarjeta - Gold/Platinum/Black
                    $dataBenefitCards = [];
                    foreach ($cards as $card) {
                        array_push($dataBenefitCards, [
                            'id'           => ++$indexBenefitCards,     // ID generado de forma manual
                            'gift_type_id' => $indexBenefit,               // Beneficio registrado
                            'card_type_id' => $card,                    // Tarjeta asociada
                        ]);
                    }
                    $this->db->table('gift_type_has_card_type')->insertBatch($dataBenefitCards);

                    // Confirmar operaciones en la base de datos
                    if ($this->db->transStatus() !== FALSE) {
                        $this->db->transCommit();
                        ftp_close($connectionFTP);
                        return true;
                    } else {
                        $this->db->transRollback();
                        ftp_close($connectionFTP);
                        return false;
                    }
                }
            }
        }

        return false;
    }

    public function editBenefit($id, $title, $stock, $description, $cards, $image)
    {
        $dataBenefit = [
            'name'          => $title,
            'description'   => $description,
            'stock'         => $stock,
            'modified'      => date('Y-m-d H:i:s'),
        ];

        $this->db = db_connect('oneDB');
        $this->db->transBegin();

        if ($image->getError() === 4) {
            $this->db->table('gift_type')->where('id', $id)->update($dataBenefit);

            // Elimino todas las relaciones actuales de este beneficio y registro las nuevas
            $this->db->table('gift_type_has_card_type')->where('gift_type_id', $id)->delete();

            // Operaciones tabla pivote gift_type_has_card_type
            // Un beneficio puede estar presente en más de una tarjeta - Gold/Platinum/Black
            // Al carecer de id autoincrementable, es necesario consultar el último id registrado
            $lastIdBenefitCards = $this->db->table('gift_type_has_card_type')->orderBy('id', 'desc')
                                        ->limit(1)->get()->getRow();

            $indexBenefitCards = $lastIdBenefitCards->id;

            $dataBenefitCards = [];
            foreach ($cards as $card) {
                array_push($dataBenefitCards, [
                    'id'           => ++$indexBenefitCards,
                    'gift_type_id' => $id,      // Beneficio actualmente editado
                    'card_type_id' => $card,    // Tarjeta asociada
                ]);
            }
            $this->db->table('gift_type_has_card_type')->insertBatch($dataBenefitCards);

            // Confirmar operaciones en la base de datos
            if ($this->db->transStatus() !== FALSE) {
                $this->db->transCommit();
                return true;
            } else {
                $this->db->transRollback();
                return false;
            }
        } else {
            if ($image->isValid() && !$image->hasMoved()) {
                $connectionFTP = ftp_connect('192.169.176.159');
                if (@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                    // activar modo pasivo FTP
                    ftp_pasv($connectionFTP, true);
                    $fileName = $image->getRandomName();

                    if (ftp_put($connectionFTP, '/httpdocs/img/beneficios/' . $fileName, $image, FTP_BINARY)) {
                        $dataBenefit['img'] = $fileName;
                        $this->db->table('gift_type')->where('id', $id)->update($dataBenefit);

                        // Elimino todas las relaciones actuales de este beneficio y registro las nuevas
                        $this->db->table('gift_type_has_card_type')->where('gift_type_id', $id)->delete();
                        // Operaciones tabla pivote gift_type_has_card_type
                        // Un beneficio puede estar presente en más de una tarjeta - Silver/Gold/Black
                        // Al carecer de id autoincrementable, es necesario consultar el último id registrado
                        $lastIdBenefitCards = $this->db->table('gift_type_has_card_type')->orderBy('id', 'desc')
                            ->limit(1)->get()->getRow();

                        $indexBenefitCards = $lastIdBenefitCards->id;
                        $dataBenefitCards = [];
                        foreach ($cards as $card) {
                            array_push($dataBenefitCards, [
                                'id'           => ++$indexBenefitCards,
                                'gift_type_id' => $id,      // Beneficio actualmente editado
                                'card_type_id' => $card,    // Tarjeta asociada
                            ]);
                        }
                        $this->db->table('gift_type_has_card_type')->insertBatch($dataBenefitCards);

                        // Confirmar operaciones en la base de datos
                        if ($this->db->transStatus() !== FALSE) {
                            $this->db->transCommit();
                            ftp_close($connectionFTP);
                            return true;
                        } else {
                            $this->db->transRollback();
                            ftp_close($connectionFTP);
                            return false;
                        }
                    }
                }
            }
        }

        return false;
    }

    public function removeBenefit($id)
    {
        $data = [
            'enabled' => 0,
            'available' => 0,
            'modified' => date('Y-m-d H:i:s'),
        ];

        $this->db = db_connect('oneDB');
        $builder = $this->db->table('gift_type');
        $query = $builder->where('id', $id)->update($data);

        return $query;
    }

}