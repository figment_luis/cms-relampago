<?php
namespace App\Models\Loyalty\Rewards;

use CodeIgniter\Model;
use App\Libraries\S3;

class AwardsModel extends Model
{
    public function __construct()
    {
        helper('cms');
    }

    public function getAward($id)
    {
        try {
            $this->db = db_connect('rewardsDB');
            $builder = $this->db->table('certificate');
            $query = $builder->where('id', $id)->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Rewards, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function getAllAwards()
    {
        try {
            $this->db = db_connect('rewardsDB');
            $builder = $this->db->table('certificate');
            $query = $builder->where('enabled', 1)->where('available', 1)->orderBy('name')->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Rewards, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function createAward($title, $subtitle, $points, $stock, $description, $image, $galleryFiles)
    {
        try {
            $data = [
                'name'                  => $title,
                'subtitle'              => $subtitle,
                'description'           => $description,
                'points'                => $points,
                'stock_start'           => $stock,
                'stock'                 => $stock,
                'max_month'             => 3,
                'enabled'               => 1,
                'available'             => 1,
                'isExperience'          => 0,
                'type'                  => 4,
                'created'               => date('Y-m-d H:i:s'),
            ];

            $this->db   = db_connect('rewardsDB');
            $builder    = $this->db->table('certificate');

            $s3         = new S3(cms_getConfigAWS3());
            $bucket     = "kinetiq-loyalty-assets";
            $fileNames  = [];
            
            if ($galleryFiles) {
                foreach($galleryFiles['images'] as $item) {
                    if ($item->isValid() && !$item->hasMoved()) {
                        $temporalFile   = $item->getTempName();
                        $itemFileName   = $item->getRandomName();
                        $uri            = "angelopolis/news/" . $itemFileName;
                        if ($s3->putObjectFile($temporalFile, $bucket, $uri, S3::ACL_PUBLIC_READ)) {
                            $fileAWS3 = 'https://' . $bucket . '.s3.amazonaws.com/' . $uri;
                            array_push($fileNames, $fileAWS3);
                        } else {
                            throw new \Exception("Error de conexión temporal al servicio de AS3, favor de intentar más tarde.", 3);
                        }
                    } else {
                        return false;
                    }
                }
                $data['img'] = implode('|', $fileNames);
                if ($image->isValid() && !$image->hasMoved()) {
                    $temporalFile   = $image->getTempName();
                    $fileName       = $image->getRandomName();
                    $uri            = "angelopolis/news/" . $fileName;
                    if ($s3->putObjectFile($temporalFile, $bucket, $uri, S3::ACL_PUBLIC_READ)) {
                        $data['img_mobile_internal'] = 'https://' . $bucket . '.s3.amazonaws.com/' . $uri;
                        $query = $builder->insert($data);
                        return $query;
                    } else {
                        throw new \Exception("Error de conexión temporal al servicio de AS3, favor de intentar más tarde.", 3);
                    }
                }
            }

            return false;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Rewards, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function editAward($id, $title, $subtitle, $points, $stock, $description, $image)
    {
        try {
            $data = [
                'name'          => $title,
                'subtitle'      => $subtitle,
                'description'   => $description,
                'points'        => $points,
                'stock'         => $stock,
                'modified'      => date('Y-m-d H:i:s'),
            ];
                
            $this->db = db_connect('rewardsDB');
            $builder  = $this->db->table('certificate');
            $currentStock = $builder->select('stock')->where('id', $id)->get()->getRow();

            if ($image->getError() === 4) {
                $this->verifyStock($id, $currentStock->stock, $stock);
                $query = $builder->where('id', $id)->update($data);
                return $query;
            } else {
                if ($image->isValid() && !$image->hasMoved()) {
                    $s3             = new S3(cms_getConfigAWS3());
                    $bucket         = "kinetiq-loyalty-assets";

                    $temporalFile   = $image->getTempName();
                    $fileName       = $image->getRandomName();
                    $uri            = "angelopolis/news/" . $fileName;
                    if ($s3->putObjectFile($temporalFile, $bucket, $uri, S3::ACL_PUBLIC_READ)) {
                        $this->verifyStock($id, $currentStock->stock, $stock);
                        $data['img_mobile_internal'] = 'https://' . $bucket . '.s3.amazonaws.com/' . $uri;
                        $query = $builder->where('id', $id)->update($data);
                        return $query;
                    } else {
                        throw new \Exception("Error de conexión temporal al servicio de AS3, favor de intentar más tarde.", 3);
                    }
                }
            }

            return false;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Rewards, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function removeAward($id)
    {
        try {
            $data = [
                'enabled' => 0,
                'available' => 0,
                'modified' => date('Y-m-d H:i:s'),
            ];

            $this->db = db_connect('rewardsDB');
            $builder = $this->db->table('certificate');
            $query = $builder->where('id', $id)->update($data);

            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Rewards, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    private function logStock($id, $stock, $type, $user)
    {
        try {
            $data = [
                'idCert' => $id,
                'user'   => $user,
            ];

            if ($type === 'decrease') {
                $data['stock_increased'] = 0;
                $data['stock_decresed']  = $stock;
            } else {
                $data['stock_increased'] = $stock;
                $data['stock_decresed']  = 0;
            }

            $this->db = db_connect('rewardsDB');
            $builder  = $this->db->table('certificate_log_stock');
            $query    = $builder->insert($data);
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Rewards, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    private function verifyStock($id, $currentStock, $newStock) {
        try {
            if ($currentStock != $newStock) {
                if ($currentStock > $newStock) {
                    $this->logStock($id, $currentStock - $newStock, 'decrease', session()->get('user_fullname'));
                } else {
                    $this->logStock($id, $newStock - $currentStock, 'increase', session()->get('user_fullname'));
                }
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function destroyGallery($id, $image) 
    {
        try {
            $s3     = new S3(cms_getConfigAWS3());
            $bucket = "kinetiq-loyalty-assets";

            $award  = $this->getAward($id)->getRow();
            $images = explode('|', $award->img);
            $fileNames = [];

            $pathDirHttp    = 'http://kinetiq-loyalty-assets.s3.amazonaws.com/angelopolis/news/';
            $pathDirHttps   = 'https://kinetiq-loyalty-assets.s3.amazonaws.com/angelopolis/news/';
            foreach ($images as $item) {
                // Legacy: Algunas imagenes fueron registradas con un protocolo http y no con https
                if (($item !== $pathDirHttp . $image) && ($item !== $pathDirHttps . $image)) {
                    array_push($fileNames, $item);
                } else {
                    // Eliminar imagen del buket AWS
                    if (!$s3->deleteObject($bucket, "angelopolis/news/" . $image)) {
                        throw new \Exception("Error de conexión temporal al servicio de AS3, favor de intentar más tarde.", 3);
                    } 
                }
            }

            $data['img'] = implode('|', $fileNames);

            $this->db = db_connect('rewardsDB');
            $builder  = $this->db->table('certificate');
            $query = $builder->where('id', $id)->update($data);

            return true;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Rewards, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function updateGallery($id, $galleryFiles) 
    {
        try {
            if ($galleryFiles) {

                $s3         = new S3(cms_getConfigAWS3());
                $bucket     = "kinetiq-loyalty-assets";

                $fileNames  = [];
                $preview    = [];
                $config     = [];

                foreach($galleryFiles['images'] as $item) {
                    if ($item->isValid() && !$item->hasMoved()) {
                        $temporalFile   = $item->getTempName();
                        $itemFileName   = $item->getRandomName();
                        $uri            = "angelopolis/news/" . $itemFileName;
                        if ($s3->putObjectFile($temporalFile, $bucket, $uri, S3::ACL_PUBLIC_READ)) {
                            $fileAWS3 = 'https://' . $bucket . '.s3.amazonaws.com/' . $uri;
                            $config[] = [
                                'key'       => $fileAWS3,
                                'caption'   => 'Imagen publicada',
                                'width'     => '70px',
                                'url' => base_url() . '/rewards/awards/gallery/destroy/'.$id.'/'.$itemFileName,
                            ];
                            array_push($preview, $fileAWS3);
                            array_push($fileNames, $fileAWS3);
                        } else {
                            throw new \Exception("Error de conexión temporal al servicio de AS3, favor de intentar más tarde.", 3);
                        }
                    } else {
                        return false;
                    }
                }

                $award = $this->getAward($id)->getRow();
                $currentImages = $award->img == "" ? [] : explode('|', $award->img);
                $data['img'] = implode('|', array_merge($currentImages, $fileNames));
                
                $this->db = db_connect('rewardsDB');
                $builder  = $this->db->table('certificate');
                $query = $builder->where('id', $id)->update($data);

                return ['initialPreview' => $preview, 'initialPreviewConfig' => $config, 'initialPreviewAsData' => true];
            }
            return false;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Rewards, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
        }
    }
}