<?php
namespace App\Models\Loyalty\Rewards;

use CodeIgniter\Model;
use App\Libraries\S3;

class BenefitsModel extends Model
{
    public function __construct()
    {
        helper('cms');
    }

    public function getBenefit($id)
    {
        try {
            $this->db = db_connect('rewardsDB');
            $builder = $this->db->table('gift_type');
            $query = $builder->where('id', $id)->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Rewards, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function getAllBenefits()
    {
        try {
            $this->db = db_connect('rewardsDB');
            $builder = $this->db->table('gift_type');
            $query = $builder->where('enabled', 1)->where('available', 1)->orderBy('name')->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Rewards, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function getCardsBenefit($id)
    {
        try {
            $this->db = db_connect('rewardsDB');
            $builder = $this->db->table('gift_type_has_card_type');
            $query = $builder->where('gift_type_id', $id)->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Rewards, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function createBenefit($title, $stock, $description, $cards, $image)
    {
        try {
            // Posible actualización: Quizá no sea obligatoria la imagen, si se tiene una por defecto en el SFTP
            if ($image->isValid() && !$image->hasMoved()) {

                $connectionFTP = ftp_connect('70.32.74.94');

                if ($connectionFTP) {
                    if (@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                        $fileName = $image->getRandomName();
                        if (ftp_put($connectionFTP, '/concierge.angelopolisrewards.com.mx/assets/beneficios/' . $fileName, $image, FTP_BINARY)) {
                            $dataBenefit = [
                                'name'                  => $title,
                                'description'           => $description,
                                'stock'                 => $stock,
                                'img'                   => 'http://concierge.angelopolisrewards.com.mx/assets/beneficios/' . $fileName,
                                'enabled'               => 1,
                                'available'             => 1,
                                'created'               => date('Y-m-d H:i:s'),
                            ];

                            $this->db   = db_connect('rewardsDB');
                            $this->db->transBegin();
                            
                            $this->db->table('gift_type')->insert($dataBenefit);
                            // Recuperar el ID del beneficio registrado.
                            $benefitID = $this->db->insertID();

                            // Operaciones tabla pivote gift_type_has_card_type
                            // Un beneficio puede estar presente en más de una tarjeta - Básico/Premium/Élite
                            $dataBenefitCards = [];
                            foreach ($cards as $card) {
                                array_push($dataBenefitCards, [
                                    'gift_type_id' => $benefitID,   // Beneficio registrado
                                    'card_type_id' => $card,           // Tarjeta asociada
                                ]);
                            }
                            $this->db->table('gift_type_has_card_type')->insertBatch($dataBenefitCards);

                            // Confirmar operaciones en la base de datos
                            if ($this->db->transStatus() !== FALSE) {
                                $this->db->transCommit();
                                ftp_close($connectionFTP);
                                return true;
                            } else {
                                $this->db->transRollback();
                                ftp_close($connectionFTP);
                                return false;
                            }
                        } else {
                            ftp_close($connectionFTP);
                            throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Rewards, favor de intentar más tarde.", 3);
                        }
                    } else {
                        ftp_close($connectionFTP);
                        throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Rewards, favor de intentar más tarde.", 3);
                    }
                } else {
                    ftp_close($connectionFTP);
                    throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Rewards, favor de intentar más tarde.", 3);
                }
            }
            return false;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Rewards, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function editBenefit($id, $title, $stock, $description, $cards, $image)
    {
        try {
            $connectionFTP = ftp_connect('70.32.74.94');
            if ($connectionFTP) {
                if (@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                    $dataBenefit = [
                        'name'          => $title,
                        'description'   => $description,
                        'stock'         => $stock,
                        'modified'      => date('Y-m-d H:i:s'),
                    ];

                    $this->db = db_connect('rewardsDB');
                    $this->db->transBegin();
                
                    if ($image->getError() === 4) {
                        $this->db->table('gift_type')->where('id', $id)->update($dataBenefit);

                        $dataBenefitCards = [];
                        foreach ($cards as $card) {
                            array_push($dataBenefitCards, [
                                'gift_type_id' => $id,      // Beneficio actualmente editado
                                'card_type_id' => $card,    // Tarjeta asociada
                            ]);
                        }
                        // Elimino todas las relaciones actuales y registro las nuevas
                        $this->db->table('gift_type_has_card_type')->where('gift_type_id', $id)->delete();
                        $this->db->table('gift_type_has_card_type')->insertBatch($dataBenefitCards);

                        // Confirmar operaciones en la base de datos
                        if ($this->db->transStatus() !== FALSE) {
                            $this->db->transCommit();
                            ftp_close($connectionFTP);
                            return true;
                        } else {
                            $this->db->transRollback();
                            ftp_close($connectionFTP);
                            return false;
                        }
                    } else {
                        if ($image->isValid() && !$image->hasMoved()) {
                            $fileName = $image->getRandomName();
                            if (ftp_put($connectionFTP, '/concierge.angelopolisrewards.com.mx/assets/beneficios/' . $fileName, $image, FTP_BINARY)) {
                                $dataBenefit['img'] = 'http://concierge.angelopolisrewards.com.mx/assets/beneficios/' . $fileName;
                                $this->db->table('gift_type')->where('id', $id)->update($dataBenefit);

                                // Operaciones tabla pivote gift_type_has_card_type
                                // Un beneficio puede estar presente en más de una tarjeta - Básico/Premium/Élite
                                $dataBenefitCards = [];
                                foreach ($cards as $card) {
                                    array_push($dataBenefitCards, [
                                        'gift_type_id' => $id,      // Beneficio actualmente editado
                                        'card_type_id' => $card,    // Tarjeta asociada
                                    ]);
                                }
                                // Elimino todas las relaciones actuales y registro las nuevas
                                $this->db->table('gift_type_has_card_type')->where('gift_type_id', $id)->delete();
                                $this->db->table('gift_type_has_card_type')->insertBatch($dataBenefitCards);

                                // Confirmar operaciones en la base de datos
                                if ($this->db->transStatus() !== FALSE) {
                                    $this->db->transCommit();
                                    ftp_close($connectionFTP);
                                    return true;
                                } else {
                                    $this->db->transRollback();
                                    ftp_close($connectionFTP);
                                    return false;
                                }
                            } else {
                                $this->db->transRollback();
                                ftp_close($connectionFTP);
                                throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Rewards, favor de intentar más tarde.", 3);
                            }
                        }
                        $this->db->transRollback();
                        ftp_close($connectionFTP);
                        return false;
                    }
                } else {
                    ftp_close($connectionFTP);
                    throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Rewards, favor de intentar más tarde.", 3);
                }
            } else {
                ftp_close($connectionFTP);
                throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Rewards, favor de intentar más tarde.", 3);
            }
            ftp_close($connectionFTP);
            return false;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Rewards, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function removeBenefit($id)
    {
        try {
            $data = [
                'enabled' => 0,
                'available' => 0,
                'modified' => date('Y-m-d H:i:s'),
            ];

            $this->db = db_connect('rewardsDB');
            $builder = $this->db->table('gift_type');
            $query = $builder->where('id', $id)->update($data);

            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Rewards, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }
}