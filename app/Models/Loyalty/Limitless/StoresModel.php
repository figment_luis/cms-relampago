<?php
namespace App\Models\Loyalty\Limitless;

use CodeIgniter\Model;
use App\Libraries\S3;

class StoresModel extends Model
{
    public function __construct()
    {
        helper('cms');
    }

    public function getStore($id)
    {
        try {
            $this->db = db_connect('limitlessDB');
            $builder = $this->db->table('store');
            $query = $builder->where('id', $id)->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function getAllGroups()
    {
        try {
            $this->db = db_connect('limitlessDB');
            $builder = $this->db->table('group');
            $query = $builder->where('enabled', 1)->where('available', 1)->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function getAllStores()
    {
        try {
            $this->db = db_connect('limitlessDB');
            $builder = $this->db->table('store');
            // $query = $builder->where('enabled', 1)->where('available', 1)->orderBy('name')->get();
            $query = $builder->orderBy('name')->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function createStore($title, $multiple, $group)
    {
        try {
            $data = [
                'name'          => $title,
                'multipler'     => $multiple,
                'group_id'      => $group,
                'enabled'       => 1,
                'available'     => 1,
                'created'       => date('Y-m-d H:i:s'),
            ];

            $this->db   = db_connect('limitlessDB');
            $builder    = $this->db->table('store');
            $query      = $builder->insert($data);

            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function editStore($id, $title, $multiple, $group)
    {
        try {
            $data = [
                'name'      => $title,
                'multipler' => $multiple,
                'group_id'  => $group,
                'modified'  => date('Y-m-d H:i:s'),
            ];

            $this->db   = db_connect('limitlessDB');
            $builder    = $this->db->table('store');
            $query      = $builder->where('id', $id)->update($data);

            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function removeStore($id)
    {
        try {
            $data = [
                'enabled' => 0,
                'available' => 0,
                'modified' => date('Y-m-d H:i:s'),
            ];

            $this->db = db_connect('limitlessDB');
            $builder = $this->db->table('store');
            $query = $builder->where('id', $id)->update($data);

            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function activateStore($id)
    {
        try {
            $data = [
                'enabled'   => 1,
                'available' => 1,
                'modified'  => date('Y-m-d H:i:s'),
            ];

            $this->db = db_connect('limitlessDB');
            $builder = $this->db->table('store');
            $query = $builder->where('id', $id)->update($data);

            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

}