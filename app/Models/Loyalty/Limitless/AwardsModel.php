<?php
namespace App\Models\Loyalty\Limitless;

use CodeIgniter\Model;
use App\Libraries\S3;

class AwardsModel extends Model
{
    public function __construct()
    {
        helper('cms');
    }

    public function getAward($id)
    {
        try {
            $this->db = db_connect('limitlessDB');
            $builder = $this->db->table('certificate');
            $query = $builder->where('id', $id)->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function getAllAwards()
    {
        try {
            $this->db = db_connect('limitlessDB');
            $builder = $this->db->table('certificate');
            $query = $builder->where('enabled', 1)->where('available', 1)->orderBy('name')->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function createAward($title, $subtitle, $points, $stock, $description, $image)
    {
        try {
            // Posible actualización: Quizá no sea obligatoria la imagen, si se tiene una por defecto en el SFTP
            if ($image->isValid() && !$image->hasMoved()) {
                $connectionFTP = ftp_connect('limitlessantea.com.mx');
                if ($connectionFTP) {
                    if (@ftp_login($connectionFTP, 'developer_limitless', '!Qc51s_yp_S')) {
                        // activar modo pasivo FTP
                        ftp_pasv($connectionFTP, true);
                        $fileName = $image->getRandomName();
                        if (ftp_put($connectionFTP, '/httpdocs/assets/img/productos/new_products/' . $fileName, $image, FTP_BINARY)) {
                            $data = [
                                'name'                  => $title,
                                'subtitle'              => $subtitle,
                                'description'           => $description,
                                'points'                => $points,
                                'stock'                 => $stock,
                                'img'                   => 'http://limitlessantea.com.mx/assets/img/productos/new_products/' . $fileName,
                                'img_mobile_internal'   => 'http://limitlessantea.com.mx/assets/img/productos/new_products/' . $fileName,
                                'enabled'               => 1,
                                'available'             => 1,
                                'isExperience'          => 0,
                                'type'                  => 1,
                                'created'               => date('Y-m-d H:i:s'),
                            ];

                            $this->db   = db_connect('limitlessDB');
                            $builder    = $this->db->table('certificate');
                            $query      = $builder->insert($data);

                            ftp_close($connectionFTP);
                            return $query;
                        } else {
                            ftp_close($connectionFTP);
                            throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Limitless, favor de intentar más tarde.", 3);
                        }
                    } else {
                        ftp_close($connectionFTP);
                        throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Limitless, favor de intentar más tarde.", 3);
                    }
                } else {
                    ftp_close($connectionFTP);
                    throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Limitless, favor de intentar más tarde.", 3);
                }
            }
            return false;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            // Excepciones personalizadas en el código: code = 3
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            //throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de soporte técnico.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function editAward($id, $title, $subtitle, $points, $stock, $description, $image)
    {
        try {
            $connectionFTP = ftp_connect('limitlessantea.com.mx');
            if ($connectionFTP) {
                if (@ftp_login($connectionFTP, 'developer_limitless', '!Qc51s_yp_S')) {
                    // activar modo pasivo FTP
                    ftp_pasv($connectionFTP, true);
                    $data = [
                        'name'          => $title,
                        'subtitle'      => $subtitle,
                        'description'   => $description,
                        'points'        => $points,
                        'stock'         => $stock,
                        'modified'       => date('Y-m-d H:i:s'),
                    ];

                    $this->db = db_connect('limitlessDB');
                    $builder  = $this->db->table('certificate');

                    if ($image->getError() === 4) {
                        $query = $builder->where('id', $id)->update($data);

                        ftp_close($connectionFTP);
                        return $query;
                    } else {
                        if ($image->isValid() && !$image->hasMoved()) {
                            $fileName = $image->getRandomName();
                            if (ftp_put($connectionFTP, '/httpdocs/assets/img/productos/new_products/' . $fileName, $image, FTP_BINARY)) {
                                $data['img']                 = 'http://limitlessantea.com.mx/assets/img/productos/new_products/' . $fileName;
                                $data['img_mobile_internal'] = 'http://limitlessantea.com.mx/assets/img/productos/new_products/' . $fileName;
                                $query = $builder->where('id', $id)->update($data);

                                ftp_close($connectionFTP);
                                return $query;
                            } else {
                                ftp_close($connectionFTP);
                                throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Limitless, favor de intentar más tarde.", 3);
                            }
                        }
                        return false;
                    }
                } else {
                    ftp_close($connectionFTP);
                    throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Limitess, favor de intentar más tarde.", 3);
                }
            } else {
                ftp_close($connectionFTP);
                throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Limitless, favor de intentar más tarde.", 3);
            }
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            // Excepciones personalizadas
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            //throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function removeAward($id)
    {
        try {
            $data = [
                'enabled' => 0,
                'available' => 0,
                'modified' => date('Y-m-d H:i:s'),
            ];
            $this->db = db_connect('limitlessDB');
            $builder = $this->db->table('certificate');
            $query = $builder->where('id', $id)->update($data);
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }
}