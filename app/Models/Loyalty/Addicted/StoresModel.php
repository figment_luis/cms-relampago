<?php
namespace App\Models\Loyalty\Addicted;

use CodeIgniter\Model;
use App\Libraries\S3;

class StoresModel extends Model
{
    public function __construct()
    {
        helper('cms');
    }

    public function getStore($id)
    {
        $this->db = db_connect('addictedConciergeDB');
        $builder = $this->db->table('store');
        $query = $builder->where('id', $id)->get();
        return $query;
    }

    public function getAllStores()
    {
        $this->db = db_connect('addictedConciergeDB');
        $builder = $this->db->table('store');
        // $query = $builder->where('active', 1)->orderBy('name')->get();
        $query = $builder->orderBy('name')->get();
        return $query;
    }

    public function createStore($title)
    {
        
        $data = [
            'name'      => $title,
            'active'    => 1,
            'created'   => date('Y-m-d H:i:s'),
        ];

        $this->db   = db_connect('addictedConciergeDB');
        $builder    = $this->db->table('store');
        $query      = $builder->insert($data);
        
        return $query;
    }

    public function editStore($id, $title)
    {
        
        $data = [
            'name'      => $title,
            'modified'  => date('Y-m-d H:i:s'),
        ];
            
        $this->db   = db_connect('addictedConciergeDB');
        $builder    = $this->db->table('store');
        $query      = $builder->where('id', $id)->update($data);

        return $query;
    }

    public function removeStore($id)
    {
        $data = [
            'active' => 0,
            'modified' => date('Y-m-d H:i:s'),
        ];

        $this->db = db_connect('addictedConciergeDB');
        $builder = $this->db->table('store');
        $query = $builder->where('id', $id)->update($data);

        return $query;
    }

    public function activateStore($id)
    {
        $data = [
            'active' => 1,
            'modified'  => date('Y-m-d H:i:s'),
        ];

        $this->db = db_connect('addictedConciergeDB');
        $builder = $this->db->table('store');
        $query = $builder->where('id', $id)->update($data);

        return $query;
    }

}