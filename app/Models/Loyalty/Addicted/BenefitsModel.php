<?php
namespace App\Models\Loyalty\Addicted;

use CodeIgniter\Model;
use App\Libraries\S3;

class BenefitsModel extends Model
{
    public function __construct()
    {
        helper('cms');
    }

    public function getBenefit($id)
    {
        $this->db = db_connect('addictedDB');
        $builder = $this->db->table('beneficios');
        $query = $builder->where('id', $id)->get();
        return $query;
    }

    public function getAllBenefits()
    {
        $this->db = db_connect('addictedDB');
        $builder = $this->db->table('beneficios');
        $query = $builder->where('enabled', 1)->orderBy('title')->get();
        return $query;
    }

    public function createBenefit($title, $points, $link, $description, $image)
    {
        // Posible actualización: Quizá no sea obligatoria la imagen, si se tiene una por defecto en el SFTP
        if ($image->isValid() && !$image->hasMoved()) {

            $connectionFTP = ftp_connect('72.167.55.214');

            if (@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                // activar modo pasivo FTP
                ftp_pasv($connectionFTP, true);
                $fileName = $image->getRandomName();

                if (ftp_put($connectionFTP, '/addicted.com.mx/assets/img/beneficios/' . $fileName, $image, FTP_BINARY)) {
                    $data = [
                        'title'         => $title,
                        'description'   => $description,
                        'image_url'     => 'beneficios/' . $fileName,
                        'link_url'      => $link,
                        'puntos'        => $points,
                        'enabled'       => 1,
                        // Posible actualización de tabla para incluir campos de auditoría (created. modified)
                        // 'created'       => date('Y-m-d H:i:s'),
                    ];

                    $this->db   = db_connect('addictedDB');
                    $builder    = $this->db->table('beneficios');
                    $query      = $builder->insert($data);

                    ftp_close($connectionFTP);
                    return $query;
                }
            }
        }

        return false;
    }

    public function editBenefit($id, $title, $points, $link, $description, $image)
    {
        $data = [
            'title'         => $title,
            'description'   => $description,
            'link_url'      => $link,
            'puntos'        => $points,
            // Posible actualización de tabla para incluir campos de auditoría (created. modified)
            // 'modified'       => date('Y-m-d H:i:s'),
        ];

        $this->db = db_connect('addictedDB');
        $builder  = $this->db->table('beneficios');

        if ($image->getError() === 4) {
            $query = $builder->where('id', $id)->update($data);
            return $query;
        } else {
            if ($image->isValid() && !$image->hasMoved()) {
                $connectionFTP = ftp_connect('72.167.55.214');
                if (@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                    // activar modo pasivo FTP
                    ftp_pasv($connectionFTP, true);
                    $fileName = $image->getRandomName();
                    if (ftp_put($connectionFTP, '/addicted.com.mx/assets/img/beneficios/' . $fileName, $image, FTP_BINARY)) {
                        $data['image_url'] = 'beneficios/' . $fileName;
                        $query = $builder->where('id', $id)->update($data);

                        ftp_close($connectionFTP);
                        return $query;
                    }
                }
            }
        }

        return false;
    }

    public function removeBenefit($id)
    {
        $data = [
            'enabled' => 0,
            // 'modified' => date('Y-m-d H:i:s'),
        ];

        $this->db = db_connect('addictedDB');
        $builder = $this->db->table('beneficios');
        $query = $builder->where('id', $id)->update($data);

        return $query;
    }
}