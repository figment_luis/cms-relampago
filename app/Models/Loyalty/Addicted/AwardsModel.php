<?php
namespace App\Models\Loyalty\Addicted;

use CodeIgniter\Model;
use App\Libraries\S3;

class AwardsModel extends Model
{
    public function __construct()
    {
        helper('cms');
    }

    public function getAward($id)
    {
        $this->db = db_connect('addictedDB');
        $builder = $this->db->table('obsequios');
        $query = $builder->where('id', $id)->get();
        return $query;
    }

    public function getAllAwards()
    {
        $this->db = db_connect('addictedDB');
        $builder = $this->db->table('obsequios');
        $query = $builder->where('enabled', 1)->orderBy('title')->get();
        return $query;
    }

    public function createAward($title, $points, $price, $link, $description, $image)
    {
        $data = [
            'title'         => $title,
            'description'   => $description,
            'link_url'      => $link ? $link : 'no-link',
            'puntos'        => $points,
            'precio'        => $price,
            'enabled'       => 1,
            // Posible actualización de tabla para incluir campos de auditoría (created. modified)
            // 'created'       => date('Y-m-d H:i:s'),
        ];

        $this->db   = db_connect('addictedDB');
        $builder    = $this->db->table('obsequios');

        if ($image->getError() === 4) {
            // Asociar una imagen por defecto al certificado
            $data['image_url'] = 'obsequios/default.jpg';
            $query = $builder->insert($data);
            return $query;
        } else {
            // Subir imagen servidor FTP
            if ($image->isValid() && !$image->hasMoved()) {

                $connectionFTP = ftp_connect('72.167.55.214');

                if (@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                    // activar modo pasivo FTP
                    ftp_pasv($connectionFTP, true);
                    $fileName = $image->getRandomName();

                    if (ftp_put($connectionFTP, '/addicted.com.mx/assets/img/obsequios/' . $fileName, $image, FTP_BINARY)) {
                        $data['image_url'] = 'obsequios/' . $fileName;
                        $query = $builder->insert($data);

                        ftp_close($connectionFTP);
                        return $query;
                    }
                }
            }
        }

        return false;
    }

    public function editAward($id, $title, $points, $price, $link, $description, $image)
    {
        $data = [
            'title'         => $title,
            'description'   => $description,
            'link_url'      => $link ? $link : 'no-link',
            'puntos'        => $points,
            'precio'        => $price,
            // Posible actualización de tabla para incluir campos de auditoría (created. modified)
            // 'modified'       => date('Y-m-d H:i:s'),
        ];

        $this->db = db_connect('addictedDB');
        $builder  = $this->db->table('obsequios');

        if ($image->getError() === 4) {
            $query = $builder->where('id', $id)->update($data);
            return $query;
        } else {
            if ($image->isValid() && !$image->hasMoved()) {
                $connectionFTP = ftp_connect('72.167.55.214');
                if (@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                    // activar modo pasivo FTP
                    ftp_pasv($connectionFTP, true);
                    $fileName = $image->getRandomName();

                    if (ftp_put($connectionFTP, '/addicted.com.mx/assets/img/obsequios/' . $fileName, $image, FTP_BINARY)) {
                        $data['image_url'] = 'obsequios/' . $fileName;
                        $query = $builder->where('id', $id)->update($data);

                        ftp_close($connectionFTP);
                        return $query;
                    }
                }
            }
        }

        return false;
    }

    public function removeAward($id)
    {
        $data = [
            'enabled' => 0,
            // 'modified' => date('Y-m-d H:i:s'),
        ];

        $this->db = db_connect('addictedDB');
        $builder = $this->db->table('obsequios');
        $query = $builder->where('id', $id)->update($data);

        return $query;
    }
}