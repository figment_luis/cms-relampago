<?php
namespace App\Models;

use CodeIgniter\Model;

class LogModel extends Model
{
    public function __construct()
    {

    }

    public function getAllLogs()
    {
        $this->db = db_connect('default');
        $builder  = $this->db->table('logs');

        $logs = $builder->select('logs.id, logs.module, logs.action, logs.description, logs.created_at, CONCAT_WS(" ",users.first_name, users.last_name) as user')->join('users', 'logs.user_id = users.id')->orderBy('logs.id', 'desc')->get()->getResult();

        return $logs;
    }

    public function createLog($description, $module = '', $action = '')
    {
        $data = [
            'module'        => $module,
            'action'        => $action,
            'description'   => $description,
            'user_id'       => session()->get('user_id'),
            'created_at'    => date('Y-m-d H:i:s'),
        ];

        $this->db = db_connect('default');
        $builder  = $this->db->table('logs');

        $builder->insert($data);
    }

}