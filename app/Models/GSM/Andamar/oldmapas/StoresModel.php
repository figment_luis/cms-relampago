<?php
namespace App\Models\GSM\Andamar;

use CodeIgniter\Model;

class StoresModel extends Model
{

    public function __construct()
    {

    }

    public function getAllStores()
    {
        $this->db = db_connect('andamarDB');
        $builder = $this->db->table('stores');
        $query = $builder->where('deleted_at', null)->orderBy('name', 'desc')->get();
        return $query;
    }

    public function getStore($id)
    {
        $this->db = db_connect('andamarDB');
        $builder = $this->db->table('stores');
        $query = $builder->where('id', $id)->get();
        return $query;
    }

    public function getLevels()
    {
        $this->db = db_connect('andamarDB');
        $builder = $this->db->table('levels');
        $query = $builder->get();
        return $query;
    }

    public function getCategories()
    {
        $this->db = db_connect('andamarDB');
        $builder = $this->db->table('categories');
        $query = $builder->where('enabled', 1)->orderBy('name')->get();
        return $query;
    }

    public function getMapLocations()
    {
        $this->db = db_connect('andamarDB');
        $builder = $this->db->table('stores');
        $query = $builder->select('map_location, map_x, map_y, location')->where('location >', 0)->orderBy('location')->get();
        return $query;
    }

    /*public function getSubcategories()
    {
        $this->db = db_connect('andamarDB');
        $builder = $this->db->table('subcategories');
        $query = $builder->where('available', 1)->orderBy('name')->get();
        return $query;
    }*/

    public function getCategoryStore($id)
    {
        $this->db = db_connect('andamarDB');
        $builder = $this->db->table('categories_has_stores');
        $query = $builder->where('stores_id', $id)->get();
        return $query;
    }

    /*public function getSubategoriesStore($id)
    {
        $this->db = db_connect('andamarDB');
        $builder = $this->db->table('subcategories_has_stores');
        $query = $builder->where('stores_id', $id)->get();
        return $query;
    }*/

    public function editStore($id, $name, $description, $email, $url, $shorturl, $phone, $hourhand, $keywords, $location, $category, $map, $logotipo, $cover)
    {
        // Convertir en array los datos de localización en el mapa
        list($map_location, $map_x, $map_y) = explode('|', $map);

        $dataStore = [
            'name'          => $name,
            'slug'          => mb_url_title($name, '-', TRUE),
            'description'   => $description,
            'location'      => $location,
            'telephone'     => $phone,
            'horario'       => $hourhand,
            'email'         => $email,
            'url'           => $url,
            'short_url'     => $shorturl,
            'keywords'      => $keywords,
            'map_location'  => $map_location,
            'map_x'         => $map_x,
            'map_y'         => $map_y,
            'updated_at'    => date('Y-m-d H:i:s'),
        ];
        $this->db = db_connect('andamarDB');
        $this->db->transBegin();

        // 1. Operaciones tabla stores
        $this->db->table('stores')->where('id', $id)->update($dataStore);
        // 2. Operaciones tabla pivote categoría
        // Update: Andamar 2021 solo acepta una categoría por tienda
        $dataCategory = [
            'category_id' => $category,
            'stores_id' => $id
        ];
        $this->db->table('categories_has_stores')->where('stores_id', $id)->update($dataCategory);
        
        // 3. Determinar si hay actualización de imagenes
        $hasNewLogotipo = $logotipo->getError() !== 4;
        $hasNewCover = $cover->getError() !== 4;

        if ($hasNewLogotipo || $hasNewCover) {
            // Abrir conexiòn FTP
            $connectionFTP = ftp_connect('72.10.50.21');
            if (ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                // Verificar si existe un nuevo logotipo a reemplazar
                if ($hasNewLogotipo) {
                    if ($logotipo->isValid() && !$logotipo->hasMoved()) {
                        $fileNameLogo = mb_url_title($name, '-', TRUE) . '_' . time() . '_logo' . '.jpg';
                        if (ftp_put($connectionFTP, '/httpdocs/assets/img/stores/' . $fileNameLogo, $logotipo, FTP_BINARY)) {
                            $this->db->table('stores')->where('id', $id)->update([
                                'image' => 'assets/img/stores/' . $fileNameLogo
                            ]);
                        } else {
                            ftp_close($connectionFTP);
                            $this->db->transRollback();
                            return false;
                        }
                    } else {
                        ftp_close($connectionFTP);
                        $this->db->transRollback();
                        return false;
                    }
                }
                // Verificar si existe un nuevo cover de tienda a actualizar
                if ($hasNewCover) {
                    if ($cover->isValid() && !$cover->hasMoved()) {
                        $fileNameCover = mb_url_title($name, '-', TRUE) . '_' . time() . '_cover' . '.jpg';
                        // Borrar todo el contenido del directorio
                        if (@ftp_chdir($connectionFTP, '/httpdocs/assets/img/microsites/' . $id)) {
                            $currentFiles = ftp_nlist($connectionFTP, ".");
                            foreach ($currentFiles as $file) {
                                // Si no se puede eliminar algun archivo cancelamos la operación.
                                if(!ftp_delete($connectionFTP, $file)) {
                                    ftp_close($connectionFTP);
                                    $this->db->transRollback();
                                    return false;
                                }
                            }
                        } else {
                            ftp_close($connectionFTP);
                            $this->db->transRollback();
                            return false;
                        }
                        // Proceder a almacenar el nuevo archivo de portada
                        if (!ftp_put($connectionFTP, '/httpdocs/assets/img/microsites/' . $id . '/' . $fileNameCover, $cover, FTP_BINARY)) {
                            ftp_close($connectionFTP);
                            $this->db->transRollback();
                            return false;
                        }
                    } else {
                        ftp_close($connectionFTP);
                        $this->db->transRollback();
                        return false;
                    }
                }
                 // Proceder a guardar los cambios en base de datos
                if ($this->db->transStatus() !== FALSE) {
                    $this->db->transCommit();
                    return true;
                }
            } else {
                ftp_close($connectionFTP);
                $this->db->transRollback();
                return false;
            }            
        } else {
            // Proceder a guardar los cambios en base de datos (En este punto no se envío actualización de imagenes)
            if ($this->db->transStatus() !== FALSE) {
                $this->db->transCommit();
                return true;
            }
        }
        
        $this->db->transRollback();
        return false;
    }

    public function getImageStore($id)
    {
        $connectionFTP = ftp_connect('72.10.50.21');
        if(ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
            if(@ftp_chdir($connectionFTP, '/httpdocs/assets/img/microsites/' . $id . '/')) {
                $content = ftp_nlist($connectionFTP, '.');
                if ($content) {
                    foreach ($content as $file) {
                        $info = new \SplFileInfo($file);
                        // Deolver el primer archivo jpg que encuentre.
                        // En teoría: 
                        //      1. Debe existir solo un archivo de imagen por tienda.
                        //      2. El formato de imagen dbería ser JPG
                        if (strtolower($info->getExtension()) == 'jpg' || strtolower($info->getExtension()) == 'png') {
                            ftp_close($connectionFTP);
                            return $file;
                        }
                        
                    }
                    
                }
            }
        }
        ftp_close($connectionFTP);
        return null;
    }

    /*public function removeGallery($id, $image)
    {
        $connectionFTP = ftp_connect('72.10.50.21');
        if(ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
            if(@ftp_chdir($connectionFTP, '/httpdocs/img/stores/desktop/' . $id . '/')) {
                if (ftp_delete($connectionFTP, $image)) {
                    ftp_close($connectionFTP);
                    return true;
                }
            }
        }
        ftp_close($connectionFTP);
        return false;
    }*/

    /*public function updateGallery($storeID, $galleryFiles)
    {
        $connectionFTP = ftp_connect('72.10.50.21');
        if (ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
            if ($galleryFiles) {
                if (@ftp_chdir($connectionFTP, '/httpdocs/img/stores/desktop/')) {
                    if (in_array($storeID, ftp_nlist($connectionFTP, '.'))) {
                        // Colocar los nuevos archivos de galería en el directorio actual
                        $preview = [];
                        $config = [];
                        foreach($galleryFiles['images'] as $image) {
                            if ($image->isValid() && !$image->hasMoved()) {
                                $fileName = $image->getRandomName();
                                if (!ftp_put($connectionFTP, '/httpdocs/img/stores/desktop/' . $storeID . '/' . $fileName, $image, FTP_BINARY)) {
                                    ftp_close($connectionFTP);
                                    break;
                                    return false;
                                }
                                $config[] = [
                                    'key' => $fileName,
                                    'caption' => 'Imagen publicada en tienda',
                                    'width' => '70px',
                                    //'size' => $fileSize,
                                    //'downloadUrl' => $newFileUrl, // the url to download the file
                                    'url' => base_url() . '/andamar/gallery/destroy/'.$storeID.'/'.$fileName,
                                ];
                                array_push($preview, 'https://andamar.com.mx/img/stores/desktop/'.$storeID.'/'.$fileName);
                            } else {
                                break;
                                ftp_close($connectionFTP);
                                return false;
                            }
                        }
                        ftp_close($connectionFTP);
                        return ['initialPreview' => $preview, 'initialPreviewConfig' => $config, 'initialPreviewAsData' => true];
                        // return true;
                    } else {
                        // Crear nuevo directorio para archivos de galería
                        if (ftp_mkdir($connectionFTP, '/httpdocs/img/stores/desktop/' . $storeID)) {
                            foreach($galleryFiles['images'] as $image) {
                                if ($image->isValid() && !$image->hasMoved()) {
                                    $fileName = $image->getRandomName();
                                    if (!ftp_put($connectionFTP, '/httpdocs/img/stores/desktop/' . $storeID . '/' . $fileName, $image, FTP_BINARY)) {
                                        break;
                                        ftp_close($connectionFTP);
                                        return false;
                                    }
                                } else {
                                    break;
                                    ftp_close($connectionFTP);
                                    return false;
                                }
                            }
                            ftp_close($connectionFTP);
                            return true;
                        }   // Crear nuevo directorio de archivos de galería
                    }
                }   // Cambiar de directorio FTP
            }
        }   // Credenciales FTP

        ftp_close($connectionFTP);
        return false;
    }*/

    public function createStore($name, $description, $email, $url, $shorturl, $phone, $hourhand, $keywords, $location, $category, $map, $logotipo, $cover)
    {
        // Convertir en array los datos de localización en el mapa
        list($map_location, $map_x, $map_y) = explode('|', $map);

        $dataStore = [
            'name'          => $name,
            'slug'          => mb_url_title($name, '-', TRUE),
            'description'   => $description,
            'location'      => $location,
            'telephone'     => $phone,
            'horario'       => $hourhand,
            'email'         => $email,
            'url'           => $url,
            'short_url'     => $shorturl,
            'keywords'      => $keywords,
            'map_location'  => $map_location,
            'map_x'         => $map_x,
            'map_y'         => $map_y,
            'created_at'    => date('Y-m-d H:i:s'),
        ];

        $this->db = db_connect('andamarDB');
        $this->db->transBegin();

        // 1. Operaciones tabla stores
        $this->db->table('stores')->insert($dataStore);
        $storeID = $this->db->insertID();
        // 2. Operaciones tabla pivote categoría
        $dataCategory = [
            'category_id' => $category,
            'stores_id' => $storeID
        ];
        $this->db->table('categories_has_stores')->insert($dataCategory);
        
        // 3. Conexión FTP
        $connectionFTP = ftp_connect('72.10.50.21');
        $successUploadLogo = false;
        $successUploadCover = false;
        if (ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
            // 4. Subir archivos al servidor FTP

            // Logotipo de tienda
            if ($logotipo->isValid() && !$logotipo->hasMoved()) {
                $fileNameLogo = mb_url_title($name, '-', TRUE) . '_' . time() . '_logo' . '.jpg';
                if (ftp_put($connectionFTP, '/httpdocs/assets/img/stores/' . $fileNameLogo, $logotipo, FTP_BINARY)) {
                    $this->db->table('stores')->where('id', $storeID)->update([
                        'image' => 'assets/img/stores/' . $fileNameLogo
                    ]);
                    $successUploadLogo = true;
                }
            }
            // Imagen de Portada
            if ($cover->isValid() && !$cover->hasMoved()) {
                $fileNameCover = mb_url_title($name, '-', TRUE) . '_' . time() . '_cover' . '.jpg';
                // Crear nuevo directorio para archivo de portada
                if (ftp_mkdir($connectionFTP, '/httpdocs/assets/img/microsites/' . $storeID)) {
                    if (ftp_put($connectionFTP, '/httpdocs/assets/img/microsites/' . $storeID . '/' . $fileNameCover, $cover, FTP_BINARY)) {
                        $successUploadCover = true;
                    }
                }
                
            }

            // Confirmar registro de información en base de datos
            if ($successUploadLogo && $successUploadCover) {
                if ($this->db->transStatus() !== FALSE) {
                    $this->db->transCommit();
                    return true;
                }
            }
        }
        
        $this->db->transRollback();
        ftp_close($connectionFTP);
        return false;
    }

    public function removeStore($id)
    {
        $data = ['deleted_at' => 1];

        $this->db   = db_connect('andamarDB');
        $builder    = $this->db->table('stores');
        $query      = $builder->where('id', $id)->update($data);

        // Update: remover registros pivote (categoría por tienda) y directorio FTP Cover y Logotipo

        return $query;
    }

}
