<?php
namespace App\Models\GSM\Luxury;

use CodeIgniter\Model;
use App\Libraries\S3;

class TrendsModel extends Model
{
    public function __construct()
    {
        helper('cms');
    }

    public function getTrend($id)
    {
        try {
            $this->db = db_connect('luxuryDB');
            $builder = $this->db->table('trends');
            $query = $builder->where('id', $id)->where('type', 1)->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Luxury Hall, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function getAllTrends()
    {
        try {
            $this->db = db_connect('luxuryDB');
            $builder = $this->db->table('trends');
            $query = $builder->where('enabled', 1)->where('type', 1)->orderBy('created', 'desc')->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Luxury Hall, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function uploadImage($image)
    {
        try {
            $connectionFTP = ftp_connect('70.32.111.65');
            if(@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                if(@ftp_chdir($connectionFTP, '/httpdocs/assets/images/temas/')) {
                   
                    $pathImage = null;
                    if ($image->isValid() && !$image->hasMoved()) {
                        $fileName = date('Y-m-d') . '_' . $image->getRandomName();
                        // Intentar subir archivo al servidor FTP
                        if (!ftp_put($connectionFTP, '/httpdocs/assets/images/temas/' . $fileName, $image, FTP_BINARY)) {
                            ftp_close($connectionFTP);
                            throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Luxury Hall, favor de intentar más tarde.", 3);
                        }
                        // Recuperar el path completo donde se registró la imagen
                        $pathImage = 'https://luxuryhall.com.mx/assets/images/temas/' . $fileName;
                    } else {
                        ftp_close($connectionFTP);
                        return false;
                    }
                    // CKEditor espera una respuesta personalizada, donde se incluya el path de imagen para proyectarla correntamente sobre el editor.
                    ftp_close($connectionFTP);
                    return ['code' => 200, 'res' => true, 'url' => $pathImage];
                
                } else {
                    ftp_close($connectionFTP);
                    throw new \Exception("Error al tratar de acceder al directorio de imagenes en el servidor FTP del proyecto Luxury Hall, favor de intentar más tarde.", 3);
                }
            } else {
                ftp_close($connectionFTP);
                throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Luxury Hall, favor de intentar más tarde.", 3);
            }
        } catch (\Exception $e) {
            // Excepciones personalizadas en el código: code = 3
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function createTrend($title, $subtitle, $cover, $content, $link_external)
    {
        try {
            if ($cover->isValid() && !$cover->hasMoved()) {
                $data = [
                    'title'             => $title,
                    'subtitle'          => $subtitle,
                    'descriptionView'   => $content,
                    'link'              => $link_external,
                    'link_target'       => 'external',
                    'type'              => 1,
                    'enabled'           => 1,
                    'created'           => date('Y-m-d H:i:s'),
                ];
                
                $s3     = new S3(cms_getConfigAWS3());
                $bucket = "kinetiq-loyalty-assets";

                // El directorio de las imagenes de temas del mes es el mismo que el de las novedades
                $temporalFile   = $cover->getTempName();
                $uri            = "luxuryhall/trends/" . $cover->getRandomName();
                if ($s3->putObjectFile($temporalFile, $bucket, $uri, S3::ACL_PUBLIC_READ)) {
                    $data['img'] = 'https://' . $bucket . '.s3.amazonaws.com/' . $uri;
                } else {
                    throw new \Exception("Error de conexión temporal al servicio de AS3, favor de intentar más tarde.", 3);
                }

                $this->db = db_connect('luxuryDB');
                $builder  = $this->db->table('trends');
                $query    = $builder->insert($data);
                return $query;
            }
            return false;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Luxury Hall, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            // Excepciones personalizadas
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function editTrend($id, $title, $subtitle, $cover, $content, $link_external)
    {
        try {
            $data = [
                'title'             => $title,
                'subtitle'          => $subtitle,
                'descriptionView'   => $content,
                'link'              => $link_external,
                'modified'          => date('Y-m-d H:i:s'),
            ];

            $this->db   = db_connect('luxuryDB');
            $builder    = $this->db->table('trends');

            $s3         = new S3(cms_getConfigAWS3());
            $bucket     = "kinetiq-loyalty-assets";

            // No se subio ningún archivo para actualizar
            if ($cover->getError() === 4) {
                $query = $builder->where('id', $id)->update($data);
                return $query;
            } else {
                if ($cover->isValid() && !$cover->hasMoved()) {
                    // El directorio de imagenes de temas del mes, es el mismo que el de novedades
                    $temporalFile   = $cover->getTempName();
                    $uri            = "luxuryhall/trends/" . $cover->getRandomName();
                    if ($s3->putObjectFile($temporalFile, $bucket, $uri, S3::ACL_PUBLIC_READ)) {
                        $data['img']  = 'https://' . $bucket . '.s3.amazonaws.com/' . $uri;
                        $query        = $builder->where('id', $id)->update($data);
                        return $query;
                    } else {
                        throw new \Exception("Error de conexión temporal al servicio de AS3, favor de intentar más tarde.", 3);
                    }
                } else {
                    return false;
                }
            }
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Luxury Hall, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            // Excepciones personalizadas
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function removeTrend($id)
    {
        try {
            $data = [
                'enabled'  => 0,
                'modified' => date('Y-m-d H:i:s'),
            ];

            $this->db = db_connect('luxuryDB');
            $builder = $this->db->table('trends');
            $query = $builder->where('id', $id)->update($data);

            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Luxury Hall, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }
}
