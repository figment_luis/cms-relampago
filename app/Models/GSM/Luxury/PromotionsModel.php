<?php
namespace App\Models\GSM\Luxury;

use CodeIgniter\Model;
use App\Libraries\S3;

class PromotionsModel extends Model
{
    public function __construct()
    {
        helper('cms');
    }

    public function getPromotion($id)
    {
        try {
            $this->db = db_connect('luxuryDB');
            $builder = $this->db->table('promotions');
            $query = $builder->where('id', $id)->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function getAllPromotions()
    {
        try {
            $this->db = db_connect('luxuryDB');
            $builder = $this->db->table('promotions');
            $query = $builder->where('deleted_at', null)->orderBy('created_at', 'desc')->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function createPromotion($title, $subtitle, $url, $image)
    {
        try {
            if ($image->isValid() && !$image->hasMoved()) {
                $connectionFTP = ftp_connect('70.32.111.65');

                if (@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                    // activar modo pasivo FTP
                    ftp_pasv($connectionFTP, true);
                    // $fileName = $image->getRandomName();
                    $fileName = mb_url_title($title, '-', TRUE) . '_' . time() . '.jpg';

                    if (ftp_put($connectionFTP, '/httpdocs/assets/images/promotions/' . $fileName, $image, FTP_BINARY)) {
                        $data = [
                            'title'     => $title,
                            'subtitle'  => $subtitle,
                            'img'       => 'assets/images/promotions/' . $fileName,
                            'url'       => $url,
                            'created_at'   => date('Y-m-d H:i:s'),
                        ];

                        $this->db   = db_connect('luxuryDB');
                        $builder    = $this->db->table('promotions');
                        $query      = $builder->insert($data);

                        ftp_close($connectionFTP);
                        return $query;
                    } else {
                        ftp_close($connectionFTP);
                        throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Antea, favor de intentar más tarde.", 3);
                    }
                } else {
                    ftp_close($connectionFTP);
                    throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Antea, favor de intentar más tarde.", 3);
                }
            }
            return false;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            // Excepciones personalizadas en el código: code = 3
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            //throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function editPromotion($id, $title, $subtitle, $url, $image)
    {
        try {
            $data = [
                'title'     => $title,
                'subtitle'  => $subtitle,
                'url'       => $url,
                'updated_at'  => date('Y-m-d H:i:s'),
            ];

            $this->db   = db_connect('luxuryDB');
            $builder    = $this->db->table('promotions');

            if ($image->getError() === 4) {
                $query = $builder->where('id', $id)->update($data);
                return $query;
            } else {
                if ($image->isValid() && !$image->hasMoved()) {
                    $connectionFTP = ftp_connect('70.32.111.65');
        
                    if (@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                        // activar modo pasivo FTP
                        ftp_pasv($connectionFTP, true);
                        // $fileName = $image->getRandomName();
                        $fileName = mb_url_title($title, '-', TRUE) . '_update_' . time() . '.jpg';
        
                        if (ftp_put($connectionFTP, '/httpdocs/assets/images/promotions/' . $fileName, $image, FTP_BINARY)) {
                            $data['img'] = 'assets/images/promotions/' . $fileName;
                            $query       = $builder->where('id', $id)->update($data);
        
                            ftp_close($connectionFTP);
                            return $query;
                        } else {
                            ftp_close($connectionFTP);
                            throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Antea, favor de intentar más tarde.", 3);
                        }
                    } else {
                        ftp_close($connectionFTP);
                        throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Antea, favor de intentar más tarde.", 3);
                    }
                }
                return false;
            }
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            // Excepciones personalizadas en el código: code = 3
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            //throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function removePromotion($id)
    {
        try {
            $data = ['deleted_at' => 1];

            $this->db = db_connect('luxuryDB');
            $builder = $this->db->table('promotions');
            $query = $builder->where('id', $id)->update($data);

            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }
}
