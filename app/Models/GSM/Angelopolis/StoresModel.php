<?php
namespace App\Models\GSM\Angelopolis;

use CodeIgniter\Model;

class StoresModel extends Model
{

    public function __construct() { }

    public function getAllStores()
    {
        try {
            $this->db = db_connect('angelopolisDB');
            $builder = $this->db->table('stores');
            $query = $builder->where('deleted_at', null)->orderBy('name', 'desc')->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function getStore($id)
    {
        try {
            $this->db = db_connect('angelopolisDB');
            $builder = $this->db->table('stores');
            $query = $builder->where('id', $id)->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function getLevels()
    {
        try {
            $this->db = db_connect('angelopolisDB');
            $builder = $this->db->table('levels');
            $query = $builder->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function getCategories()
    {
        try {
            $this->db = db_connect('angelopolisDB');
            $builder = $this->db->table('categories');
            $query = $builder->where('enabled', 1)->orderBy('name')->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function getCategoryStore($id)
    {
        try {
            $this->db = db_connect('angelopolisDB');
            $builder = $this->db->table('categories_has_stores');
            $query = $builder->where('stores_id', $id)->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function editStore($id, $name, $description, $email, $url, $shorturl, $phone, $hourhand, $keywords, $location, $category, $logotipo, $cover)
    {
        try {
            $dataStore = [
                'name'          => $name,
                'slug'          => mb_url_title($name, '-', TRUE),
                'description'   => $description,
                'location'      => $location,
                'telephone'     => $phone,
                'horario'       => $hourhand,
                'email'         => $email,
                'url'           => $url,
                'short_url'     => $shorturl,
                'keywords'      => $keywords,
                'updated_at'    => date('Y-m-d H:i:s'),
            ];
            $this->db = db_connect('angelopolisDB');
            $this->db->transBegin();

            // 1. Operaciones tabla stores
            $this->db->table('stores')->where('id', $id)->update($dataStore);
            // 2. Operaciones tabla pivote categoría
            // Update: Antea 2021 solo acepta una categoría por tienda
            $dataCategory = [
                'category_id' => $category,
                'stores_id' => $id
            ];
            $this->db->table('categories_has_stores')->where('stores_id', $id)->update($dataCategory);

            // 3. Determinar si hay actualización de imagenes
            $hasNewLogotipo = $logotipo->getError() !== 4;
            $hasNewCover = $cover->getError() !== 4;

            if ($hasNewLogotipo || $hasNewCover) {
                // Abrir conexiòn FTP
                $connectionFTP = ftp_connect('70.32.74.94');
                if (@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                    // activar modo pasivo FTP
                    ftp_pasv($connectionFTP, true);
                    // Verificar si existe un nuevo logotipo a reemplazar
                    if ($hasNewLogotipo) {
                        if ($logotipo->isValid() && !$logotipo->hasMoved()) {
                            $fileNameLogo = mb_url_title($name, '-', TRUE) . '_' . time() . '_logo' . '.jpg';
                            if (ftp_put($connectionFTP, '/angelopolispuebla.com.mx/assets/images/stores/' . $fileNameLogo, $logotipo, FTP_BINARY)) {
                                $this->db->table('stores')->where('id', $id)->update([
                                    'image' => 'assets/images/stores/' . $fileNameLogo
                                ]);
                            } else {
                                ftp_close($connectionFTP);
                                $this->db->transRollback();
                                throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Antea, favor de intentar más tarde.", 3);
                            }
                        } else {
                            ftp_close($connectionFTP);
                            $this->db->transRollback();
                            return false;
                        }
                    }

                    // Verificar si existe un nuevo cover de tienda a actualizar
                    if ($hasNewCover) {
                        if ($cover->isValid() && !$cover->hasMoved()) {
                            $fileNameCover = mb_url_title($name, '-', TRUE) . '_' . time() . '_cover' . '.jpg';
                            // Borrar todo el contenido del directorio (Solo acepta una imagen por tienda)
                            if (@ftp_chdir($connectionFTP, '/angelopolispuebla.com.mx/assets/images/microsites/' . $id)) {
                                $currentFiles = ftp_nlist($connectionFTP, ".");
                                foreach ($currentFiles as $file) {
                                    // Si no se puede eliminar algun archivo cancelamos la operación.
                                    if(!ftp_delete($connectionFTP, $file)) {
                                        ftp_close($connectionFTP);
                                        $this->db->transRollback();
                                        throw new \Exception("Error al tratar de eliminar los recursos de imagen anteriores en el servidor FTP del proyecto Antea, favor de intentar más tarde.", 3);
                                    }
                                }
                            } else {
                                ftp_close($connectionFTP);
                                $this->db->transRollback();
                                throw new \Exception("Error al tratar de localizar el directorio de imagenes en el servidor FTP del proyecto Antea, favor de intentar más tarde.", 3);
                            }
                            // Proceder a almacenar el nuevo archivo de portada
                            if (!ftp_put($connectionFTP, '/angelopolispuebla.com.mx/assets/images/microsites/' . $id . '/' . $fileNameCover, $cover, FTP_BINARY)) {
                                ftp_close($connectionFTP);
                                $this->db->transRollback();
                                throw new \Exception("Error al tratar de subir el recurso de imagen en el servidor FTP del proyecto Antea, favor de intentar más tarde.", 3);
                            }
                        } else {
                            ftp_close($connectionFTP);
                            $this->db->transRollback();
                            return false;
                        }
                    }

                    // Confirmar los cambios en base de datos (actualización logotipo)
                    if ($this->db->transStatus() !== FALSE) {
                        ftp_close($connectionFTP);
                        $this->db->transCommit();
                        return true;
                    } else {
                        ftp_close($connectionFTP);
                        $this->db->transRollback();
                        return false;
                    }
                } else {
                    ftp_close($connectionFTP);
                    $this->db->transRollback();
                    throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Antea, favor de intentar más tarde.", 3);
                }
            } else {
                // Proceder a guardar los cambios en base de datos (En este punto no se envío actualización de imagenes)
                if ($this->db->transStatus() !== FALSE) {
                    $this->db->transCommit();
                    return true;
                } else {
                    $this->db->transRollback();
                    return false;
                }
            }
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            // Excepciones personalizadas en el código: code = 3
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function getImageStore($id)
    {
        try {
            $connectionFTP = ftp_connect('70.32.74.94');
            if(@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                // activar modo pasivo FTP
                ftp_pasv($connectionFTP, true);
                if(@ftp_chdir($connectionFTP, '/angelopolispuebla.com.mx/assets/images/microsites/' . $id . '/')) {
                    $content = ftp_nlist($connectionFTP, '.');
                    if ($content) {
                        foreach ($content as $file) {
                            $info = new \SplFileInfo($file);
                            // Deolver el primer archivo jpg que encuentre.
                            // En teoría:
                            //      1. Debe existir solo un archivo de imagen por tienda.
                            //      2. El formato de imagen dbería ser JPG
                            if (strtolower($info->getExtension()) == 'jpg' || strtolower($info->getExtension()) == 'png') {
                                ftp_close($connectionFTP);
                                return $file;
                            }

                        }

                    }
                    // Verificar si es necesario retornar un recurso de imagen por defecto
                } else {
                    ftp_close($connectionFTP);
                    throw new \Exception("Error al tratar de localizar el directorio de imagenes en el servidor FTP del proyecto Antea, favor de intentar más tarde.", 3);
                }
            } else {
                ftp_close($connectionFTP);
                throw new \Exception("Error de conexión temporal al servidor FTP del proyecto The Harbor Mérida, favor de intentar más tarde.", 3);
            }
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            // Excepciones personalizadas en el código: code = 3
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function createStore($name, $description, $email, $url, $shorturl, $phone, $hourhand, $keywords, $location, $category, $logotipo, $cover)
    {
        try {
            $dataStore = [
                'name'          => $name,
                'slug'          => mb_url_title($name, '-', TRUE),
                'description'   => $description,
                'location'      => $location,
                'telephone'     => $phone,
                'horario'       => $hourhand,
                'email'         => $email,
                'url'           => $url,
                'short_url'     => $shorturl,
                'keywords'      => $keywords,
                'created_at'    => date('Y-m-d H:i:s'),
            ];

            $this->db = db_connect('angelopolisDB');
            $this->db->transBegin();

            // 1. Operaciones tabla stores
            $this->db->table('stores')->insert($dataStore);
            $storeID = $this->db->insertID();
            // 2. Operaciones tabla pivote categoría
            $dataCategory = [
                'category_id' => $category,
                'stores_id' => $storeID
            ];
            $this->db->table('categories_has_stores')->insert($dataCategory);

            // 3. Conexión FTP
            $connectionFTP = ftp_connect('70.32.74.94');
            $successUploadLogo = false;
            $successUploadCover = false;
            if (@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                // 4. Subir archivos al servidor FTP
                // activar modo pasivo FTP
                ftp_pasv($connectionFTP, true);
                // Logotipo de tienda
                if ($logotipo->isValid() && !$logotipo->hasMoved()) {
                    $fileNameLogo = mb_url_title($name, '-', TRUE) . '_' . time() . '_logo' . '.jpg';
                    if (ftp_put($connectionFTP, '/angelopolispuebla.com.mx/assets/images/stores/' . $fileNameLogo, $logotipo, FTP_BINARY)) {
                        $this->db->table('stores')->where('id', $storeID)->update([
                            'image' => 'assets/images/stores/' . $fileNameLogo
                        ]);
                        $successUploadLogo = true;
                    } else {
                        ftp_close($connectionFTP);
                        $this->db->transRollback();
                        throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Antea, favor de intentar más tarde.", 3);
                    }
                } else {
                    ftp_close($connectionFTP);
                    $this->db->transRollback();
                    return false;
                }
                // Imagen de Portada
                if ($cover->isValid() && !$cover->hasMoved()) {
                    $fileNameCover = mb_url_title($name, '-', TRUE) . '_' . time() . '_cover' . '.jpg';
                    // Crear nuevo directorio para archivo de portada
                    if (ftp_mkdir($connectionFTP, '/angelopolispuebla.com.mx/assets/images/microsites/' . $storeID)) {
                        if (ftp_put($connectionFTP, '/angelopolispuebla.com.mx/assets/images/microsites/' . $storeID . '/' . $fileNameCover, $cover, FTP_BINARY)) {
                            $successUploadCover = true;
                        } else {
                            ftp_close($connectionFTP);
                            $this->db->transRollback();
                            throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Antea, favor de intentar más tarde.", 3);
                        }
                    } else {
                        ftp_close($connectionFTP);
                        $this->db->transRollback();
                        throw new \Exception("Error al tratar de crear el directorio de imagenes en el servidor FTP del proyecto Antea, favor de intentar más tarde.", 3);
                    }
                } else {
                    ftp_close($connectionFTP);
                    $this->db->transRollback();
                    return false;
                }

                // Confirmar registro de información en base de datos
                if ($successUploadLogo && $successUploadCover) {
                    if ($this->db->transStatus() !== FALSE) {
                        ftp_close($connectionFTP);
                        $this->db->transCommit();
                        return true;
                    } else {
                        ftp_close($connectionFTP);
                        $this->db->transRollback();
                        return false;
                    }
                } else {
                    ftp_close($connectionFTP);
                    $this->db->transRollback();
                    return false;
                }
            } else {
                ftp_close($connectionFTP);
                $this->db->transRollback();
                throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Antea, favor de intentar más tarde.", 3);
            }
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            // Excepciones personalizadas en el código: code = 3
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function removeStore($id)
    {
        try {
            $data = ['deleted_at' => 1];

            $this->db   = db_connect('angelopolisDB');
            $builder    = $this->db->table('stores');
            $query      = $builder->where('id', $id)->update($data);
            // Update: remover registros pivote (categoría por tienda) y directorio FTP Cover y Logotipo
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

}