<?php
namespace App\Models\GSM\Angelopolis;

use CodeIgniter\Model;
use App\Libraries\S3;

class SlidesModel extends Model
{

    public function __construct() { }

    public function getAllSlides()
    {
        try {
            $this->db = db_connect('angelopolisDB');
            $builder = $this->db->table('slides');
            // TODO: Aplicar esta consulta a los demás sitios
            $query = $builder->groupStart()
                ->where('deleted_at IS NULL')
                ->where('end_date IS NULL')
                ->groupEnd()
                ->orWhere('end_date >=', date('Y-m-d H:i:s'))
                ->where('deleted_at IS  NULL')
                ->orderBy('created_at', 'desc')->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function getSlide($id)
    {
        try {
            $this->db = db_connect('angelopolisDB');
            $builder = $this->db->table('slides');
            $query = $builder->where('id', $id)->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function createSlide($title, $end_date, $image)
    {
        try {
            if ($image->isValid() && !$image->hasMoved()) {
                $connectionFTP = ftp_connect('70.32.74.94');

                if (@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                    // activar modo pasivo FTP
                    ftp_pasv($connectionFTP, true);
                    // $fileName = $image->getRandomName();
                    $fileName = mb_url_title($title, '-', TRUE) . '_' . time() . '.jpg';

                    if (ftp_put($connectionFTP, '/angelopolispuebla.com.mx/assets/images/slides/' . $fileName, $image, FTP_BINARY)) {
                        $data = [
                            'title'      => $title,
                            'position'   => random_int(10, 50),
                            'image'      => 'assets/images/slides/' . $fileName,
                            'end_date'   => $end_date ?: NULL,
                            'created_at' => date('Y-m-d H:i:s'),
                        ];

                        $this->db   = db_connect('angelopolisDB');
                        $builder    = $this->db->table('slides');
                        $query      = $builder->insert($data);

                        ftp_close($connectionFTP);
                        return $query;
                    } else {
                        ftp_close($connectionFTP);
                        throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Antea, favor de intentar más tarde.", 3);
                    }
                } else {
                    ftp_close($connectionFTP);
                    throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Antea, favor de intentar más tarde.", 3);
                }
            }
            return false;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            // Excepciones personalizadas en el código: code = 3
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            //throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function editSlide($id, $title, $end_date, $image)
    {
        try {
            $data = [
                'title'      => $title,
                'end_date'   => $end_date ?: NULL,
                'updated_at' => date('Y-m-d H:i:s'),
            ];

            $this->db   = db_connect('angelopolisDB');
            $builder    = $this->db->table('slides');

            if ($image->getError() === 4) {
                $query = $builder->where('id', $id)->update($data);
                return $query;
            } else {
                if ($image->isValid() && !$image->hasMoved()) {
                    $connectionFTP = ftp_connect('70.32.74.94');

                    if (@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                        // activar modo pasivo FTP
                        ftp_pasv($connectionFTP, true);
                        // $fileName = $image->getRandomName();
                        $fileName = mb_url_title($title, '-', TRUE) . '_update_' . time() . '.jpg';

                        if (ftp_put($connectionFTP, '/angelopolispuebla.com.mx/assets/images/slides/' . $fileName, $image, FTP_BINARY)) {
                            $data['image'] = 'assets/images/slides/' . $fileName;
                            $query         = $builder->where('id', $id)->update($data);

                            ftp_close($connectionFTP);
                            return $query;
                        } else {
                            ftp_close($connectionFTP);
                            throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Antea, favor de intentar más tarde.", 3);
                        }
                    } else {
                        ftp_close($connectionFTP);
                        throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Antea, favor de intentar más tarde.", 3);
                    }
                }
                return false;
            }
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            // Excepciones personalizadas
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            //throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function removeSlide($id)
    {
        try {
            $data = ['deleted_at' => 1];

            $this->db   = db_connect('angelopolisDB');
            $builder    = $this->db->table('slides');
            $query      = $builder->where('id', $id)->update($data);

            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function getSlidesOrderByPosition()
    {
        try {
            $this->db = db_connect('angelopolisDB');
            $builder = $this->db->table('slides');
            $query = $builder->where('deleted_at', null)
                ->where('end_date', null)
                ->orWhere('end_date >=', date('Y-m-d H:i:s'))->orderBy('position')->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function updateSlidesPosition($slideList)
    {
        try {
            // Generar un arreglo de ID's correspondientes a los slides ordenados en el frontend
            // El orden de los ID's se asocia con la nueva posición que ocupan los slides
            $slideList = explode('|', $slideList);
            $data = [];

            // Asociar el ID del slide con la nueva posición dentro del carrusel
            foreach($slideList as $index => $slide) {
                $data[] = ['id' => $slide, 'position' => $index + 1];
            }

            // Actualizar la nueva posición de los slides en base de datos
            $this->db   = db_connect('angelopolisDB');
            $builder    = $this->db->table('slides');
            $query      = $builder->updateBatch($data, 'id');

            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

}
