<?php
namespace App\Models\GSM\Antea;

use CodeIgniter\Model;

class StoresModel extends Model
{

    public function __construct() { }

    public function getAllStores()
    {
        try {
            $this->db = db_connect('anteaDB');
            $builder = $this->db->table('stores');
            $query = $builder->where('deleted_at', null)->orderBy('name', 'desc')->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function getStore($id)
    {
        try {
            $this->db = db_connect('anteaDB');
            $builder = $this->db->table('stores');
            $query = $builder->where('id', $id)->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function getLevels()
    {
        try {
            $this->db = db_connect('anteaDB');
            $builder = $this->db->table('levels');
            $query = $builder->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function getCategories()
    {
        try {
            $this->db = db_connect('anteaDB');
            $builder = $this->db->table('categories');
            $query = $builder->where('enabled', 1)->orderBy('name')->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function getCategoryStore($id)
    {
        try {
            $this->db = db_connect('anteaDB');
            $builder = $this->db->table('categories_has_stores');
            $query = $builder->where('stores_id', $id)->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function editStore($id, $name, $description, $email, $url, $shorturl, $phone, $hourhand, $keywords, $location, $category, $logotipo, $cover)
    {
        try {
            $dataStore = [
                'name'          => $name,
                'slug'          => mb_url_title($name, '-', TRUE),
                'description'   => $description,
                'location'      => $location,
                'telephone'     => $phone,
                'horario'       => $hourhand,
                'email'         => $email,
                'url'           => $url,
                'short_url'     => $shorturl,
                'keywords'      => $keywords,
                'updated_at'    => date('Y-m-d H:i:s'),
            ];
            $this->db = db_connect('anteaDB');
            $this->db->transBegin();

            // 1. Operaciones tabla stores
            $this->db->table('stores')->where('id', $id)->update($dataStore);
            // 2. Operaciones tabla pivote categoría
            // Update: Antea 2021 solo acepta una categoría por tienda
            $dataCategory = [
                'category_id' => $category,
                'stores_id' => $id
            ];
            $this->db->table('categories_has_stores')->where('stores_id', $id)->update($dataCategory);
            
            // 3. Determinar si hay actualización de imagenes
            $hasNewLogotipo = $logotipo->getError() !== 4;
            $hasNewCover = $cover->getError() !== 4;

            if ($hasNewLogotipo || $hasNewCover) {
                // Abrir conexiòn FTP
                $connectionFTP = ftp_connect('208.109.32.101');
                if (@ftp_login($connectionFTP, 'developer_antea', '!Qc51s_yp_S')) {
                    // activar modo pasivo FTP
                    ftp_pasv($connectionFTP, true);
                    // Verificar si existe un nuevo logotipo a reemplazar
                    if ($hasNewLogotipo) {
                        if ($logotipo->isValid() && !$logotipo->hasMoved()) {
                            $fileNameLogo = mb_url_title($name, '-', TRUE) . '_' . time() . '_logo' . '.jpg';
                            if (ftp_put($connectionFTP, '/httpdocs/assets/images/stores/' . $fileNameLogo, $logotipo, FTP_BINARY)) {
                                $this->db->table('stores')->where('id', $id)->update([
                                    'image' => 'assets/images/stores/' . $fileNameLogo
                                ]);
                            } else {
                                ftp_close($connectionFTP);
                                $this->db->transRollback();
                                throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Antea, favor de intentar más tarde.", 3);
                            }
                        } else {
                            ftp_close($connectionFTP);
                            $this->db->transRollback();
                            return false;
                        }
                    }

                    // Verificar si existe un nuevo cover de tienda a actualizar
                    if ($hasNewCover) {
                        if ($cover->isValid() && !$cover->hasMoved()) {
                            $fileNameCover = mb_url_title($name, '-', TRUE) . '_' . time() . '_cover' . '.jpg';
                            // Borrar todo el contenido del directorio (Solo acepta una imagen por tienda)
                            if (@ftp_chdir($connectionFTP, '/httpdocs/assets/images/microsites/' . $id)) {
                                $currentFiles = ftp_nlist($connectionFTP, ".");
                                foreach ($currentFiles as $file) {
                                    // Si no se puede eliminar algun archivo cancelamos la operación.
                                    if(!ftp_delete($connectionFTP, $file)) {
                                        ftp_close($connectionFTP);
                                        $this->db->transRollback();
                                        throw new \Exception("Error al tratar de eliminar los recursos de imagen anteriores en el servidor FTP del proyecto Antea, favor de intentar más tarde.", 3);
                                    }
                                }
                            } else {
                                ftp_close($connectionFTP);
                                $this->db->transRollback();
                                throw new \Exception("Error al tratar de localizar el directorio de imagenes en el servidor FTP del proyecto Antea, favor de intentar más tarde.", 3);
                            }
                            // Proceder a almacenar el nuevo archivo de portada
                            if (!ftp_put($connectionFTP, '/httpdocs/assets/images/microsites/' . $id . '/' . $fileNameCover, $cover, FTP_BINARY)) {
                                ftp_close($connectionFTP);
                                $this->db->transRollback();
                                throw new \Exception("Error al tratar de subir el recurso de imagen en el servidor FTP del proyecto Antea, favor de intentar más tarde.", 3);
                            }
                        } else {
                            ftp_close($connectionFTP);
                            $this->db->transRollback();
                            return false;
                        }
                    }

                    // Confirmar los cambios en base de datos (actualización logotipo)
                    if ($this->db->transStatus() !== FALSE) {
                        ftp_close($connectionFTP);
                        $this->db->transCommit();
                        return true;
                    } else {
                        ftp_close($connectionFTP);
                        $this->db->transRollback();
                        return false;
                    }
                } else {
                    ftp_close($connectionFTP);
                    $this->db->transRollback();
                    throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Antea, favor de intentar más tarde.", 3);
                }            
            } else {
                // Proceder a guardar los cambios en base de datos (En este punto no se envío actualización de imagenes)
                if ($this->db->transStatus() !== FALSE) {
                    $this->db->transCommit();
                    return true;
                } else {
                    $this->db->transRollback();
                    return false;
                }
            }
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            // Excepciones personalizadas en el código: code = 3
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function getImageStore($id)
    {
        try {
            $connectionFTP = ftp_connect('208.109.32.101');
            if(@ftp_login($connectionFTP, 'developer_antea', '!Qc51s_yp_S')) {
                // activar modo pasivo FTP
                ftp_pasv($connectionFTP, true);
                if(@ftp_chdir($connectionFTP, '/httpdocs/assets/images/microsites/' . $id . '/')) {
                    $content = ftp_nlist($connectionFTP, '.');
                    if ($content) {
                        foreach ($content as $file) {
                            $info = new \SplFileInfo($file);
                            // Deolver el primer archivo jpg que encuentre.
                            // En teoría: 
                            //      1. Debe existir solo un archivo de imagen por tienda.
                            //      2. El formato de imagen dbería ser JPG
                            if (strtolower($info->getExtension()) == 'jpg' || strtolower($info->getExtension()) == 'png') {
                                ftp_close($connectionFTP);
                                return $file;
                            }
                            
                        }
                        
                    }
                    // Verificar si es necesario retornar un recurso de imagen por defecto
                } else {
                    ftp_close($connectionFTP);
                    throw new \Exception("Error al tratar de localizar el directorio de imagenes en el servidor FTP del proyecto Antea, favor de intentar más tarde.", 3);
                }
            } else {
                ftp_close($connectionFTP);
                throw new \Exception("Error de conexión temporal al servidor FTP del proyecto The Harbor Mérida, favor de intentar más tarde.", 3);
            }
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            // Excepciones personalizadas en el código: code = 3
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function createStore($name, $description, $email, $url, $shorturl, $phone, $hourhand, $keywords, $location, $category, $logotipo, $cover)
    {
        try {
            $dataStore = [
                'name'          => $name,
                'slug'          => mb_url_title($name, '-', TRUE),
                'description'   => $description,
                'location'      => $location,
                'telephone'     => $phone,
                'horario'       => $hourhand,
                'email'         => $email,
                'url'           => $url,
                'short_url'     => $shorturl,
                'keywords'      => $keywords,
                'created_at'    => date('Y-m-d H:i:s'),
            ];

            $this->db = db_connect('anteaDB');
            $this->db->transBegin();

            // 1. Operaciones tabla stores
            $this->db->table('stores')->insert($dataStore);
            $storeID = $this->db->insertID();
            // 2. Operaciones tabla pivote categoría
            $dataCategory = [
                'category_id' => $category,
                'stores_id' => $storeID
            ];
            $this->db->table('categories_has_stores')->insert($dataCategory);
            
            // 3. Conexión FTP
            $connectionFTP = ftp_connect('208.109.32.101');
            $successUploadLogo = false;
            $successUploadCover = false;
            if (@ftp_login($connectionFTP, 'developer_antea', '!Qc51s_yp_S')) {
                // 4. Subir archivos al servidor FTP
                // activar modo pasivo FTP
                ftp_pasv($connectionFTP, true);
                // Logotipo de tienda
                if ($logotipo->isValid() && !$logotipo->hasMoved()) {
                    $fileNameLogo = mb_url_title($name, '-', TRUE) . '_' . time() . '_logo' . '.jpg';
                    if (ftp_put($connectionFTP, '/httpdocs/assets/images/stores/' . $fileNameLogo, $logotipo, FTP_BINARY)) {
                        $this->db->table('stores')->where('id', $storeID)->update([
                            'image' => 'assets/images/stores/' . $fileNameLogo
                        ]);
                        $successUploadLogo = true;
                    } else {
                        ftp_close($connectionFTP);
                        $this->db->transRollback();
                        throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Antea, favor de intentar más tarde.", 3);
                    }
                } else {
                    ftp_close($connectionFTP);
                    $this->db->transRollback();
                    return false;
                }
                // Imagen de Portada
                if ($cover->isValid() && !$cover->hasMoved()) {
                    $fileNameCover = mb_url_title($name, '-', TRUE) . '_' . time() . '_cover' . '.jpg';
                    // Crear nuevo directorio para archivo de portada
                    if (ftp_mkdir($connectionFTP, '/httpdocs/assets/images/microsites/' . $storeID)) {
                        if (ftp_put($connectionFTP, '/httpdocs/assets/images/microsites/' . $storeID . '/' . $fileNameCover, $cover, FTP_BINARY)) {
                            $successUploadCover = true;
                        } else {
                            ftp_close($connectionFTP);
                            $this->db->transRollback();
                            throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Antea, favor de intentar más tarde.", 3);
                        }
                    } else {
                        ftp_close($connectionFTP);
                        $this->db->transRollback();
                        throw new \Exception("Error al tratar de crear el directorio de imagenes en el servidor FTP del proyecto Antea, favor de intentar más tarde.", 3);
                    }
                } else {
                    ftp_close($connectionFTP);
                    $this->db->transRollback();
                    return false;
                }

                // Confirmar registro de información en base de datos
                if ($successUploadLogo && $successUploadCover) {
                    if ($this->db->transStatus() !== FALSE) {
                        ftp_close($connectionFTP);
                        $this->db->transCommit();
                        return true;
                    } else {
                        ftp_close($connectionFTP);
                        $this->db->transRollback();
                        return false;
                    }
                } else {
                    ftp_close($connectionFTP);
                    $this->db->transRollback();
                    return false;
                }
            } else {
                ftp_close($connectionFTP);
                $this->db->transRollback();
                throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Antea, favor de intentar más tarde.", 3);
            }
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            // Excepciones personalizadas en el código: code = 3
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    public function removeStore($id)
    {
        try {
            $data = ['deleted_at' => 1];

            $this->db   = db_connect('anteaDB');
            $builder    = $this->db->table('stores');
            $query      = $builder->where('id', $id)->update($data);
            // Update: remover registros pivote (categoría por tienda) y directorio FTP Cover y Logotipo
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            //throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        } catch (\Exception $e) {
            //throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
            throw new \Exception($e->getMessage(), $e->getCode(), $e);
        }
    }

    // Establecer las nuevas coordenadas de tienda en Mapa (App)
    public function editMap($id, $positions)
    {
        try {
            $data = [
                'pos_x' => $positions[0],
                'pos_y' => $positions[1],
            ];

            $this->db   = db_connect('angelopolisDB');
            $builder    = $this->db->table('stores');
            $query      = $builder->where('id', $id)->update($data);

            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Angelópolis, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    // Establecer el nuevo ID de tienda en el área de mapa correspondiente (Web)
    public function writeMap($newStore, $oldStore, $coords, $location)
    {
        try {
            $connectionFTP = ftp_connect('70.32.74.94');
            if (@ftp_login($connectionFTP, 'developer_antea', '!Qc51s_yp_S')) {
                // Variables de ayuda para actualizar el archivo con los datos más recientes
                $nuevo_contenido = [];      // Todo el contenido nuevo a actualizar en el archivo
                $anterior_contenido = [];   // Todo el contenido anterior a reemplazar en el archivo
                $newAreaMap;
                $oldAreaMap;
                
                // Convertir el contenido del archivo temporal de mapa a un arreglo. 
                // La intensión es ubicar las lineas de contenido a actualizar (áreas de mapa)
                $contenido_mapa = file('assets/temp/angelopolisMap-'. $location .'.txt');
                foreach ($contenido_mapa as $line_number => $line) {
                    // Si las coordenadas coinciden, esta es el área a actualizar con los nuevos datos de tienda
                    if (strpos($line, $coords) !== false) {
                        // Almacenar la linea de contenido actual
                        $oldAreaMap = $line;
                        // Reemplazar el contenido de esta linea, por el mas reciente (nombre de tienda)
                        $newAreaMap = str_replace($oldStore, $newStore, $line);
                        // Guardar los cambios realizados en esta linea dentro de un arreglo temporal
                        // No se tiene acceso directo al recurso o archivo
                        array_push($nuevo_contenido, $newAreaMap);
                        array_push($anterior_contenido, $oldAreaMap);
                    } else {
                        // Si existe más de un área con el mismo id de tienda, pero con diferentes coordenadas 
                        //      significa una actualización de tienda.
                        //      Por tanto, se recomienda dejar disponible el área actual con la leyenda Próximante (En base de datos es el ID 198)
                        if (strpos($line, $newStore) !== false) {
                            $oldAreaMap = $line;
                            $newAreaMap = str_replace($newStore, 'mapaID-198', $line);
                            array_push($nuevo_contenido, $newAreaMap);
                            array_push($anterior_contenido, $oldAreaMap);
                        }
                    }
                }
                    
                // Leer el contenido del árchivo temporal de mapa
                if (($oldMapa = file_get_contents('assets/temp/angelopolisMap-'. $location .'.txt')) === false) {
                    ftp_close($connectionFTP);
                    throw new \Exception("Error al tratar de leer el recurso de mapa descargado, favor de intentar más tarde.", 3);
                }
                // Reemplazar todo el contenido anterior por el más reciente.
                $newMapa = str_replace($anterior_contenido, $nuevo_contenido, $oldMapa);
                // Guardar los cambios en el archivo temporal
                if(file_put_contents('assets/temp/angelopolisMap-'. $location .'.txt', $newMapa) === false) {
                    ftp_close($connectionFTP);
                    throw new \Exception("Error al tratar de actualizar el nuevo recurso de mapa, favor de intentar más tarde.", 3);
                }

                // Moverse al directorio de Vistas del proyecto Web y subir el nuevo recurso de mapa actualizado
                if (@ftp_chdir($connectionFTP, '/angelopolispuebla.com.mx/application/views/')) {
                    if (ftp_put($connectionFTP, 'mapa-'. $location .'.php', 'assets/temp/angelopolisMap-'. $location .'.txt', FTP_BINARY)) {
                        ftp_close($connectionFTP);
                        return true;
                    } else {
                        ftp_close($connectionFTP);
                        throw new \Exception("Error al tratar de subir el nuevo recurso de mapa en el servidor FTP del proyecto Angelópolis, favor de intentar más tarde.", 3);
                    }
                } else {
                    ftp_close($connectionFTP);
                    throw new \Exception("Error al tratar de localizar el directorio de mapa en el servidor FTP del proyecto Angelópolis, favor de intentar más tarde.", 3);
                }
            } else {
                ftp_close($connectionFTP);
                throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Angelópolis, favor de intentar más tarde.", 3);
            }
            ftp_close($connectionFTP);
            return false;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Angelópolis, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            // Excepciones personalizadas en el código: code = 3
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
        }
    }

    // Descargar una copia completa del archivo de mapa en producción (Web)
    public function mapDownload($level) {
        try {
            $connectionFTP = ftp_connect('208.109.32.101');
            if (@ftp_login($connectionFTP, 'developer_antea', '!Qc51s_yp_S')) {
                /* Obtener una copia del archivo de vista referente al directorio/mapa publicado en el sitio Web
                    El objetivo es trabajar sobre un archivo local, modificarlo, y publicarlo nuevamente en el sitio Web
                */ 
                if (ftp_get($connectionFTP, 'assets/temp/andamarMap-' . $level . '.png', '/httpdocs/assets/images/mapa/' . $level . '.png', FTP_BINARY)) {
                    ftp_close($connectionFTP);
                    return true;
                } else {
                    ftp_close($connectionFTP);
                    throw new \Exception("Error al tratar de descargar el recurso de mapa desde el servidor FTP del proyecto Antea, favor de intentar más tarde.", 3);
                }
            } else {
                ftp_close($connectionFTP);
                throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Antea, favor de intentar más tarde.", 3);
            }
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Antea, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            // Excepciones personalizadas en el código: code = 3
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
        }
    }

}
