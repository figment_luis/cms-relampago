<?php
namespace App\Models\Thorurbana\Marina;

use CodeIgniter\Model;

class NewsModel extends Model
{
    public function __construct()
    {
        //helper('cms');
    }

    public function getNews($id)
    {
        try {
            $this->db = db_connect('marinaDB');
            $builder = $this->db->table('posts');
            $query = $builder->where('id', $id)->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Marina Puerto Cancún, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function getAllNews()
    {
        try {
            $this->db = db_connect('marinaDB');
            $builder = $this->db->table('posts');
            $query = $builder->where('deleted_at !=', 1)->orderBy('created_at', 'desc')->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Marina Puerto Cancún, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function createNews($title, $link, $image, $description)
    {
        try {
            if ($image->isValid() && !$image->hasMoved()) {
                $connectionFTP = ftp_connect('70.32.104.172');

                if (@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                    $fileName = mb_url_title($title, '-', TRUE) . '_' . time() . '.jpg';

                    if (ftp_put($connectionFTP, '/httpdocs/assets/images/posts/' . $fileName, $image, FTP_BINARY)) {
                        setlocale(LC_TIME, 'es_ES.UTF-8');
                        $data = [
                            'title'       => $title,
                            'image'       => 'assets/images/posts/' . $fileName,
                            'link'        => $link,
                            'description' => $description,
                            'type'        => 1,
                            'date'        => ucfirst(strftime('%B %Y')),
                            'created_at'  => date('Y-m-d H:i:s'),
                            'deleted_at'  => 0
                        ];

                        $this->db   = db_connect('marinaDB');
                        $builder    = $this->db->table('posts');
                        $query      = $builder->insert($data);

                        ftp_close($connectionFTP);
                        return $query;
                    } else {
                        ftp_close($connectionFTP);
                        throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Marina Puerto Cancún, favor de intentar más tarde.", 3);
                    }
                } else {
                    ftp_close($connectionFTP);
                    throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Marina Puerto Cancún, favor de intentar más tarde.", 3);
                }
            }
            return false;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Marina Puerto Cancún, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            // Excepciones personalizadas en el código: code = 3
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function editNews($id, $title, $link, $image, $description)
    {
        try {
            $data = [
                'title'         => $title,
                'link'          => $link,
                'description'   => $description,
                'updated_at'    => date('Y-m-d H:i:s'),
            ];

            $this->db   = db_connect('marinaDB');
            $builder    = $this->db->table('posts');

            if ($image->getError() === 4) {
                $query = $builder->where('id', $id)->update($data);
                return $query;
            } else {
                if ($image->isValid() && !$image->hasMoved()) {
                    $connectionFTP = ftp_connect('70.32.104.172');
        
                    if (@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                        $fileName = mb_url_title($title, '-', TRUE) . '_' . time() . '.jpg';
        
                        if (ftp_put($connectionFTP, '/httpdocs/assets/images/posts/' . $fileName, $image, FTP_BINARY)) {
                            $data['image'] = 'assets/images/posts/' . $fileName;
                            $query         = $builder->where('id', $id)->update($data);
        
                            ftp_close($connectionFTP);
                            return $query;
                        } else {
                            ftp_close($connectionFTP);
                            throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Marina Puerto Cancún, favor de intentar más tarde.", 3);
                        }
                    } else {
                        ftp_close($connectionFTP);
                        throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Marina Puerto Cancún, favor de intentar más tarde.", 3);
                    }
                }
                return false;
            }
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Marina Puerto Cancún, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            // Excepciones personalizadas
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function removeNews($id)
    {
        try {
            $data = ['deleted_at' => 1];

            $this->db = db_connect('marinaDB');
            $builder = $this->db->table('posts');
            $query = $builder->where('id', $id)->update($data);

            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Marina Puerto Cancún, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }
}
