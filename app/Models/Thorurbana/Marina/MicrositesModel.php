<?php
namespace App\Models\Thorurbana\Marina;

use CodeIgniter\Model;

class MicrositesModel extends Model
{
    public function __construct()
    {
        helper('cms');
        $this->months = ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'];
    }

    public function getMicrosite($id)
    {
        try {
            $this->db = db_connect('marinaDB');
            $builder = $this->db->table('landings');
            $query = $builder->where('id', $id)->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Marina Puerto Cancún, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function getAllMicrosites()
    {
        try {
            $this->db = db_connect('marinaDB');
            $builder = $this->db->table('landings');
            $query = $builder->orderBy('created_at', 'desc')->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Marina Puerto Cancún, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function createMicrosite($title, $slug, $start_date, $end_date, $image)
    {
        try {
            if ($image->isValid() && !$image->hasMoved()) {
                $connectionFTP = ftp_connect('70.32.104.172');

                if (@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                    // $fileName = $image->getRandomName();
                    $fileName = mb_url_title($title, '-', TRUE) . '_' . time() . '.jpg';
                    $now = getdate();
                    $subdirectories = [$now['year'], $this->months[$now['mon'] - 1]];
                    $path = '/httpdocs/assets/landings/';
                    foreach ($subdirectories as $dir) {
                        if (!in_array($path . $dir, ftp_nlist($connectionFTP, $path))) {
                            if (!ftp_mkdir($connectionFTP, $path . $dir . '/')) {
                                ftp_close($connectionFTP);
                                throw new \Exception("Error al tratar de crear el directorio para el recurso de imagen, favor de intentar más tarde.", 3);
                            }
                        }
                        $path .= $dir .'/';
                    }

                    if (ftp_put($connectionFTP, $path . $fileName, $image, FTP_BINARY)) {
                        $data = [
                            'title'         => $title,
                            'slug'          => mb_url_title($slug, '-', TRUE),
                            'content'       => 'assets/landings/' . $subdirectories[0] . '/' . $subdirectories[1] . '/' . $fileName,
                            'start_date'    => $start_date,
                            'end_date'      => $end_date,
                            'enabled'       => 1,
                            'created_at'    => date('Y-m-d H:i:s'),
                        ];

                        $this->db   = db_connect('marinaDB');
                        $builder    = $this->db->table('landings');
                        $query      = $builder->insert($data);

                        ftp_close($connectionFTP);
                        return $query;
                    } else {
                        ftp_close($connectionFTP);
                        throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Marina Puerto Cancún, favor de intentar más tarde.", 3);
                    }
                } else {
                    ftp_close($connectionFTP);
                    throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Marina Puerto Cancún, favor de intentar más tarde.", 3);
                }
            }
            return false;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Marina Puerto Cancún, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            // Excepciones personalizadas en el código: code = 3
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function editMicrosite($id, $title, $slug, $start_date, $end_date, $enabled, $image)
    {
        try {
            $data = [
                'title'         => $title,
                'slug'          => mb_url_title($slug, '-', TRUE),
                'start_date'    => $start_date,
                'end_date'      => $end_date,
                'enabled'       => $enabled ? 1 : 0,
                'updated_at'    => date('Y-m-d H:i:s'),
            ];

            $this->db   = db_connect('marinaDB');
            $builder    = $this->db->table('landings');

            if ($image->getError() === 4) {
                $query = $builder->where('id', $id)->update($data);
                return $query;
            } else {
                if ($image->isValid() && !$image->hasMoved()) {
                    $connectionFTP = ftp_connect('70.32.104.172');
        
                    if (@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                        $fileName = mb_url_title($title, '-', TRUE) . '_update_' . time() . '.jpg';
                        $now = getdate();
                        $subdirectories = [$now['year'], $this->months[$now['mon'] - 1]];
                        $path = '/httpdocs/assets/landings/';
                        foreach ($subdirectories as $dir) {
                            if (!in_array($path . $dir, ftp_nlist($connectionFTP, $path))) {
                                if (!ftp_mkdir($connectionFTP, $path . $dir . '/')) {
                                    ftp_close($connectionFTP);
                                    throw new \Exception("Error al tratar de crear el directorio para el recurso de imagen, favor de intentar más tarde.", 3);
                                }
                            }
                            $path .= $dir .'/';
                        }
        
                        if (ftp_put($connectionFTP, $path . $fileName, $image, FTP_BINARY)) {
                            $data['content'] = 'assets/landings/' . $subdirectories[0] . '/' . $subdirectories[1] . '/' . $fileName;
                            $query       = $builder->where('id', $id)->update($data);
        
                            ftp_close($connectionFTP);
                            return $query;
                        } else {
                            ftp_close($connectionFTP);
                            throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Marina Puerto Cancún, favor de intentar más tarde.", 3);
                        }
                    } else {
                        ftp_close($connectionFTP);
                        throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Marina Puerto Cancún, favor de intentar más tarde.", 3);
                    }
                }
                return false;
            }
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Marina Puerto Cancún, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            // Excepciones personalizadas en el código: code = 3
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function removeMicrosite($id)
    {
        try {
            $data = ['enabled' => 0];

            $this->db = db_connect('marinaDB');
            $builder = $this->db->table('landings');
            $query = $builder->where('id', $id)->update($data);

            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Marina Puerto Cancún, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function activateMicrosite($id)
    {
        $data = [
            'enabled'   => 1,
            'updated_at'  => date('Y-m-d H:i:s'),
        ];

        $this->db = db_connect('marinaDB');
        $builder = $this->db->table('landings');
        $query = $builder->where('id', $id)->update($data);

        return $query;
    }
}
