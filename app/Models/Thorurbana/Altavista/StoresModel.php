<?php
namespace App\Models\Thorurbana\Altavista;

use CodeIgniter\Model;

class StoresModel extends Model
{

    public function __construct()
    {

    }

    public function getAllStores()
    {
        try {
            $this->db = db_connect('altavistaDB');
            $builder = $this->db->table('stores');
            $query = $builder->where('deleted_at', 0)->orderBy('name', 'asc')->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Altavista 147, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function getStore($id)
    {
        try {
            $this->db = db_connect('altavistaDB');
            $builder = $this->db->table('stores');
            $query = $builder->where('id', $id)->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Altavista 147, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function getSubcategoriesByCategoryId($id)
    {
        try {
            $this->db = db_connect('altavistaDB');
            $builder = $this->db->table('subcategories');
            $query = $builder->where('category_id', $id)->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Altavista 147, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function getCategories()
    {
        try {
            $this->db = db_connect('altavistaDB');
            $builder = $this->db->table('categories');
            $query = $builder->where('enabled', 1)->orderBy('name')->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Altavista 147, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function getSubcategories()
    {
        try {
            $this->db = db_connect('altavistaDB');
            $builder = $this->db->table('subcategories');
            $query = $builder->where('enabled', 1)->orderBy('subcat_name')->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Altavista 147, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function getLocations()
    {
        try {
            $this->db = db_connect('altavistaDB');
            $builder = $this->db->table('locations');
            $query = $builder->orderBy('name')->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Altavista 147, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function getCategoryStore($id)
    {
        try {
            $this->db = db_connect('altavistaDB');
            $builder = $this->db->table('categories_has_stores');
            $query = $builder->where('stores_id', $id)->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Altavista 147, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function getSubCategoriesStore($id)
    {
        try {
            $this->db = db_connect('altavistaDB');
            $builder = $this->db->table('subcategories_has_stores');
            $query = $builder->where('stores_id', $id)->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Altavista 147, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function editStore($id, $name, $slug, $description, $email, $url, $thor2go, $phone, $phone2, $hourhand, $keywords, $location, $category, $subcategories, $qrcode)
    {
        try {
            $dataStore = [
                'name'          => $name,
                'description'   => $description,
                'map_location'  => $location,
                'telephone'     => $phone,
                'telephone_2'   => $phone2,
                'hourhand'      => $hourhand,
                'email'         => $email,
                'url'           => $url,
                'keywords'      => $keywords,
                'thor2go'       => $thor2go ? 1 : 0,
                'update_at'     => date('Y-m-d H:i:s'),
            ];
            $this->db = db_connect('altavistaDB');
            $this->db->transBegin();
            // 1. Operaciones tabla stores
            $this->db->table('stores')->where('id', $id)->update($dataStore);
            // 2. Operaciones tabla pivote categoría. Townsquare Metepec, solo acepta una categoría por tienda
            $dataCategory = [
                'category_id' => $category,
                'stores_id' => $id
            ];
            $this->db->table('categories_has_stores')->where('stores_id', $id)->update($dataCategory);

            // Borrar todas las posibles subcategorías asociadas
            $this->db->table('subcategories_has_stores')->where('stores_id', $id)->delete();
            // Un valor de 100 en subcategoría, significa que esta tienda pertenece a Servicios (No hay subcategorías)
            if ($subcategories[0] != 100) {
                // 3. Operaciones tabla pivote subcategorías.
                // Eliminar subcagorías previas
                // Registrar nuevas subcategorías asociadas
                $dataSubcategories = [];
                foreach ($subcategories as $subcategory) {
                    array_push($dataSubcategories, [
                        'subcategory_id' => $subcategory,
                        'stores_id' => $id,
                    ]);
                }
                $this->db->table('subcategories_has_stores')->insertBatch($dataSubcategories);
            }

            // 4. Determinar si hay actualización de imagenes
            if ($qrcode->getError() === 4) {
                // No se envio archivo actualizado de QR para sustituir
                if ($this->db->transStatus() !== FALSE) {
                    $this->db->transCommit();
                    return true;
                }
            } else {
                // 5. Abrir conexión FTP para subir codigo QR
                $connectionFTP = ftp_connect('72.47.208.40');
                if (@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                    if ($qrcode->isValid() && !$qrcode->hasMoved()) {
                        // El archivo QR se sobre-escribe
                        $fileName = $slug . '.png';
                        if (ftp_put($connectionFTP, '/httpdocs/assets/images/qr_codes/' . $fileName, $qrcode, FTP_BINARY)) {
                            // Actualizar registro de tienda, indicar que tiene un código QR
                            $this->db->table('stores')->where('id', $id)->update(['link_qr' => 1]);
                            // 6. Confirmar registro en base de datos
                            if ($this->db->transStatus() !== FALSE) {
                                $this->db->transCommit();
                                return true;
                            } else {
                                ftp_close($connectionFTP);
                                $this->db->transRollback();
                                return false;
                            }
                        } else {
                            ftp_close($connectionFTP);
                            $this->db->transRollback();
                            throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Altavista 147, favor de intentar más tarde.", 3);
                        }
                    } else {
                        ftp_close($connectionFTP);
                        $this->db->transRollback();
                        return false;
                    }
                } else {
                    ftp_close($connectionFTP);
                    $this->db->transRollback();
                    throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Altavista 147, favor de intentar más tarde.", 3);
                }
            }
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Altavista 147, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            // Excepciones personalizadas en el código: code = 3
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function getGalleryStore($id)
    {
        try {
            $connectionFTP = ftp_connect('72.47.208.40');
            if(@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                if(@ftp_chdir($connectionFTP, '/httpdocs/assets/images/stores/' . $id . '/')) {
                    $content = ftp_nlist($connectionFTP, '.');
                    if ($content) {
                        $images = [];
                        foreach ($content as $file) {
                            $info = new \SplFileInfo($file);
                            // Devolver todos los archivos jpg que encuentre.
                            if (strtolower($info->getExtension()) == 'jpg' || strtolower($info->getExtension()) == 'png' || strtolower($info->getExtension()) == 'jpeg') {
                                array_push($images, $file);
                            }
                        }
                        ftp_close($connectionFTP);
                        return $images;
                    }
                    ftp_close($connectionFTP);
                    return null;
                } else {
                    ftp_close($connectionFTP);
                    throw new \Exception("Error al tratar de recuperar las imagenes del servidor FTP del proyecto Altavista 147, favor de intentar más tarde.", 3);
                }
            } else {
                ftp_close($connectionFTP);
                throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Altavista 147, favor de intentar más tarde.", 3);
            }
        } catch (\Exception $e) {
            // Excepciones personalizadas en el código: code = 3
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function removeGallery($id, $image)
    {
        try {
            $connectionFTP = ftp_connect('72.47.208.40');
            if(@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                if(@ftp_chdir($connectionFTP, '/httpdocs/assets/images/stores/' . $id . '/')) {
                    if (ftp_delete($connectionFTP, $image)) {
                        ftp_close($connectionFTP);
                        return true;
                    } else {
                        ftp_close($connectionFTP);
                        return false;
                    }
                } else {
                    ftp_close($connectionFTP);
                    throw new \Exception("Error al tratar de acceder al directorio de imagenes en el servidor FTP del proyecto Altavista 147, favor de intentar más tarde.", 3);
                }
            } else {
                ftp_close($connectionFTP);
                throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Altavista 147, favor de intentar más tarde.", 3);
            }
            ftp_close($connectionFTP);
            return false;
        } catch (\Exception $e) {
            // Excepciones personalizadas en el código: code = 3
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function updateGallery($id, $slug, $images)
    {
        try {
            $connectionFTP = ftp_connect('72.47.208.40');
            if(@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                if(@ftp_chdir($connectionFTP, '/httpdocs/assets/images/stores/' . $id . '/')) {
                    $preview = [];
                    $config = [];
                    foreach($images['images'] as $image) {
                        if ($image->isValid() && !$image->hasMoved()) {
                            $fileName = $slug . '_' . $image->getRandomName();
                            // Intentar subir archivo al servidor FTP
                            if (!ftp_put($connectionFTP, '/httpdocs/assets/images/stores/' . $id . '/' . $fileName, $image, FTP_BINARY)) {
                                ftp_close($connectionFTP);
                                throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Altavista 147, favor de intentar más tarde.", 3);
                            }
                            
                            // Configurar las propiedades del nuevo archivo para enviarlas al frontend
                            $config[] = [
                                'key'       => $fileName,
                                'caption'   => $fileName,
                                'width'     => '70px',
                                'url'       => base_url() . '/altavista/stores/gallery/destroy/'.$id.'/'.$fileName,
                            ];
                            array_push($preview, 'https://www.altavista147.com/assets/images/stores/'.$id.'/'.$fileName);
                        } else {
                            ftp_close($connectionFTP);
                            return false;
                        }
                    }

                    ftp_close($connectionFTP);
                    return ['initialPreview' => $preview, 'initialPreviewConfig' => $config, 'initialPreviewAsData' => true];
                
                } else {
                    ftp_close($connectionFTP);
                    throw new \Exception("Error al tratar de acceder al directorio de imagenes en el servidor FTP del proyecto Altavista 147, favor de intentar más tarde.", 3);
                }
            } else {
                ftp_close($connectionFTP);
                throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Altavista 147, favor de intentar más tarde.", 3);
            }
        } catch (\Exception $e) {
            // Excepciones personalizadas en el código: code = 3
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function createStore($name, $description, $email, $url, $thor2go, $phone, $phone2, $hourhand, $keywords, $location, $category, $subcategories, $qrcode, $galleryFiles)
    {
        try {
            $slug = mb_url_title($name, '-', TRUE);

            $dataStore = [
                'name'          => $name,
                'slug'          => $slug,
                'description'   => $description,
                'map_location'  => $location,
                'telephone'     => $phone,
                'telephone_2'   => $phone2,
                'hourhand'      => $hourhand,
                'email'         => $email,
                'url'           => $url,
                'keywords'      => $keywords,
                'thor2go'       => $thor2go ? 1 : 0,
                'link_qr'       => $qrcode->getError() === 4 ? 0 : 1,
                'created_at'    => date('Y-m-d H:i:s'),
                'deleted_at'    => 0,
            ];

            $this->db = db_connect('altavistaDB');
            $this->db->transBegin();

            // 1. Operaciones tabla stores
            $this->db->table('stores')->insert($dataStore);
            $storeID = $this->db->insertID();
            // 2. Operaciones tabla pivote categoría
            $dataCategory = [
                'category_id' => $category,
                'stores_id' => $storeID
            ];
            $this->db->table('categories_has_stores')->insert($dataCategory);

            // Un valor de 100 en subcategoría, significa que esta tienda pertenece a Servicios (No hay subcategorías)
            if ($subcategories[0] != 100) {
                // 3. Operaciones tabla pivote subcategorías.
                $dataSubcategories = [];
                foreach ($subcategories as $subcategory) {
                    array_push($dataSubcategories, [
                        'subcategory_id' => $subcategory,
                        'stores_id' => $storeID,
                    ]);
                }
                $this->db->table('subcategories_has_stores')->insertBatch($dataSubcategories);
            }

            // 4. Abrir conexión FTP para subir recursos de imagen
            $connectionFTP = ftp_connect('72.47.208.40');
            if (@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {

                // 5. Subir codigo QR si esta disponile
                if ($qrcode->getError() !== 4) {
                    if ($qrcode->isValid() && !$qrcode->hasMoved()) {
                        $fileName = $slug . '.png';
                        if (!ftp_put($connectionFTP, '/httpdocs/assets/images/qr_codes/' . $fileName, $qrcode, FTP_BINARY)) {
                            ftp_close($connectionFTP);
                            $this->db->transRollback();
                            throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Altavista 147, favor de intentar más tarde.", 3);
                        }
                    } else {
                        ftp_close($connectionFTP);
                        $this->db->transRollback();
                        return false;
                    }
                }

                // 6. Subir archivos de galería de tienda
                if (ftp_mkdir($connectionFTP, '/httpdocs/assets/images/stores/' . $storeID)) {
                    foreach($galleryFiles['images'] as $image) {
                        if ($image->isValid() && !$image->hasMoved()) {
                            $fileName = $slug . '_' . $image->getRandomName();
                            // Intentar subir archivo al servidor FTP
                            if (!ftp_put($connectionFTP, '/httpdocs/assets/images/stores/' . $storeID . '/' . $fileName, $image, FTP_BINARY)) {
                                ftp_close($connectionFTP);
                                throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Altavista 147, favor de intentar más tarde.", 3);
                            }
                            
                        } else {
                            ftp_close($connectionFTP);
                            $this->db->transRollback();
                            return false;
                        }
                    }
                } else {
                    ftp_close($connectionFTP);
                    $this->db->transRollback();
                    throw new \Exception("Error al tratar de crear el directorio de galería de imagenes en el servidor FTP del proyecto Altavista 147, favor de intentar más tarde.", 3);
                }
                
                // 7. Confirmar registro de información en base de datos
                if ($this->db->transStatus() !== FALSE) {
                    ftp_close($connectionFTP);
                    $this->db->transCommit();
                    return true;
                } else {
                    ftp_close($connectionFTP);
                    $this->db->transRollback();
                    return false;
                }
                
            } else {
                ftp_close($connectionFTP);
                $this->db->transRollback();
                throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Altavista 147, favor de intentar más tarde.", 3);
            }
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Altavista 147, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            // Excepciones personalizadas en el código: code = 3
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function removeStore($id)
    {
        try {
            $data = ['deleted_at' => 1];

            $this->db = db_connect('altavistaDB');
            $builder  = $this->db->table('stores');
            $query    = $builder->where('id', $id)->update($data);

            // Update: remover registros pivote (categoría por tienda) y directorio FTP Cover y Logotipo
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Altavista 147, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

}
