<?php
namespace App\Models\Thorurbana\Altavista;

use CodeIgniter\Model;

class EventsModel extends Model
{
    public function __construct() { }

    public function getEvent($id)
    {
        try {
            $this->db = db_connect('altavistaDB');
            $builder = $this->db->table('events');
            $query = $builder->where('id', $id)->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Altavista 147, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function getAllEvents()
    {
        try {
            $this->db = db_connect('altavistaDB');
            $builder = $this->db->table('events');
            $query = $builder->where('deleted_at !=', 1)->orderBy('created_at', 'desc')->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Altavista 147, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
        
    }

    public function createEvent($title, $description, $galleryFiles)
    {
        try {
            if (count($galleryFiles['images'])) {
                $connectionFTP = ftp_connect('72.47.208.40');

                if (@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                    $this->db = db_connect('altavistaDB');
                    $this->db->transBegin();

                    setlocale(LC_TIME, 'es_ES.UTF-8');
                    $dataEvent = [
                        'name'              => $title,
                        'slug'              => mb_url_title($title, '-', TRUE),
                        'event_description' => $description,
                        'recent'            => 1,
                        'num_img'           => count($galleryFiles['images']),
                        'event_date'        => ucfirst(strftime('%B %Y')),
                        'created_at'        => date('Y-m-d H:i:s'),
                        'deleted_at'        => 0
                    ];

                    $this->db->table('events')->insert($dataEvent);
                    $eventID = $this->db->insertID();

                    if (ftp_mkdir($connectionFTP, '/httpdocs/assets/images/events/' . $eventID)) {
                        foreach($galleryFiles['images'] as $index => $image) {
                            if ($image->isValid() && !$image->hasMoved()) {
                                
                                $fileName = mb_url_title($title, '-', TRUE) . ' ' . ($index + 1) . '.jpg';
                                
                                if (!ftp_put($connectionFTP, '/httpdocs/assets/images/events/' . $eventID . '/' . $fileName, $image, FTP_BINARY)) {
                                    // Error al subir el archivo actual al servidor FTP
                                    $this->db->transRollback();
                                    ftp_close($connectionFTP);
                                    throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Altavista 147, favor de intentar más tarde.", 3);
                                }
                            } else {
                                // Error en el archivo actual (tipo mime no permitido, tamaño excedido, corrupto)
                                $this->db->transRollback();
                                ftp_close($connectionFTP);
                                return false;
                            }
                        }
                        // 5. Confirmar operaciones en la base de datos
                        if ($this->db->transStatus() !== FALSE) {
                            $this->db->transCommit();
                            ftp_close($connectionFTP);
                            return true;
                        } else {
                            ftp_close($connectionFTP);
                            $this->db->transRollback();
                            return false;
                        }
                    } else {
                        $this->db->transRollback();
                        ftp_close($connectionFTP);
                        throw new \Exception("Error al tratar de crear el directorio en el servidor FTP del proyecto Altavista 147, favor de intentar más tarde.", 3);
                    }
                } else {
                    ftp_close($connectionFTP);
                    throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Altavista 147, favor de intentar más tarde.", 3);
                }
            }
            return false;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Altavista 147, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            // Excepciones personalizadas en el código: code = 3
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            //throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function editEvent($id, $title, $description)
    {
        try {
            $data = [
                'name'              => $title,
                'event_description' => $description,
                'updated_at'        => date('Y-m-d H:i:s'),
            ];

            $this->db   = db_connect('altavistaDB');
            $builder    = $this->db->table('events');
            
            $query      = $builder->where('id', $id)->update($data);
            return $query;
            
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Altavista 147, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            // Excepciones personalizadas
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function getGalleryEvent($id)
    {
        try {
            $connectionFTP = ftp_connect('72.47.208.40');
            if(@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                if(@ftp_chdir($connectionFTP, '/httpdocs/assets/images/events/' . $id . '/')) {
                    $content = ftp_nlist($connectionFTP, '.');
                    if ($content) {
                        $images = [];
                        foreach ($content as $file) {
                            $info = new \SplFileInfo($file);
                            // Devolver todos los archivos jpg que encuentre.
                            if (strtolower($info->getExtension()) == 'jpg') {
                                array_push($images, $file);
                            }
                        }
                        ftp_close($connectionFTP);
                        return $images;
                    }
                    ftp_close($connectionFTP);
                    // Retornar un arreglo vacío
                    return [];
                } else {
                    ftp_close($connectionFTP);
                    throw new \Exception("Error al tratar de recuperar las imagenes del servidor FTP del proyecto Altavista 147, favor de intentar más tarde.", 3);
                }
            } else {
                ftp_close($connectionFTP);
                throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Altavista 147, favor de intentar más tarde.", 3);
            }
        } catch (\Exception $e) {
            // Excepciones personalizadas en el código: code = 3
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function updateGalleryEvent($id, $slug, $images)
    {
        try {
            $connectionFTP = ftp_connect('72.47.208.40');
            if(@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                if(@ftp_chdir($connectionFTP, '/httpdocs/assets/images/events/' . $id . '/')) {
                   
                    $fileName = null;
                    $preview = [];
                    $config = [];

                    foreach($images['images'] as $image) {
                         // Leer el contenido actual del directorio FTP para proceder a numerar los nuevos archivos
                        $content = ftp_nlist($connectionFTP, '.');

                        if ($image->isValid() && !$image->hasMoved()) {
                            // Generar nombre para el archivo actual. Solo se permiten 5 imagenes por evento
                            for ($i = 1; $i <= 5; $i++) {
                                if (!in_array("$slug $i" . ".jpg", $content)) {
                                    $fileName = "$slug $i" . ".jpg";
                                    break;
                                }
                            }
                            
                            // Intentar subir archivo al servidor FTP
                            if (!ftp_put($connectionFTP, '/httpdocs/assets/images/events/' . $id . '/' . $fileName, $image, FTP_BINARY)) {
                                ftp_close($connectionFTP);
                                throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Altavista 147, favor de intentar más tarde.", 3);
                            }

                            // Generar descripción personalizada para imagen principal del evento (frontend)
                            $caption = explode(' ', $fileName)[1] == '1.jpg' ? '<b>Imagen Principal</b><br> (si elimina esta imagen, la siguiente que suba será considerada como imagen principal)' : $fileName;
                            
                            // Configurar las propiedades del nuevo archivo para enviarlas al frontend
                            $config[] = [
                                'key'       => $fileName,
                                'caption'   => $caption,
                                'width'     => '70px',
                                'url'       => base_url() . '/altavista/events/gallery/destroy/'.$id.'/'.$fileName,
                            ];
                            array_push($preview, 'https://www.altavista147.com/assets/images/events/'.$id.'/'.$fileName);
                        } else {
                            ftp_close($connectionFTP);
                            return false;
                        }
                    }

                    // Actualizar número de imagenes en la base de datos
                    $this->db = db_connect('altavistaDB');
                    $builder = $this->db->table('events');
                    $builder->set('num_img', 'num_img+' . count($images['images']), false)->where('id', $id)->update();

                    ftp_close($connectionFTP);
                    return ['initialPreview' => $preview, 'initialPreviewConfig' => $config, 'initialPreviewAsData' => true];
                
                } else {
                    ftp_close($connectionFTP);
                    throw new \Exception("Error al tratar de acceder al directorio de imagenes en el servidor FTP del proyecto Altavista 147, favor de intentar más tarde.", 3);
                }
            } else {
                ftp_close($connectionFTP);
                throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Altavista 147, favor de intentar más tarde.", 3);
            }
        } catch (\Exception $e) {
            // Excepciones personalizadas en el código: code = 3
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function removeEvent($id)
    {
        try {
            $data = ['deleted_at' => 1];

            $this->db = db_connect('altavistaDB');
            $builder = $this->db->table('events');
            $query = $builder->where('id', $id)->update($data);

            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Altavista 147, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function removeGalleryEvent($id, $image)
    {
        try {
            $connectionFTP = ftp_connect('72.47.208.40');
            if(@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                if(@ftp_chdir($connectionFTP, '/httpdocs/assets/images/events/' . $id . '/')) {
                    if (ftp_delete($connectionFTP, $image)) {
                        // Actualizar número de imagenes en la base de datos
                        $this->db = db_connect('altavistaDB');
                        $builder = $this->db->table('events');
                        $builder->set('num_img', 'num_img-1', false)->where('id', $id)->update();
                        // Cerrar conexión FTP
                        ftp_close($connectionFTP);
                        return true;
                    }
                } else {
                    ftp_close($connectionFTP);
                    throw new \Exception("Error al tratar de recuperar las imagenes del servidor FTP del proyecto Altavista 147, favor de intentar más tarde.", 3);
                }
            } else {
                ftp_close($connectionFTP);
                throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Altavista 147, favor de intentar más tarde.", 3);
            }
            ftp_close($connectionFTP);
            return false;
        } catch (\Exception $e) {
            // Excepciones personalizadas en el código: code = 3
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
        }
    }
}
