<?php
namespace App\Models\Thorurbana\Townsquare;

use CodeIgniter\Model;

class PopupsModel extends Model
{
    public function __construct()
    {
        $this->months = ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'];
    }

    public function getAllPopups()
    {
        try {
            $this->db = db_connect('townsquareDB');
            $builder = $this->db->table('popups');
            // $query = $builder->where('enabled', 1)->orderBy('created', 'desc')->get();
            $query = $builder->orderBy('created_at', 'desc')->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Town Square Metepec, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function getPopup($id)
    {
        try {
            $this->db = db_connect('townsquareDB');
            $builder = $this->db->table('popups');
            $query = $builder->where('id', $id)->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Town Square Metepec, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function createPopup($title, $start_date, $end_date, $link, $image_desktop, $image_mobile)
    {
        try {
            if (($image_desktop->isValid() && !$image_desktop->hasMoved()) && ($image_mobile->isValid() && !$image_mobile->hasMoved())) {
                $connectionFTP = ftp_connect('208.109.212.230');

                if (@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {
                    $popups     = [$image_desktop, $image_mobile];
                    $suffix     = ['desktop', 'mobile'];
                    $fileNames  = [];

                    $now = getdate();
                    $subdirectories = [$now['year'], $this->months[$now['mon'] - 1]];
                    $path = '/httpdocs/assets/popups/';
                    foreach ($subdirectories as $dir) {
                        if (!in_array($path . $dir, ftp_nlist($connectionFTP, $path))) {
                            if (!ftp_mkdir($connectionFTP, $path . $dir . '/')) {
                                ftp_close($connectionFTP);
                                throw new \Exception("Error al tratar de crear el directorio para el recurso de imagen, favor de intentar más tarde.", 3);
                            }
                        }
                        $path .= $dir .'/';
                    }

                    foreach ($popups as $index => $popup) {
                        $fileName = mb_url_title($title, '-', TRUE) . '_' . $suffix[$index] . '_' . time() . '.jpg';

                        if (ftp_put($connectionFTP, $path . $fileName, $popup, FTP_BINARY)) {
                            array_push($fileNames, 'assets/popups/' . $subdirectories[0] . '/' . $subdirectories[1] . '/' . $fileName);
                        } else {
                            ftp_close($connectionFTP);
                            throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Town Square Metepec, favor de intentar más tarde.", 3);
                        }
                    }

                    $data = [
                        'title'         => $title,
                        'image_desktop' => $fileNames[0],
                        'image_mobile'  => $fileNames[1],
                        'start_date'    => $start_date,
                        'end_date'      => $end_date,
                        'link'          => $link,
                        'target'        => $link ? '_blank' : null,
                        'enabled'       => 1,
                        'created_at'    => date('Y-m-d H:i:s'),
                    ];

                    $this->db = db_connect('townsquareDB');
                    $builder  = $this->db->table('popups');

                    // El popup actualmente registrado es el que queda activado por defecto
                    $this->removeAllPopups($builder);

                    $query = $builder->insert($data);

                    ftp_close($connectionFTP);
                    return $query;
                } else {
                    ftp_close($connectionFTP);
                    throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Town Square Metepec, favor de intentar más tarde.", 3);
                }
            }

            return false;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Town Square Metepec, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            // Excepciones personalizadas en el código: code = 3
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function editPopup($id, $title, $start_date, $end_date, $link, $enabled, $image_desktop, $image_mobile)
    {
        try {
            $connectionFTP = ftp_connect('208.109.212.230');
            if (@ftp_login($connectionFTP, 'developer', '!Qc51s_yp_S')) {

                $data = [
                    'title'         => $title,
                    'start_date'    => $start_date,
                    'end_date'      => $end_date,
                    'link'          => $link,
                    'target'        => $link ? '_blank' : null,
                    'enabled'       => $enabled ? 1 : 0,
                    'updated_at'    => date('Y-m-d H:i:s'),
                ];

                $this->db = db_connect('townsquareDB');
                $builder  = $this->db->table('popups');

                $popups     = [$image_desktop, $image_mobile];
                $suffix     = ['desktop', 'mobile'];
                $fileNames  = [];

                foreach ($popups as $index => $popup) {
                    if ($popup->getError() === 4) {
                        // No se subio el tipo de popup actual
                        array_push($fileNames, null);
                    } else {
                        if ($popup->isValid() && !$popup->hasMoved()) {
                            $fileName = mb_url_title($title, '-', TRUE) . '_' . $suffix[$index] . '_update_' . time() . '.jpg';
                            $now = getdate();
                            $subdirectories = [$now['year'], $this->months[$now['mon'] - 1]];
                            $path = '/httpdocs/assets/popups/';
                            foreach ($subdirectories as $dir) {
                                if (!in_array($path . $dir, ftp_nlist($connectionFTP, $path))) {
                                    if (!ftp_mkdir($connectionFTP, $path . $dir . '/')) {
                                        ftp_close($connectionFTP);
                                        throw new \Exception("Error al tratar de crear el directorio para el recurso de imagen, favor de intentar más tarde.", 3);
                                    }
                                }
                                $path .= $dir .'/';
                            }
                            if (ftp_put($connectionFTP, $path . $fileName, $popup, FTP_BINARY)) {
                                array_push($fileNames, 'assets/popups/' . $subdirectories[0] . '/' . $subdirectories[1] . '/'  . $fileName);
                            } else {
                                ftp_close($connectionFTP);
                                throw new \Exception("Error al tratar de subir el recurso de imagen al servidor FTP del proyecto Town Square Metepec, favor de intentar más tarde.", 3);
                            }
                        } else {
                            ftp_close($connectionFTP);
                            return false;
                        }
                    }
                }

                if ($fileNames[0])
                    $data['image_desktop'] = $fileNames[0];
                if ($fileNames[1])
                    $data['image_mobile'] = $fileNames[1];

                // Si el registro de popup actual se vuelve a activar, es necesario desactivar cualquier otro popup
                if ($enabled)
                    $this->removeAllPopups($builder);

                $query = $builder->where('id', $id)->update($data);

                ftp_close($connectionFTP);
                return $query;
            } else {
                ftp_close($connectionFTP);
                throw new \Exception("Error de conexión temporal al servidor FTP del proyecto Town Square Metepec, favor de intentar más tarde.", 3);
            }
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto Town Square Metepec, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            // Excepciones personalizadas en el código: code = 3
            if ($e->getCode() == 3)
                throw new \Exception($e->getMessage(), $e->getCode(), $e);
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    // public function removePopup($id)
    // {
    //     $data = ['enabled' => 0];

    //     $this->db = db_connect('andamarDB');
    //     $builder  = $this->db->table('popups');
    //     $query    = $builder->where('id', $id)->update($data);

    //     return $query;
    // }

    public function removeAllPopups($builder)
    {
        $data = [
            'enabled' => 0,
            'updated_at' => date('Y-m-d H:i:s'),
        ];
        $builder->where('enabled', 1)->update($data);
    }

}
