<?php
namespace App\Models\Thorurbana\Harbor;

use CodeIgniter\Model;

class NewsletterModel extends Model
{
    public function __construct()
    {
        //helper('cms');
    }

    public function getNewsletter($id)
    {
        try {
            $this->db = db_connect('harborDB');
            $builder = $this->db->table('monthly_newsletters');
            $query = $builder->where('id', $id)->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto The Harbor Mérida, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function getAllNewsletters()
    {
        try {
            $this->db = db_connect('harborDB');
            $builder = $this->db->table('monthly_newsletters');
            $query = $builder->orderBy('id', 'DESC')->get();
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto The Harbor Mérida, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function createNewsletter($title, $link)
    {
        try {
            setlocale(LC_TIME, 'es_ES.UTF-8');
            $data = [
                'title'       => $title,
                'url'        => $link,
                'date'        => ucfirst(strftime('%B %Y')),
                'enabled'     => 1,
                'created_at'  => date('Y-m-d H:i:s'),
            ];

            $this->db = db_connect('harborDB');
            $builder  = $this->db->table('monthly_newsletters');

            // Desactivar los newsletter anteriores
            $this->removeAllNewsletters($builder);

            $query = $builder->insert($data);
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto The Harbor Mérida, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function editNewsletter($id, $title, $link)
    {
        try {
            $data = [
                'title'         => $title,
                'url'           => $link,
                'updated_at'    => date('Y-m-d H:i:s'),
            ];

            $this->db   = db_connect('harborDB');
            $builder    = $this->db->table('monthly_newsletters');

            $query = $builder->where('id', $id)->update($data);
            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto The Harbor Mérida, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Se ha presentado un problema al tratar de procesar su solicitud, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function removeNewsletter($id)
    {
        try {
            $data = [
                'enabled' => 0,
                'updated_at' => date('Y-m-d H:i:s'),
                'deleted_at' => date('Y-m-d H:i:s'),
            ];

            $this->db = db_connect('harborDB');
            $builder = $this->db->table('monthly_newsletters');
            $query = $builder->where('id', $id)->update($data);

            return $query;
        } catch (\CodeIgniter\Database\Exceptions\DatabaseException $e) {
            throw new \Exception("Error de conexión temporal a la base de datos del proyecto The Harbor Mérida, favor de intentar más tarde.", 1, $e);
        } catch (\Exception $e) {
            throw new \Exception("Error interno en el sistema, favor de contactar al área de IT.", 2, $e);
        }
    }

    public function removeAllNewsletters($builder)
    {
        $data = [
            'enabled' => 0,
            'updated_at' => date('Y-m-d H:i:s'),
        ];
        $builder->where('enabled', 1)->update($data);
    }
}
