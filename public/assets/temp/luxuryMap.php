<?php

$scroll =	strpos($_SERVER['HTTP_USER_AGENT'],'iPhone') ||
			strpos($_SERVER['HTTP_USER_AGENT'],'iPad') ?
			'style="overflow:scroll;-webkit-overflow-scrolling:touch;"':'';

?>

<div id="main">
	<div id="map_container">
		<div id="mapHover"></div>
		<div id="map_list_container">
			<div class="planta_toggle planta_toggle_active" id="planta_baja_toggle" onclick="toggleMap(true);"><span>Planta </span>Baja</div>
			<div class="planta_toggle" id="planta_alta_toggle"  onclick="toggleMap(false);"><span>Planta </span> Alta</div>
			<div class="clear"></div>
			<?php
				foreach($establishments as $floor=>$establishments)
				{
					echo ul_from_array('id="'.$floor.'"',$establishments,true);
				}
			?>
		</div>
		<div id="map_img_container" <?= $scroll ?>>
			<div id="mapHover"></div>
			<div class="planta_img" id="planta_baja_img"><img src="assets/img/planta_baja.png" usemap="#planta_baja_map"></div>
			<div class="planta_img" id="planta_alta_img"><img src="assets/img/planta_alta.png" usemap="#planta_alta_map"></div>
		</div>
	</div>
	<div id="mobile_map">
		<div class="planta_toggle planta_toggle_active" id="planta_baja_toggle_mobile" onclick="toggleMap(true);">Planta Baja</div>
		<div class="planta_toggle" id="planta_alta_toggle_mobile"  onclick="toggleMap(false);">Planta Alta</div>
		<div id="pz_container_baja"><img src="assets/img/m1.png" style="width:100%;" /></div>
		<div id="pz_container_alta"><img src="assets/img/m2.png" style="width:100%;" /></div>
	</div>
</div>
<map name="planta_baja_map">
	<area class="lxmap bo" id="Sandro" href="#" coords="38,242,183,296,225,257,83,209" shape="poly">
    <area class="lxmap bo" id="AllSaints"  href="#" coords="229,256,246,238,159,208,176,190,125,173,84,207" shape="poly">
    <area class="lxmap bo" id="Kiehl´s"  href="#" coords="162,207,177,191,265,219,247,237" shape="poly">
	<area class="lxmap bo" id="Uterqüe" shape="poly" coords="123,170,265,219,266,241,302,205,301,183,161,139" href="#">
	<area class="lxmap bo" id="Guess" shape="poly" coords="162,139,300,183,303,205,325,182,325,160,269,142,269,137,263,130,198,110" href="#">
	<area class="lxmap re" id="Jo Malone" shape="poly" coords="71,123,67,102,99,79,143,93,113,117,114,137,109,135" href="#">
	<area class="lxmap re" id="Próximanente" shape="poly" coords="198,110,144,94,113,117,114,137,152,148" href="#">
	<area class="lxmap re" id="El Ganso" shape="poly" coords="132,74,231,103,230,89,221,77,212,72,212,65,223,56,250,62,252,65,256,61,158,33,129,53" href="#">
	<area class="lxmap bo" id="Rapsodia" shape="poly" coords="230,89,230,103,245,107,230,89,278,96,300,124,309,116,323,120,274,59,264,56,253,64,278,96,352,107,373,134,394,113,393,92,326,73,352,107" href="#">
	<area class="lxmap bo" id="Michael Kors" shape="poly" coords="393,92,394,113,422,86,421,65,293,31,292,32,327,73,274,58,269,51,263,55" href="#">
	<area class="lxmap bo" id="Scalpers" shape="poly" coords="308,34,421,65,422,86,445,62,445,43,417,35,419,34,335,11" href="#">
	<area class="lxmap bo" id="Brooks Brothers" shape="poly" coords="431,23,446,27,474,1,547,18,510,59,509,79,445,63,445,42,419,35" href="#">
	<area class="lxmap bo" id="Tommy Hilfiger" shape="poly" coords="510,80,557,91,585,59,585,54,592,46,593,30,548,18,511,59" href="#">
	<area class="lxmap" id="Sanitarios" shape="poly" coords="585,53,585,59,658,79,662,74,665,53,656,51,656,46,604,32,598,39,598,44,593,44" href="#">
	<area class="lxmap bo" id="Sephora" shape="poly" coords="642,223,712,116,715,95,584,60,552,101,636,123,593,188,644,201" href="#">
	<area class="lxmap bo" id="U Adolfo Dominguez" shape="poly" coords="552,101,528,129,615,154,636,123" href="#">
<!--	<area class="lxmap bo" id="Thomas Pink" shape="poly" coords="502,160,643,202,642,224,665,188,666,168,528,129" href="#">-->
	<area class="lxmap bo" id="Abercrombie & Fitch" shape="poly" coords="577,321,510,298,566,223,473,194,528,129,615,155,593,187,644,203,643,221" href="#">
	<area class="lxmap bo" id="Victoria´s Secret" shape="poly" coords="445,229,539,259,567,221,474,194" href="#">
	<area class="lxmap bo" id="Coach" shape="poly" coords="509,299,537,259,445,228,413,267" href="#">
	<area class="lxmap bo" id="Próximanente" shape="poly" coords="373,311,373,334,519,449,584,347,585,325,577,321,576,322,412,266" href="#">
	<area class="lxmap bo" id="Pandora" shape="poly" coords="247,313,298,259,350,277,351,280,300,338,246,317" href="#">
	<area class="lxmap bo" id="Concierge" coords="533,93,518,88,512,96,527,101" shape="poly" href="" >

<!--

	<area class="lxmap re" id="Starbucks" shape="poly" coords="342,205,390,239,435,189,374,171" href="#">
	<area class="lxmap re" id="50 friends" shape="poly" coords="453,87,513,103,437,190,374,171" href="#">
	-->




	<area  class="lxmap re" id="Próximamente" shape="poly" coords="455,87,513,103,454,171,391,154" style="outline:none;" target="_self"     />
	<area  class="lxmap re" id="Starbucks" shape="poly" coords="391.3999938964844,241,462.3999938964844,161,401.6000061035156,142.20000457763672,341.6000061035156,205.20000457763672" style="outline:none;" target="_self"     />
</map>
<map name="planta_alta_map">
	<area class="lxmap bo" id="Julio" shape="poly" coords="42,260,167,303,211,264,208,242,87,203,39,238" href="#">
	<area class="lxmap bo" id="Massmarcas" shape="poly" coords="87,202,208,242,211,263,250,229,249,207,128,171" href="#">
	<area class="lxmap bo" id="Aldo" shape="poly" coords="128,171,249,207,250,228,287,196,286,175,169,141" href="#">
	<area class="lxmap bo" id="Steve Madden" shape="poly" coords="170,140,286,174,287,195,321,165,321,145,315,143,306,152,232,131,231,120,205,113" href="#">
	<area class="lxmap bo" id="Cloe" shape="poly" coords="74,125,157,149,176,135,204,113,102,85,71,105" href="#">
	<area class="lxmap bo" id="Adidas Originals" shape="poly" coords="138,61,139,80,239,108,239,88,265,68,165,43" href="#">
	<area class="lxmap bo" id="M•A•C" shape="poly" coords="239,108,263,115,274,107,301,113,305,118,345,129,345,136,351,139,383,110,382,91,272,63,239,87" href="#">
	<area class="lxmap bo" id="Calvin Klein Jeans" shape="poly" coords="274,62,382,91,383,110,411,86,410,66,303,40" href="#">
	<area class="lxmap bo" id="Lacoste" shape="poly" coords="304,39,410,65,411,85,438,62,437,43,401,34,407,29,407,11,357,0" href="#">
	<area class="lxmap bo" id="Brantano" shape="poly" coords="445,48,444,66,488,76,490,58,527,23,485,13" href="#">
	<area class="lxmap bo" id="Studio F." shape="poly" coords="490,58,488,75,596,104,612,85,647,93,676,57,527,22" href="#">
	<area class="lxmap bo" id="Dreaming" shape="poly" coords="590,110,704,138,702,157,748,92,751,72,677,56,647,93,612,85" href="#">
	<area class="lxmap bo" id="Cuadra" shape="poly" coords="566,138,682,168,682,187,702,158,703,138,590,110" href="#">
	<area class="lxmap bo" id="Timberland" shape="poly" coords="554,152,672,183,670,205,682,187,681,168,566,138" href="#">
	<area class="lxmap bo" id="Próximanente" shape="poly" coords="540,167,660,199,659,219,670,204,671,183,553,153" href="#">
	<area class="lxmap bo" id="Converse" shape="poly" coords="527,182,648,216,647,236,659,218,660,199,541,166" href="#">
	<area class="lxmap bo" id="Didactic Store" shape="poly" coords="527,182,513,198,636,233,635,254,647,237,647,216" href="#">
	<area class="lxmap bo" id="Optikal Shop" shape="poly" coords="499,215,624,251,623,272,635,254,635,233,513,198" href="#">
	<area class="lxmap bo" id="Merrell" shape="poly" coords="484,232,610,269,609,291,623,272,623,251,498,215" href="#">
	<area class="lxmap bo" id="Próximanente" shape="poly" coords="415,311,414,335,538,394,609,291,609,269,483,232" href="#">
    <area class="lxmap bo" id="Próximanente"  coords="323,354,349,363,350,369,342,375,316,367,314,361" shape="poly"  href="#" >
</map>
<script>
	<?= isset($_POST['store']) ? 'setTimeout(function(){toggleMap('.$_POST['location'].');flashMapItem("#'.$_POST['store'].'");},1000);':''; ?>
</script>
