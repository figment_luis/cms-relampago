<!doctype html>
<html>
	<head>
		<?php $this->load->view('modules/head',['controller' => $controller, 'scripts' => $scripts]); ?>
	</head>
	<body>

	<?php $this->load->view('modules/newsletter'); ?>
		<div id="wrapper">
			<div id="top">
				<div id="top_wrapper">
					<?php $this->load->view('modules/top'); ?>
				</div>
			</div>
			<div id="content">
				<div id="content_wrapper">
					<?php $this->load->view('modules/directory',$directory_data); ?>
					<div id="map" class="content">
						<div id="map_hover"></div>
						<div id="map_wrapper" style="overflow:hidden;">
							<div id="floor_toggle">
								<span title="0" class="map_toggle_active">Planta baja</span><br><span title="1">Primer piso</span><br><span title="2">Segundo piso</span>
							</div>
							<div id="map_container0" class="floor_item">
								<div id="floor0">
								  <img class="map_img" id="img0" src="<?= base_url('assets/img/m0.png'); ?>" width="2206" height="1105" usemap="#Map0" border="0" />
								</div>
							</div>
							<div id="map_container1" class="floor_item">
								<div id="floor1">
									<img class="map_img" id="img1" src="../../assets/img/m1.png" width="2206" height="1105" usemap="#Map1" border="0" />
								</div>
							</div>
							<div id="map_container2" class="floor_item">
								<div id="floor2">
									<img class="map_img" id="img2" src="<?= base_url('assets/img/m2.png'); ?>" width="2206" height="1105" usemap="#Map2" border="0" />
								</div>
							</div>
							<div id="map_establishments">
								<div id="map_establishments_title">
									<div id="map_establishments_toggle">Mostrar establecimientos</div>
									<!-- <div id="floor_toggle">
										<span title="0" class="map_toggle_active">PLANTA BAJA</span>&nbsp;/&nbsp;<span title="1">PRIMER PISO</span>&nbsp;/&nbsp;<span title="2">SEGUNDO PISO</span>
									</div> -->
									<div id="map_establishments_filter">
										<input type="text" name="establishments_filter_input" placeholder="Filtrar" />
									</div>
								</div>
								<div id="map_establishments_data">
									<ul id="list0" class="map_establishments_list" style="display:none;">
										<?php
											$this->db->where(['location'=>'0']);
                      $this->db->where(['available'=>'1']);
											$query = $this->db->get('stores');

											foreach($query->result() as $row)
											{
												echo '<li>'.$row->name.'</li>';
											}
										?>
									</ul>
									<ul id="list1" class="map_establishments_list">
										<?php
											$this->db->where(['location'=>'1']);
                      $this->db->where(['available'=>'1']);
											$query = $this->db->get('stores');

											foreach($query->result() as $row)
											{
												echo '<li>'.$row->name.'</li>';
											}
										?>
									</ul>
									<ul id="list2" class="map_establishments_list">
										<?php
											$this->db->where(['location'=>'2']);
                      $this->db->where(['available'=>'1']);
											$query = $this->db->get('stores');

											foreach($query->result() as $row)
											{
												echo '<li>'.$row->name.'</li>';
											}
										?>
									</ul>
									<div class="clear"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="bottom">
        <?php  $this->load->view('modules/news_tab');  ?>
				<div id="bottom_wrapper">
					<?php $this->load->view('modules/bottom'); ?>
				</div>
			</div>
		</div>

<map class="highlightMapData" name="Map0" id="Map0"><area shape="poly" coords="1420,630,1438,623,1443,635,1424,641" href="#" id="Sanitarios Familiares"><area shape="poly" coords="1365,557,1366,553,1371,547,1383,551,1377,561" href="#" id="Daniel Wellington" />
  <area shape="poly" coords="311,523,592,525,592,529,539,771,230,769,230,763" href="#" id="Liverpool" />
  <area shape="poly" coords="590,549,624,549" href="#" id="Próximamente" />
  <area shape="poly" coords="621,667,611,727,611,731,611,737,588,739,588,732,599,666" href="#" id="MAC" />
  <area shape="poly" coords="659,667,649,727,650,733,611,733,612,727,620,667" href="#" id="Ivonne" />
  <area shape="poly" coords="698,550,695,570,641,571,641,568,621,568,625,549" href="#" id="Sanitarios Familiares" />
  <area shape="poly" coords="704,667,695,727,693,731,676,730,674,733,651,732,649,726,660,667" href="#" id="Stradivarius" />
  <area shape="poly" coords="648,628,671,629,670,639,669,647,648,645" href="#" id="La Crépe Parisienne" />
  <area shape="poly" coords="706,632,718,632,717,641,717,647,705,647,705,641" href="#" id="Mabe" />
  <area shape="poly" coords="746,622,761,623,759,644,745,644" href="#" id="Krispy Kreme" />
  <area shape="rect" coords="767,657,785,638" href="#" id="Elán">
  <area shape="poly" coords="870,629,882,629,884,635,881,640,879,645,868,641" href="#" id="Moyo Frozen Yogurt" />
  <area shape="poly" coords="891,641,913,640,912,658,889,660" href="#" id="Woow-Guau" />
  <area shape="poly" coords="946,635,968,635,968,657,943,654" href="#" id="Cassava Roots">
  <area shape="poly" coords="1544,640,1543,648,1895,760,1898,755,1973,580,1973,572,1641,473" href="#" id="Palacio de Hierro" />
  <area shape="poly" coords="1363,779,1389,773,1389,767,1356,704,1326,711" href="#" id="Telcel" />
  <area shape="poly" coords="1268,725,1298,719,1330,779,1331,785,1303,792,1300,785" href="#" id="Intercam" />
  <area shape="poly" coords="1192,767,1223,762,1243,799,1244,806,1213,813,1212,806" href="#" id="Multiva" />
  <area shape="poly" coords="1018,666,1017,672,1071,791,1135,776,1137,780,1245,755,1246,750,1230,718,1172,732,1132,641" href="#" id="Zara" />
  <area shape="poly" coords="1035,716,1024,718,1042,759,1053,757" href="#" id="Módulo de Concierge"/>
  <area shape="poly" coords="1186,630,1232,718,1172,732,1132,642" href="#" id="Bershka" />
  <area shape="poly" coords="1223,622,1276,722,1235,731,1185,630" href="#" id="Nike" />
  <area shape="poly" coords="1263,613,1318,714,1274,722,1223,622" href="#" id="Pull &amp; Bear" />
  <area shape="poly" coords="1296,606,1333,674,1306,681,1306,684,1302,685,1263,613" href="#" id="Zara Home" />
  <area shape="poly" coords="1332,674,1294,607,1364,590,1390,635,1390,642,1382,643,1391,661" href="#" id="Massimo Dutti" />
  <area shape="poly" coords="1367,591,1393,634,1394,640,1384,641,1383,637,1364,641,1337,597" href="#" id="Próximamente" />
  <area shape="poly" coords="1380,589,1400,625,1400,631,1391,633,1365,592" href="#" id="Camper" />
  <area shape="poly" coords="1410,582,1428,611,1428,617,1400,623,1396,618,1381,588" href="#" id="Womensecret" />
  <area shape="poly" coords="1449,645,1474,685,1474,692,1441,698,1445,707,1370,724,1356,704,1319,713,1305,685,1306,680,1396,661,1394,658" href="#" id="La Europea" />
  <area shape="poly" coords="1444,711,1443,705,1436,695,1472,687,1506,740,1505,746,1474,754" href="#" id="Movistar"/>
  <area shape="poly" coords="990,448,990,516,708,512,718,447" href="#" id="City Market" />
  <area shape="poly" coords="991,517,986,620,869,620,869,615,874,568,841,568,848,516" href="#" id="H&amp;M" />
  <area shape="poly" coords="840,620,844,568,876,568,870,620" href="#" id="Julio" />
  <area shape="poly" coords="760,534,755,572,785,571,779,615,780,620,839,620,846,535" href="#" id="American Eagle Outfitters" />
  <area shape="poly" coords="1436,575,1435,583,1474,643,1509,634,1509,624,1471,568" href="#" id="Oysho" />
  <area shape="poly" coords="1505,561,1522,567,1509,588,1520,604,1510,622,1473,568" href="#" id="Robert´s" />
  <area shape="poly" coords="1558,578,1577,584,1553,625,1551,631,1534,624,1535,618" href="#" id="Sebastian" />
  <area shape="poly" coords="1539,572,1558,578,1535,619,1535,625,1514,619,1516,613" href="#" id="Cuadra" />
  <area shape="poly" coords="1522,566,1540,572,1521,605,1510,589" href="#" id="Swarovski" />
  <area shape="poly" coords="1478,520,1507,470,1480,466,1443,526" href="#" id="The North Face" />
  <area shape="poly" coords="1507,472,1531,479,1511,513,1480,516" href="#" id="Samsung Store" />
  <area shape="poly" coords="1552,483,1534,478,1513,514,1515,520,1529,524,1531,517" href="#" id="Adidas Originals" />
  <area shape="poly" coords="1569,488,1552,483,1530,518,1530,525,1549,529,1547,523" href="#" id="True Religion" />
  <area shape="poly" coords="1585,493,1569,488,1548,523,1549,530,1564,535,1564,527" href="#" id="Hills Collection" />
  <area shape="poly" coords="1604,499,1584,493,1565,527,1564,535,1583,540,1584,533" href="#" id="Brantano" />
  <area shape="poly" coords="1623,505,1604,499,1583,534,1585,541,1600,545" href="#" id="Salomon" />
  <area shape="poly" coords="994,512,1054,532,1054,537,989,620" href="#" id="Sephora" />
  <area shape="poly" coords="1003,502,1061,521,1062,528,1055,537,1054,532,995,512,995,510" href="#" id="Luuna" />
  <area shape="poly" coords="1014,489,1072,508,1073,513,1063,526,1062,521,1003,502" href="#" id="Smart Bamboo" />
  <area shape="poly" coords="1029,472,1088,490,1088,495,1074,513,1074,508,1014,490" href="#" id="BCBGMAXAZRIA" />
  <area shape="poly" coords="1036,461,1095,480,1096,484,1088,494,1087,489,1027,472" href="#" id="Be Watch" />
  <area shape="poly" coords="1104,469,1105,475,1097,486,1096,481,1038,462,1046,451" href="#" id="Adrianna Papell" />
  <area shape="poly" coords="1055,442,1111,460,1113,465,1106,476,1103,469,1047,452" href="#" id="La Martina" />
  <area shape="poly" coords="1062,433,1118,450,1120,455,1112,464,1110,460,1056,443" href="#" id="Próximamente" />
  <area shape="poly" coords="1137,428,1137,433,1126,448,1127,442,1071,423,1070,424,1066,422,1078,408" href="#" id="Cumpanio" />
  <area shape="poly" coords="1450,452,1450,458,1429,493,1394,481,1394,476,1415,442" href="#" id="Uterqüe" />
  <!-- <area shape="poly" coords="1122,353,1157,365,1148,379,1205,397,1192,413,1192,418,1097,390,1096,384" href="#" id="Burberry" /> -->
  <area shape="poly" coords="1122,353,1157,365,1148,379,1205,397,1192,413,1192,418,1097,390,1096,384" href="#" id="Hamleys" />
  <area shape="poly" coords="1158,364,1221,383,1197,414,1196,420,1192,419,1193,413,1204,398,1148,379" href="#" id="Próximamente" />
  <area shape="poly" coords="1237,388,1220,383,1197,415,1197,420,1212,424,1213,418" href="#" id="Emwa" />
  <area shape="poly" coords="1269,397,1245,429,1244,435,1230,432,1230,425,1254,394" href="#" id="Harmont &amp; Blaine" />
  <area shape="poly" coords="1293,405,1269,437,1269,443,1245,435,1245,429,1270,397" href="#" id="Hackett" />
  <area shape="poly" coords="1318,412,1295,444,1294,451,1269,442,1269,439,1293,404" href="#" id="Hugo Boss" />
  <area shape="poly" coords="1343,419,1319,453,1318,459,1293,450,1294,444,1318,412" href="#" id="Próximamente" />
  <area shape="poly" coords="1366,426,1343,460,1343,466,1319,458,1319,454,1343,420" href="#" id="Replay" />
  <area shape="poly" coords="1397,436,1373,474,1343,466,1344,461,1366,427" href="#" id="Michael Kors" />
  <area shape="poly" coords="1398,437,1414,441,1395,476,1394,482,1375,475,1378,469" href="#" id="Pandora" />
  <area shape="poly" coords="747,620,781,620,780,613,785,572,753,572,748,613" href="#" id="Studio F." />
  <area shape="poly" coords="735,572,754,572,748,614,749,620,729,619,729,613" href="#" id="Carlo Rossetti" />
  <area shape="poly" coords="660,570,698,571,691,614,693,619,656,619,655,612" href="#" id="United Colors of Benetton" />
  <area shape="poly" coords="640,571,631,613,633,618,655,619,654,612,660,571" href="#" id="Parfois" />
  <area shape="poly" coords="621,568,640,569,632,613,634,620,614,619,614,610" href="#" id="Pepe Jeans" />
  <area shape="poly" coords="739,667,724,774,725,781,671,766,671,760,676,731,695,731,704,667" href="#" id="GAP" />
  <area shape="poly" coords="740,668,760,668,754,708,735,708" href="#" id="Cloe" />
  <area shape="poly" coords="797,668,781,791,781,797,725,780,725,774,733,708,754,708,758,668" href="#" id="Sfera" />
  <area shape="poly" coords="797,667,828,667,827,689,858,688,856,710,793,710" href="#" id="Mango" />
  <area shape="poly" coords="859,668,828,668,827,689,858,689" href="#" id="Zingara" />
  <area shape="poly" coords="913,669,913,705,929,705,928,724,981,726,976,797,950,797,952,784,947,786,947,782,826,782,825,787,821,786,819,808,781,798,792,709,857,710,858,668" href="#" id="Forever 21" />
  <area shape="poly" coords="938,668,936,706,981,707,982,669" href="#" id="High Life" />
  <area shape="poly" coords="915,668,938,668,937,707,931,707,930,699,913,699" href="#" id="Samsonite" />
  <area shape="poly" coords="1109,522,1125,502,1143,542,1126,545,1125,549,1120,550,1120,545" href="#" id="Brooks Brothers" />
  <area shape="poly" coords="1085,552,1108,522,1119,545,1119,551,1086,558" href="#" id="Sunglass Hut" />
  <area shape="poly" coords="1144,498,1164,536,1145,540,1125,502" href="#" id="Mayoral" />
  <area shape="poly" coords="1162,494,1182,531,1164,536,1145,498" href="#" id="Próximamente" />
  <area shape="poly" coords="1182,490,1201,527,1202,533,1188,536,1187,532,1183,532,1162,494" href="#" id="Tous" />
  <area shape="poly" coords="1201,486,1222,524,1203,528,1184,490" href="#" id="Rapsodia" />
  <area shape="poly" coords="1217,482,1238,521,1221,524,1199,487" href="#" id="Próximamente" />
  <area shape="poly" coords="1235,479,1257,517,1238,521,1218,482" href="#" id="Lacoste" />
  <area shape="poly" coords="1253,475,1275,512,1257,516,1235,478" href="#" id="98 Coast Av." />
  <area shape="poly" coords="1272,471,1297,478,1305,493,1268,500,1253,475" href="#" id="Coach" />
  <area shape="poly" coords="1351,496,1298,479,1306,493,1289,498,1293,507,1294,510,1298,512,1326,506,1326,504,1350,498" href="#" id="Steve Madden" />
  <area shape="poly" coords="1074,565,1089,595,1090,602,1042,611,1043,605" href="#" id="Victoria´s Secret" />
  <area shape="poly" coords="1094,559,1110,591,1110,598,1091,602,1088,595,1075,566,1077,562" href="#" id="Scalpers" />
  <area shape="poly" coords="1136,562,1149,582,1148,588,1110,598,1110,592,1099,570" href="#" id="Scappino" />
  <area shape="poly" coords="1147,540,1167,580,1165,585,1149,589,1148,584,1136,562,1134,564,1126,548,1128,545" href="#" id="Bimba y Lola" />
  <area shape="poly" coords="1186,575,1187,581,1167,586,1166,580,1148,541,1164,537" href="#" id="Nine West" />
  <area shape="poly" coords="1186,531,1190,542,1199,540,1214,570,1215,575,1187,581,1186,576,1165,537" href="#" id="Desigual" />
  <area shape="poly" coords="1222,524,1243,564,1242,569,1215,575,1214,570,1200,540,1207,538,1200,535,1202,530" href="#" id="Aldo" />
  <area shape="poly" coords="1258,517,1280,555,1280,561,1244,569,1243,564,1223,525" href="#" id="Abercrombie & Fitch" />
  <area shape="poly" coords="1295,509,1318,547,1319,552,1281,561,1280,555,1257,515" href="#" id="Tommy Hilfiger" />
  <area shape="poly" coords="1317,512,1336,542,1338,548,1318,552,1317,548,1301,516" href="#" id="El Ganso" />
  <area shape="poly" coords="1345,498,1369,534,1370,541,1337,548,1337,541,1316,511,1326,509,1326,502" href="#" id="Bath & Body Works" />
  <area shape="poly" coords="1355,497,1398,509,1408,527,1408,532,1371,540,1368,535,1344,498" href="#" id="Guess" />
  <area shape="poly" coords="1227,576,1246,571,1251,582,1231,586" href="#" id="Mindora" />
  <area shape="poly" coords="1456,533,1474,529,1480,540,1461,544" href="#" id="Lionesse Beauty" />
  <area shape="poly" coords="1027,633,1037,631,1040,635,1045,634,1044,629,1071,614,1079,618,1083,627,1087,625,1089,628,1084,630,1087,636,1088,644,1050,645,1047,640,1043,640,1044,644,1034,646" href="#" id="Anfiteatro" />
  <area shape="poly" coords="1113,617,1116,627,1104,629,1101,620" href="#" id="La ciudad de Colima" />
  <area shape="poly" coords="1199,586,1224,580,1231,594,1206,600" href="#" id="Starbucks Coffee" />
  <area shape="poly" coords="1261,582,1281,579,1287,589,1267,594" href="#" id="Häagen-Dazs" />
  <area shape="poly" coords="1304,560,1323,556,1328,567,1309,572" href="#" id="Lush" />
  <area shape="poly" coords="1266,568,1269,576,1281,573,1276,566" href="#" id="Acapella" />
  <!-- <area shape="poly" coords="1111,602,1116,613,1137,607,1130,597,1120,599,1113,600" href="#" id="L´atelier du chocolat" /> -->
  <area shape="poly" coords="1111,602,1116,613,1137,607,1130,597,1120,599,1113,600" href="#" id="Joyería Cristal" />
  <area shape="poly" coords="1223,761,1250,754,1236,731,1268,725,1300,786,1300,792,1244,806,1243,798" href="#" id="Santander" />
  <area shape="poly" coords="1298,719,1326,713,1361,778,1329,784,1330,779" href="#" id="Centro de Atención a Clientes AT&T" />
  <area shape="poly" coords="1212,425,1230,431,1229,425,1255,392,1237,387,1212,419" href="#" id="Scotch &amp; Soda" />
  <area shape="poly" coords="1061,432,1070,423,1126,442,1126,448,1120,455,1119,450" href="#" id="Enrique Tomas" />
  <area shape="poly" coords="697,571,718,572,716,585,733,585,728,619,693,619,691,614" href="#" id="iShop" />
  <area shape="poly" coords="1152,602,1157,614,1177,610,1171,598" href="#" id="BEN & FRANK" />
  <area shape="poly" coords="1355,571,1356,567,1363,557,1376,561,1367,575" href="#" id="Daniel Wellington" />
  <area shape="poly" coords="1396,540,1406,537,1410,544,1410,552,1400,553,1395,547" href="#" id="Prédiré" />
  <area shape="poly" coords="929,701,929,724,981,727,981,700" href="#" id="Sanitarios">
  <area shape="poly" coords="1403,634,1420,630,1424,641,1408,646" href="#" id="Elevador">
  <area shape="poly" coords="1029,590,1043,594,1038,606,1024,601" href="#" id="Elevador panorámico">
  <area shape="poly" coords="1178,444,1264,470,1131,497" href="#" id="Playground">
</map>

<map class="highlightMapData" name="Map1" id="Map1">
	<area shape="poly" coords="288,440,582,443,526,681,526,687,365,686,366,678,397,573,428,573,436,538,383,537,372,573,396,573,364,681,363,686,206,684,206,678" href="#" id="Liverpool" />
  <area shape="poly" coords="557,584,589,584,585,628,569,628,561,652,538,648" href="#" id="Aéropostale" />
  <area shape="poly" coords="581,625,604,625,604,639,635,639,639,585,592,582" href="#" id="Aerie" />
  <area shape="poly" coords="616,467,692,467,682,529,604,529" href="#" id="Martí" />
  <area shape="poly" coords="671,582,698,583,692,624,690,628,664,629,663,623" href="#" id="Flexi" />
  <area shape="poly" coords="690,465,679,529,680,533,652,533,651,529,663,465" href="#" id="Próximamente" />
  <area shape="poly" coords="710,496,703,529,704,534,682,535,681,529,687,497" href="#" id="Carolina Lemke" />
  <area shape="poly" coords="735,583,730,626,692,627,698,583" href="#" id="Juguetron" />
  <area shape="poly" coords="670,646,687,646,690,627,730,626,736,583,796,583,785,667,840,668,836,727,836,732,784,718,782,714,664,682,664,674" href="#" id="C&amp;A" />
  <area shape="poly" coords="796,584,827,584,817,667,786,668" href="#" id="Quarry" />
  <area shape="poly" coords="855,583,826,583,817,667,839,668,839,674,846,673" href="#" id="Cuidado con el Perro" />
  <area shape="poly" coords="918,583,915,625,932,627,931,643,976,644,974,697,975,703,842,701,845,673,849,668,856,583" href="#" id="Old Navy" />
  <area shape="poly" coords="990,535,990,530,995,435,846,433,841,466,842,470,855,470,855,484,891,484,888,529,888,535" href="#" id="H&amp;M" />
  <area shape="poly" coords="710,501,730,501,726,528,725,534,705,534" href="#" id="Bizzarro" />
  <area shape="poly" coords="730,488,770,486,764,529,763,534,725,534,726,527" href="#" id="Lob  " />
  <area shape="poly" coords="790,485,784,531,786,535,764,534,765,529,770,485" href="#" id="Pottery Barn Kids" />
  <area shape="poly" coords="766,535,765,530,771,484,837,485,839,491,833,535" href="#" id="Pottery Barn" />
  <area shape="poly" coords="838,484,870,485,866,529,866,534,833,534,833,528" href="#" id="Men´s Fashion" />
  <area shape="poly" coords="2008,488,2008,493,1931,677,1564,561,1565,557,1611,479,1773,519,1775,521,1776,521,1775,518,1612,478,1664,390" href="#" id="Palacio de Hierro" />
  <area shape="poly" coords="1550,433,1549,440,1588,451,1588,445,1628,380,1591,369" href="#" id="BBVA Bancomer" />
  <area shape="poly" coords="1585,450,1622,461,1625,457,1664,391,1628,380" href="#" id="Williams-Sonoma" />
  <area shape="poly" coords="1561,488,1600,500,1575,541,1572,546,1536,535,1538,530" href="#" id="Puma" />
  <area shape="poly" coords="1542,482,1561,488,1543,521,1539,521,1540,517,1530,503" href="#" id="Ópticas Lux" />
  <area shape="poly" coords="1524,476,1493,483,1530,539,1540,522,1540,519,1538,515,1530,504,1543,482" href="#" id="Ferrioni" />
  <area shape="poly" coords="1455,492,1454,498,1495,558,1516,555,1516,550,1532,547,1532,542,1494,483" href="#" id="Adidas" />
  <area shape="poly" coords="1446,526,1445,530,1417,536,1416,535,1417,529,1401,502,1427,496" href="#" id="Visage Óptica" />
  <area shape="poly" coords="1210,599,1226,595,1226,591,1200,545,1180,550,1196,577,1195,583,1196,585,1203,584" href="#" id="Vans" />
  <area shape="poly" coords="1308,523,1307,529,1334,573,1351,569,1351,565,1345,554,1349,555,1348,549,1330,518" href="#" id="Nutrisa" />
  <area shape="poly" coords="1363,541,1347,514,1328,518,1345,545,1347,550,1363,547" href="#" id="Coqueta y Audaz" />
  <area shape="poly" coords="1381,543,1380,538,1364,510,1347,514,1362,542,1363,547" href="#" id="GNC" />
  <area shape="poly" coords="1399,534,1400,539,1383,543,1381,538,1366,510,1384,506" href="#" id="TAF" />
  <!-- <area shape="poly" coords="1403,503,1385,507,1399,534,1401,539,1418,536,1418,529" href="#" id="Claire´s" /> -->
  <area shape="poly" coords="1403,503,1385,507,1399,534,1401,539,1418,536,1418,529" href="#" id="SALLY" />
  <area shape="poly" coords="1178,587,1196,583,1196,578,1181,549,1163,553,1176,581" href="#" id="Chilim Balam" />
  <!-- Nike - El local original se dividió en tres  -->
  <!--area shape="poly" coords="1056,449,1058,456,991,534,998,431" href="#" id="Próximamente" /-->
  <area href="#" coords="991,534,1020,503,995,497" shape="poly" id="New Era">
  <area href="#" coords="1020,505,1040,481,997,466,995,495" shape="poly" id="New Balance">
  <area href="#" coords="1038,479,1059,454,1001,433,997,464" shape="poly" id="Timberland">
  
  <area shape="poly" coords="1005,418,1037,428,1046,418,1075,428,1076,432,1058,454,1056,449,998,431,998,427" href="#" id="Próximamente" />
  <area shape="poly" coords="1145,481,1155,499,1156,504,1074,522,1074,516,1104,480,1106,490" href="#" id="Miniso" />
  <!-- <area shape="poly" coords="1156,457,1176,495,1176,501,1157,504,1155,500,1135,461" href="#" id="The Body Shop" /> -->
  <area shape="poly" coords="1156,457,1176,495,1176,501,1157,504,1155,500,1135,461" href="#" id="Levi´s" />
  <area shape="poly" coords="1193,449,1202,466,1184,469,1195,491,1195,496,1176,501,1176,495,1156,457" href="#" id="Starbucks Coffee" />
  <area shape="poly" coords="1196,496,1215,492,1214,487,1203,466,1184,470" href="#" id="Kipling" />
  <area shape="poly" coords="1141,434" href="#" id="Próximamente" />
  <area shape="poly" coords="1023,583,1023,589,1079,710,1082,706,1143,693,1143,690,1083,570" href="#" id="Innovasport" />
  <area shape="poly" coords="1122,608,1123,613,1106,617,1104,611,1083,571,1103,566" href="#" id="Devlyn" />
  <area shape="poly" coords="1123,562,1136,590,1137,595,1132,596,1136,605,1137,611,1123,613,1122,607,1102,567" href="#" id="Converse" />
  <area shape="poly" coords="1142,558,1156,585,1156,591,1137,595,1136,590,1122,562" href="#" id="Game Planet" />
  <area shape="poly" coords="1143,558,1163,553,1176,581,1178,587,1156,592,1157,585" href="#" id="Merrell" />
  <area shape="poly" coords="1219,455,1234,483,1234,489,1215,492,1215,487,1198,459" href="#" id="Georgie Boy" />
  <area shape="poly" coords="1212,445,1211,450,1215,455,1220,454,1233,482,1234,487,1255,483,1254,479,1232,441" href="#" id="Stax" />
  <area shape="poly" coords="1250,438,1272,476,1271,480,1254,484,1253,481,1232,442" href="#" id="Capa de Ozono" />
  <area shape="poly" coords="1270,434,1292,471,1293,476,1272,481,1271,476,1251,438" href="#" id="Aldo Conti" />
  <area shape="poly" coords="1289,429,1311,467,1311,473,1293,476,1292,471,1270,434" href="#" id="Skechers" />
  <area shape="poly" coords="1307,425,1330,463,1330,468,1312,472,1311,468,1289,430" href="#" id="Skechers" />
  <area shape="poly" coords="1345,425,1340,422,1342,417,1426,441,1426,447,1331,467,1330,460,1312,433" href="#" id="Sportico" />
  <area shape="poly" coords="1444,403,1517,424,1517,430,1450,443,1425,435,1425,430" href="#" id="Terraza Garabatos" />
  <area shape="poly" coords="1477,336,1559,360,1560,366,1523,419,1440,395,1440,389" href="#" id="Crate &amp; Barrel" />
  <area shape="poly" coords="1416,299,1466,313,1466,318,1458,330,1459,335,1442,361,1384,344,1386,342" href="#" id="Próximamente" />
  <area shape="poly" coords="1399,349,1382,372,1381,377,1439,395,1439,390,1456,366" href="#" id="Garabatos" />
  <area shape="poly" coords="1328,327,1399,349,1382,372,1381,378,1309,357,1309,351" href="#" id="PF Chang´s" />
  <area shape="poly" coords="1304,357,1303,363,1263,351,1263,345,1287,316,1325,328" href="#" data-alternative="Terraza PF Chang´s" id="Terraza PF Chang´s" />
  <area shape="poly" coords="1321,272,1418,298,1409,310,1402,322,1305,294" href="#" id="Casa Antonella" />
  <area shape="poly" coords="1304,294,1344,306,1327,327,1287,315" href="#" data-alternative="Terraza Ikebana" id="Terraza Ikebana" />
  <area shape="poly" coords="1396,321,1396,326,1383,345,1379,343,1328,326,1344,306" href="#" id="Ikebana" />
  <area shape="poly" coords="1141,434,1151,454,1151,458,1119,464,1119,460" href="#" id="XTI Store" />
  <area shape="poly" coords="1157,412,1172,410,1192,446,1191,450,1152,458,1152,455,1141,434" href="#" id="Próximamente" />
  <area shape="poly" coords="1210,403,1231,439,1231,443,1194,451,1192,447,1172,410" href="#" id="Próximamente" />
  <area shape="poly" coords="1228,398,1250,434,1250,439,1232,443,1232,439,1210,402" href="#" id="Próximamente" />
  <area shape="poly" coords="1246,395,1267,431,1268,434,1250,438,1249,434,1228,398" href="#" id="Yayas" />
  <area shape="poly" coords="1247,395,1260,392,1268,396,1287,427,1287,430,1270,434,1268,430" href="#" id="MOBO" />
  <area shape="poly" coords="1293,403,1307,423,1307,426,1287,429,1287,427,1265,394" href="#" id="¡Ay Gúey!" />
  <area shape="poly" coords="1339,416,1339,422,1310,427,1306,423,1293,403" href="#" id="Discovery The Store" />
  <area shape="poly" coords="1055,408,1084,417,1084,423,1077,432,1075,428,1046,419" href="#" id="Steren" />
  <area shape="poly" coords="1055,408,1064,397,1092,407,1094,412,1085,422,1084,416" href="#" id="Optikal" />
  <area shape="poly" coords="1072,388,1100,396,1101,401,1094,410,1092,407,1064,397" href="#" id="Krono Shop" />
  <area shape="poly" coords="1077,376,1110,386,1110,392,1102,401,1100,396,1072,388,1068,394,1068,386" href="#" id="L´Occitane" />
  <area shape="poly" coords="1008,417,1007,419,1037,428,1068,394,1067,387,1075,377,1049,368,1021,403,1014,403,1014,407" href="#" id="Próximamente" />
  <area shape="poly" coords="994,351,1049,367,1020,404,1014,404,994,403" href="#" id="Hot Wings" />
  <area shape="poly" coords="1000,259,1060,276,1060,279,1058,278,1026,314,1024,320,997,312" href="#" id="Italianni´s" />
  <area shape="poly" coords="1099,291,1068,327,1068,332,1025,320,1026,315,1058,280" href="#" id="Próximamente" />
  <area shape="poly" coords="1141,302,1119,328,1109,338,1108,343,1068,331,1068,326,1100,289" href="#" id="Chili´s" />
  <area shape="poly" coords="1110,338,1108,344,1141,355,1149,344,1149,339,1119,329" href="#" id="Casa del Lino" />
  <area shape="poly" coords="1169,316,1169,322,1150,344,1149,339,1119,330,1141,306" href="#" id="Próximamente" />
  <area shape="poly" coords="1205,334,1213,324,1231,301,1159,279,1151,287,1126,279,1114,293,1142,302,1140,307,1168,315,1170,322" href="#" id="Vicente Asador de Brasa" />
  <area shape="poly" coords="1249,277,1151,249,1132,274,1230,299,1231,303,1249,282" href="#" id="Steak Company" />
  <area shape="poly" coords="1167,233,1266,255,1267,260,1250,282,1248,277,1151,249" href="#" id="L´Ostería" />
  <area shape="poly" coords="1245,230,1279,240,1279,246,1267,260,1266,255,1164,231,1172,221,1247,221,1247,226" href="#" id="Santomar" />
  <area shape="poly" coords="1187,652,1187,659,1198,681,1229,674,1230,667,1219,646" href="#" id="Burger King" />
  <area shape="poly" coords="1234,643,1244,664,1244,670,1230,674,1229,667,1219,646" href="#" id="Liston de Maria" />
  <area shape="poly" coords="1250,639,1261,661,1261,667,1243,671,1244,665,1236,643" href="#" id="Sushi Roll" />
  <area shape="poly" coords="1264,636,1275,657,1275,663,1262,666,1261,662,1250,639" href="#" id="Green Grass" />
  <area shape="poly" coords="1279,633,1290,654,1290,659,1277,662,1276,657,1266,636" href="#" id="Próximamente" />
  <area shape="poly" coords="1293,630,1303,651,1304,657,1290,659,1290,655,1278,633" href="#" id="Mc Donalds" />
  <area shape="poly" coords="1308,626,1318,646,1319,653,1303,656,1304,651,1293,630" href="#" id="Argentina Express" />
  <area shape="poly" coords="1322,624,1333,644,1333,650,1320,652,1319,648,1308,627" href="#" id="Domino´s Pizza" />
  <area shape="poly" coords="1347,647,1347,640,1337,620,1322,624,1333,644,1333,649" href="#" id="Subway" />
  <area shape="poly" coords="1365,613,1377,633,1377,639,1362,642,1361,637,1351,617" href="#" id="Carl´s Jr" />
  <area shape="poly" coords="1379,611,1391,631,1392,637,1377,640,1377,634,1365,614" href="#" id="China Express" />
  <area shape="poly" coords="1395,607,1406,627,1406,634,1392,637,1391,632,1380,611" href="#" id="Sbarro" />
  <area shape="poly" coords="1422,601,1434,622,1435,628,1407,634,1406,628,1396,607" href="#" id="KFC" />
  <area shape="poly" coords="915,624,932,625,933,619,947,619,948,582,919,581" href="#" id="Liz Minelli" />
  <area shape="poly" coords="947,619,985,620,987,584,949,581" href="#" id="Calvin Klein" />
  <area shape="poly" coords="870,484,892,484,888,535,866,534" href="#" id="Huawei" />
  <area shape="poly" coords="1103,480,1116,464,1134,462,1144,481,1105,490" href="#" id="Birkenstock" />
  <area shape="poly" coords="1001,606,1002,621,1011,614,1019,607" href="#" id="Yogen Früz">
  <area shape="poly" coords="1002,622,1019,606,1019,623" href="#" id="Próximamente">
  <area shape="poly" coords="1337,620,1349,617,1361,636,1361,643,1347,647,1347,640" id="Próximamente" href="#">
</map>

<map class="highlightMapData" name="Map2" id="Map2">
  <area shape="poly" coords="1141,434" href="#" id="Próximamente" />
  <area shape="poly" coords="667,674,668,684,656,749,576,748,575,740,590,674" href="#" id="Dreaming" />
  <area shape="poly" coords="672,676,782,676,764,807,656,781,654,770" href="#" id="Games City" />
  <area shape="poly" coords="709,455,976,457,965,633,601,631,615,558,689,558" href="#" id="Próximamente" />
  <area shape="poly" coords="1038,532,1038,542,1081,625,1374,561,1374,552,1327,481,1310,476" href="#" id="Cinépolis" />
  <area shape="poly" coords="1343,382,1394,397,1394,406,1382,421,1425,435,1425,444,1392,486,1293,457,1293,449" href="#" id="Próximamente" />
  <area shape="poly" coords="1411,484,1458,418,1527,439,1526,444,1480,515,1410,493" href="#" id="Crate &amp; Barrel" />
  <area shape="poly" coords="1627,471,1978,576,1901,765,1529,641,1530,633" href="#" id="Palacio de Hierro" />
  <area shape="poly" coords="876,676,868,755,804,750,799,811,799,819,768,808,784,676" href="#" id="Próximamente" />
  <area shape="poly" coords="957,676,944,860,800,820,807,752,872,758,878,677" href="#" id="The Home Store" />
  <area shape="poly" coords="305,533,618,535,619,544,564,786,218,785,215,775" href="#" id="Restaurante Liverpool y Experiencia Gourmet" />
</map>


<script>
	<?= isset($_POST['location']) ? isset($_POST['store']) ? 'setTimeout(function(){toggleMap('.($_POST['location'] == 0 ? 0 : (($_POST['location']) == 1 ? 1 : 2)).');flashMapItem(undefined,\'[id="'.$_POST['store'].'"]\');},1000);':'setTimeout(function(){toggleMap('.$_POST['location'].');},1000);':'setTimeout(function(){toggleMap(0);},1000);'; ?>
</script>



	</body>
</html>
